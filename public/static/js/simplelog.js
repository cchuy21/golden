var SimpleLog = {
    'level': 15,
    'log': function(msg, label, type) {
        if((this.level & 8) === 8) {
            if( label !== undefined) {
                console.log(label);
            }
            if(type === "alert"){
                alert(msg);
                return;
            }
            console.log(msg);
        }
    },
    'debug': function(msg, label, type) {
        if((this.level & 4) === 4) {
            if( label !== undefined) {
                console.log(label);
            }
            if(type === "alert"){
                alert(msg);
                return;
            }
            console.debug(msg);
        }
    },
    'warn': function(msg, label, type) {
        if((this.level & 2) === 2) {
            if( label !== undefined) {
                console.log(label);
            }
            if(type === "alert"){
                alert(msg);
                return;
            }
            console.warn(msg);
        }
    },
    'error': function(msg, label, type) {
        if((this.level & 1) === 1) {
            if( label !== undefined) {
                console.log(label);
            }
            if(type === "alert"){
                alert(msg);
                return;
            }
            console.error(msg);
        }
    }
};