'(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)';

var fchaunica="";
var hraunica="";

function getParent(ob, cl) {
	do {
		ob = ob.parent();
	} while (!ob.hasClass(cl));
	return ob;
}
function padre(element, clase) {
	var tr = false;
	while (!tr) {
		if (element.parent().hasClass(clase)) {
			tr = true;
			return element.parent();
			break;
		} else {
			element = element.parent();
		}
	}
}
/*manda los mensajes cuando se realiza una acción en un botón especifico*/
/*--------------------Objects-----------------------*/
var app2 = {
	'load' : function(url, container2, options, callback) {
		var container = jQuery(container2);

		if (jQuery.isFunction(options)) {
			callback = options;
		} else {
			if (jQuery.isFunction(options.done)) {
				callback = options.done;
			}
			if (jQuery.isFunction(options.before)) {
				options.before();
			}
		}
		var sbt = url.substr(1, 8);
		if (sbt == "monitoreo")
			container.load("/user/gf");
		container.load(url, function() {
			callback();
		});
	},
};

var app = {
	'load' : function(url, options, callback) {
		var container = jQuery(jQuery("li.active a").attr("href"));

		if (jQuery.isFunction(options)) {
			callback = options;
		} else {
			if (jQuery.isFunction(options.done)) {
				callback = options.done;
			}
			if (jQuery.isFunction(options.before)) {
				options.before();
			}
		}

		container.load(url, function() {

			callback();

		});
	},
	'exec' : function(url, ops) {
		var options = {
			'type' : 'POST',
			'dataType' : 'json',
			'before' : function() {
				return true;
			},
			'success' : function() {
			},
			'data' : {}
		};
		for (o in ops) {
			if (options[o] !== undefined) {
				options[o] = ops[o];
			}
		}
		if (jQuery.isFunction(options.before)) {
			if (options.before() === true) {
				jQuery.ajax({
					type : options.type,
					dataType : options.dataType,
					url : url,
					data : options.data
				}).done(function(d) {
					if (jQuery.isFunction(options.success)) {
						options.success(d);
					}
				});
			}
		}
	},
	'currentUser' : -1,
	'currentServiceType' : '',
	'currentAddress' : -1,
	'currentSystemuser' : -1
};

//Funcion generica para autocomplete, todos deben tener el elemente enseguida
//Cambiar esto a un plugin para que se corrija
function autoCompleteF(element, url, callback) {
	if ( typeof (element) === 'string') {
		element = jQuery(element);
	}
	element.on("keyup", function() {
		var dt = jQuery(this).val().toUpperCase();
		element.autocomplete({
			source : url + "/" + dt,
			minLength : 0,
			type : "POST",
			data : {},
			"contentType" : 'application/x-www-form-urlencoded; charset=UTF-8',
			select : function(event, ui) {
				jQuery(element).next().attr("value", ui.item.id ? ui.item.id : null);
				jQuery(element).next().trigger('change');
				if (callback !== undefined && jQuery.isFunction(callback)) {
					callback(ui);
				}
			}
		});
	});

}

function autoCompleteA(element, url) {
	if ( typeof (element) === 'string') {
		element = jQuery(element);
	}
	element.on("keyup", function() {
		var dt = jQuery(this).val().toUpperCase();
		element.autocomplete({
			source : url + "/" + dt,
			minLength : 3,
			type : "POST",
			data : {},
			select : function(event, ui) {
				element.attr("value", ui.item.label ? ui.item.label : null);
				element.val("value", ui.item.label ? ui.item.label : null);
				element.trigger("change");
			}
		});
	});

}

function initAjustWindow(id, data) {
	app.currentUser = id;
	app.load("/user/index/" + id + "/" + data, {
		'done' : function() {
			initUserBlock(id);
		}
	});
}

function initUserWindow(id, data) {
	jQuery('#tabsUsr a').click(function(e) {
		e.preventDefault()
		jQuery(this).tab('show')
	});

	app.currentUser = id;
	
	app2.load("/user/index/" + id + "/" + data, "#reservations", {
		'done' : function() {

			if (data != "3" || data != "5" || data != "6" || data != "4" || data != "7" || data != "9") {
				if (data == "41" || data == "42") {
					initUserBlock(id, data);
				} else {
					initUserBlock(id);
				}
			} else {
				console.log(id);
				initUserBlock(id);
				//initServiceBlock('#editar');
				
			}

		}
	});
}

/*SearchBox Call*/
var sbCallBack = function(o) {
	initUserWindow(o.item.id, app.currentServiceType);

};
//datos que puede proporcionar el usuario para realizar una busqueda en los menus de editar, consultar reservaciones, etc.
var searchBox = {
	'element' : 0,
	'servicetype' : ['MTY-APTO', 'APTO-MTY'],
	'mode' : -1,
	'info' : [["Busqueda de Cliente", "Nombre Completo, Telefono o Correo", "cliente"], ["Busqueda de Reservación", "Folio, Usuario, Telefono o Correo", "reservacion"], ["Consulta de Reservación", "Folio, Usuario, Telefono o Correo", "reservacion"], ["Consulta de Reservación Cancelada", "Folio, Usuario, Telefono o Correo", "reservacion"], ["Consultar Costo", "Nombre de Colonia", "colonia"]],
	'set' : function(params, values) {
		this.element.attr(params, values);
	},
	'response' : [

	function() {
		autoCompleteF("#cliente", "/datos/datosCliente", sbCallBack);
	},
	function() {
		autoCompleteF("#reservacion", "/datos/reservasPendientes", sbCallBack);

	},
	function() {
		autoCompleteF("#reservacion", "/datos/datosReservacion", sbCallBack);
	},
	function() {
		autoCompleteF("#reservacion", "/datos/datosReservacionC", sbCallBack);
	},
	function() {
		autoCompleteA("#colonia", "/datos/costosCol");
	}],
	'setData' : function(i, st) {
		app.currentServiceType = st;

		i = i - 1;
		var heading = this.element.find(".panel-heading");
		var input = this.element.find(".panel-body .box-input");
		if (this.mode !== i) {
			input.val("");
		}
		this.mode = i;
		heading.html(searchBox.info[i][0] + ":");
		input.attr("placeholder", searchBox.info[i][1]);
		input.attr("id", searchBox.info[i][2]);
		searchBox.element.show();
		input.off("keypress");
		input.keypress(searchBox.response[i]);
		input.focus();
	}
};

/*-----------------Initializations---------------*/
jQuery(document).ready(function() {

	function ld(sr) {
		jQuery("." + sr).off("click");
		jQuery("." + sr).on("click", function() {
			if (!jQuery("#" + sr + " #content div").length)
				app2.load("/prueba/gettem/reports." + sr, "#" + sr + " #content", {
					'done' : function() {
						initreport();
						initOcupacion();
						if (sr == "monitoreo") {
							jQuery(".stic").off("click");
							jQuery(".stic").on("click", function() {
								var d = jQuery(this).data("reser");
								var ur = "/assignment/imp2/" + d;
								window.open(ur, 'Ticket', "width=350, height=400");
								//se le determina un tamaño al ticket que se va a imprimir
							});
						}
					}
				});
		});
	}
	jQuery('.tool').tooltip();
	initFormLogin();
	jQuery.ajax({
		url : "/systemuser/getype/", //la url que requiere para enviar los datos
		data : {},
		type : "POST", //metodo por el cual se van a enviar los datos
		dataType : "json",
		success : function(source) {
			data = source;

			if (data == "ADMINISTRADOR" || data == "OBSERVADOR") {
				initMenuBtns();
				initOcupacion();
				initreport();
				initconfiguration();
				//timeasig();
				//initassigments();
				jQuery("#mnasigna").on("click",function(){
					jQuery("#mnasignadn").trigger("click");
				});
				setInterval(function() {
					jQuery.ajax({
						type : "POST",
						url : "/assignment/ultimos",
						data : { },
						dataType : 'json',
						success : function(data) {

							var bol = data;

							if (bol) {
								//accion cuando sea success
							}
						}
					});
					jQuery.ajax({
						type : "POST",
						url : "/assignment/ul",
						data : { },
						dataType : 'json',
						success : function(data) {
							var bol = data;
							if ((bol && !jQuery(".btn-cob").length) || (bol && (jQuery(".btn-cob").length && !jQuery("#modal-assigments").is(":visible")))) {
								if (jQuery("li.active a[href='#assignments']").length) {
									alertamenu();
									loadasig(1, "llegadas",0);
									loadasig(1, "salidas",1);
								}
							}
						}
					});
				}, 120000);

				ld("ocupacion");
				ld("monitoreo");
				ld("reports");

			} else if (data == "ASIGNACIONES") {
				initreport();
				initMenuBtns();
				timeasig();
				initassigments();
				initOcupacion();
				setInterval(function() {
					jQuery.ajax({
						type : "POST",
						url : "/assignment/ultimos",
						data : { },
						dataType : 'json',
						success : function(data) {

							var bol = data;

							if (bol) {
								//accion para cuando sea aceptado el evento.
							}
						}
					});
					jQuery.ajax({
						type : "POST",
						url : "/assignment/ul",
						data : { },
						dataType : 'json',
						success : function(data) {
							var bol = data;
							if ((bol && !jQuery(".btn-cob").length) || (bol && (jQuery(".btn-cob").length && !jQuery("#modal-assigments").is(":visible")))) {
								if (jQuery("li.active a[href='#assignments']").length) {
									alertamenu();
									loadasig(1, "llegadas",0);
									loadasig(1, "salidas",1);
								}
							}
						}
					});
				}, 120000);
				ld("ocupacion");
				ld("monitoreo");
				ld("reports");
			} else if (data == "RESERVACIONES" || data == "SUPER RESERVACIONES") {
				ld("ocupacion");
				ld("monitoreo");
				initMenuBtns();
				initreport();
				initOcupacion();
			}
		}
	});
});
/*-------------------Code-----------------------*/
function initFormLogin() {
	//para iniciar sesión en el sistema
	if (jQuery('.form-signin').length > 0) {
		jQuery('.form-signin').alajax({
			type : 'json',
			beforeSend : function() {
				jQuery('.form-signin').fadeTo(1, 0.5);
			},
			success : function(obj) {
				//obtiene el valor del nombre de usuario que escribieron
				var nm = jQuery("input[name='user[username]']").val();
				//manda los datos por el metodo post(ocultos)
				jQuery.ajax({
					url : "/systemuser/iduser/",
					data : {},
					type : "POST",
					dataType : "json",
					success : function(source) {
						data = source;
						app.currentSystemuser = data;
						jQuery('.form-signin').fadeTo(1, 1);
						if (obj.error) {
							//marca error si el usuario o password es incorrecto
							jQuery('.alert').html(obj.error).fadeIn();
						} else {
							//te manda a la página principal del sitio
							top.location = "/index.php";
						}
					}
				});
			}
		});
		jQuery('.form-signin').fadeIn(function() {
			//manda el mensaje en consola del navegador
			console.log("Formulario de inicio de sesión cargado.");
			//location.reload();
			//alert("hola");
		});
	}
}
/*------------------------------------------------------------------------*/
function initMenuBtns() {
    jQuery(".neditar").on("click",function(){
		var dat = jQuery(this).data("reser");
		initUserWindow(dat, 9);

    });
	jQuery(".btn-menu").on("click", function() {

		if (jQuery(this).data("info") != "1") {
			jQuery(".btn-hid").css("display", "none");
		}
		var st = jQuery(this).data("st");
		app.currentServiceType = st;
		if (st == 5 || st == "5") {
			app2.load("/prueba/gett/co", ".con_div", {
				'done' : function() {
					jQuery(".co").on("click", function() {
						var dat = jQuery(this).data("reser");
						initUserWindow(dat, 5);
					});
				}
			});
			jQuery(".con_div").css("display", "block");

		} else if (st == 3 || st == "3") {
		    console.log(st);
			app2.load("/prueba/gett/cu", ".con_div", {
				'done' : function() {
					jQuery(".cu").on("click", function() {
						var dat = jQuery(this).data("reser");
						initUserWindow(dat, 3);
					});

				}
			});

			jQuery(".con_div").css("display", "block");
		} else if (st == 7 || st == "7") {
			app2.load("/prueba/gett/ca", ".con_div", {
				'done' : function() {
					jQuery(".ca").on("click", function() {
						var dat = jQuery(this).data("reser");
						initUserWindow(dat, 7);
					});
				}
			});
			jQuery(".con_div").css("display", "block");
		} else {
			jQuery(".con_div").css("display", "none");
		}
	});

	jQuery.ajax({
		url : "/systemuser/iduser/",
		data : {},
		type : "POST",
		dataType : "json",
		success : function(source) {
			data = source;
			app.currentSystemuser = data;
		}
	});

	if (jQuery(".btn-menu").length > 0) {
		jQuery(".btn-menu").each(function() {
			var btn = jQuery(this);
			btn.on("click", function() {

				var tipo = jQuery(this).data('st');
				jQuery('#tipo').val(tipo);
				jQuery('.btn-menu').removeClass("principal");
				jQuery(".btn-menu").addClass('opaco');
				jQuery(this).removeClass("opaco");
				jQuery(this).addClass("principal");
				app.currentServiceType = tipo;
				if (btn.attr("searchbox") !== "false") {
					searchBox.setData(btn.attr("data-info"), btn.attr("data-st"));
				} else {
					initUserWindow(-1, btn.attr("data-info"));
				}
			});
		});
	}
	initSearchBox();
	sysmovil();
	function sysmovil() {
		jQuery(".rapp-hab").off("click").on("click", function() {
			var id = jQuery(this).data("appid");
			app.exec("/movil/apphab", {
				'data' : {
					'id' : id
				},
				'before' : function() {

					return true;
				},
				'success' : function(d) {
					alert("las reservacion fue aceptada");
					app2.load("/assignment/crgtemp/systemuser.movil", "#cont_app", {
						'done' : function() {
							sysmovil();
						}
					});
				}
			});
		});
		jQuery(".rapp-del").off("click").on("click", function() {
			var id = jQuery(this).data("appid");
			app.exec("/movil/appdel", {
				'data' : {
					'id' : id
				},
				'before' : function() {

					return true;
				},
				'success' : function(d) {
					alert("las reservacion fue borrada");
					app2.load("/assignment/crgtemp/systemuser.movil", "#cont_app", {
						'done' : function() {
							sysmovil();
						}
					});
					/*    app.exec("/user/notify/", {

					 });
					 */
				}
			});
		});
		jQuery(".rapp-ver").off("click").on("click", function() {
			var id = jQuery(this).data("appid");
			app2.load("/assignment/crgtemp/systemuser.ver/" + id, "#bdy", {
				'done' : function() {
					jQuery("#modalmovil").modal("show");
				}
			});
		});
	}
	
	
}

function initSearchBox() {
	if (jQuery(".search-box").length > 0) {
		searchBox.element = jQuery(".search-box");
		searchBox.element.hide();
	}
}

//manda un mensaje cuando se ha enviado el correo electronico de los datos de la reservación a los usuarios
function initUserBlock(userid, da) {
	jQuery(".envcan").off("click").on("click",function(){
		var id = jQuery(".folio-res").html();
		var url = "/service/correocan/" + id + "/" + 2;
		jQuery.ajax({
			type : "POST",
			dataType : 'json',
			url : url,
			"contentType" : 'application/x-www-form-urlencoded; charset=UTF-8',
			data : {}
		});
	});
	jQuery('#rmail').off("click");
	jQuery('#rmail').on("click", function() {
		var id = jQuery(".folio-res").html();
		var url = "/service/correo/" + id + "/" + 0;
		jQuery.ajax({
			type : "POST",
			dataType : 'json',
			url : url,
			"contentType" : 'application/x-www-form-urlencoded; charset=UTF-8',
			data : {}
		});

		alert("El mensaje se envio satisfactoriamente");

	});
	var win = app.currentServiceType;
	jQuery("#user").transition({
		scale : 1
	}, 500);
	app.currentUser = jQuery('.user-data[name="id"]').val();

	jQuery("span.close-service").click(function() {
		location.reload();
	});
	function tablas(selector, input, funct) {
		var co = 0;
		jQuery(selector).click(function() {
			jQuery(this).hide();
			var tb = jQuery(this).parent().parent().parent().parent();
			if (jQuery(this).children("span.glyphicon-ok").length) {// click return
				if (input == "input" || input == "inpsel")
					tb.find("input.form-control").attr("type", "hidden");
				if (input == "select" || input == "inpsel") {
					tb.find("select.form-control").attr("type", "hidden");
					tb.find("select").hide();
					tb.find("td.tdselec").removeClass("tdwidth");
				}
				tb.find('.penb').css("display", "");
				jQuery("#user-annotations").css("display", "none");
				jQuery("#most-annotations").html(jQuery("#user-annotations").val()).css("display", "block");
				tb.find("span.spn").css("display", "");
				jQuery(".tables_c .pasajero").addClass("active");
				jQuery(".tables_c .pasajero").removeClass("alert-success");
				jQuery(".tables_c .pasajero").removeClass("alert");
				tb.find("td > span.glyphicon").css("margin-top", "0px");
				jQuery('.glyphicon-user').css("display", "block");
				jQuery('.glyphicon-star').css("display", "none");
				funct();
			} else {//click editar
				jQuery("#user-annotations").css("display", "block");
				jQuery("#most-annotations").css("display", "none");

				if (input == "input" || input == "inpsel")
					tb.find("input.form-control").attr("type", "text");
				if (input == "select" || input == "inpsel") {
					tb.find("select").show();
					tb.find("td.tdselec").addClass("tdwidth");
				}
				tb.find('.okb').css("display", "");
				tb.find("span.spn").css("display", "none");
				jQuery(".tables_c .pasajero").removeClass("active");
				jQuery(".tables_c .pasajero").addClass("alert-success");
				jQuery(".tables_c .pasajero").addClass("alert");
				tb.find("td > span.glyphicon").css("margin-top", "60%");
				jQuery('.glyphicon-user').css("display", "none");
				jQuery('.glyphicon-star').css("display", "block");
				jQuery('#ds-star').hide();
			}
		});
	}


	jQuery("#user-agreement_id").on("change", function() {
		//muestra si el usuario tiene algun beneficio o no por los servicios que ha solicitado
		if (jQuery(this).val() == "SC") {
			var ap = '<option value="3">11 gratis</option>';
		} else {
			var ap = '<option value="">Sin Beneficio</option>';
		}
		jQuery("#user-revenue_id").html(ap);
	});
	jQuery("#user-empresa").on("change", function() {
		var id = jQuery(this).val();

		jQuery.ajax({
			url : "/business/conv",
			data : {
				'id' : id
			},
			type : "POST",
			dataType : "json",
			"contentType" : 'application/x-www-form-urlencoded; charset=UTF-8',
			success : function(source) {
				data = source;
				var cnv = jQuery("#user-agreement_id").val();
				
				var opci = "<option data-convenio='Sin Convenio' value='SC'>Seleccionar Convenio</option>";
				for (var i = 0; i < data.length; i++) {
					if (cnv === data[i][0]) {
						console.log(cnv + " " + data[i][0]);
						opci = opci + "<option selected id='" + data[i][0] + "' data-convenio='" + data[i][1] + "' value='" + data[i][0] + "'>" + data[i][1] + "</option>";

					} else {
	    				console.log(opci);
						opci = opci + "<option id='" + data[i][0] + "' data-convenio='" + data[i][1] + "' value='" + data[i][0] + "'>" + data[i][1] + "</option>";
					}
				}
				console.log(opci);
				jQuery("#user-agreement_id").html(opci);
			},
			error : function(dato) {

			}
		});
	});
	//guardar user primera tabla
	jQuery("#ds-starh").on("click", function() {
		jQuery("#nueva").find(".add-id").trigger("change");
		jQuery("#nueva").find(".add-id").trigger("change");
		jQuery("#editar").find(".add-id").trigger("change");
		jQuery("#editar").find(".add-id").trigger("change");
		//if (jQuery(".user-button").children("span.glyphicon-ok").length) {
		if (jQuery("#user-distinguished").val() == 0) {
			jQuery("#user-distinguished").val(1);
			jQuery(this).addClass("dis");
			jQuery(this).removeClass("gray");
		} else {
			jQuery("#user-distinguished").val(0);
			jQuery(".glyphicon-star").removeClass("dis");
			jQuery(".glyphicon-star").addClass("gray");
		}
		jQuery("#user-distinguished").trigger("change");
	});

	function conv() {
		var agr = jQuery("#user-agreement_id option:selected").data("convenio");
		jQuery(".agreement_complete").html(agr);
		var em = jQuery("#user-empresa option:selected").html();
		jQuery(".business_complete").html(em);

		var nm = jQuery("#user-name").val().toUpperCase();

		var em = '<a href="mailto:' + jQuery("#user-email").val() + '">' + jQuery("#user-email").val() + '</a>';
		jQuery(".email_complete").html(em);
		var tf = jQuery("#user-telephones").val().toUpperCase();
		jQuery(".tel_complete").html(tf);
		if (jQuery(".glyphicon-star").hasClass("dis")) {
			nm = nm + '<span class="glyphicon glyphicon-star tool dis" data-toggle="tooltip" data-placement="top" title="" style="margin-left:10px; " data-original-title="Distinguido"></span>';
		} else {
			//nm=nm+'<span class="glyphicon glyphicon-star tool gray" data-toggle="tooltip" data-placement="top" title="" style="margin-left:10px;" data-original-title="Distinguido"></span>';
		}
		jQuery(".name_complete").html(nm);
	}
	tablas(".convenio-button", "inpsel", conv);
	jQuery("#user-disable").on("click", function() {
		var id = app.currentUser;
		app.exec("/user/disable", {
			'data' : {
				'id' : id
			},
			'before' : function() {
				if (confirm("¿Realmente deseas eliminar este usuario?")) {
					return true;
				}
				return false;
			},
			'success' : function(d) {
				alert("Se ha borrado con exito");
			},
			'error' : function(d) {
				alert("no se ha podido borrar");
			}
		});
	});
	Shadowbox.init({
		handleOversize : "drag",
		modal : true
	});
	var userblock = jQuery("#user");
	var data = userblock.find(".user-data");
	console.log(data);
	var u = User.create(data);
	app.currentUser = parseInt(u.data.id);
	var fclick = null;
	var f1 = function() {
		
		var er = jQuery("#user-agreement_id option:selected").attr("id");
		jQuery("#user-empresa").trigger("change");
		jQuery("#user-annotations").trigger("change");
		jQuery("#user-agreement_id #" + er).attr("selected", true).trigger("change");
		jQuery("#user-agreement_id").trigger("change");
		u.publish();
		jQuery(".mask").show();
		jQuery(".unmasked").addClass("masked").removeClass("unmasked");
		jQuery('.usr-btn-cont').hide();
		jQuery(".add-id").trigger("change");
		jQuery("#editar #updateService").delay(500).trigger("click"); 
	};
	var f2 = function() {
		jQuery(".id-usern").on("change",function(){
			var id=jQuery(this).attr("value");
			app.currentUser = id;
			app2.load("/user/index/" + id + "/1","#reservations", {
				'done' : function() {
					initUserBlock(id);
				}
			});
			
		});
		var correo = jQuery("#user-email").val();
		var tel = jQuery("#user-telephones").val();

		var url = "/user/valid/" + correo + "/" + tel;
		
		jQuery.ajax({
			url : url,
			data : {},
			type : "POST",
			dataType : "json",
			success : function(source) {
				data = source;
				msnjco();

			},
			error : function(source) {
				var er = jQuery("#user-agreement_id option:selected").attr("id");
				jQuery("#user-agreement_id").trigger("change");
				jQuery("#user-annotations").trigger("change");
				jQuery("#user-agreement_id #" + er).attr("selected", true).trigger("change");
				u.publish();
				fclick = f1;

			}
		});
		function msnjco() {
			var correo = data['correo'];
			var tel = data['telefono'];
			var msn = "";
			if (correo == "si" || tel == "si") {
				if (correo == "si" && tel == "si") {
					msn = "El correo y el telefono";
				} else if (correo == "si") {
					msn = "El correo";
				} else if (tel == "si") {
					msn = "El telefono";
				}
				var r = confirm(msn + " ya existe desea continuar");
				if (r == true) {
					jQuery("#user-agreement_id").trigger("change");
					u.publish();
					fclick = f1;

				}
			} else {
			   jQuery("#user-agreement_id").trigger("change");
				u.publish();
				fclick = f1;
			}

		}
		
	};

	if (app.currentUser !== -1 || app.currentUser != -1) {
		jQuery(".mask").show();
		jQuery(".panel-service, .panel-address").show();
		initAddressBlock();
		if (jQuery("#editar #service").length) {
			initServiceBlock("#editar");
			instans("editar");

			jQuery(".add-id").trigger("change");
		}
		if (jQuery("#editarn #service").length) {
			initeditarblock(app.currentUser);
		}

		if (jQuery("#nueva #service").length) {
			initServiceBlock("#nueva");
			instans("nueva");

			clicker();
		}
		fclick = f1;
	} else {
		initAddressBlock();
		jQuery(".usr-btn-cont").show();
		jQuery(".masked").addClass("unmasked").removeClass("masked");
		fclick = f2;
	}

	jQuery('.user-save').on('click', fclick);
	jQuery(".enable-btn").on("click", function() {

		jQuery(".usr-btn-cont").show();
		jQuery(".masked").addClass("unmasked").removeClass("masked");
		jQuery(".mask").hide();
	});
	jQuery('#user-agreement_id').change(function() {
		var idag = jQuery(this).val();
		var desc = jQuery('#' + idag).data('description');
		jQuery('#desc').html(desc);
	});

	jQuery("#newu").on("click", function() {
		if (app.currentUser != -1) {
			initAjustWindow(app.currentUser, da);

		} else {

		}
	});
}

/*Nota se guarda usuario pero las direcciones siguen siendo en automatico*/
function initeditarblock(user){
    console.log(user);
}
function initAddressBlock() {
	
	jQuery('.tool').tooltip();
	jQuery(".btns-golden").prop('disabled', false);
	jQuery(".tables_c").find('disabled').removeClass('disabled');
	if (jQuery('.address-block').length > 0) {
		var options = {
			'display' : function(value) {
				var a = jQuery(this).find('a');
				a.html(value);
				SimpleLog.debug("Display options add address: " + a.attr('href'));
				jQuery(a.attr('href')).find('.alias').val(value).trigger('change');
			}
		};
		var deleteClick = function() {
			var aC = getParent(jQuery(this), "sd");
			var id = jQuery(this).attr("data-aid");
			//alert(id);
			app.exec("/address/disable", {
				'data' : {
					'aid' : id
				},
				'before' : function() {
					if (confirm("¿Realmente deseas eliminar la dirección?")) {
						return true;
					}
					return false;
				},
				'success' : function(d) {
					aC.remove();
					refreshad(app.currentUser);
					if (jQuery('.address-container').length < 1) {
						aC.fadeOut();
						return;
					}
					jQuery(".alias-raw").first().find("a").trigger("click");
				}
			});
		};
		var deleteFn = function() {
			var btn = jQuery(this);
			btn.on("click", deleteClick);
		};
		var btnAdd = jQuery('.add-btn');
		var btnsEdit = jQuery(".address-edit");
		var addresses = jQuery('.address-container');
		var btnsRemove = jQuery(".address-delete");
		jQuery('.alias-raw').editable(options);
		addresses.each(function() {
			var addr = jQuery(this);
			var a = Address.create(addr.find(".address_data"), true);
			var az = addr.find('.suburb_id-ac');
			var btnMap = addr.find('.map-btn');
			btnMap.on('click', function() {
			});
		});

		btnsRemove.each(deleteFn);
		btnAdd.off("click").on("click", function() {
			var addModal = jQuery('#addreess-modal');
			var n = jQuery('.spn-dire').length + 1;
			addModal.find(".modal-title").html("Agregar dirección");
			addModal.find(".modal-body").load("/address/getFields/" + app.currentUser + "/Nueva" + n, function() {
				var fields = addModal.find(".address_data");
				var a = Address.create(fields, false);
				jQuery('#addreess-modal').modal('show');
				var az = '.suburb_id-ac';
				jQuery('#addreess-modal').on('shown.bs.modal', function(e) {
					autoCompleteF(az, "/datos/datosSuburb");
				});
				addModal.find(".btn-save").on("click", function() {
					var stre = jQuery("input[name='street']").val();
					var num = jQuery("input[name='number']").val();
					var sub = jQuery("input[name='suburb_id']").val();
					if (stre != '' && num != '' && sub != '') {
						jQuery(".address_data").each(function() {
							jQuery(this).val(jQuery(this).val().toUpperCase());
						});
						a.publish(true);
						refreshad(app.currentUser);
						jQuery('#addreess-modal').modal('hide');
						jQuery("#addresses").fadeIn();
						

					} else {
						alert("campos requeridos de Calle, Número y Colonia");
					}
				});
			});
		});

		btnsEdit.each(function() {
			var btn = jQuery(this);
			btn.on('click', function() {
				var addModal = jQuery('#addreess-modal');
				addModal.find(".modal-title").html("Editar dirección");
				addModal.find(".modal-body").load("/address/getFields/" + jQuery(this).attr('data-aid') + "/0", function() {
					var fields = addModal.find(".address_data");
					var a = Address.create(fields, false);
					jQuery('#addreess-modal').modal('show');
					var az = '.suburb_id-ac';
					jQuery('#addreess-modal').on('shown.bs.modal', function(e) {
						autoCompleteF(az, "/datos/datosSuburb");
					});
					addModal.find(".btn-save").on("click", function() {
						var stre = addModal.find("input[name='street']").val();
						var num = addModal.find("input[name='number']").val();
						var sub = addModal.find("input[name='suburb_id']").val();
						addModal.find(".address_data").each(function() {

							jQuery(this).val(jQuery(this).val().toUpperCase()).trigger("change");

						});
						if (stre != '' && num != '' && sub != '') {
							a.publish(true);
							refreshad(app.currentUser);
							jQuery('#addreess-modal').modal('hide');
						} else {
							alert("campos requeridos de Calle, Número y Colonia");
						}
					});
				});
			});
		});
	}
}
function restrictions(n,pt) {
	pt.find(".restime"+n).off("change").on("change",function(){
		restrictionsexec(n,pt);
	});	
	pt.find(".restime"+n).off("change").on("change",function(){
		restrictionsexec(n,pt);
	});
	pt.find('.res-date' + n).off("change").on("change",function(){
		restrictionsexec(n,pt);
	});
}
function refreshad(user){
    /*
    app2.load("/datos/ndirec/" + user, ".seldireccion", {
					'done' : function() {

					

					}
				});*/
}
function restrictionsexec(n,pt) {
   if (pt.find(".abierto"+n).is(":checked")) {
        //code
    }else{
    	console.log(jQuery("#service_type").val()+"  "+n);
    	if((jQuery("#service_type").val()=="MTY-APTO" && n==":first") || (jQuery("#service_type").val()=="APTO-MTY" && n==":last")){
			var dat=new Date(pt.find(".resdate"+n).val());
			var mesres=pt.find(".resdate"+n).val().substring(8,11);	
			switch (mesres) {
	            case 'Jan':
					mesres="01";
					break;
				case 'Feb':
					mesres="02";
					break;
				case 'Mar':
					mesres="03";
					break;
				case 'Apr':
					mesres="04";
					break;
				case 'May':
					mesres="05";
					break;
				case 'Jun':
					mesres="06";
					break;
				case 'Jul':
					mesres="07";
					break;
				case 'Aug':
					mesres="08";
					break;
				case 'Sep':
					mesres="09";
					break;
				case 'Oct':
					mesres="10";
					break;
				case 'Nov':
					mesres="11";
					break;
				case 'Dec':
					mesres="12";
					break;
	        }
			var diares=pt.find(".resdate"+n).val().substring(5,7);
			var fcha=pt.find(".resdate"+n).val().substring(12)+"-"+mesres+"-"+diares;
			var hra=pt.find(".restime"+n).val();
			console.log(fchaunica+" "+hraunica);
			if(fchaunica==fcha && hra==hraunica){
				jQuery(".restime"+n).val("");
			}else{
				fchaunica=fcha;
				hraunica=hra;
				if (fcha != "" && hra != "") {
					app.exec("/restriction/busq2/", {
						'data' : {
								fecha:fcha,
								hora:hra
						},
						'before' : function() {
							return true;
						},
						'success' : function(d) {
							if(d.substring(0, 1)=="Q"){
								
							}else if(d==""){
								
							}else{
								jQuery(".restime"+n).val("");
							}
							jQuery(".mnsj").html(d);
		
						}
					});
				}
			}
		}
	}
}
function initServiceBlock(ptñ) {

	//desabilitar el backspace para que no con el navegador al teclearla pero que si pueda borrar al momento de estar en focus con un input
	jQuery(document).unbind('keydown').bind('keydown', function(event) {
		var doPrevent = false;
		if (event.keyCode === 8) {
			var d = event.srcElement || event.target;
			if ((d.tagName.toUpperCase() === 'INPUT' && (d.type.toUpperCase() === 'TEXT' || d.type.toUpperCase() === 'PASSWORD' || d.type.toUpperCase() === 'FILE' || d.type.toUpperCase() === 'EMAIL' || d.type.toUpperCase() === 'SEARCH' || d.type.toUpperCase() === 'DATE' )
			) || d.tagName.toUpperCase() === 'TEXTAREA') {
				doPrevent = d.readOnly || d.disabled;
			} else {
				doPrevent = true;
			}
		}

		if (doPrevent) {
			event.preventDefault();
		}
	});
	
	jQuery('#tabsRes a').click(function(e) {
		e.preventDefault();
		jQuery(this).tab('show');
	});
	var ptnr = jQuery(ptñ);
	restrictions(":first",jQuery("#nueva"));
	restrictions(":last",jQuery("#nueva"));
	app.currentUser = jQuery('.user-data[name="id"]').val();
	jQuery("#s-user-id").val(app.currentUser).trigger("change");
	ptnr.find(".dirhidden:first").tagsinput();
	ptnr.find(".dirhidden:last").tagsinput();
	ptnr.find("#nespecial").on("change",function(){
		if(jQuery(this).is(":checked")){
			jQuery("#hdespecial").val(1).trigger("change");
		}else{
			jQuery("#hdespecial").val(0).trigger("change");
		}
	});
	ptnr.find(".ejpagar").on("keyup", function() {
		var vf = jQuery(this).val().toUpperCase();
		jQuery(this).autocomplete({
			source : "/datos/datosPagos/" + vf,
			minLength : 3,
			select : function(event, ui) {
				var v = ui.item.id;
				jQuery(".reservation_field[name='payment']").val(v).trigger("change");
			}
		});
	});
    ptnr.find("input").attr("autocomplete","off");
	ptnr.find(".bloqueada input").attr("disabled", "disabled");
	ptnr.find(".bloqueada select").attr("disabled", "disabled");
	ptnr.find(".bloqueada button").attr("disabled", "disabled");
	ptnr.find(".solicito:last").html('<tr><td colspan="2">&nbsp;</td></tr>');
	ptnr.find(".solicito:last").html('<tr><td colspan="2">&nbsp;</td></tr>');
	ptnr.find(".sol:last").css("margin-top", "10px");

	jQuery("a[href='#editar']").click(function() {
		function ds(pos) {
			var er = jQuery("#editar").find(".bootstrap-tagsinput:" + pos).width();

			var cnt = 0;
			jQuery("#editar").find(".tag.label.label-info").each(function() {
				var wd = 0;
				var wd2 = 0;
				jQuery(this).children().css("margin-left", "8px");
				wd = jQuery(this).width();

				wd2 = er - wd - 6;

				jQuery(this).children().css("margin-left", wd2);
				wd = 0;

				if (cnt == 0) {
					jQuery(this).css("background", "ghostwhite");
					cnt++;
				} else {
					jQuery(this).css("background", "white");
					cnt = 0;
				}
			});
		}
		ds('first');

	});
	ptnr.find("#service_type").on("change", function() {
		
		var stype = jQuery(this).val();
		if (stype == "APTO-MTY") {
			ptnr.find(".servicetype:first").html("APTO-MTY");
			ptnr.find(".servicetype:last").html("MTY-APTO");
			ptnr.find(".res_type:first").val("APTO-MTY");
			ptnr.find(".res_type:last").val("MTY-APTO");
			ptnr.find(".restime:last").trigger("change");
			
		} else {
			ptnr.find(".servicetype:last").html("APTO-MTY");
			ptnr.find(".servicetype:first").html("MTY-APTO");
			ptnr.find(".res_type:first").val("MTY-APTO");
			ptnr.find(".res_type:last").val("APTO-MTY");
			ptnr.find(".restime:first").trigger("change");

		}
		if (jQuery("#roundtrip").is(":checked")) {
		    ptnr.find(".restime:first").trigger("change");
		    ptnr.find(".restime:last").trigger("change");
		} else {
		    ptnr.find(".restime:first").trigger("change");
		}
		ptnr.find(".res_type:first").trigger("change");
		ptnr.find(".res_type:last").trigger("change");
		impresion();
		
	});
	ptnr.find("#service_type").trigger('change');
	function tipo(position) {
		var typ = ptnr.find(".servicetype:" + position).html();
		ptnr.find("input.res_type:" + position).val(typ);
		ptnr.find("input.res_type:" + position).trigger("change");

	}


	app.currentUser = jQuery("#user-id").val();

	function sumatoria() {

		var fina1 = (parseInt(ptnr.find("#final-first").val())) + (parseInt(ptnr.find("#final-last").val()));
		ptnr.find("#final_cost").html(fina1);
		var dom = (parseInt(ptnr.find("#dom-first").val())) + (parseInt(ptnr.find("#dom-last").val()));
		ptnr.find("#dom_cost").html(dom);
		var fin1 = (parseInt(ptnr.find("#dom-first").val())) + (parseInt(ptnr.find("#final-first").val()));
		var fin2 = (parseInt(ptnr.find("#dom-last").val())) + (parseInt(ptnr.find("#final-last").val()));
		ptnr.find("#total-first").val(fin1);
		ptnr.find("#total-last").val(fin2);
		if (jQuery("#roundtrip").is(":checked")) {
			//aqui ver descuento
			
			var total = (parseInt(ptnr.find("#total-first").val())) + (parseInt(ptnr.find("#total-last").val()));
			if((parseInt(ptnr.find("#des-first").val())) > (parseInt(ptnr.find("#des-last").val()))){
			    var totn = parseInt(ptnr.find("#des-last").val()) ;

			} else {
			    var totn = parseInt(ptnr.find("#des-first").val()) ;

			}
			total = total - totn;
			
		} else {
			var total = (parseInt(ptnr.find("#total-first").val())) + (parseInt(ptnr.find("#total-last").val()));
		}
		if(jQuery("#user-agreement_id").val()=="Sindescuento"){
			total = (parseInt(ptnr.find("#total-first").val())) + (parseInt(ptnr.find("#total-last").val()));
		}
		if(jQuery("#user-empresa").val()==5529){
			total = (parseInt(ptnr.find("#total-first").val())) + (parseInt(ptnr.find("#total-last").val()));
			console.log("particular");
		}
		if(ptnr.find("#cost_mimagen").is(":checked")) {
			total= total + (parseInt(jQuery("#ima_man").val()));
		}
		ptnr.find("#total_cost").html(total);
		ptnr.find("#total-manual").val(total);
		ptnr.find("#costt").val(total).trigger("change");

		ptnr.find(".selectben").trigger("change");
		if (ptnr.find("#man").is(':checked')) {
			var e = jQuery.Event("keyup");
			e.which = 13;
			//choose the one you want
			ptnr.find("#ajus_man").trigger(e);
		}

	}

	function campos(selector, def) {
		ptnr.find(selector).focus(function() {

			var text = jQuery(this).val();
			if (text == "El mismo" || text == "" || (text == "1" && selector == ".passenger")) {
				jQuery(this).attr("data-valor", text);
				jQuery(this).attr("value", "");
				jQuery(this).val("");
				jQuery(this).removeAttr("placeholder");
				jQuery(this).trigger("change");
			}
		});
		ptnr.find(selector).keyup(function() {
			var valor = jQuery(this).val();
			jQuery(this).attr("value", valor);
			jQuery(this).val(valor).trigger("change");
			jQuery(this).data("valor", valor);

		});
		ptnr.find(selector).off("focusout").on("focusout", function() {
			var text = jQuery(this).data("val");
			if (jQuery(this).val() == "") {
				jQuery(this).attr("value", text);
				jQuery(this).val(text).trigger("change");
				jQuery(this).data("val", text);
			}
			jQuery(this).trigger("keyup");
		});
	}

	campos(".solicitante");
	campos(".passenger");

	ptnr.find(".passenger").on("keyup", function() {

		var val = jQuery(this).val();
		//para evaluar los caracteres que se ingresan en el número total de pasajeros cuando se reeliza una reservaciòn
		if (isNaN(val)) {
			alert("Solo se admiten números");

		}
	});
	ptnr.find(".passenger:first").keyup(function() {
		var cp = ptnr.find(".passenger:first").val();

		if (ptnr.find("#roundtrip").is(':checked')) {

		} else {
			ptnr.find(".passenger:last").val(cp);
			ptnr.find(".passenger:last").attr("value", cp);
			if (ptnr.find(".passenger:last").val() == "") {
				ptnr.find(".passenger:last").val(1);
				ptnr.find(".passenger:last").attr("value", 1);
			}
		}
		ptnr.find(".passenger").trigger("change");
	});

	ptnr.find(".id_vehicle:first").change(function() {

		if (ptnr.find("#roundtrip").is(":checked")) {

		} else {
			var idveh = jQuery(this).val();
			//determina la camioneta por defaul en el apartado de los datos de la reservación para poder elegir si dejar esa opciòn o modificarla
			if (idveh == 1) {
				var opveh = "<option value='1' selected='selected'>Automóvil</option><option value='2'>Camioneta</option>";
				ptnr.find(".id_vehicle:last").html(opveh);
				ptnr.find(".id_vehicle:last").trigger("change");
			} else if (idveh == 2) {
				var opveh = "<option value='1'>Automóvil</option><option value='2' selected='selected'>Camioneta</option>";
				ptnr.find(".id_vehicle:last").html(opveh);
				ptnr.find(".id_vehicle:last").trigger("change");
			}

		}
	});
	//cuando se selecciona viaje redondo se habilitan todas los campos requeridos para llenar
	ptnr.find("#roundtrip").click(function() {
		if (jQuery(this).is(':checked')) {

			jQuery("#editar #reservation2").css("visibility", "visible");
			ptnr.find("#reservation2").removeClass("bloqueada");
			ptnr.find("#reservation2 input").removeAttr("disabled");
			ptnr.find("#reservation2 select").removeAttr("disabled");
			ptnr.find("#reservation2 button").removeAttr("disabled");
			if (ptnr.find(".abierto:last").is(":checked") && ptnr.find(".res_type").val() == "MTY-APTO") {
				ptnr.find("input[name='reservation_time2']").prop("disabled", true);
			}

		} else {

			ptnr.find("#reservation2").addClass("bloqueada");
			ptnr.find("#reservation2 input").attr("disabled", "disabled");
			ptnr.find("#reservation2 select").attr("disabled", "disabled");
			ptnr.find("#reservation2 button").attr("disabled", "disabled");
			ptnr.find("#final-last").val(0);
			ptnr.find("#dom-last").val(0);

		}
		sumatoria();
	});

	if (ptnr.find('.panel-address').length > 0) {
		ptnr.find(".panel-service").fadeIn();
	} else {
		ptnr.find(".panel-service").fadeOut();
	}
	if (ptnr.find(".reservation").length > 0) {
		ptnr.find('.panel-service').addClass('active');

		ptnr.find(".servicetype").first().html(searchBox.servicetype[app.currentServiceType]);
		ptnr.find(".servicetype").last().html(searchBox.servicetype[(parseInt(app.currentServiceType) === 0) ? 1 : 0]);

		var resBlocks = ptnr.find(".reservation");
		resBlocks.each(function() {
			var data = jQuery(this).find(".reservation_field");
			if (ptñ == "#nueva") {
				var res = Reservation.create(data);

				jQuery(this).attr('model-id', res.id);
				var className = jQuery(this).find('.address-ac').attr('class');
			} else {
				var edres = upReservation.create(data);
				jQuery(this).attr('model-id', edres.id);
				var className = jQuery(this).find('.address-ac').attr('class');
			}

		});
	}
	function hr(pos) {
		if (ptnr.find(".idate:" + pos).val()) {
			var timedef = ptnr.find(".idate:" + pos).val().substr(0, 16);
		}
		
		ptnr.find('.res-date:' + pos).datetimepicker({
			timepicker : false,
			scrollInput : false,
			mask : false,
			minDate : 0,
			scrollMonth : false,
			format : 'D, d M Y',
			lang : 'es',
			value : timedef,
			onSelectDate : function(c, i) {
				var nd = new Date();
				var hr = new Date(ptnr.find('.res-date:' + pos).val() + " " + ptnr.find('.restime:' + pos).val());
				if (nd > hr) {
					if (ptnr.find('.restime:' + pos).val() != "") {
						alert("Horario innadecuado");
						ptnr.find('.restime:' + pos).val("");
					}
				}
				jQuery(i).trigger("change");
				
				if (ptnr.find(".add-id:first").val() !== "") {
					ptnr.find(".add-id").trigger("change");
				}
				if (ptnr.find(".add-id:last").val() !== "") {
					ptnr.find(".add-id").trigger("change");

				}
			}
		});
	}

	hr("first");
	hr('last');

	function abr(pos, opos) {

		ptnr.find(".abr:" + pos).click(function() {
			//si el dia del servicio no esta definido se puede seleccionar como abierto se inhabilita el campo de fecha del servicio
			if (ptnr.find(".abierto:" + pos).is(':checked')) {
				ptnr.find("input.restime:" + pos).attr("disabled", "disabled");
				ptnr.find("input.resdate:" + pos).attr("disabled", "disabled");

			} else {
				//sino se ha seleccionado el servicio como abierto, se habilita el espacio para ingresar fecha del servicio
				ptnr.find("input.restime:" + pos).removeAttr("disabled");
				ptnr.find("input.resdate:" + pos).removeAttr("disabled");

			}
		});
		ptnr.find("input.restime:" + pos).change(function() {
			if (ptnr.find(".abierto:" + pos).is(':checked')) {
				ptnr.find("input.restime:" + pos).attr("disabled", "disabled");
				ptnr.find("input.resdate:" + pos).attr("disabled", "disabled");
			} else {
				ptnr.find("input.restime:" + pos).removeAttr("disabled");
				ptnr.find("input.resdate:" + pos).removeAttr("disabled");
			}
		});
	}

	abr("first", "last");
	abr("last", "first");

	function impresion() {

		if (ptnr.find(".res_type:first").val() == "MTY-APTO") {
			var pos = "1";
			var opos = "2";
		} else {
			var pos = "2";
			var opos = "1";
		}
		if (ptnr.find("input[name='reservation_time" + opos + "']").hasClass("warning-input")) {

			ptnr.find("input[name='reservation_time" + opos + "']").removeClass("warning-input");
			ptnr.find("#leyenda").prop('checked', false);
		}
		var typ = ptnr.find("select[name='flight_type" + pos + "']").val();

		if (ptnr.find(".masked-time[name='reservation_time" + pos + "']").val() != "" && ptnr.find(".masked-time[name='flight_hour" + pos + "']").val() != "") {
			dif = 0;
			if (typ == "Nacional")
				var dif = 2;
			else if (typ == "Internacional")
				var dif = 4;

			var hrres = ptnr.find(".masked-time[name='reservation_time" + pos + "']").val();
			var hrsres = parseInt(hrres.substring(0, 2));
			var minres = parseInt(hrres.substring(3, 5));
			minres = minres / 60;
			var fnr = hrsres + minres;
			var hrvue = ptnr.find(".masked-time[name='flight_hour" + pos + "']").val();
			var hrsvue = parseInt(hrvue.substring(0, 2));
			var minvue = parseInt(hrvue.substring(3, 5));
			minvue = minvue / 60;
			var fnv = hrsvue + minvue;

			var diferen = fnv - fnr;
			if (dif == 0) {

			} else if (diferen <= dif) {
				//seleccionar la leyenda bajo riesgo del usuario, si es seleccionada o no
				ptnr.find("#leyenda").prop('checked', true);
				ptnr.find("input[name='reservation_time" + pos + "']").addClass("warning-input");
			} else {
				ptnr.find("#leyenda").prop('checked', false);
				ptnr.find("input[name='reservation_time" + pos + "']").removeClass("warning-input");

			}
		}
	}

	//manda un alerta cuando se intenta ingresar una hora invalida en el campo de la hora a la que solicita el servicio
	ptnr.find('.resfhour.masked-time').mask("99:99", {
		completed : function() {

			var maskedcom = jQuery(this).val();
			var hrmasked = parseInt(maskedcom.substring(0, 2));
			var minmasked = parseInt(maskedcom.substring(3, 5));
			if (hrmasked > 23 || hrmasked < 0 || minmasked > 59 || minmasked < 0) {
				jQuery(this).val("");
				alert("hora invalida");
			} else {
				impresion();
			}
			var nm = jQuery(this).attr("name");
			if (nm == "reservation_time1" || nm == "reservation_time2") {
				var elp = padre(jQuery(this), "reservation");
				if (elp.find(".res_type").val() == "APTO-MTY") {
					elp.find("input.resfhour").val(jQuery(this).val());
					elp.find("input.resfhour").trigger("change");
				}
			}

		}
	});
	function her(pos) {
		ptnr.find('.restime.masked-time:' + pos).mask("99:99", {
			completed : function() {
				var nd = new Date();
				var hr = new Date(ptnr.find('.res-date:' + pos).val() + " " + ptnr.find('.restime:' + pos).val());
				if (nd > hr) {
					if (ptnr.find('.res-date:' + pos).val() != "") {
						alert("Horario innadecuado");
						ptnr.find('.restime:' + pos).val("");
					}
				}

				var maskedcom = jQuery(this).val();
				var hrmasked = parseInt(maskedcom.substring(0, 2));
				var minmasked = parseInt(maskedcom.substring(3, 5));
				if (hrmasked > 23 || hrmasked < 0 || minmasked > 59 || minmasked < 0) {
					jQuery(this).val("");
					alert("hora invalida");
				} else {
					impresion();
				}
				var nm = jQuery(this).attr("name");
				if (nm == "reservation_time1" || nm == "reservation_time2") {
					var elp = padre(jQuery(this), "reservation");
					if (elp.find(".res_type").val() == "APTO-MTY") {
						elp.find("input.resfhour").val(jQuery(this).val());
						elp.find("input.resfhour").trigger("change");
					}
				}

			}
		});
	}

	her("first");
	her("last");

	function pasajeros(pos) {
		ptnr.find(".passenger:" + pos).change(function() {
			var we = parseInt(ptnr.find(".passenger:" + pos).val());
			if (we >= 5) {
				var opveh = "<option value='2' selected='selected'>Camioneta</option>";
			} else {
				if (ptnr.find(".id_vehicle:" + pos).val() == 1)
					var opveh = "<option value='1' selected='selected'>Automóvil</option><option value='2'>Camioneta</option>";
				else
					var opveh = "<option value='1'>Automóvil</option><option value='2' selected='selected'>Camioneta</option>";
			}
			ptnr.find(".id_vehicle:" + pos).html(opveh);
			ptnr.find(".id_vehicle:" + pos).trigger("change");
		});
	}

	pasajeros("first");
	pasajeros("last");
	ptnr.find('.select-vehicle').each(function() {
		var o = jQuery(this);
		o.click(function() {
			var p = o.parent();
			var p2 = getParent(o, "panel-body");
			p2 = p2.find('.passengers');
			if ((p2.val() >= 5) && parseInt(o.attr('data-vehicle')) === 1)
				return;
			var i = p.find('input').val(o.attr('data-vehicle')).trigger('change');
			if (!o.hasClass('v-active')) {
				o.parent().find('.v-active').removeClass('v-active');
				o.addClass('v-active');
			}
		});
	});
	ptnr.find('.glyphicon-calendar').on('click', function() {
		jQuery(this).parent().prev('.res-date').focus();
	});

	/*chuy 03-04-13*/

	ptnr.find(".dirhidden:last").change(function() {

		if (jQuery(this).val() == "") {
			ptnr.find(".add-ac:last").show();
			ptnr.find(".add-di:last").hide(250);
		}
	});

	ptnr.find(".dirhidden:first").change(function() {

		if (jQuery(this).val() == "") {
			ptnr.find(".add-ac:first").show();
			ptnr.find(".add-di:first").hide(250);
		}
		ptnr.find(".add-ac:first").val("");
		ptnr.find(".add-ac:first").trigger("keypress");
	});
	ptnr.find(".add-ac:first").on("focus", function() {
		jQuery(this).val("*");
		jQuery(this).trigger("keydown");
	});
	ptnr.find(".add-ac:last").on("focus", function() {
		jQuery(this).val("*");
		jQuery(this).trigger("keydown");
	});
	ptnr.find(".add-ac").on("keyup", function() {
		var aut = jQuery(this).val();
		if (aut.substr(0, 1) == '*') {
			auto = aut.substr(1);
			jQuery(this).val(auto);
		}
	});

	function showdirecciones(element, position) {
		ptnr.find(element + ":" + position).click(function() {
			ptnr.find(".add-ac:" + position).show(250);
			ptnr.find(".add-di:first").hide(250);
		});
	}

	showdirecciones(".add-di", "first");
	showdirecciones(".add-di", "last");

	function direccion(el, pos) {
		ptnr.find(el + ":" + pos).change(function() {
			var colad = ptnr.find(el + ":" + pos).val();
			colad = colad.replace(/#/g, " ");
			var al = ['casa'];
			var alc = 0;
			jQuery(".sd span.al").each(function() {

				al[alc] = jQuery(this).html();

				alc++;
			});

			var lng = (colad.split(',').length);

			colad = colad + "/" + al + "/" + app.currentUser;

			var ur = "/prueba/ad/" + colad;
			var data;
			jQuery.ajax({
				url : ur,
				data : "nocache=" + Math.random(),
				type : "GET",
				dataType : "json",
				success : function(source) {
					data = source;
					var ret = [];
					for (var i = 0; i < lng; i++) {
						ret.push(data[i]);
					}
					ptnr.find(".add-id:" + pos).val(ret).trigger("change");
				},
				error : function(data) {

					ptnr.find(".add-id:" + pos).val("").trigger("change");

				}
			});
		});
	}

	direccion(".dirhidden", "first");
	direccion(".dirhidden", "last");

	/*chuy end*/
	/*chuy 23-04-13*/

	/*chuy end*/
	/************chuy 24-03-14*************/
	//muestra le costo total del servicio que se va a realizar
	function precio(position) {

		ptnr.find(".id_vehicle:" + position).change(function() {
			jQuery("#ben").trigger("change");
			if (ptnr.find(".add-id:" + position).val() != "") {
				//se activa el trigger al momento de cambiar la opcion para poder mostrarlo al usuario
				ptnr.find(".add-id:" + position).trigger("change");
			}
		});
		ptnr.find("input.restime:" + position).change(function() {
			if (ptnr.find(".add-id:" + position).val() != "") {
				ptnr.find(".add-id:" + position).trigger("change");
			}
		});
		ptnr.find("#user-agreement_id").change(function() {
			if (ptnr.find(".add-id:" + position).val() != "") {
				ptnr.find(".add-id:" + position).trigger("change");
			}
		});
		ptnr.find(".seldireccion:" + position).change(function(){
			if ((ptnr.find("#roundtrip").is(':checked') && position == "last") || position == "first") {
				if (ptnr.find(".abierto:" + position).is(':checked')) {
					var hr = "12:00";
				} else {
					var hr = ptnr.find(".masked-time.restime:" + position).val();
				}
				var hora = ptnr.find(".res-date.resdate:" + position).val() + " " + hr;
				hora = hora.replace("/", "-").replace("/", "-");
				var conve = jQuery('#user-id').val();
				var veh = ptnr.find(".id_vehicle:" + position).val();
				var adicionales = ptnr.find(".seldireccion:" + position).val();

				if (veh == "") {
					veh = 1;
				}
				var dist = jQuery("#user-distinguished").val();
				var cost = "/cost/precio/" + veh + "/" + conve + "/" + adicionales + "/" + hora + "/" + dist;
				var data;
				jQuery.ajax({
					url : cost,
					data : "nocache=" + Math.random(),
					type : "GET",
					dataType : "json",
					success : function(source) {
						data = source;
						showcost(position);
						sumatoria();
					},
					error : function(dato) {

						ptnr.find("#final-" + position).val("0");
						ptnr.find("#dom-" + position).val("0");
						ptnr.find("#ad-" + position).val("0");
						ptnr.find("#des-" + position).val("0");
						sumatoria();
					}
				});
				function showcost(pos) {
					var dom_cost = '',
					    total_cost = '',
					    final_cost = '',
					    des_cost = '';
					final_cost = data['mayor'];
					dom_cost = data['extra-dom'];
					ad_cost = data['total'];
					des_cost = data['des'];
					ptnr.find(".cost-reservation:" + pos).val(ad_cost);
					ptnr.find(".cost-reservation:" + pos).trigger("change");
					ptnr.find("#final-" + pos).val(final_cost);
					ptnr.find("#dom-" + pos).val(dom_cost);
					ptnr.find("#des-" + pos).val(des_cost);
					ptnr.find("#ad-" + pos).val(ad_cost);
				}
			}
		});
		ptnr.find(".add-id:" + position).change(function() {
			if ((ptnr.find("#roundtrip").is(':checked') && position == "last") || position == "first") {

				if (ptnr.find(".abierto:" + position).is(':checked')) {
					var hr = "12:00";
				} else {
					var hr = ptnr.find(".masked-time.restime:" + position).val();
				}

				var hora = ptnr.find(".res-date.resdate:" + position).val() + " " + hr;
				hora = hora.replace("/", "-").replace("/", "-");
				var conve = jQuery('#user-id').val();
				var veh = ptnr.find(".id_vehicle:" + position).val();
				var adicionales = ptnr.find(".add-id:" + position).val();

				if (veh == "") {
					veh = 1;
				}
				var tfa = jQuery("#service_type").val();
				var tfa1 = '';
				if(tfa=="MTY-APTO"){
				    if(position=="first")
				        var tfa1 = "cdapto";
                    else
    				    var tfa1 = "aptocd";
				}else{
				    if(position=="last")
				        var tfa1 = "cdapto";
                    else
    				    var tfa1 = "aptocd";
				}
				var dist = jQuery("#user-distinguished").val();
				var cost = "/cost/precio2/" + veh + "/" + conve + "/" + adicionales + "/" + hora + "/" + dist + "/" + tfa1;
				var data;
				jQuery.ajax({
					url : cost,
					data : "nocache=" + Math.random(),
					type : "GET",
					dataType : "json",
					success : function(source) {
						data = source;
						showcost(position);
						sumatoria();
					},
					error : function(dato) {

						ptnr.find("#final-" + position).val("0");
						ptnr.find("#dom-" + position).val("0");
						ptnr.find("#ad-" + position).val("0");
						ptnr.find("#des-" + position).val("0");
						sumatoria();
					}
				});
				function showcost(pos) {
					var dom_cost = '',
					    total_cost = '',
					    final_cost = '',
					    des_cost = '';
					final_cost = data['mayor'];
					dom_cost = data['extra-dom'];
					ad_cost = data['total'];
					des_cost = data['des'];
					ptnr.find(".cost-reservation:" + pos).val(ad_cost);
					ptnr.find(".cost-reservation:" + pos).trigger("change");
					ptnr.find("#final-" + pos).val(final_cost);
					ptnr.find("#dom-" + pos).val(dom_cost);
					ptnr.find("#des-" + pos).val(des_cost);
					ptnr.find("#ad-" + pos).val(ad_cost);
				}

			}
		});

	}

	precio("first");

	precio("last");

	function auto(elemento, url, position) {

		var ur = url;

		ptnr.find(elemento + ":" + position).on("keyup", function() {
			url = ur;
			var ad = jQuery(this).val().toUpperCase();
			url = url + "/" + ad;
			ptnr.find(elemento + ":" + position).autocomplete({
				source : url,
				minLength : 0,
				select : function(event, ui) {
					var v = ui.item.id ? ui.item.id : null;
					var va = ui.item.value ? ui.item.value : null;

					va = va.replace(",", "-").replace(",", "-");

					ptnr.find(".dirhidden:" + position).tagsinput('add', va);

					ptnr.find(elemento + ":" + position).hide();
					ptnr.find(".add-di:" + position).show(250);

				}
			});
		});
		ptnr.find(elemento + ":" + position).on("click", function() {
			var er = ur + "/*";
			ptnr.find(elemento + ":" + position).autocomplete({
				source : er,
				minLength : 1,
				select : function(event, ui) {
					var v = ui.item.id ? ui.item.id : null;
					var va = ui.item.value ? ui.item.value : null;

					va = va.replace(",", "-").replace(",", "-");
					ptnr.find(".dirhidden:" + position).tagsinput('add', va);

					ptnr.find(elemento + ":" + position).hide();
					ptnr.find(".add-di:" + position).show(250);

				}
			});
		});

		tagsdirec(position);
		ptnr.find(elemento + ":" + position).focusout(function() {

			if (jQuery(this).val() == "" || jQuery(this).val() == undefined || jQuery(this).val() == null || jQuery(this).val() == "*") {
				ptnr.find(".add-di:" + position).show(250);
				jQuery(this).val("");
				jQuery(this).trigger("keydown");
				jQuery(this).hide();
			}
			ptnr.find(".dirhidden:" + position).trigger("change");
		});

	}

	function tagsdirec(position) {
		ptnr.find(".dirhidden:" + position).change(function() {
			var er = ptnr.find(".bootstrap-tagsinput:" + position).width();
			var cnt = 0;
			ptnr.find(".tag.label.label-info").each(function() {
				var wd = 0;
				var wd2 = 0;
				jQuery(this).children().css("margin-left", "8px");
				wd = jQuery(this).width();

				wd2 = er - wd - 6;
				jQuery(this).children().css("margin-left", wd2);
				wd = 0;

				if (cnt == 0) {
					jQuery(this).css("background", "ghostwhite");
					cnt++;
				} else {
					jQuery(this).css("background", "white");
					cnt = 0;
				}
			});
		});
	}


	app.currentUser = jQuery('.user-data[name="id"]').val();
	auto(".add-ac", '/datos/datosDireccion/' + app.currentUser, "first");
	auto(".add-ac", '/datos/datosDireccion/' + app.currentUser, "last");

	ptnr.find("#roundtrip").on('click', function() {
		if (ptnr.find("#roundtrip").is(':checked')) {
			jQuery("#valtip").val("red");
			if (ptnr.find(".add-id:last").val() !== "") {
				ptnr.find(".add-id").trigger("change");
				ptnr.find(".add-id").trigger("change");

			}
			
		} else {
			jQuery("#valtip").val("sen");

			if (ptnr.find(".add-id:first").val() !== "") {
				ptnr.find(".add-id").trigger("change");
			}
		}
		jQuery("#valtip").trigger("change");
		sumatoria();
		ptnr.find("#ben").trigger("change");
	});
	/***************chuy end***************/
	/*chuy 26-03-14*/
	ptnr.find("#user-annotations").on("keyup", function() {
		ptnr.find("#hidannotations").val(ptnr.find("#user-annotations").val()).trigger("change");
	});
	ptnr.find("#ben").change(function() {
		if (jQuery(this).is(':checked')) {
			var tben = (parseInt(jQuery("#ben_disp").html()));
			if (tben >= 1) {
				var op = "";

				var veh = (parseInt(ptnr.find(".id_vehicle:first").val()));
				if (ptnr.find("#roundtrip").is(':checked'))
					veh = veh + (parseInt(ptnr.find(".id_vehicle:last").val()));
				for (var i = tben; i > 0; i = i - 1) {
					if (i <= veh && i < 5)
						if (i != 3)
							op += "<option value='" + i + "'>" + i + "</option>";
						else {
							if (veh != 4)
								op += "<option value='" + i + "'>" + i + "</option>";
						}
				}
				ptnr.find(".selectben").html(op);
			}
		} else {
			ptnr.find(".selectben").html('<option value="0">    </option>');
			ptnr.find("#ben_cost").html(0);
		}
		ptnr.find(".selectben").trigger("change");
	});
	ptnr.find(".selectben").change(function() {
		var ben = ptnr.find(".selectben").val();
		var veh1 = (parseInt(ptnr.find(".id_vehicle:first").val()));
		var trfn = 0;
		var trf1 = (parseInt(ptnr.find("#final-first").val()));
		var veh2 = 0
		if (ptnr.find("#roundtrip").is(':checked')) {
			var veh2 = (parseInt(ptnr.find(".id_vehicle:last").val()));
			var veh = veh1 + veh2;
			var trf2 = (parseInt(ptnr.find("#final-last").val()));
			switch (ben) {
			case "1":
				if (veh1 == veh2 && veh == 2) {
					if (trf1 >= trf2) {
						trfn = (-1) * trf2;
					} else {
						trfn = (-1) * trf1;
					}
				} else if (veh1 == 1 && veh == 3) {
					trfn = (-1) * trf1;
				} else if (veh1 == 2 && veh == 3) {
					trfn = (-1) * trf2;
				} else {
					trfn = 0;
				}

				break;
			case "2":
				if (veh1 == veh2 && veh == 4) {
					if (trf1 >= trf2)
						trfn = (-1) * trf2;
					else
						trfn = (-1) * trf1;
				} else if (veh1 == 1 && veh == 3) {
					trfn = (-1) * trf2;
				} else if (veh1 == 2 && veh == 3) {
					trfn = (-1) * trf1;
				} else if (veh == 2) {
					trfn = (-1) * (trf2 + trf1);
				}
				break;
			case "3":
				if (veh == 4 && veh1 == 2)
					trfn = (-1) * trf1;
				else if (veh == 4 && veh1 == 1)
					trfn = (-1) * trf2;
				else if (veh == 3)
					trfn = (-1) * (trf2 + trf1);
				break;
			case "4":
				trfn = (-1) * (trf2 + trf1);
				break;
			}
		} else {
			if (veh1 == ben) {
				trfn = (-1) * (trf1);
			} else {
				trfn = 0;
			}
		}
		ptnr.find("#ben_cost").html(trfn);
		var prs = (parseInt(ptnr.find("#final_cost").html()) + parseInt(ptnr.find("#dom_cost").html()));

		if (jQuery("#roundtrip").is(":checked")) {
			//aqui ver descuento

			if((parseInt(ptnr.find("#des-first").val())) > (parseInt(ptnr.find("#des-last").val()))){
			    var totn = parseInt(ptnr.find("#des-last").val()) ;

			} else {
			    var totn = parseInt(ptnr.find("#des-first").val()) ;

			}
			if(jQuery("#user-agreement_id").val()=="Sindescuento"){
				prs = (parseInt(ptnr.find("#total-first").val())) + (parseInt(ptnr.find("#total-last").val()));
			}else{
				if (trfn == 0)
					prs = prs - totn;
			}
		} else {

		}
		if(jQuery("#user-empresa").val()==5529){
			prs = (parseInt(ptnr.find("#total-first").val())) + (parseInt(ptnr.find("#total-last").val()));

		}
		if(ptnr.find("#cost_mimagen").is(":checked")) {
			prs= prs + (parseInt(jQuery("#ima_man").val()));
		}
		ptnr.find("#total_cost").html(prs + trfn);
		ptnr.find("#total-manual").val(prs + trfn);
		ptnr.find("#costt").val(prs + trfn).trigger("change");

	});

	/*fin*/
	/************chuy 02-04-14**********/
	ptnr.find("#man").click(function() {
		if (ptnr.find("#man").is(':checked')) {
			ptnr.find(".manual").removeClass("backgray");
			ptnr.find("#ben").attr("disabled", "");
			ptnr.find(".selectben").html("<option value='0'></option>");
			ptnr.find(".selectben").trigger("change");
			ptnr.find("#ajus_man").removeAttr("disabled");
			ptnr.find("#ajus_man").css("display", "block");
			if (ptnr.find("#man").is(':checked')) {
				var e = jQuery.Event("keyup");
				e.which = 13;
				//choose the one you want
				ptnr.find("#ajus_man").trigger(e);
			}
			ptnr.find(".manual").removeClass("backgray");
			ptnr.find(".chan").addClass("backgray");

		} else {
			ptnr.find(".manual").addClass("backgray");
			ptnr.find(".chan").removeClass("backgray");
			ptnr.find("#ben").removeAttr("disabled");
			ptnr.find("#ajus_man").attr("disabled", "");
			ptnr.find("#ajus_man").css("display", "none");
			ptnr.find("#ben").trigger("change");
			sumatoria();
		}

	});
	ptnr.find("#cost_mimagen").click(function() {
		if (ptnr.find("#cost_mimagen").is(':checked')) {
			ptnr.find("#tnuevat").val(jQuery("#ima_man").val()).trigger("change");
			ptnr.find("#ima_man").css("display", "block");
			if (ptnr.find("#man").is(':checked')) {
				ptnr.find("#ima_man").trigger("keyup");
			}else{
				sumatoria();
			}
		} else {
			ptnr.find("#tnuevat").val(0).trigger("change");
			ptnr.find("#ima_man").css("display", "none");
			sumatoria();
		}

	});
	ptnr.find("#ajus_man").keyup(function() {
		var tota = parseInt(ptnr.find("#total_cost").val());
		if (tota != 0) {
			tota = jQuery(this).val();
		} else {
			tota = 0;
		}
		if (ptnr.find("#cost_mimagen").is(':checked')) {
			
			tota= parseInt(tota) + (parseInt(jQuery("#ima_man").val()));
		}
		ptnr.find("#total_cost").html(tota);
		ptnr.find("#total-manual").val("m" + tota);
		ptnr.find("#costt").val("m" + tota).trigger("change");
	});

	ptnr.find("#ima_man").keyup(function() {
		ptnr.find("#tnuevat").val(jQuery(this).val()).trigger("change");
		if (ptnr.find("#man").is(':checked')) {
			ptnr.find("#ajus_man").trigger("keyup");
			
		}else{
			sumatoria();

		}
	});

	/*end*/
	/*chuy 03-04-14*/

	function aerolinea(position) {
		var aereolina = [{
			"id" : 0,
			"aereo" : null,
			"nac" : null,
			"inter" : null
		}, {
			"id" : 33,
			"aereo" : "American",
			"nac" : "A",
			"inter" : "A"
		}, {
			"id" : 14,
			"aereo" : "COPA",
			"nac" : "A",
			"inter" : "A"
		}, {
			"id" : 122,
			"aereo" : "Interjet",
			"nac" : "A",
			"inter" : "A"
		}, {
			"id" : 193,
			"aereo" : "Magnicharter",
			"nac" : "A",
			"inter" : "A"
		}, {
			"id" : 126,
			"aereo" : "United (Continental)",
			"nac" : "A",
			"inter" : "A"
		}, {
			"id" : 242,
			"aereo" : "Volaris",
			"nac" : "A",
			"inter" : "A"
		}, {
			"id" : 247,
			"aereo" : "Aeromexico",
			"nac" : "B",
			"inter" : "B"
		}, {
			"id" : 358,
			"aereo" : "Delta",
			"nac" : "B",
			"inter" : "B"
		}, {
			"id" : 612,
			"aereo" : "VivaAereobus",
			"nac" : "C",
			"inter" : "C"
		}, {
			"id" : 33476,
			"aereo" : "TAR",
			"nac" : "A",
			"inter" : "A"
		}, {
			"id" : 33477,
			"aereo" : "Aerolitoral",
			"nac" : "B",
			"inter" : "B"
		}, {
			"id" : 54721,
			"aereo" : "Calafia",
			"nac" : "A",
			"inter" : "A"
		}, {
			"id" : 59990,
			"aereo" : "Aeromar",
			"nac" : "B",
			"inter" : "B"
		}];

		ptnr.find("select.resair:" + position).change(function() {
			var ae = jQuery(this).val();
			var aer = 0;
			var rt = '<option>Terminal</option><option value="A">A</option><option value="B">B</option><option value="C">C</option>';
			var nc = "<option value='null'>Nac./int.</option><option value='Nacional'>Nacional</option><option value='Internacional'>Internacional</option>";
			if (ae == '') {
				var ter = ptnr.find("select.resfterminal:" + position).val();
				var nac = ptnr.find("select.resftype:" + position).val();
				if (ter)
					rt = rt.replace('<option value="' + ter + '">' + ter + '</option>', '<option value="' + ter + '" selected="selected">' + ter + '</option>');
				if (nac == "Nacional")
					nc = nc.replace("<option value='Nacional'>Nacional</option>", "<option value='Nacional' selected='selected'>Nacional</option>");
				else if (nac == "Internacional")
					nc = nc.replace("<option value='Internacional'>Internacional</option>", "<option value='Internacional' selected='selected'>Internacional</option>");
				ptnr.find("select.resfterminal:" + position).html(rt);
				ptnr.find("select.resftype:" + position).html(nc);
			} else {
				for (var i = 0; i < aereolina.length; i++) {
					if (aereolina[i]['id'] == ae) {
						aer = i;
					}
				}
			}
			if (aer != 0) {
				var ter = ptnr.find("select.resfterminal:" + position).val();
				var nac = ptnr.find("select.resftype:" + position).val();
				var rt = "";
				if (nac == "Nacional")
					nc = nc.replace("<option value='Nacional'>Nacional</option>", "<option value='Nacional' selected='selected'>Nacional</option>");
				else if (nac == "Internacional")
					nc = nc.replace("<option value='Internacional'>Internacional</option>", "<option value='Internacional' selected='selected'>Internacional</option>");
				if (aereolina[aer]['nac'] == aereolina[aer]['inter'])
					rt = rt + "<option value='" + aereolina[aer]['nac'] + "'>" + aereolina[aer]['nac'] + "</option>";
				else if (nac == "Nacional")
					rt = rt + "<option value='null'>Terminal<option><option value='" + aereolina[aer]['nac'] + "' selected='selected'>" + aereolina[aer]['nac'] + "</option><option value='" + aereolina[aer]['inter'] + "'>" + aereolina[aer]['inter'] + "</option>";
				else if (nac == "Internacional")
					rt = rt + "<option value='null'>Terminal<option><option value='" + aereolina[aer]['nac'] + "' selected='selected'>" + aereolina[aer]['nac'] + "</option><option value='" + aereolina[aer]['inter'] + "'>" + aereolina[aer]['inter'] + "</option>";
                if(aer == 9){
				
			        if(jQuery('.hiddenterminal:'+ position).val()=='C'){
			            var rt = '<option>Terminal</option><option value="A">A</option><option value="C" selected>C</option>';
			        } else if(jQuery('.hiddenterminal:'+ position).val()=='A'){
			            var rt = '<option>Terminal</option><option value="A" selected>A</option><option value="C" >C</option>';
			        } else {
			            var rt = '<option>Terminal</option><option value="A">A</option><option value="C" >C</option>';
			        }
			        if(jQuery('.hiddennacional:'+ position).val()=='Nacional'){
			        var nc = "<option value='null'>Nac./int.</option><option value='Nacional' selected>Nacional</option><option value='Internacional'>Internacional</option>";
			        } else if(jQuery('.hiddennacional:'+ position).val()=='Internacional'){
			        var nc = "<option value='null'>Nac./int.</option><option value='Nacional'>Nacional</option><option value='Internacional' selected>Internacional</option>";
			        } else {
			        var nc = "<option value='null'>Nac./int.</option><option value='Nacional'>Nacional</option><option value='Internacional'>Internacional</option>";
			        }
                }
				ptnr.find("select.resfterminal:" + position).html(rt);
				ptnr.find("select.resftype:" + position).html(nc);
				ptnr.find("select.resfterminal:" + position).trigger("change");
				ptnr.find("select.resftype:" + position).trigger("change");
			}

		});

		ptnr.find("select.resfterminal:" + position).change(function() {
			var ae = ptnr.find("select.resair:" + position).val();
			var aer = 0;
			if (ae == '') {
			} else {
				for (var i = 0; i < aereolina.length; i++) {
					if (aereolina[i]['id'] == ae) {
						aer = i;
					}
				}
			}
			if (aer != 0) {
				var nac = ptnr.find("select.resftype:" + position).val();
				if (aereolina[aer]['nac'] == aereolina[aer]['inter']) {
					var nc = "<option value='null'>Nac./int.</option><option value='Nacional'>Nacional</option><option value='Internacional'>Internacional</option>";
					if (nac == "Nacional")
						nc = nc.replace("<option value='Nacional'>Nacional</option>", "<option value='Nacional' selected='selected'>Nacional</option>");
					else if (nac == "Internacional")
						nc = nc.replace("<option value='Internacional'>Internacional</option>", "<option value='Internacional' selected='selected'>Internacional</option>");
				} else {
					var ter = jQuery(this).val();
					var nc = "<option value='null'>Nac./int.</option>";
					if (ter == aereolina[aer]['nac']) {
						var nc = nc + "<option value='Nacional' selected='selected'>Nacional</option><option value='Internacional'>Internacional</option>";
					} else if (ter == aereolina[aer]['inter']) {
						var nc = nc + "<option value='Nacional'>Nacional</option><option value='Internacional' selected='selected'>Internacional</option>";
					} else {
						var nc = "<option value='null'>Nac./int.</option><option value='Nacional'>Nacional</option><option value='Internacional'>Internacional</option>";
					}
					if(aer==9){
					}else{
					    ptnr.find("select.resftype:" + position).html(nc);
					}
				}
			}
		});
		ptnr.find("select.resftype:" + position).change(function() {
			impresion();
			var ae = ptnr.find("select.resair:" + position).val();
			var aer = 0;
			if (ae == '') {
			} else {
				for (var i = 0; i < aereolina.length; i++) {
					if (aereolina[i]['id'] == ae) {
						aer = i;
					}
				}
			}
			if (aer != 0) {
				if (aereolina[aer]['nac'] == aereolina[aer]['inter']) {
					var rt = "<option value='" + aereolina[aer]['nac'] + "'>" + aereolina[aer]['nac'] + "</option>";
				} else {
					var nac = jQuery(this).val();

					var rt = "<option value='null'>Terminal</option><option";
					if (nac == "Nacional") {
						var rt = "<option value='" + aereolina[aer]['nac'] + "'>" + aereolina[aer]['nac'] + "</option>";
					} else if (nac == "Internacional") {
						var rt = "<option value='" + aereolina[aer]['inter'] + "'>" + aereolina[aer]['inter'] + "</option>";
					} else {
						rt = "<option value='null'>Terminal</option><option value='" + aereolina[aer]['nac'] + "'>" + aereolina[aer]['nac'] + "</option><option value='" + aereolina[aer]['inter'] + "'>" + aereolina[aer]['inter'] + "</option>"
					}
					if(aer==9){
					}else{
					    ptnr.find("select.resfterminal:" + position).html(rt);
					    ptnr.find("select.resfterminal:" + position).trigger('change');
					}
				}
			} else {

			}

		});
	}
	aerolinea("first");
	aerolinea("last");
	function abierto(position) {
		ptnr.find(".abierto:" + position).on("click", function() {
			ptnr.find("input.restime:" + position).val("00:00");
			ptnr.find("input.resdate:" + position).val("abierto");
			ptnr.find("input.resdate:" + position).trigger("change");
			ptnr.find("input.restime:" + position).trigger("change");
		});
	}
	abierto("first");
	abierto("last");
	tipo("first");
	tipo("last");
	/*end*/
	ptnr.find('#remail').on("click", function() {
		var id = ptnr.find(".folio-res").html();
		var url = "/service/correo/" + id + "/" + 1;
		jQuery.ajax({
			type : "POST",
			dataType : 'json',
			url : url,
			"contentType" : 'application/x-www-form-urlencoded; charset=UTF-8',
			data : {}
		});

		alert("El mensaje se envio satisfactoriamente");

	});

	function adddire(position) {
		var add1 = jQuery("#editar").find('.addva:' + position);
		var addv = add1.val();
		console.log(addv);
		addv = addv.split("&%$");

		for (var i = 0; i < addv.length; i++) {
			jQuery("#editar").find(".dirhidden:" + position).tagsinput('add', addv[i]);
		}
		jQuery("#editar").find(".add-ac").hide(250);
		jQuery("#editar").find(".add-di").show(250);

	}

	if (ptñ == "#editar") {
		if ( !jQuery(".envcan").length > 0 ){
			adddire('first');
			adddire('last');
		}
		jQuery("#editar").find('.reservation_field').trigger('change');
		jQuery("#editar").find('.service_field').trigger('change');
		jQuery("#editar").find('#man').trigger('click');
	}
	jQuery("#service_type").trigger('change');
}

function clicker() {

	jQuery(".finish-btn").click(function() {
		jQuery(".close-service").trigger("click");
		//location.reload();

	});

	function sour(data) {
		var ser_id = data['ser_id'],
		    reservation2 = data['reservation2_id'],
		    ad1 = data['reservation1_direcc1'],
		    ad2 = data['reservation2_direcc2'];
		if (reservation2 === "vacio") {
			jQuery("#editar #reservation2 .reservation_field").each(function() {
				var campo = jQuery(this);
				var d = campo.attr('name');
				var datac = data['reservation2_' + d];
				if (campo.is('select')) {
					if ( typeof datac === "undefined" || datac == null || datac == "") {
						campo.find("option").first().prop("selected", true).trigger("change");
					} else {
						campo.find("option[value='" + datac + "']").prop("selected", true).trigger("change");
					}
				} else {
					campo.val("");
				}
			});
			if (jQuery("#editar #roundtrip").is(":checked")) {
				jQuery("#editar #roundtrip").attr("checked", false);
				jQuery("#editar #roundtrip").trigger("change");
			} else {

				jQuery("#editar #roundtrip").trigger("change");
			}
		} else {
			jQuery("#editar #reservation2 .reservation_field").each(function() {
				var campo = jQuery(this);
				var d = campo.attr('name');
				var datac = data['reservation2_' + d];
				if (campo.is('select')) {
					if ( typeof datac === "undefined" || datac == null || datac == "") {
						campo.find("option").first().prop("selected", true).trigger("change");
					} else {
						campo.find("option[value='" + datac + "']").prop("selected", true).trigger("change");
					}
				} else {
					campo.val(datac);
				}
			});
			jQuery("#editar #reservation2 .dirhidden").tagsinput("removeAll");
			jQuery("#editar #reservation2 .dirhidden").tagsinput('add', ad2);
			jQuery("#editar #reservation2 .dirhidden").trigger("change");

			if (jQuery("#editar #roundtrip").is(":checked")) {
				jQuery("#editar #roundtrip").trigger("change");
			} else {
				jQuery("#editar #roundtrip").attr("checked", true);
				jQuery("#editar #roundtrip").trigger("change");
			}
			jQuery(".folio-res").html(ser_id);
		}
		jQuery("#editar #reservation1 .reservation_field").each(function() {
			var campo = jQuery(this);
			var d = campo.attr('name');
			var datac = data['reservation1_' + d];
			if (campo.is('select')) {
				campo.find("option[value='" + datac + "']").prop("selected", true).trigger("change");
			} else {
				campo.val(datac);
			}
		});
		jQuery("#editar #reservation1 .dirhidden").tagsinput("removeAll");
		jQuery("#editar #reservation1 .dirhidden").tagsinput('add', ad1);
		jQuery("#editar #reservation1 .dirhidden").trigger("change");

	}

}

function instans(ins) {
	if (ins == "nueva") {
		
		var btnSave = jQuery("#saveService");
		btnSave.off('click');
		btnSave.on('click', function() {
			if (jQuery("#nueva input.resfhour").val() == "") {
				jQuery("#nueva #leyenda").prop("checked", true);
			}

			jQuery("#nueva").find(".reservation_field[name='payment']").val(jQuery("#nueva").find(".ejpagar").val());
			jQuery("#nueva").find(".reservation_field[name='payment']").trigger("change");
			SimpleLog.log(Reservation.instances, "Button save reservations: ");
			var R1 = jQuery('#nueva').find(".reservation").first().attr("model-id");
			var ommit = [];
			if (!jQuery('#nueva').find("#roundtrip").is(':checked')) {
				var R2 = jQuery('#nueva').find(".reservation").last().attr("model-id");
				ommit.push(R2);

			}
			console.log(ommit);
			var paym = jQuery(".ejpagar").val();
			var ad = jQuery(".reservation_field[name='addresses1']").val();
			var ad2 = jQuery(".reservation_field[name='addresses2']").val();
			
				if (paym == "Firma por cobrar" || paym == "Efectivo MN" || paym == "Tarjeta de Credito/Debito" || paym == "Cortesia") {
					if (jQuery("#nueva input[name='tipservice']").trigger("change").val() == "red") {
							confirmar = 1;
					} else {
						confirmar = confirm("¿El servicio es sencillo?");
					}
					

					if (confirmar){
						Reservation.masivePublish(ommit);
						btnSave.off('click');
					}
				} else{
					alert("Forma de pago no valida");
				}
			
		});
		jQuery(".btn-again").off("click");
		jQuery(".btn-again").on("click",function(){
			//again
			var atr=[];
			jQuery("#nueva #reservation1 .reservation_field").each(function(){
				atr[jQuery(this).attr("name")]=jQuery(this).val();
			});
			var data={
				addresses1:atr["addresses1"],
				tipservice:"sen",
				airline_id1:atr["airline_id1"],
				annotations:atr["annotations"],
				cost:atr["cost"],
				tnueva:atr["tnueva"],
				cost1:atr["cost1"],
				flight_hour1:atr["flight_hour1"],
				flight_number1:atr["flight_number1"],
				flight_type1:atr["flight_type1"],
				passengers1:atr["passengers1"],
				payment:atr["payment"],
				especial:atr["especial"],
				requested:atr["requested"],
				reservation_date1:atr["reservation_date1"],
				reservation_time1:atr["reservation_time1"],
				terminal1:atr["terminal1"],
				type1:atr["type1"],
				user_id:atr["user_id"],
				vehicle_id1:atr["vehicle_id1"]
			};
			if (jQuery('#nueva').find("#roundtrip").is(':checked')) {
                jQuery("#nueva #reservation2 .reservation_field").each(function(){
					atr[jQuery(this).attr("name")]=jQuery(this).val();			
				});
				var data={
					addresses1:atr["addresses1"],
					tipservice:"red",
					airline_id1:atr["airline_id1"],
					annotations:atr["annotations"],
					cost:atr["cost"],
					cost1:atr["cost1"],
					tnueva:atr["tnueva"],
					flight_hour1:atr["flight_hour1"],
					flight_number1:atr["flight_number1"],
					flight_type1:atr["flight_type1"],
					passengers1:atr["passengers1"],
					payment:atr["payment"],
					especial:atr["especial"],
					requested:atr["requested"],
					reservation_date1:atr["reservation_date1"],
					reservation_time1:atr["reservation_time1"],
					terminal1:atr["terminal1"],
					type1:atr["type1"],
					user_id:atr["user_id"],
					vehicle_id1:atr["vehicle_id1"],
					addresses2:atr["addresses2"],
					airline_id2:atr["airline_id2"],
					annotations:atr["annotations"],
					cost2:atr["cost2"],
					flight_hour2:atr["flight_hour2"],
					flight_number2:atr["flight_number2"],
					flight_type2:atr["flight_type2"],
					passengers2:atr["passengers2"],
					reservation_date2:atr["reservation_date2"],
					reservation_time2:atr["reservation_time2"],
					terminal2:atr["terminal2"],
					type2:atr["type2"],
					vehicle_id2:atr["vehicle_id2"]
				};
            }
			
			jQuery.ajax({
				 type: "POST",
				 url: "/reservation/addagain",
				 data: data, 
				 cache: false,
				 success: function(data){
					 alert("reservacion "+JSON.parse(data).service_id);
					 jQuery(".btn-again").off("click");
				 }
			 });

		});
	} else if (ins == "editar") {
		
		var updatebtn = jQuery("#updateService");

		updatebtn.on('click', function() {
			updatebtn.off('click');
			instans('editar');
            jQuery("#user-annotations").trigger("change");
			if (jQuery("#editar .abierto:first").is(":checked")) {
				jQuery("#editar .abierto:first").trigger("click");
				jQuery("#editar .abierto:first").trigger("click");
			}
			if (jQuery("#editar .abierto:last").is(":checked")) {
				jQuery("#editar .abierto:last").trigger("click");
				jQuery("#editar .abierto:last").trigger("click");
			}
			if (jQuery("#editar input.resfhour").val() == "") {
				jQuery("#editar #leyenda").prop("checked", true);
			}
			SimpleLog.log(upReservation.instances, "Button update reservations: ");
			var U1 = jQuery('#editar').find(".reservation").first().attr("model-id");

			var ommit2 = [];

			if (!jQuery('#editar').find("#roundtrip").is(':checked')) {
				var U2 = jQuery('#editar').find(".reservation").last().attr("model-id");
				ommit2.push(U2);
			}
			upReservation.masivePublish(ommit2)

		});
		jQuery(".btn-again").off("click");
		jQuery(".btn-again").on("click",function(){
			jQuery('#serviceModal').modal("hide");
		});
		//jQuery(".add-id").trigger("change");
	}
	jQuery("#cancelService").on("click", function() {
		var id = jQuery(".service_field[name='id']").val();
		var cfv = jQuery("#canceledby").val();
		app.exec("/reservation/cancel", {
			'data' : {
				'id' : id,
				'canceled' : cfv
			},
			'before' : function() {
				if (confirm("¿Realmente deseas cancelar la reservación?")) {
					return true;
				} else {
					return false;
				}
			},
			'success' : function(d) {

				alert("Servicio Cancelado");
			},
			'error' : function(e) {
				alert("No se ha podido cancelar");
			}
		});
	});
}

function menu() {
	jQuery(".repprin").on("click", function() {
		var tmp = jQuery(this).data("temp");
		app2.load("/reportes/template/" + tmp, "#rep-cont", {
			'done' : function() {
				initreport();
				if (tmp == "content") {
					jQuery('#gr7dia').trigger("click");
				} else if(tmp == "colonias") {
				    
				    jQuery(".btn-reset").off("click").on("click", function(){
				        jQuery("#cloniaext").trigger("click");
				    });
                    jQuery(".palclaves").keypress(function(event){
                        var keycode = (event.keyCode ? event.keyCode : event.which);
                        if(keycode == '13'){
                            jQuery(".palabrasclaves").append("<label>"+jQuery(this).val()+"<br /></label><br />");
                            if(jQuery("#idbusqueda").val() == ''){
                                jQuery("#idbusqueda").val(jQuery(this).val());
                            } else {
                                jQuery("#idbusqueda").val(jQuery("#idbusqueda").val() + ", " + jQuery(this).val());
                            }
                            jQuery(this).val("");
                            jQuery("#ahrefcol").attr("href","/reportes/excelcargarcolonias/" + jQuery("#idbusqueda").val());
                            console.log("/reportes/cargarcolonias/" + jQuery("#idbusqueda").val());
                            jQuery.ajax({
                    			url : encodeURI("/reportes/cargarcolonias/" + jQuery("#idbusqueda").val()),
                    			type : "POST",
                    			dataType : "html",
                    			"contentType" : 'application/x-www-form-urlencoded; charset=UTF-8',
                    			success : function(source) {
                                    console.log("asd", source);
                                    jQuery("#idbodycolonia").html(source);
                    			},
                    			error : function(dato) {
                    			}
                    		});
                            
                            /*app2.load( , "#idbodycolonia", {
                        		'done' : function() {
                        			
                        		}
                        	});*/
                        	
                        	
                        }
                    });
                    
				}
			}
		});
	});
}

function recarbit(page, tipo) {
	var fe = jQuery("#febit").val();
	if (!fe || fe == "") {
		var d = new Date();
		var an = d.getFullYear();
		var mes = (d.getMonth() + 1);
		var dia = d.getDate();
		fe = an + "-" + mes + "-" + dia;
	}
	app2.load("/reportes/bitaco/" + fe + "/" + page + "/" + tipo, "#rep-cont", {
		'done' : function() {
			bitacora();
		}
	});
}

function bitacora() {
	jQuery('#febit').datetimepicker({
		format : 'Y-m-d',
		timepicker : false,
		onSelectDate : function() {
			var pg = jQuery(".actpage").data("page");
			var tip = jQuery("#tempbit").val();
			recarbit(pg, tip);
		}
	});
	jQuery(".bitpage").on('click', function() {
		var dt = jQuery(this).data("page");
		var tmp = jQuery(this).data("temp");
		recarbit(dt, tmp);
	});
}

function initreport() {
	bitacora();
	console.log("call");
	jQuery('#fech2').datetimepicker({
		format : 'Y-m-d',
		timepicker : false,
		onShow : function(ct) {
			this.setOptions({
				maxDate : jQuery('#date_timepicker_end').val() ? jQuery('#date_timepicker_end').val() : false
			})
		},
		onSelectDate : function() {
			app2.load("/reportes/scriptcall", "#scrgraficas", {
				'done' : function() {
					var f1 = jQuery("#fech1").val();
					var f2 = jQuery("#fech2").val();
					if(f1==""){
						f1=f2;
					}
					var ur = "/reportes/xlsccall/" + f1 + "/" + f2;
					jQuery(".callxls").attr("href", ur);
					
				}
			});
		}
	});
	jQuery('#fech1').datetimepicker({
		format : 'Y-m-d',
		onShow : function(ct) {
			this.setOptions({
				minDate : jQuery('#date_timepicker_start').val() ? jQuery('#date_timepicker_start').val() : false,
			})
		},
		timepicker : false,
		onSelectDate : function() {
			app2.load("/reportes/scriptcall", "#scrgraficas", {
				'done' : function() {
					var f1 = jQuery("#fech1").val();
					var f2 = jQuery("#fech2").val();
					if(f2==""){
						f2=f1;
					}
					var ur = "/reportes/xlsccall/" + f1 + "/" + f2;
					jQuery(".callxls").attr("href", ur);
				}
			});
		}
	});
    jQuery("#dateemp2").datetimepicker({
		format : 'Y-m-d',
		timepicker : false,
		onSelectDate : function() {
		    if(jQuery("#dateemp").val() != ""){
    		    app2.load("/main/cantemp/"+jQuery("#dateemp").val()+"/"+jQuery("#dateemp2").val(), "#cntemp", {
    				'done' : function() {
    					
    				}
    			});
		    }
			
		}
	});
		    
	jQuery("#dateemp").datetimepicker({
		format : 'Y-m-d',
		timepicker : false,
		onSelectDate : function() {
    		app2.load("/main/cantemp/"+jQuery("#dateemp").val()+"/"+jQuery("#dateemp2").val(), "#cntemp", {
				'done' : function() {
					
				}
			});
		}
	});
	jQuery('#can2').datetimepicker({
		format : 'Y-m-d',
		timepicker : false,
		onShow : function(ct) {
			this.setOptions({
				maxDate : jQuery('#date_timepicker_end').val() ? jQuery('#date_timepicker_end').val() : false
			})
		},
		onSelectDate : function() {
			tablacan();
		}
	});
	jQuery('#can1').datetimepicker({
		format : 'Y-m-d',
		onShow : function(ct) {
			this.setOptions({
				minDate : jQuery('#date_timepicker_start').val() ? jQuery('#date_timepicker_start').val() : false,
			})
		},
		timepicker : false,
		onSelectDate : function() {
			tablacan();
		}
	});
	function tablacan() {
		app2.load("/reportes/scriptcan", "#scrgraficas", {
			'done' : function() {
				var f1 = jQuery("#can1").val();
				var f2 = jQuery("#can2").val();
				var ur = "/reportes/xlscan/" + f1 + "/" + f2;
				jQuery(".canxls").attr("href", ur);
			}
		});
	}


	


	jQuery('#fecha2').datetimepicker({
		format : 'Y-m-d',
		timepicker : false,
		onShow : function(ct) {
			this.setOptions({
				maxDate : jQuery('#date_timepicker_end').val() ? jQuery('#date_timepicker_end').val() : false
			})
		},
		onSelectDate : function() {
			tablaope();
		}
	});
	jQuery('#fecha1').datetimepicker({
		format : 'Y-m-d',
		onShow : function(ct) {
			this.setOptions({
				minDate : jQuery('#date_timepicker_start').val() ? jQuery('#date_timepicker_start').val() : false,
			})
		},
		timepicker : false,
		onSelectDate : function() {
			tablaope();
		}
	});
	function tablaope() {
		app2.load("/reportes/script", "#scrgraficas", {
			'done' : function() {
				var f1 = jQuery("#fecha1").val();
				var f2 = jQuery("#fecha2").val();
				var ur = "/reportes/xlsc/" + f1 + "/" + f2;
				jQuery(".lindoxls").attr("href", ur);
			}
		});
	}


	jQuery("#bits").tablesorter();
	jQuery("#bitl").tablesorter();
	jQuery("#ocupacion").find("table").tablesorter();
	jQuery(".repprin").off("click");
	menu();

	jQuery('#reporte a').on("click", function(e) {
		e.preventDefault()
		jQuery(this).tab('show')
	});

	jQuery(".rep").on("click", function() {
		jQuery(".stro").removeClass("stro");
		jQuery(this).addClass("stro");

	});
	jQuery(".rep2").on("click", function() {
		jQuery(".stro2").removeClass("stro2");
		jQuery(this).addClass("stro2");
		var is = jQuery(this).attr("id");

		if (is == "grrng") {
			jQuery(".rn").css("opacity", 1).delay(500);
		} else {
			jQuery(".rn").css("opacity", 0).delay(500);
		}
	});

	jQuery('#f2').datetimepicker({
		format : 'Y-m-d',
		timepicker : false,
		onShow : function(ct) {
			this.setOptions({
				maxDate : jQuery('#date_timepicker_end').val() ? jQuery('#date_timepicker_end').val() : false
			})
		},
		onSelectDate : function() {
			jQuery(".stro2").trigger("click");
		}
	});
	jQuery('#f1').datetimepicker({
		format : 'Y-m-d',
		onShow : function(ct) {
			this.setOptions({
				minDate : jQuery('#date_timepicker_start').val() ? jQuery('#date_timepicker_start').val() : false,

			})
		},
		timepicker : false,
		onSelectDate : function() {
			jQuery(".stro2").trigger("click");
		}
	});

	jQuery("#grhoy").on("click", function() {
		var d = new Date();
		var dt = jQuery(".stro").data("rep");
		var fec = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
		if (dt != "dia") {
			var fec2 = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + (d.getDate() + 1);
		} else {
			var fec2 = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
		}
		jQuery("#f2").val(fec);
		jQuery("#f1").val(fec2);

		app2.load("/reportes/cargar/difechas/" + dt + "/-1", "#scrgraficas", {
			'done' : function() {

			}
		});
	});
	jQuery("#grayer").on("click", function() {
		var d = new Date();
		var dt = jQuery(".stro").data("rep");
		var fec = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + (d.getDate() - 1);
		if (dt != "dia") {
			var fec2 = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
		} else {
			var fec2 = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + (d.getDate() - 1);
		}
		jQuery("#f2").val(fec);
		jQuery("#f1").val(fec2);

		app2.load("/reportes/cargar/difechas/" + dt + "/-1", "#scrgraficas", {
			'done' : function() {

			}
		});
	});
	jQuery("#grmesa").on("click", function() {
		var d = new Date();
		var dt = jQuery(".stro").data("rep");
		var fec = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + 01;
		if (dt != "dia") {
			var fec2 = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + (d.getDate() + 1);
		} else {
			var fec2 = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
		}
		jQuery("#f2").val(fec);
		jQuery("#f1").val(fec2);

		app2.load("/reportes/cargar/difechas/" + dt + "/-1", "#scrgraficas", {
			'done' : function() {

			}
		});
	});
	jQuery("#grrng").on("click", function() {
		var dt = jQuery(".stro").data("rep");
		app2.load("/reportes/cargar/difechas/" + dt + "/-1", "#scrgraficas", {
			'done' : function() {

			}
		});
	});

	function diasDeMes(mes, anio) {
		var dias = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
		var ultimo = 0;
		if (mes == 1) {
			var fecha = new Date(anio, 1, 29)
			var vermes = fecha.getMonth();
			if (vermes != mes) {
				ultimo = 28
			}
		}
		if (ultimo == 0) {
			ultimo = dias[mes]
		}
		return ultimo;
	}


	jQuery("#grmesp").on("click", function() {
		var d = new Date();
		var dt = jQuery(".stro").data("rep");
		var fec = d.getFullYear() + "-" + d.getMonth() + "-" + 01;
		var di = diasDeMes((d.getMonth() - 1), d.getFullYear());
		if (dt != "dia") {
			var fec2 = d.getFullYear() + "-" + d.getMonth() + "-" + di;
		} else {
			var fec2 = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + 01;
		}
		jQuery("#f2").val(fec);
		jQuery("#f1").val(fec2);

		app2.load("/reportes/cargar/difechas/" + dt + "/-1", "#scrgraficas", {
			'done' : function() {

			}
		});
	});

	function mostrarFecha(days) {
		milisegundos = parseInt(35 * 24 * 60 * 60 * 1000);

		fecha = new Date();
		day = fecha.getDate();
		month = fecha.getMonth() + 1;
		year = fecha.getFullYear();
		tiempo = fecha.getTime();
		milisegundos = parseInt(days * 24 * 60 * 60 * 1000);
		total = fecha.setTime(tiempo + milisegundos);
		day = fecha.getDate();
		month = fecha.getMonth() + 1;
		year = fecha.getFullYear();

		return year + "-" + month + "-" + day;
	}

	tipgraficas("gr7", "sevendays");
	tipgraficas("gr15", "fifteendays");

	jQuery("#SerRea.rep").on("click", function() {
		jQuery(".stro2").trigger("click");
	});

	function tipgraficas(gr, rng) {

		jQuery("#" + gr).on("click", function() {
			var modo = jQuery(".stro").data("rep");
			app2.load("/reportes/cargar/" + rng + "/" + modo + "/-1", "#scrgraficas", {
				'done' : function() {
				}
			});
		});
	}

}

function timeasig() {
	jQuery(".timeatr").each(function() {
		var th = jQuery(this);
		var ti = th.data("time");
		var hr = ti.substr(0, 2);
		var min = ti.substr(3, 2);
		var seg = ti.substr(6, 2);
		var tiempo = new Date();
		var hora = tiempo.getHours();
		var minuto = tiempo.getMinutes();
		var segundo = tiempo.getSeconds();
		var nhr = hr - hora;
		var nmin = min - minuto;
		var nseg = seg - segundo;
		var tu = (nhr * 60 * 60 * 100) + (nmin * 60 * 100) + (nseg * 100);
		ttiempoa(th, tu);
		th.attr("class", "timeya");
	});
	function ttiempoa(th, tu) {

		var stopwatch = th, // Stopwatch element on the page

		    d = 0,
		    incrementTime = 1000, // Timer speed in milliseconds
		    currentTime =
		    tu, // Current time in hundredths of a second
		    updateTimer = function() {
			if (currentTime <= 0) {
				timerComplete();

			} else {
				var tim = formatTime(currentTime).split(":");

				var hrs = parseInt(tim[0]);
				if (hrs >= 60) {
					var integ = parseInt(hrs / 60);
					var md = (hrs % 60);
					if (integ >= 10) {
						if (md >= 10)
							stopwatch.html("&nbsp;" + integ + ":" + md);
						else
							stopwatch.html("&nbsp;" + integ + ":0" + md);
					} else {
						if (md >= 10)
							stopwatch.html("&nbsp;0" + integ + ":" + md);
						else
							stopwatch.html("&nbsp;0" + integ + ":0" + md);
					}
				} else {
					stopwatch.html("&nbsp;00:" + formatTime(currentTime).substr(0, 5));
				}

				currentTime -= incrementTime / 10;
			}
		},
		    timerComplete = function() {

			if (d == 0) {
				
				
				th.css("font-weight", "bold");
				th.css("text-decoration", "blink");
				var ti = th.data("time");
				var hr = ti.substr(0, 2);
				var min = ti.substr(3, 2);
				var seg = ti.substr(6, 2);
				var tiempo = new Date();
				var hora = tiempo.getHours();
				var minuto = tiempo.getMinutes();
				var segundo = tiempo.getSeconds();
				var hora = tiempo.getHours();
				var minuto = tiempo.getMinutes();
				var segundo = tiempo.getSeconds();
				var nhr = hora - hr;
				var nmin = minuto - min;
				var nseg = segundo - seg;
				var tu = (nhr * 60 * 60 * 100) + (nmin * 60 * 100) + (nseg * 100);

				ttiempo(th, tu, "-");
				th.attr("class", "timeya");
			}

			d++;
		},
		    init = function() {

			stopwatch = th;

			jQuery.timer(updateTimer, incrementTime, true);
		};

		jQuery(init);
	}


	jQuery(".stopwatch").each(function() {
		var th = jQuery(this);
		var ti = th.data("time");
		var hr = ti.substr(0, 2);
		var min = ti.substr(3, 2);
		var seg = ti.substr(6, 2);
		var tiempo = new Date();
		var hora = tiempo.getHours();
		var minuto = tiempo.getMinutes();
		var segundo = tiempo.getSeconds();
		var nhr = hora - hr;
		var nmin = minuto - min;
		var nseg = segundo - seg;
		var tu = (nhr * 60 * 60 * 100) + (nmin * 60 * 100) + (nseg * 100);
		ttiempo(th, tu, "&nbsp;");
		th.attr("class", "timeya");
	});
	function ttiempo(th, tu, r) {
		var stopwatch = th, // Stopwatch element on the page
		    incrementTime = 1000, // Timer speed in milliseconds
		    currentTime =
		    tu, // Current time in hundredths of a second
		    updateTimer = function() {
			var tim = formatTime(currentTime).split(":");
			var hrs = parseInt(tim[0]);
			if (hrs >= 60) {
				var integ = parseInt(hrs / 60);
				if (integ>=2) {
                    //padrellamar classtim
					if (stopwatch.parent().hasClass("classtim")) {
                        //code
						stopwatch.parent().css("background-color","#5C04B3");
						stopwatch.parent().css("background","#5C04B3");

                    }
                }
				var md = (hrs % 60);
				if (integ >= 10) {
					if (md >= 10)
						stopwatch.html(r + integ + ":" + md);
					else
						stopwatch.html(r + integ + ":0" + md);
				} else {
					if (md >= 10)
						stopwatch.html(r + "0" + integ + ":" + md);
					else
						stopwatch.html(r + "0" + integ + ":0" + md);
				}
			} else {
				stopwatch.html(r + "00:" + formatTime(currentTime).substr(0, 5));
			}

			currentTime += incrementTime / 10;
		},
		timerComplete = function() {
			th.attr("class", "timeya");
		},
		    init = function() {

			stopwatch = th;

			jQuery.timer(updateTimer, incrementTime, true);
		};

		jQuery(init);

	}

}

function pad(number, length) {
	var str = '' + number;
	while (str.length < length) {
		str = '0' + str;
	}
	return str;
}

function formatTime(time) {
	var min = parseInt(time / 6000),
	    sec = parseInt(time / 100) - (min * 60),
	    hundredths = pad(time - (sec * 100) - (min * 6000), 2);
	return (min > 0 ? pad(min, 2) : "00");
}

function loadasig(er, tipo,rec) {
	if (rec==1) {
        //code
		app2.load("/assignment/get/asignaciones.ttviajes", "#ttviajes", {
			'done' : function() {
				app2.load("/assignment/get/asignaciones.recursos", "#recursos", {
					'done' : function() {
						
								initassigments();
							
					}
				});
			}
		});
	}
	//app2.load("/assignment/get/asignaciones.titulo" + tipo, "#titulo" + tipo, {
		//'done' : function() {
		
	app2.load("/assignment/get/asignaciones." + tipo + "asignadas", "#" + tipo + "asignadas", {
		'done' : function() {
			app2.load("/assignment/get/asignaciones.salidasmana", "#salidasmana", {
				'done' : function() {
					if (er) {
						app2.load("/assignment/get/asignaciones." + tipo, "#" + tipo, {
							'done' : function() {
								timeasig();
								initassigments();
				
							}
						});
					}else{
						timeasig();
						initassigments();
					}
						
				}
			});
								
		}
	});
		//}
	//});
}

function initassigments() {
	//alertamenu();
	/*jQuery(".btncorte").off("click");
	 jQuery(".btncorte").on("click",function(){
	 window.open("http://resv.goldentransportaciones.com/assignment/cortefirma");
	 window.open("http://resv.goldentransportaciones.com/assignment/corteturno");
	 });*/
	var d = new Date();
	var fec = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
	var a = new Date(d.getTime() - 24 * 60 * 60 * 1000);
	var fecayer = a.getFullYear() + "-" + (a.getMonth() + 1) + "-" + a.getDate();
	var c1 = '<a href="/assignment/cortecaja/' + fecayer + ' 15:00:00/' + fec + ' 03:29:59" class="crt" data="' + fecayer + ' 15:00:00/' + fec + ' 03:29:59" target="_blank"><button type="button" class="btn btn-default tool btn-xs" title="Corte hasta las 3:00 a.m." style="float: left;background: black; color: white;"><span style="color: white" class="glyphicon">03:00</span></button></a>';
	var c2 = '<a href="/assignment/cortecaja/' + fec + ' 03:30:00/' + fec + ' 14:59:59" class="crt" data="' + fec + ' 03:30:00/' + fec + ' 14:59:59" target="_blank"><button type="button" class="btn btn-default tool btn-xs" title="Corte hasta las 3:00 p.m." style="float: left;background: black; color: white;"><span style="color: white" class="glyphicon">15:00</span></button></a>';
	var c3 = '<a href="/assignment/cortecaja/' + fec + ' 15:00:00/' + fec + ' 23:59:59" class="crt" data="' + fec + ' 15:00:00/' + fec + ' 23:59:59" target="_blank"><button type="button" class="btn btn-default tool btn-xs" title="Corte hasta las 3:00 a.m." style="float: left;background: black; color: white;"><span style="color: white" class="glyphicon">03:00</span></button></a>';
	if (d.getHours() > 15) {
		jQuery(".corte1").html(c3 + c2);
	} else if (d.getHours() < 3) {
		jQuery(".corte1").html(c1);
	} else {
		jQuery(".corte1").html(c1 + c2);
	}
	jQuery('.tool').tooltip();
	jQuery(".crt").off("click");
	jQuery(".crt").on("click", function() {
		var hre = jQuery(this).attr("data");
		window.open("http://resv.goldenmty.com/assignment/cortefirma/" + hre);
	});
	setInterval(function() {
		var d = new Date();

		var c1 = '<a href="/assignment/cortecaja/' + fecayer + ' 15:00:00/' + fec + ' 03:29:59" class="crt" data="' + fecayer + ' 15:00:00/' + fec + ' 03:29:59" target="_blank"><button type="button" class="btn btn-default tool btn-xs" title="Corte hasta las 3:00 a.m." style="float: left;background: black; color: white;"><span style="color: white" class="glyphicon">03:00</span></button></a>';
		var c2 = '<a href="/assignment/cortecaja/' + fec + ' 03:30:00/' + fec + ' 14:59:59" class="crt" data="' + fec + ' 03:30:00/' + fec + ' 14:59:59" target="_blank"><button type="button" class="btn btn-default tool btn-xs" title="Corte hasta las 3:00 p.m." style="float: left;background: black; color: white;"><span style="color: white" class="glyphicon">15:00</span></button></a>';
		var c3 = '<a href="/assignment/cortecaja/' + fec + ' 15:00:00/' + fec + ' 23:59:59" class="crt" data="' + fec + ' 15:00:00/' + fec + ' 23:59:59" target="_blank"><button type="button" class="btn btn-default tool btn-xs" title="Corte hasta las 3:00 a.m." style="float: left;background: black; color: white;"><span style="color: white" class="glyphicon">03:00</span></button></a>';
		if (d.getHours() > 15) {
			jQuery(".corte1").html(c3 + c2);
		} else if (d.getHours() < 3) {
			jQuery(".corte1").html(c1);
		} else {
			jQuery(".corte1").html(c1 + c2);
		}
		jQuery('.tool').tooltip();
	}, 1800000);
	initrecursos();
	assing();
	jQuery("#confrec").off("click");
	jQuery("#confrec").on("click", function() {

		var tmpl = jQuery(this).data("templ");
		app2.load("/assignment/gettemplate/" + tmpl, "#modal-assigments", {
			'done' : function() {
				$('#admun a').click(function(e) {
					e.preventDefault()
					$(this).tab('show')
				});

				status("#confoperator");
				status("#confunity");

			}
		});
		jQuery('#modal-assigments').modal("show");
	});
	jQuery("#sumagas").off("change").on("change",function(){
		if (jQuery(this).val()!="") {
			var tt=parseInt(jQuery("#gas").html())+parseInt(jQuery(this).val());
			jQuery("#gas").html(tt);
			var ttt=parseInt(jQuery("#vtales").html());
			var subal=(tt)/(ttt);
			jQuery("#gasdiv").html(subal.toFixed(2));
			
			app.exec("/prueba/gasol", {
				'data' : {
					'cantidad' : tt
				},
				'before' : function() {
					return true;
				},
				'success' : function(d) {
					jQuery("#sumagas").val(0);
	
				}
			});
		}
		
	});
	jQuery("#vpo").off("click").on("click", function() {
		app2.load("/assignment/tm/asignaciones.vpo", "#modal-assigments", {
			'done' : function() {
				jQuery('#feca').datetimepicker({
					format : 'Y-m-d',
					timepicker : false,
					onShow : function(ct) {
						this.setOptions({
							maxDate : jQuery('#date_timepicker_end').val() ? jQuery('#date_timepicker_end').val() : false
						})
					}
				});
				jQuery('#modal-assigments').modal("show");
				
				jQuery("#opvuel").off("change").on("change", function() {
					jQuery("#opvuelb").trigger("click");
				});
				jQuery("#feca").off("change").on("change", function() {
					jQuery("#opvuelb").trigger("click");
				});
				jQuery("#opvuelb").off("click").on("click", function() {
					var val = jQuery('#opvuel').val();
					var feca = jQuery("#feca").val();
					if(val!="" && feca!="")
					app2.load("/assignment/tm/asignaciones.tviajespo/" + val + "/" + feca, "#modal-assigments .cnt", {
						'done' : function() {

						}
					});
				});
			}
		});
	});
}

function initrecursos() {
	jQuery(".libera").off("click");
	jQuery(".libera").on("click", function() {
		var id = jQuery(this).data('unit');

		app.exec("/assignment/liberar", {
			'data' : {
				'id' : id
			},
			'before' : function() {
				return true;
			},
			'success' : function(d) {
				//timesave();
				loadasig(0, "llegadas",0);
				loadasig(0, "salidas",1);
				loadbol(0);
			}
		});
	});
}

function loadcobranza() {
	var c = 0;
	app2.load("/assignment/get/asignaciones.salidasasignadas", "#salidasasignadas", {
		'done' : function() {
			app2.load("/assignment/get/asignaciones.llegadasasignadas", "#llegadasasignadas", {
				'done' : function() {
					app2.load("/assignment/get/asignaciones.recursos", "#recursos", {
						'done' : function() {
							timeasig();
							initassigments();
						}
					});
				}
			});
		}
	});
}

function loadbol(er) {
	if (er)
		app2.load("/assignment/get/asignaciones.tickets", "#tickets", {
			'done' : function() {
				timeasig();
				initassigments();
			}
		});
	app2.load("/assignment/get/asignaciones.recursos", "#recursos", {
		'done' : function() {
			app2.load("/assignment/get/asignaciones.ticketsasignados", "#ticketsasignados", {
				'done' : function() {
					//app2.load("/assignment/get/asignaciones.titulollegadas", "#titulollegadas", {
					//	'done' : function() {
							timeasig();
							initassigments();
					//	}
					//});
				}
			});
		}
	});

}

function assing() {
	jQuery(".actbole").off("click");
	jQuery(".actbole").on("click", function() {
		loadbol(1);
	});
	jQuery(".actsal").off("click");
	jQuery(".actsal").on("click", function() {
		loadasig(1, "salidas",1);
	});
	jQuery(".actlle").off("click");
	jQuery(".actlle").on("click", function() {
		loadasig(1, "llegadas",1);
	});
	jQuery(".selsal").off("click");
	jQuery(".selsal").on("click", function() {
		if (jQuery(this).prop("checked")) {
			jQuery(".imprsal").prop("checked", true);
		} else {
			jQuery(".imprsal").prop("checked", false);
		}
	});
	jQuery(".imptickets").off("click");
	jQuery(".imptickets").on("click", function() {

		var c = 0;
		var idim;
		jQuery(".impr:checked").each(function() {

			if (idim === undefined)
				idim = jQuery(this).val();
			else
				idim = idim + "." + jQuery(this).val();
			c++;
			if (c == 50) {
				var ue = "/assignment/imp2/" + idim;
				window.open(ue, '_blank, width=100');

				c = 0;
				idim = "";
			}
		});
		var ue = "/assignment/imp2/" + idim;
		window.open(ue, '_blank, width=100');

	});
	jQuery(".borrartic").off("click");
	jQuery(".borrartic").on("click", function() {

		var c = 0;
		var idim;
		jQuery(".bor:checked").each(function() {

			if (idim === undefined)
				idim = jQuery(this).val();
			else
				idim = idim + "." + jQuery(this).val();
			c++;
		});
		app.exec("/assignment/borrartick/" + idim, {
			'before' : function() {
				if (confirm("¿Realmente deseas borrar los boletos?")) {
					return true;
				} else {
					return false;
				}
			},
			'success' : function(d) {
				//timesave();
				if (d) {
					alert("boletos eliminados");
				} else {
					alert("uno o varios boletos no pudieron ser eliminados");
				}
				app2.load("/assignment/index", "#assignments #content", {
					'done' : function() {
						timeasig();
						initassigments();
					}
				});
			}
		});

	});
	jQuery(".selleg").off("click");
	jQuery(".selleg").on("click", function() {
		if (jQuery(this).prop("checked")) {
			jQuery(".imprlleg").prop("checked", true);
		} else {
			jQuery(".imprlleg").prop("checked", false);
		}
	});
	jQuery(".borrartodos").off("click");
	jQuery(".borrartodos").on("click", function() {
		if (jQuery(this).prop("checked")) {
			jQuery(".bor").prop("checked", true);
		} else {
			jQuery(".bor").prop("checked", false);
		}
	});
	jQuery(".stic").off("click");
	jQuery(".danger .stic").on("click", function() {

		var dng = padre(jQuery(this), "danger");
		dng.find("td img").remove();
	});

	jQuery(".stic").on("click", function() {
		var d = jQuery(this).data("reser");
		var ur = "/assignment/imp2/" + d;
		//
		window.open(ur, 'Ticket', "width=350, height=1000");
	});
	jQuery(".remo").off("click");
	jQuery(".remo").on("click", function() {
		var id = jQuery(this).data("reser");
		app.exec("/reservation/cancelassig", {
			'data' : {
				'id' : id
			},
			'before' : function() {
				if (confirm("¿Realmente deseas cancelar la asignacion?")) {
					return true;
				} else {
					return false;
				}
			},
			'success' : function(d) {
				//timesave();
				app2.load("/assignment/index", "#assignments #content", {
					'done' : function() {
						timeasig();
						initassigments();
					}
				});
			}
		});
	});
	jQuery(".reasig").off("click");
	jQuery(".reasig").on("click", function() {
		var id = jQuery(this).data('reser');

		app2.load("/assignment/modal/reser/" + id, "#modal-assigments", {
			'done' : function() {
				jQuery(".btn-asig-reser").on("click", function() {
					var unit = jQuery('#modal-assigments .unidad').val();
					if (unit == "") {
						unit = null;
					}
					var tipo = jQuery('#modal-assigments .tipo').val();
					app.exec("/assignment/reasinreser", {
						'data' : {
							'id' : id,
							'unit' : unit
						},
						'before' : function() {
							return true;
						},
						'success' : function(d) {
							//timesave();
							app2.load("/assignment/index", "#assignments #content", {
								'done' : function() {
									timeasig();
									initassigments();
									ajaxgps(id, unit);

								}
							});
						}
					});
					jQuery("#modal-assigments").modal("hide");
				});

			}
		});
		jQuery("#modal-assigments").modal("show");
	});
	jQuery(".reas").off("click");
	jQuery(".reas").on("click", function() {
		app2.load("/assignment/get/assignment.busquedare", "#modal-assigments", {
			'done' : function() {
				jQuery(".busres").on("keyup", function() {
					var dt = jQuery(this).val().toUpperCase();
					jQuery(this).autocomplete({
						source : "/datos/reserauto2/" + dt,
						minLength : 1,
						select : function(event, ui) {

							//alert(ui.item.id);
							jQuery(".btas").val(ui.item.id);
							jQuery(".btas").css("display", "-webkit-inline-box");
						}
					});
				});
				jQuery(".btas").off("click");
				jQuery(".btas").on("click", function() {
					var id = jQuery(this).attr("value");

					app2.load("/assignment/modal/reser/" + id, "#modal-assigments", {
						'done' : function() {
							jQuery("btn-asig-reser").on("click", function() {
								var unit = jQuery(".unidad").val();
								var id = jQuery(".reser-id").val();
								app.exec("/assignment/reasinreser", {
									'data' : {
										'id' : id,
										'unit' : unit
									},
									'before' : function() {
										return true;
									},
									'success' : function(d) {
										app2.load("/assignment/index", "#assignments #content", {
											'done' : function() {
												timeasig();
												initassigments();
												ajaxgps(id, unit);

											}
										});
									}
								});
							});
							jQuery("#modal-assigments").modal("show");
						}
					});

				});
				jQuery("#modal-assigments").modal("show");
			}
		});
	});
	jQuery(".reasigbol").off("click");
	jQuery(".reasigbol").on("click", function() {
		var id = jQuery(this).data("bol");
		app2.load("/assignment/modal/reasbol/" + id, "#modal-assigments", {
			'done' : function() {
				jQuery(".btn-reasig-boleto").off("click");
				jQuery(".btn-reasig-boleto").on("click", function() {
					var unit = jQuery('#modal-assigments .unidad').val();
					var tipo = jQuery('#modal-assigments .tipo').val();
					app.exec("/assignment/reasinbol", {
						'data' : {
							'type' : tipo,
							'id' : id,
							'unit' : unit
						},
						'before' : function() {
							return true;
						},
						'success' : function(d) {
							//timesave();
							jQuery(".actbole").trigger("click");
							//recien agregado
							ajaxgpsbol(id, unit);
						}
					});
					jQuery("#modal-assigments").modal("hide");
				});
				jQuery("#modal-assigments").modal("show");
			}
		});
	});

	jQuery(".watchcancel").off("click");
	jQuery(".watchcancel").on("click", function() {
		app2.load("/user/gettemplate/assignment.rescancel", "#modal-assigments", {
			'done' : function() {
				jQuery(".close-user").on("click", function() {
					jQuery("#modal-assigments").modal("hide");
				});
				jQuery("#modal-assigments").modal("show");
			}
		});
	});
	jQuery(".searchreser").off("click");
	jQuery(".searchreser").on("click", function() {

		app2.load("/user/gettemplate/assignment.pb", "#modal-assigments", {
			'done' : function() {
				jQuery(".pbresr").off("keyup");
				jQuery(".pbresr").on("keyup", function() {
					var dt = jQuery(this).val().toUpperCase();
					jQuery(this).autocomplete({
						source : "/datos/reserauto/" + dt,
						minLength : 1,
						select : function(event, ui) {

							//alert(ui.item.id);
							jQuery(".btas").val(ui.item.id);
							jQuery(".btas").css("display", "-webkit-inline-box");

						}
					});
				});

				jQuery(".btas").off("click");
				jQuery(".btas").on("click", function() {
					var id = jQuery(this).val();

					app2.load("/assignment/modal/reser/" + id, "#modal-assigments", {
						'done' : function() {
							jQuery(".btn-asig-reser").on("click", function() {
								var unit = jQuery('#modal-assigments .unidad').val();
								if (unit == "") {
									unit = null;
								}
								var tipo = jQuery('#modal-assigments .tipo').val();

								app.exec("/assignment/asinr", {
									'data' : {
										'id' : id,
										'unit' : unit
									},
									'before' : function() {
										return true;
									},
									'success' : function(d) {
										//timesave();
										loadasig(1, "llegadas",0);
										loadasig(1, "salidas",1);
										ajaxgps(id, unit);
									}
								});

								jQuery("#modal-assigments").modal("hide");
							});

						}
					});
					jQuery("#modal-assigments").modal("show");
				});

			}
		});
		jQuery("#modal-assigments").modal("show");

	});
	jQuery(".cobranza").off("click");
	//asignar boleto
	jQuery(".cobranza").on("click", function() {
		jQuery("#modal-assigments").modal("show");
		var id = jQuery(this).data("reser");
		//timesave();
		app2.load("/assignment/modal/cob/" + id, "#modal-assigments", {
			'done' : function() {
				jQuery(".convred").off("change");
				jQuery(".convred").on("change", function() {
					if (jQuery(".convred").is(":checked")) {
						jQuery(".mostrardoble").css("display", "-webkit-inline-box");
						jQuery(".mostrar").css("display", "none");
					} else {
						jQuery(".mostrardoble").css("display", "none");
						jQuery(".mostrar").css("display", "-webkit-inline-box");
					}
				});
				jQuery(".convsen").off("change");
				jQuery(".convsen").on("change", function() {
					if (jQuery(".convsen").is(":checked")) {
						jQuery(".mostrardoble").css("display", "-webkit-inline-box");
						jQuery(".mostrar").css("display", "none");
					} else {
						jQuery(".mostrardoble").css("display", "none");
						jQuery(".mostrar").css("display", "-webkit-inline-box");
					}
				});
				jQuery(".btn-cob").off("click");
				jQuery(".btn-cob").on("click", function() {
					var id = jQuery("#cobreserid").val();
					var pg = jQuery("#fpaag").val();
					var red = jQuery(".convred").val();
					if (!jQuery(".convred").length)
						var red = jQuery(".convsen").val();
					app.exec("/assignment/asinreser/asignado", {
						'data' : {
							'id' : id,
							'pago' : pg
						},
						'before' : function() {
							return true;
						},
						'success' : function(d) {
							jQuery("#modal-assigments").modal("hide");
							loadcobranza();
							window.open("http://resv.goldenmty.com/assignment/imppago/" + id);
						}
					});
					if (jQuery(".convsen").is(":checked")) {
						app.exec("/assignment/sencillo", {
							'data' : {
								'id' : id
							},
							'before' : function() {
								return true;
							},
							'success' : function(d) {

							}
						});
					}
					if (jQuery(".convred").is(":checked")) {//cambie nobre clase a .convred de convren
						var cst = jQuery(".mostrardoble").html();
						app.exec("/assignment/redondo", {
							'data' : {
								'id' : id,
								'costo' : cst
							},
							'before' : function() {
								return true;
							},
							'success' : function(d) {

							}
						});
					}
				});
			}
		});
	});

	jQuery('.por-boletos').off("click");
	jQuery('.por-boletos').on("click", function() {
		var id = jQuery(this).data('reser');

		app2.load("/assignment/modal/boleto/" + id, "#modal-assigments", {
			'done' : function() {
				jQuery(".btn-asig-boleto").off("click");
				jQuery(".btn-asig-boleto").on("click", function() {
					var unit = jQuery('#modal-assigments .unidad').val();
					if (unit == "") {
						unit = null;
					}
					var tipo = jQuery('#modal-assigments .tipo').val();
					app.exec("/assignment/asingbol/porasignar", {
						'data' : {
							'type' : tipo,
							'id' : id,
							'unit' : unit
						},
						'before' : function() {
							return true;
						},
						'success' : function(d) {
							//timesave();
							loadbol(1);
							ajaxgpsbol(id, unit);
						}
					});
					jQuery("#modal-assigments").modal("hide");
				});

			}
		});
		jQuery("#modal-assigments").modal("show");
	});
	jQuery(".boleto").off("click");
	jQuery(".boleto").on("click", function() {
		var id = jQuery(this).data('reser');
		var tipo = jQuery('#modal-assigments .tipo').val();

		app.exec("/assignment/asingbol/asignado", {
			'data' : {
				'type' : tipo,
				'id' : id
			},
			'before' : function() {
				if (confirm("¿Realmente deseas terminar el boleto?")) {
					return true;
				} else {
					return false;
				}
			},
			'success' : function(d) {

				//timesave();
				loadbol(0);
			}
		});

	});
	//asignar reservacion
	jQuery('.am').off("click");
	jQuery('.am').on("click", function() {
		var id = jQuery(this).data('reser');
		app2.load("/assignment/modal/reser2/" + id, "#modal-assigments", {
			'done' : function() {
				jQuery(".btn-asig-reseram").on("click", function() {
					var unit = jQuery('#modal-assigments .unidad').val();
					if (unit == "") {
						unit = null;
					}
					var tipo = jQuery('#modal-assigments .tipo').val();

					app.exec("/assignment/asinreser/porasignar", {
						'data' : {
							'type' : tipo,
							'id' : id,
							'unit' : unit
						},
						'before' : function() {
							return true;
						},
						'success' : function(d) {
							//timesave();
							app2.load("/assignment/index", "#assignments #content", {
								'done' : function() {
									timeasig();

									initassigments();
								}
							});
						}
					});
					jQuery("#modal-assigments").modal("hide");
				});

			}
		});
		jQuery("#modal-assigments").modal("show");

	});
	function ajaxgps(id, unidad) {
		jQuery.ajax({
			url : "/assignment/informa/" + id + "/" + unidad,
			type : "POST",

			dataType : "json",
			"contentType" : 'application/x-www-form-urlencoded; charset=UTF-8',
			success : function(source) {
				data = source;

				jQuery.ajax({
					
					url : encodeURI("http://gps.goldentransportaciones.com/services/addservice/?fecha=" + data['fecha'] + "&nombre_pasajero=" + data['nombre'] + "&direccion=" + data['dir'] + "&observaciones=" + data["observaciones"] + "&telefono=" + data['telefono'] + "&chofer_external_id=" + data['chofer_external_id'] + "&external_id=" + data['external_id'] + "&aeropuertociudad=" + data['aeropuertociudad'] + "&numero_reservacion=" + data['service']),
					type : "GET",
					crossDomain : true,
					dataType : 'jsonp',
					"contentType" : 'application/x-www-form-urlencoded; charset=UTF-8',
					success : function(source) {
						data = source;

					},
					error : function(dato) {

					}
				});
			},
			error : function(dato) {
			}
		});
	}

	function ajaxgpsbol(id, unidad) {
		jQuery.ajax({
			url : "/assignment/informabol/" + id + "/" + unidad,
			type : "POST",

			dataType : "json",
			"contentType" : 'application/x-www-form-urlencoded; charset=UTF-8',
			success : function(source) {
				data = source;

				jQuery.ajax({
					url : encodeURI("http://gps.goldentransportaciones.com/services/addservice/?fecha=" + data['fecha'] + "&nombre_pasajero=" + data['nombre'] + "&direccion=" + data['dir'] + "&observaciones=" + data["observaciones"] + "&telefono=" + data['telefono'] + "&chofer_external_id=" + data['chofer_external_id'] + "&external_id=" + data['external_id'] + "&aeropuertociudad=" + data['aeropuertociudad'] + "&numero_reservacion=" + data['service']),
					type : "GET",
					crossDomain : true,
					dataType : 'jsonp',
					"contentType" : 'application/x-www-form-urlencoded; charset=UTF-8',
					success : function(source) {
						data = source;

					},
					error : function(dato) {
					}
				});
			},
			error : function(dato) {
			}
		});
	}


	jQuery('.por-asignars').off("click");
	jQuery('.por-asignars').on("click", function() {
		var id = jQuery(this).data('reser');

		app2.load("/assignment/modal/reser/" + id, "#modal-assigments", {
			'done' : function() {
				jQuery(".btn-asig-reser").on("click", function() {
					var unit = jQuery('#modal-assigments .unidad').val();
					if (unit == "") {
						unit = null;
					}
					var tipo = jQuery('#modal-assigments .tipo').val();

					app.exec("/assignment/asinreser/porasignar", {
						'data' : {
							'type' : tipo,
							'id' : id,
							'unit' : unit
						},
						'before' : function() {
							return true;
						},
						'success' : function(d) {
							//timesave();
							loadasig(1, "salidas",1);
							ajaxgps(id, unit);
						}
					});
					jQuery("#modal-assigments").modal("hide");
				});

			}
		});
		jQuery("#modal-assigments").modal("show");
	});
	jQuery('.por-asignarl').off("click");
	jQuery('.por-asignarl').on("click", function() {
		var id = jQuery(this).data('reser');

		app2.load("/assignment/modal/reser/" + id, "#modal-assigments", {
			'done' : function() {
				jQuery(".btn-asig-reser").on("click", function() {
					var unit = jQuery('#modal-assigments .unidad').val();
					if (unit == "") {
						unit = null;
					}
					var tipo = jQuery('#modal-assigments .tipo').val();

					app.exec("/assignment/asinreser/porasignar", {
						'data' : {
							'type' : tipo,
							'id' : id,
							'unit' : unit
						},
						'before' : function() {
							return true;
						},
						'success' : function(d) {
							//timesave();
							loadasig(1, "llegadas",1);
							ajaxgps(id, unit);
						}
					});
					jQuery("#modal-assigments").modal("hide");
				});

			}
		});
		jQuery("#modal-assigments").modal("show");
	});
	jQuery(".asignadol").off("click");
	jQuery(".asignadol").on("click", function() {
		var id = jQuery(this).data('reser');

		jQuery.ajax({
			url : "/assignment/serv/" + id,
			data : {},
			type : "POST",
			dataType : "json",
			success : function(source) {

				app.exec("/assignment/asinreser/asignado", {
					'data' : {
						'id' : id
					},
					'before' : function() {
						if (confirm("¿Realmente deseas terminar la reservación " + source + "?")) {
							return true;
						} else {
							return false;
						}
					},
					'success' : function(d) {
						//timesave();
						loadasig(0, "llegadas",1);
					}
				});
			}
		});

	});
	jQuery(".asignados").off("click");
	jQuery(".asignados").on("click", function() {
		var id = jQuery(this).data('reser');
		jQuery.ajax({
			url : "/assignment/serv/" + id,
			data : {},
			type : "POST",
			dataType : "json",
			success : function(source) {

				app.exec("/assignment/asinreser/asignado", {
					'data' : {
						'id' : id
					},
					'before' : function() {
						if (confirm("¿Realmente deseas terminar la reservación " + source + "?")) {
							return true;
						} else {
							return false;
						}
					},
					'success' : function(d) {
						//timesave();
						loadasig(0, "salidas",1);
					}
				});
			}
		});

	});

	// asiganr
	jQuery('.optasi').off("click");
	jQuery('.optasi').on("click", function() {
		jQuery('#modal-assigments').modal("show");

		var idop = jQuery(this).data('op');
		app2.load("/assignment/modal/asingunity/" + idop, "#modal-assigments", {
			'done' : function() {

				jQuery("#asunit").on("change", function() {
					var ide = jQuery(this).val();
					app2.load("/assignment/desc/" + ide + "/operator", "#desunit", {
						'done' : function() {

						}
					});
				});

				//guardar unidad y oprador
				jQuery('#modal-assigments .btn-asig').off("click");
				jQuery('#modal-assigments .btn-asig').on("click", function() {
					var id = jQuery('.assin input.idun').val();
					var sl = jQuery('.assin select').val();

					app.exec("/assignment/asinunit/", {
						'data' : {
							'aid' : id,
							'op' : sl
						},
						'before' : function() {
							return true;
						},
						'success' : function(d) {
							var ideco = jQuery('.assin input.ecoun').val();
							//timesave();
							app2.load("/assignment/get/asignaciones.recursos", "#recursos", {
								'done' : function() {
									timeasig();
									initassigments();
									var slnom = jQuery('#descnombre').html();

									jQuery.ajax({
										url : "http://gps.goldentransportaciones.com/units/updatecreatevehicle/",
										type : "GET",
										data : {
											chofer_external_id : sl,
											chofer_name : slnom,
											unidad : ideco
										},
										crossDomain : true,
										dataType : 'jsonp',
										"contentType" : 'application/x-www-form-urlencoded; charset=UTF-8',
										success : function(source) {
											data = source;

										},
										error : function(dato) {

										}
									});
								}
							});
						}
					});
					jQuery("#modal-assigments").modal('hide');
				});

			}
		});
	});
	jQuery(".desasunit").off("click");
	jQuery(".desasunit").on("click", function() {
		var unit = jQuery(this).data("unit");
		app.exec("/assignment/desasig/", {
			'data' : {
				'id' : unit
			},
			'before' : function() {
				if (confirm("¿Realmente deseas desasignar esta unidad?")) {
					return true;
				} else {
					return false;
				}
			},
			'success' : function(d) {
				//timesave();
				app2.load("/assignment/get/asignaciones.recursos", "#recursos", {
					'done' : function() {
						timeasig();
						initassigments();
					}
				});
			}
		});
	});
}

function status(prm) {

	pr = jQuery(prm);
	pr.find(".btn-status").off("click");
	pr.find(".btn-status").on("click", function() {

		var tipo = jQuery(prm).data("tipo");
		pr = jQuery(prm);
		var id = pr.find("select").val();
		var vl = jQuery(prm).find("input[name='status']:checked").val();
		var cause = jQuery(prm).find("textarea").val();
		app.exec("/assignment/status/" + tipo + "/" + vl, {
			'data' : {
				'aid' : id,
				'cause' : cause
			},
			'before' : function() {
				return true;

			},
			'success' : function(d) {
				alert("LISTO");
			}
		});
		app2.load("/assignment/get/asignaciones.recursos", "#recursos", {
			'done' : function() {

				initassigments();
			}
		});
	});
	pr.find("select").off("change");
	pr.find("select").on("change", function() {
		var ide = jQuery(this).val();
		var tipo = jQuery(prm).data("tipo");
		app2.load("/assignment/desc/" + ide + "/" + tipo, prm + " #descr", {
			'done' : function() {
				jQuery.ajax({
					url : "/assignment/extrac/" + ide + "/" + tipo,
					data : {},
					type : "POST",
					dataType : "json",
					success : function(source) {
						data = source;
						var hab = data['hab'];
						if (hab == 0) {
							jQuery(prm).find("#deshab").prop("checked", true);
						} else {
							jQuery(prm).find("#hab").prop("checked", true);
						}
						var cause = data['cause'];
						jQuery(prm).find("textarea").val(cause);
					}
				});
			}
		});
	});

}

function initconfiguration() {
	var det;

	jQuery.ajax({
		url : "/systemuser/getype/", //la url que requiere para enviar los datos
		data : {},
		type : "POST", //metodo por el cual se van a enviar los datos
		dataType : "json",
		success : function(source) {
			data = source;
			det = data;
		}
	});

	var sysblock = jQuery("#Principal");
	var sysModal = jQuery('#panel-modal');
	var deleteuser = sysblock.find('.system-delete');
	var edituser = sysblock.find('.system-edit');
	var newuser = sysblock.find('.system-add');
	newuser.off("click");
	edituser.off("click");
	deleteuser.off("click");
	jQuery(".btn-usersave").off("click");
	jQuery('.tool').tooltip();
	jQuery('.mn-config').off("click");
	jQuery('.mn-esp').off("click");
	jQuery(".close-user").off("clcik");
	jQuery('.mn-esp').on('click', function() {
		var mn = jQuery(this).data('menu');
		app2.load("/configuration/gettable/" + mn, "#ctnesp", {
			'done' : function() {
				jQuery("#calenesp").datetimepicker({
					timepicker : false,
					scrollInput : false,
					mask : false,
					maxDate : 0,
					format : 'Y-m-d',
					lang : 'es',
					onSelectDate : function(c, i) {
						var d = new Date(c);
							var fec = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
							var a = new Date(d.getTime() + 24 * 60 * 60 * 1000);
							var fecayer = a.getFullYear() + "-" + (a.getMonth() + 1) + "-" + a.getDate();
							var htc='<center><h1>Caja</h1></center><a href="/assignment/cortecaja/' + fec + ' 03:30:00/' + fec + ' 09:59:59" target="_blank"><button class="btn btn-success">hasta las 3 p.m. parte 1</button></a><a href="/assignment/cortecaja/' + fec + ' 10:00:00/' + fec + ' 14:59:59" target="_blank"><button class="btn btn-success">hasta las 3 p.m. parte 2</button></a><a href="/assignment/cortecaja/' + fec + ' 15:00:00/' + fec + ' 21:59:59" target="_blank"><button class="btn btn-success">hasta las 3 a.m. parte 1</button></a><a href="/assignment/cortecaja/' + fec + ' 21:59:00/' + fecayer + ' 03:29:59" target="_blank"><button class="btn btn-success">hasta las 3 a.m. parte 2</button></a>';
							var htf='<center><h1>Firma</h1></center><a href="/assignment/cortefirma/' + fec + ' 03:30:00/' + fec + ' 09:59:59" target="_blank"><button class="btn btn-success">hasta las 3 p.m. parte 1</button></a><a href="/assignment/cortefirma/' + fec + ' 10:00:00/' + fec + ' 14:59:59" target="_blank"><button class="btn btn-success">hasta las 3 p.m. parte 2</button></a><a href="/assignment/cortefirma/' + fec + ' 15:00:00/' + fec + ' 21:59:59" target="_blank"><button class="btn btn-success">hasta las 3 a.m. parte 1</button></a><a href="/assignment/cortefirma/' + fec + ' 21:59:00/' + fecayer + ' 03:29:59" target="_blank"><button class="btn btn-success">hasta las 3 a.m. parte 2</button></a>';
							jQuery(".cortcaja").html(htc);
							jQuery(".cortfr").html(htf);
							jQuery(".fesp").html("Cortes de "+fec+"(año-mes-dia)");
					}
				});
			}
		});
	});
	jQuery('.mn-config').on('click', function() {
		var mn = jQuery(this).data('menu');
		app2.load("/configuration/gettable/" + mn, "#mn-cont", {
			'done' : function() {
				buttonsys();
				sear();
				if (mn == "corte.excel") {
					jQuery('.calendarxls').datetimepicker({
						timepicker : false,
						scrollInput : false,
						mask : false,
						maxDate : 0,
						format : 'Y-m-d',
						lang : 'es',
						onSelectDate : function(c, i) {
							var d = new Date(c);
							var fec = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
							var a = new Date(d.getTime() + 24 * 60 * 60 * 1000);
							var fecayer = a.getFullYear() + "-" + (a.getMonth() + 1) + "-" + a.getDate();
							var c1 = '<a href="/assignment/xls/' + fec + ' 15:30:00/' + fecayer + ' 03:29:59" target="_blank"><button type="button" class="btn btn-default tool btn-xl" title="Corte hasta las 3:00 a.m." style="float: left;background: black; color: white;"><span style="color: white" class="glyphicon">03:00</span></button></a>';
							var c2 = '<a href="/assignment/xls/' + fec + ' 03:30:00/' + fec + ' 15:29:59" target="_blank"><button type="button" class="btn btn-default tool btn-xl" title="Corte hasta las 3:00 p.m." style="float: left;background: black; color: white;"><span style="color: white" class="glyphicon">15:00</span></button></a>';
							jQuery("#excelink").html(c2 + c1);
						}
					});
				} else if (mn == "corte.excelfirma") {
					jQuery('.calendarxlsfirma').datetimepicker({
						timepicker : false,
						scrollInput : false,
						mask : false,
						maxDate : 0,
						format : 'Y-m-d',
						lang : 'es',
						onSelectDate : function(c, i) {
							var d = new Date(c);
							var fec = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
							var a = new Date(d.getTime() + 24 * 60 * 60 * 1000);
							var fecayer = a.getFullYear() + "-" + (a.getMonth() + 1) + "-" + a.getDate();
							var c1 = '<a href="/assignment/xlfr/' + fec + ' 15:30:00/' + fecayer + ' 03:29:59" target="_blank"><button type="button" class="btn btn-default tool btn-xl" title="Corte hasta las 3:00 a.m." style="float: left;background: black; color: white;"><span style="color: white" class="glyphicon">03:00</span></button></a>';
							var c2 = '<a href="/assignment/xlfr/' + fec + ' 03:30:00/' + fec + ' 15:29:59" target="_blank"><button type="button" class="btn btn-default tool btn-xl" title="Corte hasta las 3:00 p.m." style="float: left;background: black; color: white;"><span style="color: white" class="glyphicon">15:00</span></button></a>';
							//xlsfirma
							jQuery("#excelinkfirma").html(c2 + c1);
						}
					});
				} else if (mn == "corte.movil") {
					jQuery("#btcosto").off("click").on("click", function() {
						var appauto = jQuery("#appauto").val();
						var appcam = jQuery("#appcam").val();
						app.exec("/movil/appcosto", {
							'data' : {
								'costoauto' : appauto,
								'costocam' : appcam
							},
							'before' : function() {

								return true;
							},
							'success' : function(d) {
								alert("Costo actualizado");
							}
						});
					});
				} else if (mn == "reports.movil") {
					jQuery("#btn-repmovil").on("click", function() {
						var mes = jQuery(".mes").val();
						var anio = jQuery(".anio").val();
						app2.load("/reportes/removil/" + mes + "/" + anio, "#panel-cnt", {
							'done' : function() {

							}
						});
					});
				} else if (mn == "configuration.cambios") {
					var idcam;
					//script para cambios
					jQuery("#reservacion_cambio").on("keyup", function() {
						var dt = jQuery(this).val().toUpperCase();
						jQuery(this).autocomplete({
							source : "/datos/reserauto/" + dt,
							minLength : 1,
							select : function(event, ui) {
								idcam = ui.item.id;
								jQuery("#campo_cambio").html('<option value="payment" selected>Forma de pago</option><option value="cost">Costo</option><option value="vehicle_id">Tipo de vehiculo</option>').trigger("change");
								jQuery("#valor_cambio").trigger("change");
							}
						});
					});
					jQuery("#campo_cambio").on("change", function() {
						if (jQuery(this).val() == "payment") {
							jQuery(".vlc").html("<select class='form-control' id='valor_cambio'><option value='Efectivo MN'>Efectivo MN</option><option value='Firma por cobrar'>Firma por cobrar</option><option value='Tarjeta de Credito/Debito'>Tarjeta de Credito/Debito</option><option value='Cortesia'>Cortesia</option></select>").trigger("change");
						} else if (jQuery(this).val() == "cost") {
							jQuery(".vlc").html('<input type="number" class="form-control" id="valor_cambio">');
						} else if (jQuery(this).val() == "vehicle_id") {
							jQuery(".vlc").html("<select class='form-control' id='valor_cambio'><option value='1'>Automóvil</option><option value='2'>Camioneta</option></select>").trigger("change");
						}
						jQuery("#valor_cambio").on("change", function() {
							jQuery(".cambut").html("<button class='btn btn-success' data-id='" + idcam + "'>Mandar solicitud</button>");
							jQuery(".cambut button").on("click", function() {
								var idc = jQuery(this).data("id");
								var camc = jQuery("#campo_cambio").val();
								var valc = jQuery("#valor_cambio").val();
								app.exec("/cambios/solicitud", {
									'data' : {
										'id' : idc,
										'campo' : camc,
										'valor' : valc
									},
									'before' : function() {
										return true;
									},
									'success' : function(d) {
										alert("Se ha enviado la solicitud");
									}
								});
							});
						});
					});
				}
			}
		});
	});
	buttonsys();
}

function buttonsys() {
	jQuery("#cambio").on("keyup", function() {
		var dt = jQuery(this).val().toUpperCase();
		jQuery(this).autocomplete({
			source : "/datos/datosRed/" + dt,
			minLength : 1,
			select : function(event, ui) {

				//alert(ui.item.id);
				jQuery(".btcam").val(ui.item.id);
				jQuery(".btcam").css("display", "-webkit-inline-box");
				app2.load("/datos/configdatos/" + ui.item.id, "#pagoadm", {
					'done' : function() {

						jQuery("#admcambio").off("click");
						jQuery("#admcambio").on("click", function() {
							var pg = jQuery('#pgo').val();
							var pment = jQuery('#pment').val();
							app.exec("/prueba/cambiopag", {
								'data' : {
									'id' : ui.item.id,
									'cost' : pg,
									'payment' : pment
								},
								'before' : function() {

									return true;
								},
								'success' : function(d) {
									alert("Se ha actualizado la información");
								}
							});
						});
					}
				});
			}
		});
	});
	jQuery(".btn-lluv").off("click");
	jQuery(".btn-lluv").on("click",function(){
		if (jQuery(this).html()=="Normal") {
			var lluv=1;
		}else{
			var lluv=0;
		}
		app.exec("/restriction/lluvia", {
			'data' : {
				'id' : lluv
			},
			'before' : function() {

				return true;
			},
			'success' : function(d) {
				jQuery(".btn-lluv").html(d);
			}
		});
	});
	jQuery(".btcam").off("click");
	jQuery(".btcam").on("click", function() {
		var id = jQuery(this).val();
		app.exec("/assignment/cambio", {
			'data' : {
				'id' : id
			},
			'before' : function() {

				return true;
			},
			'success' : function(d) {
				alert("las reservaciones fueron cambiadas");
			}
		});
	});
	var sysblock = jQuery("#Principal");
	var sysModal = jQuery('#panel-modal');
	var deleteuser = sysblock.find('.system-delete');
	var edituser = sysblock.find('.system-edit');
	var newuser = sysblock.find('.system-add');

	jQuery(".close-user").on("click", function() {
		sysModal.modal("hide");
	});
	newuser.on("click", function() {

		var typ = jQuery(this).data('typ');
		if (typ == "systemuser") {
			var mn = "systemuser.controluser";
			var frm = "systemuser.form";
		} else if (typ == "zone") {
			var mn = "zone.controlzone";
			var frm = "zone.form";
		} else if (typ == "operator") {
			var frm = "unity.operatorform";
			var mn = "unity.controloperator";
		} else if (typ == "unity") {
			var frm = "unity.unityform";
			var mn = "unity.controlunity";
		} else if (typ == "suburb") {
			var frm = "suburb.form";
			var mn = "suburb.controlsub";
		} else if (typ == "business") {
			var frm = "business.form";
			var mn = "business.control";
		} else if (typ == "agreement") {
			var mn = "agreement.controlagreement";
			var frm = "agreement.form";
		} else if (typ == "restricciones") {
			var mn = "configuration.restricciones";
			var frm = "configuration.restform";
		}

		app2.load("/configuration/getform/" + frm + "/-1", "#panel-modal", {
			'done' : function() {
				var data = sysModal.find(".system_data");
				if (typ == "systemuser") {
					sys = Systemuser.create(data);
					autoCompleteA(".sub", '/datos/datosSuburb/');
				} else if (typ == "zone")
					zon = Zone.create(data);
				else if (typ == "agreement")
					agr = Agree.create(data);
				else if (typ == "operator") {
					op = Operator.create(data);
					autoCompleteA(".sub", '/datos/datosSuburb/');
					jQuery("#pic").on("change",function(){
						
					});
				} else if (typ == "suburb") {
					sub = Suburb.create(data);
				} else if (typ == "unity")
					uni = Unity.create(data);
				else if (typ == "business") {

					bus = Business.create(data);

					function busagr() {
						var agrer = "";
						sysModal.find('#agr').find('input:checked').each(function() {

							if (agrer == "") {
								agrer = jQuery(this).val();
							} else {
								agrer = agrer + "," + jQuery(this).val();
							}
						});
						sysModal.find('input[name="agreements_bus"]').val(agrer).trigger("change");
					}


					sysModal.find('#agr').find('input').on("change", busagr);

				} else if (typ == "restricciones") {
					rest = Restriccion.create(data);
				}

				jQuery(".btn-usersave").on("click", function() {
					var vali = 1;
					if (typ == "systemuser") {

						if (jQuery(".confemail").is(':empty')) {

							jQuery(".confemail").val("sin correo");
						}
						sys.publish();
					} else if (typ == "zone")
						zon.publish();
					else if (typ == "agreement")
						agr.publish();
					else if (typ == "operator"){						
						op.publish();
					}else if (typ == "suburb")
						sub.publish();
					else if (typ == "unity") {
						if (jQuery("input[name='insurance_policy']").val() == "" || jQuery("input[name='serial_number']").val() == "") {
							vali = 0;
							if (jQuery("input[name='insurance_policy']").val() == "")
								jQuery("input[name='insurance_policy']").attr("style", "border:red 1px solid;");
							else
								jQuery("input[name='insurance_policy']").attr("style", "");

							if (jQuery("input[name='serial_number']").val() == "")
								jQuery("input[name='serial_number']").attr("style", "border:red 1px solid;");
							else
								jQuery("input[name='serial_number']").attr("style", "");
						} else {
							uni.publish();
							vali = 1;
							jQuery("input[name='insurance_policy']").attr("style", "");
							jQuery("input[name='serial_number']").attr("style", "");
						}
					} else if (typ == "business") {
						bus.publish();
					} else if (typ == "restricciones") {
						rest.publish();
					}
					if (vali == 1) {
						sysModal.modal("hide");
						app2.load("/configuration/gettable/" + mn, "#mn-cont", {
							'done' : function() {

								initconfiguration();
							}
						});
					}

				});
			}
		});
		sysModal.modal("show");
	});
	jQuery(".ticket-delete").on("click", function() {
		var id = jQuery(this).data("aid");
		var mn = "tickets.pendientes";
		app.exec("/configuration/disableticket", {
			'data' : {
				'id' : id
			},
			'before' : function() {

				return true;
			},
			'success' : function(d) {
				app2.load("/configuration/gettable/" + mn, "#mn-cont", {
					'done' : function() {

						jQuery(".ticket-delete").off("click");
						initconfiguration();

					}
				});
			}
		});

	});
	jQuery(".all-delete").on("click", function() {
		var mn = "tickets.pendientes";
		var ids = {};
		var c = 0;
		jQuery(".tickcheck:checked").each(function() {
			ids[c] = jQuery(this).val();
			c++;
		});

		app.exec("/configuration/disableticket/muchos", {
			'data' : {
				'id' : ids
			},
			'before' : function() {

				return true;
			},
			'success' : function(d) {
				app2.load("/configuration/gettable/" + mn, "#mn-cont", {
					'done' : function() {

						jQuery(".all-delete").off("click");
						initconfiguration();

					}
				});
			}
		});
	});
	jQuery(".ticket-hab").on("click", function() {
		var id = jQuery(this).data("aid");
		var mn = "tickets.pendientes";
		app.exec("/configuration/enableticket", {
			'data' : {
				'id' : id
			},
			'before' : function() {

				return true;
			},
			'success' : function(d) {
				app2.load("/configuration/gettable/" + mn, "#mn-cont", {
					'done' : function() {

						jQuery(".ticket-hab").off("click");
						initconfiguration();

					}
				});
			}
		});
 
	});
	jQuery("#allboldes").on("click", function() {
		if (jQuery(this).is(":checked")) {
			jQuery(".desab .tickcheck").attr("checked", true);
		} else {
			jQuery(".desab .tickcheck").attr("checked", false);
		}
	});
	jQuery("#allbolhabi").on("click", function() {
		if (jQuery(this).is(":checked")) {
			jQuery(".habi .tickcheck").attr("checked", true);
		} else {
			jQuery(".habi .tickcheck").attr("checked", false);
		}
	});
	jQuery(".all-hab").on("click", function() {
		var mn = "tickets.pendientes";
		var ids = {};
		var c = 0;
		jQuery(".tickcheck:checked").each(function() {
			ids[c] = jQuery(this).val();
			c++;
		});
		app.exec("/configuration/enableticket/muchos", {
			'data' : {
				'id' : ids
			},
			'before' : function() {

				return true;
			},
			'success' : function(d) {
				app2.load("/configuration/gettable/" + mn, "#mn-cont", {
					'done' : function() {

						jQuery(".all-hab").off("click");
						initconfiguration();

					}
				});
			}
		});

	});
	edituser.on("click", function() {
		var tbtn = jQuery(this);
		var frmid = tbtn.parent("div.sys").data("system");
		var typ = tbtn.parent('div.sys').data('typ');
		if (typ == "systemuser") {
			var mn = "systemuser.controluser";
			var frm = "systemuser.form";

		} else if (typ == "zone") {
			var mn = "zone.controlzone";
			var frm = "zone.form";
		} else if (typ == "operator") {
			var frm = "unity.operatorform";
			var mn = "unity.controloperator";
		} else if (typ == "unity") {
			var frm = "unity.unityform";
			var mn = "unity.controlunity";
		} else if (typ == "suburb") {
			var frm = "suburb.form";
			var mn = "suburb.controlsub";
		} else if (typ == "business") {
			var frm = "business.form";
			var mn = "business.control";
		} else if (typ == "agreement") {
			var frm = "agreement.form";
			var mn = "agreement.controlagreement";
		} else if (typ == "restricciones") {
			var mn = "configuration.restricciones";
			var frm = "configuration.restform";
		}
		app2.load("/configuration/getform/" + frm + "/" + frmid, "#panel-modal", {
			'done' : function() {
			    sysModal.find(".system_data").trigger("change");
				var data = sysModal.find(".system_data");

				if (typ == "systemuser") {
					sys = Systemuser.create(data);
					autoCompleteA(".sub", '/datos/datosSuburb/');
				} else if (typ == "zone"){
					zon = Zone.create(data);
				}else if (typ == "operator") {
					autoCompleteA(".sub", '/datos/datosSuburb/');
					op = Operator.create(data);
				} else if (typ == "suburb"){
				    sysModal.find(".system_data").trigger("change");
				    var data = sysModal.find(".system_data");
					sub = Suburb.create(data);
					
				}else if (typ == "unity")
					uni = Unity.create(data);
				else if (typ == "business") {
					bus = Business.create(data);
					function busagr() {
						var agrer = "";
						sysModal.find('#agr').find('input:checked').each(function() {

							if (agrer == "") {
								agrer = jQuery(this).val();
							} else {
								agrer = agrer + "," + jQuery(this).val();
							}
						});
						sysModal.find('input[name="agreements_bus"]').val(agrer).trigger("change");
					}


					sysModal.find('#agr').find('input').on("change", busagr);

				} else if (typ == "agreement")
					agr = Agree.create(data);
				else if (typ == "restricciones")
					rest = Restriccion.create(data);

				jQuery(".btn-usersave").on('click', function() {
				    sysModal.find(".system_data").trigger("change");
					if (typ == "systemuser")
						sys.publish();
					else if (typ == "zone")
						zon.publish();
					else if (typ == "operator")
						op.publish();
					else if (typ == "suburb"){
					    console.log(sub);
					    sysModal.find('select[name="id_zone"]').trigger("change");
					    var sv = sysModal.find('select[name="id_zone"]').val();
					    sub.publish();
					}else if (typ == "unity")
						uni.publish();
					else if (typ == "business") {
						var agrer = "";
						sysModal.find('#agr').find('input:checked').each(function() {

							if (agrer == "") {
								agrer = jQuery(this).val();
							} else {
								agrer = agrer + "," + jQuery(this).val();
							}
						});
						sysModal.find('input[name="agreements_bus"]').val(agrer);

						bus.publish();
					} else if (typ == "agreement")
						agr.publish();
					else if (typ == "restricciones")
						rest.publish();

					sysModal.modal("hide");
					app2.load("/configuration/gettable/" + mn, "#mn-cont", {
						'done' : function() {

							edituser.off("click");
							jQuery(".btn-usersave").off("click");
							initconfiguration();
						}
					});
				});
			}
		});
		sysModal.modal("show");
	});

	deleteuser.on("click", function() {
		var sysC = jQuery(this).parent("div.sa");
		var tbtn = jQuery(this);
		var sid = tbtn.parent("div.sys").data("system");
		var tbtn = jQuery(this);
		var frmid = tbtn.parent("div.sys").data("system");
		var typ = tbtn.parent('div.sys').data('typ');
		if (typ == "systemuser") {
			var mn = "systemuser.controluser";

		} else if (typ == "zone") {
			var mn = "zone.controlzone";

		} else if (typ == "operator") {

			var mn = "unity.controloperator";
		} else if (typ == "agreement") {

			var mn = "agreement.controlagreement";
		} else if (typ == "unity") {

			var mn = "unity.controlunity";
		} else if (typ == "suburb") {

			var mn = "suburb.controlsub";
		} else if (typ == "business") {

			var mn = "business.control";
		} else if (typ == "restricciones") {
			var mn = "configuration.restricciones";
			typ = "restriction";
		}
		app.exec("/" + typ + "/disable", {
			'data' : {
				'aid' : sid
			},
			'before' : function() {
				if (typ == "suburb") {
					if (confirm("¿Realmente deseas eliminar la dirección?")) {
						return true;
					}
				} else
					return true;

				return false;
			},
			'success' : function(d) {
				app2.load("/configuration/gettable/" + mn, "#mn-cont", {
					'done' : function() {

						deleteuser.off("click");
						initconfiguration();

					}
				});
			}
		});
	});

}

function sear() {

	jQuery(".searchsub").off("keyup");
	jQuery(".searchsub").on("keyup", function() {

		if (jQuery(this).val() != "") {
			jQuery(".tablecolonia tbody>tr").hide();
			jQuery(".tablecolonia td:contiene-palabra('" + jQuery(this).val() + "')").parent("tr").show();
		} else {
			jQuery(".tablecolonia tbody>tr").show();
		}
	});

	jQuery.extend(jQuery.expr[":"], {
		"contiene-palabra" : function(elem, i, match, array) {
			return (elem.textContent || elem.innerText || jQuery(elem).text() || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
		}
	});

}

function re() {
	initOcupacion();
	
}
function sumaocupacion(n) {
	console.log(n);
    jQuery("#rtotal"+n).on("change",function(){
		console.log("entro"+n);
		var sumaoc=0;
		var sumocant=parseInt(jQuery("#sumtotal").html());
		for (var i=1;i<=9;i++) {
            sumaoc=sumaoc+parseInt(jQuery("#rtotal"+i).val());
        }
		jQuery("#sumtotal").html(sumaoc);
		var dif=sumaoc-sumocant;
		jQuery("#sumlibres").html(parseInt(jQuery("#sumlibres").html())+dif);
	});
}
function restric(){

	jQuery(".btn-addrestr").off("click").on("click",function(){
		var dte=jQuery(".ocu-date").val();
		jQuery.ajax({
			url : "/ocupacion/creater/", //la url que requiere para enviar los datos
			data : {
				fecha:dte
			},
			type : "POST", //metodo por el cual se van a enviar los datos
			dataType : "json",
			success : function(source) {
				var fr = jQuery('.ocu-date').val();
				app2.load("/ocupacion/ocu/" + fr, "#ocupacion", {
					'done' : function() {
						
						re();
						
					}
				});	
			}
		});
	});

}
function initOcupacion() {
	restric();
	ocup();
	jQuery('.ocu-date').datetimepicker({
		timepicker : false,
		scrollInput : false,
		mask : false,
		format : "Y-m-d",
		lang : 'es',
		value : '',
		onSelectDate : function(c, i) {
			i.datetimepicker('destroy');
			var fr = jQuery('.ocu-date').val();
			
			app2.load("/ocupacion/ocu/" + fr, "#ocupacion", {
				'done' : function() {
					re();
					
				}
			});			
		}
	});
	jQuery("input[name='optionsl']").off("change");
	jQuery("input[name='optionsl']").on("change", function() {
		var ff = jQuery('.ocu-date').val();
		var fr = jQuery(this).val();
		app2.load("/ocupacion/cambio/l/" + fr + "/" + ff, "#contl", {
			'done' : function() {
				re();
				
			}
		});
	});
	jQuery("input[name='optionss']").off("change");
	jQuery("input[name='optionss']").on("change", function() {
		var ff = jQuery('.ocu-date').val();
		var fr = jQuery(this).val();

		app2.load("/ocupacion/cambio/s/" + fr + "/" + ff, "#conts", {
			'done' : function() {
				re();
			}
		});
	});
} 
function alertamenu() {
	
	jQuery.ajax({
		url : "/systemuser/getype/", //la url que requiere para enviar los datos
		data : {},
		type : "POST", //metodo por el cual se van a enviar los datos
		dataType : "json",
		success : function(source) {
			data = source;
			if (data == "ASIGNACIONES"|| data == "ADMINISTRADOR") {

				jQuery(".list-dis").fadeIn(500);
				jQuery(".list-dis").off("click");
				jQuery(".list-dis").on("click",function(){
					jQuery(this).fadeOut(1000);
					jQuery("a[href=#assignments]").trigger("click");
				});
			}
		}
	});
}
function ocup(){
	jQuery(".inptocu").off("change").on("change",function(){
		for(var i=1;i<=9;i++){
			var tot=jQuery("#rtotal"+i).val();
			var hechas=jQuery("#rhechas"+i).html();
			var hechas2=jQuery("#hchas"+i).val();
			var alt=jQuery(".inptocu[name='alt"+i+"']").val();
			var amx=jQuery(".inptocu[name='amx"+i+"']").val();
			var intr=jQuery(".inptocu[name='int"+i+"']").val();
			var vol=jQuery(".inptocu[name='vol"+i+"']").val();
			var extra=jQuery(".inptocu[name='extra"+i+"']").val();
			var thechas=parseInt(hechas2)+parseInt(alt)+parseInt(amx)+parseInt(intr)+parseInt(extra)+parseInt(vol);
			if(parseInt(hechas)<parseInt(tot) && thechas>parseInt(tot)){
				alert("ya sobrepaso el limite");
			}
			jQuery("#rhechas"+i).html(thechas);
			jQuery("#rextra"+i).html(tot-thechas);
			jQuery("#libres"+i).val(tot-thechas);

		}
	});
	sumaocupacion(1);
	sumaocupacion(2);
	sumaocupacion(3);
	sumaocupacion(4);
	sumaocupacion(5);
	sumaocupacion(6);
	sumaocupacion(7);
	sumaocupacion(8);
	sumaocupacion(9);
	jQuery(".btn-saveres").off("click").on("click",function(){
		var arr=[];
		jQuery(".inptocu").each(function(){
			arr[jQuery(this).attr("name")]=jQuery(this).val();
		});
			
			jQuery.ajax({
				url : "/ocupacion/save/", //la url que requiere para enviar los datos
				data :{
					id1:arr["id1"],
					id2:arr["id2"],
					id3:arr["id3"],
					id4:arr["id4"],
					id5:arr["id5"],
					id6:arr["id6"],
					id7:arr["id7"],
					id8:arr["id8"],
					id9:arr["id9"],
					alt1:arr["alt1"],
					alt2:arr["alt2"],
					alt3:arr["alt3"],
					alt4:arr["alt4"],
					alt5:arr["alt5"],
					alt6:arr["alt6"],
					alt7:arr["alt7"],
					alt8:arr["alt8"],
					alt9:arr["alt9"],
					amx1:arr["amx1"],
					amx2:arr["amx2"],
					amx3:arr["amx3"],
					amx4:arr["amx4"],
					amx5:arr["amx5"],
					amx6:arr["amx6"],
					amx7:arr["amx7"],
					amx8:arr["amx8"],
					amx9:arr["amx9"],
					int1:arr["int1"],
					int2:arr["int2"],
					int3:arr["int3"],
					int4:arr["int4"],
					int5:arr["int5"],
					int6:arr["int6"],
					int7:arr["int7"],
					int8:arr["int8"],
					int9:arr["int9"],
					vol1:arr["vol1"],
					vol2:arr["vol2"],
					vol3:arr["vol3"],
					vol4:arr["vol4"],
					vol5:arr["vol5"],
					vol6:arr["vol6"],
					vol7:arr["vol7"],
					vol8:arr["vol8"],
					vol9:arr["vol9"],
					extra1:arr["extra1"],
					extra2:arr["extra2"],
					extra3:arr["extra3"],
					extra4:arr["extra4"],
					extra5:arr["extra5"],
					extra6:arr["extra6"],
					extra7:arr["extra7"],
					extra8:arr["extra8"],
					extra9:arr["extra9"],
					cambio1:arr["cambio1"],
					cambio2:arr["cambio2"],
					cambio3:arr["cambio3"],
					cambio4:arr["cambio4"],
					cambio5:arr["cambio5"],
					cambio6:arr["cambio6"],
					cambio7:arr["cambio7"],
					cambio8:arr["cambio8"],
					cambio9:arr["cambio9"],
					libres1:arr["libres1"],
					libres2:arr["libres2"],
					libres3:arr["libres3"],
					libres4:arr["libres4"],
					libres5:arr["libres5"],
					libres6:arr["libres6"],
					libres7:arr["libres7"],
					libres8:arr["libres8"],
					libres9:arr["libres9"]
				},
				type : "POST", //metodo por el cual se van a enviar los datos
				dataType : "json",
				success : function(source) {
					alert("¡Ha Guardado con exito!");
				}
			});
		});
	}