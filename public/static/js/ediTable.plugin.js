(function ( $ ) {
    $.fn.ediTable = function(opt) {
        var options = {
            'model': null,
            'template': null
        };
        for(o in opt) {
            options[o] = opt[o];
        }
        return this.each(function() {
            var editable = jQuery(this);
            var btnEdit = editable.find(".btnEdit");
            var raw = editable.find(".raw");
            var fields = editable.find(".fields");
            btnEdit.on("click", function() {
                if(jQuery(this).hasClass("editing")){
                    if(promt("¿Deseas guardar los cambios?")){
                        options.model.publish();
                    }
                    jQuery(this).removeClass("glyphicon-ok").removeClass("editing").addClass("glyphicon-pencil");
                    raw.fadeIn();
                    fields.fadeOut();
                } else {
                    jQuery(this).removeClass("glyphicon-pencil").addClass("editing").addClass("glyphicon-ok")
                    raw.fadeOut();
                    fields.fadeIn();
                }
                    
            });
        });
    };
}( jQuery ));