var SimpleModel = function(p) {
	this.properties = {
		'url': undefined,
		'type': undefined,
		'actions': {
			'publish': 'publish',
			'delete': undefined,
			'masivePublish': 'masivePublish'
		},
		'callbacks': {
			'beforePublish': undefined,
			'afterPublish': undefined,
			'beforeDelete': undefined,
			'afterDelete': undefined,
			'afterMasivePublish': undefined,
			'beforeMasivePublish': undefined,
                        'onCreate': undefined,
                        'onPublishing': undefined
		},
		'handlers': {
			'errorHandler': undefined,
			'warningHandler': undefined,
			'dataHandler': undefined,
			'domHandler': undefined,
			'validationHandler': undefined
		},
		'fields': undefined,
		'autopublish': true,
		'autovalidate': true,
		'validator': 'default'
	};
	this.instances = {
		'length': 0,
		'objects': {}
	};
	this.masivePublish = function(ommit) {
		var ins = this.instances.objects;
		var methods = this.properties.callbacks;
		var data = {};
		var self = this;
		if( jQuery.isFunction(methods.beforeMasivePublish) ) {
			var bmp = methods.before(ins);
			if( !bmp ) {
				if( jQuery.isFunction(handlers.errorHandler) ) {
					handlers.errorHandler("ERROR_BEFORE_PUBLISH");
					return;
				}
			}
		}
                var its;
		for(its in ins) {
                        if(ommit.indexOf(its) !== -1){ continue};
                        var v = ins[its].validate();
			if(v.errors === 0) {
				data[ins[its].id] = (ins[its].data);
			} else {
                                if(this.properties.handlers.errorHandler !== undefined) {
                                        this.properties.handlers.errorHandler("ERROR_ON_MASIVEPUBLISH", ins[its]);
                                }
				return;
			}
		}
                if(jQuery.isFunction(this.properties.callbacks.onPublishing)) {
                    this.properties.callbacks.onPublishing("masive");
                }
		jQuery.post(
			this.properties.url+'/'+this.properties.actions.masivePublish,
			data,
			"json"
		).done(function(data) {
                        if(!jQuery.isPlainObject(data)) {
                            data = jQuery.parseJSON( data );
                        }
                        var d;
			for(d in data) {
				if(data[d]!==null){
					if(self.instances.objects[d]!== undefined && (data[d].id !== undefined || data[d].id !== '')) {
						self.instances.objects[d].data.id = data[d].id;
					}
					if(data[d].error !== undefined) {
						if( jQuery.isFunction(handlers.errorHandler) ) {
							handlers.errorHandler("ERROR_INSTANCE_MASIVEPUBLISH", data[d].error);
						}
					}
				}
			}
			if( jQuery.isFunction(methods.afterMasivePublish)){
				var amp = methods.afterMasivePublish(data, self.instances);
				if( !amp ) {
					if( jQuery.isFunction(handlers.errorHandler) ) {
						handlers.errorHandler("ERROR_AFTER_MASIVEPUBLISH");
						return;
					}
				}
			}
		})
		.fail(function(data) {
			if( jQuery.isFunction(handlers.errorHandler) ) {
				handlers.errorHandler("MASIVEPUBLISH_FAIL", data);
			}
		});
	};
	this.validator = function(fields, domObjects) {
		var r = {
			'errors': 0,
			'warnings': 0
		};
		var validators = this.properties.validator.split("|");
		if(validators.indexOf("default") !== -1) {
			//Validation base
			for(i in domObjects) {
				var o = jQuery(domObjects[i]);
				var ref = o;
				if(this.properties.fields[o.attr("name")].reference !== undefined) {
					ref = o.parent().find(this.properties.fields[o.attr("name")].reference);
				}
				var v = fields[o.attr("name")];
				if( ref.hasClass("border-error") ) { ref.removeClass("border-error"); }
				if( ref.hasClass("border-warning") ) { ref.removeClass("border-warning"); }
				if( this.properties.fields[o.attr("name")] === undefined)
					continue;
				if( this.properties.fields[o.attr("name")].required !== undefined ) {
					if( v === '' || v === undefined) {
						ref.addClass("border-error");
						if( jQuery.isFunction(this.properties.handlers.errorHandler) ) {
								this.properties.handlers.errorHandler("ERROR_DOMFIELD_VALIDATION", o);
						}
						r.errors++;
						continue
					}
				}
				if( this.properties.fields[o.attr("name")].type !== undefined ) {
					if( typeOf(v) !== this.properties.fields[o.attr("name")].type ) {
						ref.addClass("border-warning");
						r.warnings++;
						continue;
					}
				}
			}
		}
		if( (validators.indexOf("handler") !== -1) && (jQuery.isFunction(this.properties.handlers.validationHandler)) ) {
			var hr = this.properties.handlers.validationHandler(fields, domObjects, this.properties.fields);
		}
		if(hr !== undefined) {
			if(hr.errors > 0) {
				r.errors += hr.errors;
			}
			if(hr.warnings > 0) {
				r.warnings += hr.warnings;
			}
		}
		return r;
	};
	for(var k in p) {
		if( jQuery.isPlainObject(this.properties[k])) {
			for(var sk in p[k]) {
				this.properties[k][sk] = p[k][sk];
			}
		} else {
			this.properties[k] = p[k];
		}
	}

	this.create = function(data) {
		var parent = this;
		var methods = parent.properties.callbacks;
		var handlers = parent.properties.handlers;
		var actions = parent.properties.actions;
		var d = new Date();
		var id = 0;
		do {
			id = parseInt((((d.getTime()/100000) - parseInt((d.getTime()/100000)))*100000)*(parseInt(Math.random()*10)));
		} while(id === 0);
		/*Instance, this will be returned to manage the new object that belongs to the model*/
		var Instance = {
			'id': id,
			'type': (parent.properties.type !== undefined) ? parent.properties.type:'generic',
			'data': {},
			'fields': [],
			'validate': function() {
				return parent.validator(this.data, this.fields);
			},
			'publish': function() {
				if( jQuery.isFunction(methods.beforePublish) ) {
					var bp = methods.before(this.data, this.fields, parent.properties.fields);
					if( !bp ) {
						if( jQuery.isFunction(handlers.errorHandler) ) {
							handlers.errorHandler("ERROR_BEFORE_PUBLISH");
							return;
						}
					}
				}
				if(parent.properties.autovalidate) {
					var r = this.validate();
					if(r.errors > 0) {
						if( jQuery.isFunction(handlers.errorHandler) ) {
							handlers.errorHandler("ERROR_ON_VALIDATION");
							return;
						}
					}
					if(r.warnings > 0) {
						if( jQuery.isFunction(handlers.warningHandler) ) {
							handlers.warningHandler("WARNING_ON_VALIDATION");
							return;
						}
					}
				}
				var self = this;
                                if(jQuery.isFunction(parent.properties.callbacks.onPublishing)) {
                                    parent.properties.callbacks.onPublishing("instance");
                                }
				jQuery.post(
					parent.properties.url+'/'+actions.publish,
					self.data,
					"json"
				).done(function(data) {
                                        if(!jQuery.isPlainObject(data)) {
                                            data = jQuery.parseJSON( data );
                                        }
					if( data.id !== undefined && data.id !== '' ) {
						self.data.id = data.id;
					}
                                        if(data.error !== undefined) {
                                            if( jQuery.isFunction(handlers.errorHandler) ) {
                                                handlers.errorHandler("ERROR_ON_PUBLISH_RESPONSE", data.error);
                                            }
                                        }
					if( jQuery.isFunction(methods.afterPublish)){
						var ap = methods.afterPublish(data, self.data, self.fields);
						if( !ap ) {
							if( jQuery.isFunction(handlers.errorHandler) ) {
								handlers.errorHandler("ERROR_AFTER_PUBLISH");
							}
						}
					}
				})
				.fail(function(data) {
					if( jQuery.isFunction(handlers.errorHandler) ) {
						handlers.errorHandler("PUBLISH_FAIL", data);
					}
				});
			},
			'delete': function() {

			}
		};
		data.each(function () {
			var input = jQuery(this);
			input.attr("model-id", id);
			if( input.hasClass('ommit') ) return;
			if( parent.properties.fields[input.attr("name")] !== undefined ) {
				var onchange = function() {
					Instance.data[jQuery(this).attr('name')] = this.value;
					if( parent.properties.autopublish ) {
						Instance.publish();
					}
				};
				if( input.attr('type') === "checkbox" || input.attr('type') === "radio" ) {
					var group = jQuery("input[name='"+input.attr('name')+"']");
					var i = 0;
					input = jQuery('<input type="hidden" name="'+input.attr('name')+'" value="" />');
					input.insertBefore(group);
					input.change( onchange );
					var dataProccessor = function(i) {
						i.val('');
						var j = 0;
						for( var v in i.data() ) {
							var s = ',';
							if(j === 0)
								s = '';
							i.val(i.val()+s+i.data()[v]);
						}
						i.trigger('change');
					};
					group.each(function() {
						var special = jQuery(this);
						if(this.checked) {
							input.data( special.attr('name'), this.value );
						}
						special.addClass('ommit').attr('name', special.attr('name')+i).change(function() {
							if( this.checked ) {
								input.data( special.attr('name'), this.value );
							} else {
								input.removeData( special.attr('name') );
							}
							dataProccessor(input);
						});
						if( special.attr('type') === 'checkbox' ) {
							i++;
						}
					});
					dataProccessor(input);
				} else {
					Instance.data[input.attr("name")] = input.attr("value");
					if( input.attr("value") === '' || input.attr("value") === undefined ) {
						if( parent.properties.fields[input.attr("name")].default !== undefined ) {
							Instance.data[input.attr("name")] = parent.properties.fields[input.attr("name")].default;
						}
					}
					input.change( onchange );
				}
				Instance.fields.push(input);
			}
		});
		this.instances.length++;
		this.instances.objects[Instance.id] = Instance;
                if(jQuery.isFunction(this.properties.callbacks.onCreate)) {
                    this.properties.callbacks.onCreate(Instance);
                }
		return Instance;
	};

};

var User = new SimpleModel(
        {
                'url': '/user',
                'type': 'user',
                'callbacks': {
                        'afterPublish': function(data, sdata, sfields) {
                            if(data.id !== undefined && parseInt(data.id) !== -1) {

                      	        if(app.currentUser == -1){
                      	        	jQuery('.user-save').css('display','none');
                      	        	jQuery('.tbdirec').css('display','table');
            						jQuery('.create-reser').css('display','inline');
                      	        	app.currentUser = data.id;
                          		  	jQuery('.create-reser').on("click",function(){
                          		  		jQuery.ajax({
                          		  			url:"/user/validdireccion/"+app.currentUser,
                          		  			 type: "POST",
							                dataType: "json",
                                            "contentType": 'application/x-www-form-urlencoded; charset=UTF-8',
							                success: function (source) {
							                    data = source;
							                   if(data){
							                   	alert("falta agregar direccion");
							                   }else{
												jQuery(".id-usern").attr("value",app.currentUser);
												jQuery(".id-usern").val(app.currentUser);
												jQuery(".id-usern").trigger("change");
											   }
							                },
							                error: function (dato) {

							                }
                          		  		});
                          		  	});
                  		  	    	var dirs={};
									var c=0;
							    	jQuery(".sd").each(function(){
										dirs[c]=jQuery(this).data("addreess");
										c++;
									});

		                  		  		/*jQuery.ajax({
							              	url: "/user/direcciones",
							                data: {'ids': dirs,
						                    	'user':app.currentUser},
							                type: "POST",
							                dataType: "json",
                                            "contentType": 'application/x-www-form-urlencoded; charset=UTF-8',
							                success: function (source) {
							                    data = source;
							                   console.log(data);

							                },
							                error: function (dato) {

							                }
							            });*/
            						//initAddressBlock();



                				}else{

                				}

                            }

                            return true;
                        },
                        'onCreate': function(instance) {
                            app.currentUser = instance.data.id;
                            console.log(app.currentUser);


                        }
                },
                'handlers': {
                        'errorHandler': function(error, data) {
                            switch(error) {
                                case 'ERROR_ON_PUBLISH_RESPONSE': (function(error) {
                                        alert("Hubo un error al intentar crear el usuario: "+error);
                                })(data);
                            }
                        }
                },
                'fields': {
                        'id': {'required': true},
                        'email': {},
                        'name': {'required': true},
                        'distinguished': {},
                        'annotations': {'default':' '},
                        'telephones': {},
                        'business_id':{default:"5529"},
                        'benefit_id': {},
                        'agreement': {default:"SC"}
                },
                'autopublish': false
        }
);
var Business = new SimpleModel(
    {
            'url': "/business",
            'type': "business",
            'callbacks': {
                    'afterPublish': function() {
                        return true;
                        alert("listo");
                    }
            },
            'fields': {
                'id': {'required': true},
                'name': {'required': true},
                'email':{},
                'telephones':{},
                'agreements_bus':{},
                'annotations':{},
                'username':{},
                'password':{}
                /*fin*/
            },
            'autopublish': false
    }
);
var Address = new SimpleModel(
    {
            'url': "/address",
            'type': "address",
            'callbacks': {
                    'afterPublish': function() {

                        if(jQuery('.sd').length < 1){
                            jQuery(".panel-service").removeClass("showing").addClass("hide").fadeOut();
                        } else {
                            if(!jQuery(".panel-service").addClass("showing")) {
                                jQuery(".panel-service").removeClass("hide").addClass("showing").fadeIn();


                            }
                        }
                        app2.load("/address/getBase/"+app.currentUser,"#addresses", {
							'done': function () {
	        					initAddressBlock();


   							 }
   						});
                        return true;
                    }
            },
            'fields': {
                'id': {'required': true},
                'alias': {},
                'user_id': {'required': true},
                'street': {'required': true},
                'between': {},
                'suburb_id': {'required': true},
                'reference': {},
                /*chuy 26-03-14*/
                'quadrant': {},
                'telephones':{},
                'status':{'default':1}
                /*fin*/
            },
            'autopublish': false
    }
);

var upReservation = new SimpleModel(
    {
            'url': "/upreservation",
            'type': "upreservation",
            'handlers': {
                    'errorHandler': function(error, instance) {
                        if(error === "ERROR_DOMFIELD_VALIDATION") {
                            //alert(error+"-----"+instance.attr("name"));
                        } else {
                            //alert(error+"-----"+instance.id);
                        }

                    },
                    'validationHandler': function(data, fields, pfields) {
                        var i = 0;
                        if(jQuery("#ui_notifIt").length > 0){
                            jQuery("#ui_notifIt").fadeOut().remove();
                        }
                        var msg = "Los siguientes campos son requeridos <br>";
                        for(fi in data) {
                            if((data[fi] === undefined || data[fi] === "") && pfields[fi].required) {
                                //Los siguientes campos son obligatorios
                                if(pfields[fi].alias !== undefined) {
                                	if(pfields[fi].alias=="Forma de pago"){
                                		jQuery("#editar").find(".service_field[name='payment']").addClass("border-error");
                                	}else{
                                		jQuery("#editar").find(".service_field[name='payment']").removeClass("border-error");
                                	}
                                    msg += pfields[fi].alias + "<br>";
                                } else {
                                    msg += fi + "<br>";
                                }
                                i++;
                            }
                        }
                        if(i > 0) {
                            notif({
                                type: "error",
                                msg: msg,
                                position: "right",
                                opacity: 0.8,
                                multiline: true,
                                autohide: true,
                                time: 1000,
                                bgcolor: "#D9534F"
                            });
                            jQuery('#addreess-modal').modal('hide');
                        }
                        return {'errors': i};
                    }
            },
            'callbacks': {
                'onPublishing': function() {
                    jQuery('#serviceModal').modal({'show':true, 'backdrop':'static'});
                },
                'error': function(){
                	console.log("error");
                },
                'afterMasivePublish': function(data) {
                	/*
                    var ids = [];
                    for(d in data) {
                        ids.push(data[d].id);
                    }

                    var serviceData = jQuery("#editar .service_field");

                    var idser=jQuery("#editar .service_field[name='id']").val();
                    var serviceu=Service.create(serviceData);
                        serviceu.data.id=idser;
                        serviceu.data.annotations=jQuery("#editar #user-annotations").val();
                        serviceu.data.cost=jQuery("#editar #total-manual").val();
                        serviceu.data.payment=jQuery("#editar .ejpagar").val();

                        serviceu.data.user_id = app.currentUser;

                        serviceu.data.reservation1 = ids[0];



                        if(ids[1] !== undefined) {
                        	serviceu.data.reservation2 = ids[1];

                        }else{
                        	serviceu.data.reservation2 = null;
                        }



						  jQuery.ajax({

			                url: "/service/fec/"+serviceu.data.id,
			                data: {},
			                type: "POST",
			                dataType: "json",
		                        "contentType": 'application/x-www-form-urlencoded; charset=UTF-8',
			                success: function (source) {

			                },
			                error: function (dato) {

			                }
			            });

					   for(s in Service.instances.objects) {
                            Service.instances.objects[s].publish();
                       }
                        */

                    notifedituser(data);
                    jQuery(".bdg-stp1").css("background-color", "#5CB85C");
                    alert("cambios realizados");

                    return true;

                }
            },
            'fields': {
                'id1': {'default':''},
                'addresses1': {'required': true, 'alias': "Dirección", 'reference':'.add-ac'},
                'vehicle_id1': {'required': true, 'default':1},
                'passengers1': {'required': true, 'default':1},
                'type1':{'required': true},
                'terminal1': {'default':null},
                'cost1':{},
                'airline_id1': {},
                'flight_number1': {},
                'flight_hour1': {},
                'flight_type1': {'default':null},
                'reservation_date1': {'required': true, 'alias':"Fecha del servicio", 'reference':'input[name=reservation_date]'},
                'reservation_time1': {'required': true, 'alias':"Hora del servicio"},
                'payment':{'required':true, 'alias':'Forma de pago'},
                'especial':{'default':0},
                'id2': {'default':''},
                'addresses2': {'required': true, 'alias': "Dirección", 'reference':'.add-ac'},
                'vehicle_id2': {'required': true, 'default':1},
                'passengers2': {'required': true, 'default':1},
                'type2':{'required': true},
                'terminal2': {'default':null},
                'cost2':{},
                'airline_id2': {},
                'flight_number2': {},
                'flight_hour2': {},
                'flight_type2': {'default':null},
                'reservation_date2': {'required': true, 'alias':"Fecha del servicio", 'reference':'input[name=reservation_date]'},
                'reservation_time2': {'required': true, 'alias':"Hora del servicio"},
                'tipservice':{},
                 'requested': {},
                'service_id': {'default':''},
		        'user_id': {'default':''},
		        'annotations': {},
                'cost':{},
                'tnueva':{},


            },
            'autopublish': false,
            'validator': 'default|handler'
    }
);

var Reservation = new SimpleModel(
    {
            'url': "/reservation",
            'type': "reservation",
            'handlers': {
                    'errorHandler': function(error, instance) {
                        if(error === "ERROR_DOMFIELD_VALIDATION") {
                            //alert(error+"-----"+instance.attr("name"));
                        } else {
                            //alert(error+"-----"+instance.id);
                        }

                    },
                    'validationHandler': function(data, fields, pfields) {
                        var i = 0;
                        if(jQuery("#ui_notifIt").length > 0){
                            jQuery("#ui_notifIt").fadeOut().remove();
                        }
                        var msg = "Los siguientes campos son requeridos <br>";

                        for(fi in data) {
                            if((data[fi] === undefined || data[fi] === "") && pfields[fi].required) {
                                //Los siguientes campos son obligatorios

                                if(pfields[fi].alias !== undefined) {
                                	if(pfields[fi].alias=="Forma de pago"){
                                		jQuery("#nueva").find(".service_field[name='payment']").addClass("border-error");
                                	}else{
                                		jQuery("#nueva").find(".service_field[name='payment']").removeClass("border-error");
                                	}
                                    msg += pfields[fi].alias + "<br>";
                                } else {
                                    msg += fi + "<br>";
                                }
                                i++;
                            }
                        }
                        if(i > 0) {
                            notif({
                                type: "error",
                                msg: msg,
                                position: "right",
                                opacity: 0.8,
                                multiline: true,
                                autohide: true,
                                time: 1000,
                                bgcolor: "#D9534F"
                            });
                            jQuery('#addreess-modal').modal('hide');
                        }
                        return {'errors': i};
                    }
            },
            'callbacks': {
                'onPublishing': function() {
                    jQuery('#serviceModal').modal({'show':true,'keyboard':false, 'backdrop':'static'});
                },
                'error': function(){
                	console.log("error");
                	jQuery("#serviceModal").modal("hide");

                },
                'afterMasivePublish': function(data) {
                    jQuery(".bdg-stp1").css("background-color", "#5CB85C");
                    notifuser(data);
                    return true;

                }
            },
            'fields': {
                'id1': {'default':''},
                'addresses1': {'required': true, 'alias': "Dirección", 'reference':'.add-ac'},
                'vehicle_id1': {'required': true, 'default':1},
                'passengers1': {'required': true, 'default':1},
                'type1':{'required': true},
                'terminal1': {'default':null},
                'cost1':{},
                'airline_id1': {},
                'flight_number1': {},
                'flight_hour1': {},
                'flight_type1': {'default':null},
                'reservation_date1': {'required': true, 'alias':"Fecha del servicio", 'reference':'input[name=reservation_date]'},
                'reservation_time1': {'required': true, 'alias':"Hora del servicio"},
                'payment':{'required':true, 'alias':'Forma de pago'},
                'especial':{'default':0},
                'id2': {'default':''},
                'addresses2': {'required': true, 'alias': "Dirección", 'reference':'.add-ac'},
                'vehicle_id2': {'required': true, 'default':1},
                'passengers2': {'required': true, 'default':1},
                'type2':{'required': true},
                'terminal2': {'default':null},
                'cost2':{},
                'airline_id2': {},
                'flight_number2': {},
                'flight_hour2': {},
                'flight_type2': {'default':null},
                'reservation_date2': {'required': true, 'alias':"Fecha del servicio", 'reference':'input[name=reservation_date]'},
                'reservation_time2': {'required': true, 'alias':"Hora del servicio"},
                'tipservice':{},
                 'requested': {},
                'service_id': {'default':''},
		        'user_id': {'default':''},
		        'annotations': {},
                'cost':{},
                'tnueva':{},
            },
            'autopublish': false,
            'validator': 'default|handler'
    }
);
function notifuser(data){
		var btn = jQuery('#serviceModal').find('.finish-btn');
        var tck = jQuery('#serviceModal').find('.imprimir');
        var txt = jQuery(".service_text");
        var sCont = jQuery(".service_id_container");
        var s_id = jQuery(".service_id");
        var crSer = jQuery(".creating-service");
        jQuery(".bdg-stp2").css("background-color", "#5CB85C");
        crSer.hide();
        s_id.html("#"+data.service_id);
        txt.fadeIn();
        sCont.fadeIn();
        btn.fadeIn();
		jQuery(".btn-again").remove();
         var url="/service/correo/"+data.service_id+"/"+0;
  		  jQuery.ajax({
            type: "POST",
            dataType:'json',
            url: url,
            "contentType": 'application/x-www-form-urlencoded; charset=UTF-8',
            data:{}
   		 });
   		  var bene =jQuery("#nueva .selectben").val();
		jQuery.ajax({

                url: "/service/revenues/"+bene+"/"+app.currentUser+"/"+data.service_id,
                data: {},
                type: "POST",
                dataType: "json",
                    "contentType": 'application/x-www-form-urlencoded; charset=UTF-8',
                success: function (source) {
                	  var data =source;
                	  var dis=data["d"],acu=data['a'],usa=data['u'];
                	  jQuery(".benusa").html(usa);
                	  jQuery(".bendis").html(dis);
                	  jQuery(".benacu").html(acu);
                	  jQuery(".cservices").html((parseInt(jQuery(".cservices").html())+1));
                	  /*app2.load("/service/t/"+app.currentUser+"/service.list","#listser .modal-body",{
						'done':function(){


						}
					});*/
                },
                error: function (dato) {
                   console.log(dato);
                }
            });
            jQuery.ajax({
                url: "/service/service/"+data.service_id,
                data: {},
                type: "POST",
                dataType: "json",
                    "contentType": 'application/x-www-form-urlencoded; charset=UTF-8',
                success: function (source) {
                    data = source;
                    showimpr(data);

                },
                error: function (dato) {
                   console.log("crear service");
                }
            });
}
function notifedituser(data){
			jQuery("#editar .reservation_field[name='id1']").val(data.reservation1).trigger("change");
			jQuery("#editar .reservation_field[name='id2']").val(data.reservation2).trigger("change");
			var id_data=jQuery("#editar .reservation_field[name='service_id']").val();
            	  /*jQuery.ajax({
	                url: "/service/fec/"+id_data,
	                data: {},
	                type: "POST",
	                dataType: "json",
	                "contentType": 'application/x-www-form-urlencoded; charset=UTF-8',
	                success: function (source) {
	                },
	                error: function (dato) {

	                }
	            });*/

            	var tck = jQuery('#serviceModal').find('.imprimir');

                var s_id = jQuery(".service_id");
				s_id.html("#"+id_data);
				jQuery(".btn-again").remove();
            	jQuery(".bdg-stp2").css("background-color", "#5CB85C");
            	 var btn = jQuery('#serviceModal').find('.finish-btn');
            	 btn.fadeIn();
            	 s_id.html(id_data);
            	 s_id.fadeIn();

	            jQuery.ajax({
	              	url: "/service/service/"+id_data,
	                data: {},
	                type: "POST",
	                dataType: "json",
                        "contentType": 'application/x-www-form-urlencoded; charset=UTF-8',
	                success: function (source) {
	                    data = source;
	                    showimpr(data);

	                },
	                error: function (dato) {
	                   console.log("crear service");
	                }
	            });
	            var edi=jQuery("#editar");
}

var Service = new SimpleModel({
    'url': "/reservation",
    'type': "service",
    'actions': {
        'publish': 'service'
    },
    'callbacks': {
		'error': function(){

            	jQuery("#serviceModal").modal("hide");

        },
        'afterPublish': function(data) {

            if(data.service_id !== undefined) {
            	console.log("crear");
                var btn = jQuery('#serviceModal').find('.finish-btn');
                var tck = jQuery('#serviceModal').find('.imprimir');
                var txt = jQuery(".service_text");
                var sCont = jQuery(".service_id_container");
                var s_id = jQuery(".service_id");
                var crSer = jQuery(".creating-service");
				jQuery(".btn-again").remove();
                jQuery(".bdg-stp2").css("background-color", "#5CB85C");
                crSer.hide();
                s_id.html("#"+data.service_id);
                txt.fadeIn();
                sCont.fadeIn();
                btn.fadeIn();
                /*jQuery.ajax({
	                url: "/service/fec/"+data.service_id,
	                data: {},
	                type: "POST",
	                dataType: "json",
	                "contentType": 'application/x-www-form-urlencoded; charset=UTF-8',
	                success: function (source) {
	                },
	                error: function (dato) {

	                }
	            }); */
                /*jQuery.ajax({
	                url: "/prueba/fec/"+data.service_id,
	                data: {},
	                type: "POST",
	                dataType: "json",
	                "contentType": 'application/x-www-form-urlencoded; charset=UTF-8',
	                success: function (source) {



	                },
	                error: function (dato) {

	                }
            	});  */
                jQuery.ajax({

	                url: "/service/service/"+data.service_id,
	                data: {},
	                type: "POST",
	                dataType: "json",
                        "contentType": 'application/x-www-form-urlencoded; charset=UTF-8',
	                success: function (source) {
	                    data = source;
	                    showimpr(data);

	                },
	                error: function (dato) {
	                   console.log("crear service");
	                }
	            });

            }else{
            	var id_data=jQuery("#editar .service_field[name='id']").val();
            	 /* jQuery.ajax({
	                url: "/service/fec/"+id_data,
	                data: {},
	                type: "POST",
	                dataType: "json",
	                "contentType": 'application/x-www-form-urlencoded; charset=UTF-8',
	                success: function (source) {
	                },
	                error: function (dato) {

	                }
	            }); */

            	var tck = jQuery('#serviceModal').find('.imprimir');

                var s_id = jQuery(".service_id");
				s_id.html("#"+id_data);
				jQuery(".btn-again").remove();

            	jQuery(".bdg-stp2").css("background-color", "#5CB85C");
            	 var btn = jQuery('#serviceModal').find('.finish-btn');
            	 btn.fadeIn();
            	 s_id.html(id_data);
            	 s_id.fadeIn();

	            jQuery.ajax({
	              	url: "/service/service/"+id_data,
	                data: {},
	                type: "POST",
	                dataType: "json",
                        "contentType": 'application/x-www-form-urlencoded; charset=UTF-8',
	                success: function (source) {
	                    data = source;
	                    showimpr(data);

	                },
	                error: function (dato) {
	                   console.log("crear service");
	                }
	            });
	            var edi=jQuery("#editar");

            }
        }

    },
    'fields': {
        'id': {'default':''},
        'user_id': {'required': true, 'default':''},
        'reservation1': {'required': true},
        'reservation2': {'required': true},
        'requested': {'required': true},
        'payment':{'required':true},
        'annotations': {},
        'especial':{'default':0},
        'cost':{},
        'tnueva':{},
    },
    'autopublish': false
});

function showimpr(data) {
	var tck = jQuery('#serviceModal').find('.imprimir');
    var ser_id = data['ser_id'],
    	reservation1 = data['reservation1_id'],
        reservation2 = data['reservation2_id'],
        tipo1 = data['reservation1_type'],
        tipo2 = data['reservation2_type'],
        ad1=data['reservation1_direcc1'],
        ad2=data['reservation2_direcc2'];
    if (reservation2 ==="vacio") {
    	var opci="<button href='#' data-href='http://resv.goldenmty.com/assignment/imp2/"+reservation1+"'  type='button' class='btn btn-success btn-imprimir'>imprimir "+tipo1+"</button>";
    	   }else{
    	var opci="<button href='#' data-href='http://resv.goldenmty.com/assignment/imp2/"+reservation1+"'  type='button' class='btn btn-success btn-imprimir'>imprimir "+tipo1+"</button><button href='#' data-href='http://resv.goldenmty.com/assignment/imp2/"+reservation2+"' type='button' class='btn btn-success btn-imprimir'>imprimir "+tipo2+"</button><button data-href='"+reservation1+","+reservation2+"' class='btn btn-success btn-ambos'>imprimir ambos</button>";
    }
   tck.html(opci);
   tck.fadeIn();
   jQuery(".btn-imprimir").on("click",function(){
   		var href=jQuery(this).data('href');
   		window.open(href,'Ticket',"width=350, height=400"); //MODIFICA TAMAÑO DE VENTANA
   });
   jQuery(".btn-ambos").on("click",function(){

   		var href=jQuery(this).data('href');
   		var res=href.split(",");
   		var url1="http://resv.goldenmty.com/assignment/imp2/"+res[0];
   		var url2="http://resv.goldenmty.com/assignment/imp2/"+res[1];
   		window.open(url1,'Ticket1',"width=350, height=400"); //tabm
   		window.open(url2,'Ticket2',"width=350, height=400");  //modf 350 800 en todo

   });

}
var Systemuser = new SimpleModel(
        {
                'url': '/systemuser',
                'type': 'systemuser',
                'callbacks': {
                        'afterPublish': function(data, sdata, sfields) {
							alert("se ha generado la peticion correctamente");
							window.location="http://resv.goldenmty.com";
                        },
                        'onCreate': function(instance) {

                        }
                },
               'handlers': {
                        'errorHandler': function(error, data) {
                            switch(error) {
                                case 'ERROR_ON_PUBLISH_RESPONSE': (function(error) {
                                        alert("Hubo un error al intentar crear el usuario: "+error);
                                })(data);
                            }
                        }
                },
            'fields': {
                'id': {'default':-1},
                'name': {'alias': "nombre"},
                'lastname': { 'alias': "apellido"},
                'username': {'alias' : "nombre de usuario"},
                'address':{},
                'suburb': {},
                'telephones':{},
                'group': {'default':'ADMINISTRADOR'},
                'email': {'default':'s/correo'},
                'password': {},
                'password2': {},
                'turn': {'default':'MIXTO'}
            },
            'autopublish': false
	    }
	);
	var Unity = new SimpleModel(
        {
                'url': '/unity',
                'type': 'unity',
                'callbacks': {
                        'afterPublish': function(data, sdata, sfields) {

                        },
                        'onCreate': function(instance) {

                        }
                },
                'handlers': {
                        'errorHandler': function(error, data) {
                            switch(error) {
                                case 'ERROR_ON_PUBLISH_RESPONSE': (function(error) {
                                        alert("Hubo un error al intentar registrar la unidad: "+error);
                                })(data);
                            }
                        }
                },
            'fields': {
                'id': {'required':true},
                'type': {'default':'AUTOMOVIL','alias': "tipo de auto",'required':true},
                'serial_number': {'required':true},
                'license_plate': {},
                'color':{},
                'economic':{'required':true},
                'insurance_policy': {'required':true},
                'expiration_date':{},
                'driver': {},
                'brand':{},
				'model':{},
				'phone_id':{},
                'year':{'required':true},
            },
            'autopublish': false
    }
);
var Zone = new SimpleModel(
        {
                'url': '/zone',
                'type': 'zone',
                'callbacks': {
                        'afterPublish': function(data, sdata, sfields) {

                        },
                        'onCreate': function(instance) {

                        }
                },
                'handlers': {
                        'errorHandler': function(error, data) {
                            switch(error) {
                                case 'ERROR_ON_PUBLISH_RESPONSE': (function(error) {
                                        alert("Hubo un error al intentar registrar la zona: "+error);
                                })(data);
                            }
                        }
                },
            'fields': {
                'id': {'default':-1},
                'name': {'required': true, 'alias': "zona"},
                'cost_car': {},
                'des_car': {},
                'cost_sub': {'deafult':0},
                'cost_van':{},
                'des_van':{},
                'cost_car2': {},
                'des_car2': {},
                'cost_van2':{},
                'des_van2':{},
                'cdapto_auto':{},
                'aptocd_auto':{},
                'promo':{},
                'cdapto_cam':{},
                'aptocd_cam':{},
                'empresa_auto':{},
                'empresa_cam':{},
                'zipcode': {},
                'area' : {'default':0}
            },
            'autopublish': false
    }
);


var Operator = new SimpleModel(
        {
                'url': '/operator',
                'type': 'operator',
                'callbacks': {
                        'afterPublish': function(data, sdata, sfields) {
							console.log(data);
                        },
                        'onCreate': function(instance) {

                        }
                },
                'handlers': {
                        'errorHandler': function(error, data) {
                            switch(error) {
                                case 'ERROR_ON_PUBLISH_RESPONSE': (function(error) {
                                        alert("Hubo un error al intentar registrar la zona: "+error);
                                })(data);
                            }
                        }
                },
            'fields': {
                'id': {'default':-1},
                'unity': {},
                'name': {'required':true},
                'lastname': {'required':true},
                'secondlastname':{},
                'username': {'required':true},
                'email':{},
                'address':{},
                'between':{},
                'suburb':{},
                'turn': {'required':true, 'default':'MIXTO'},
                'telephones':{},
                'cellular':{},
                'enable':{'default':1},
                'group':{},
                'picture':{}

            },
            'autopublish': false
    }
);
var Suburb = new SimpleModel(
        {
                'url': '/suburb',
                'type': 'suburb',
                'callbacks': {
                        'afterPublish': function(data, sdata, sfields) {

                        },
                        'onCreate': function(instance) {

                        }
                },
                'handlers': {
                        'errorHandler': function(error, data) {
                            switch(error) {
                                case 'ERROR_ON_PUBLISH_RESPONSE': (function(error) {
                                        alert("Hubo un error al intentar registrar la zona: "+error);
                                })(data);
                            }
                        }
                },
            'fields': {
                'id': {'default':-1},
                'suburb': {},
                'id_zone': {'default':'Z01'},
                'enable':{'default':1}
            },
            'autopublish': false
    }
);
var Agree = new SimpleModel(
        {
                'url': '/agreement',
                'type': 'agreement',
                'callbacks': {
                        'afterPublish': function(data, sdata, sfields) {
							console.log(sdata);
							return true;
                        },
                        'onCreate': function(instance) {

                        }
                },

                'handlers': {
                        'errorHandler': function(error, data) {
                            switch(error) {
                                case 'ERROR_ON_PUBLISH_RESPONSE': (function(error) {
                                        alert("Hubo un error al intentar registrar el convenio: "+error);
                                })(data);
                            }
                        }
                },
            'fields': {
                'id': {},
                'agreement':{},
                'description': {},
                'aeropuerto': {},
                'metropolitan':{},
                'intermedia':{},
                'periphery':{},
                'foraneo':{},
                'restringida': {},
                'camioneta':{},
                'promo':{}

            },
            'autopublish': false
    }
);
var Restriccion = new SimpleModel(
        {
                'url': '/restriction',
                'type': 'restriction',
                'callbacks': {
                        'afterPublish': function(data, sdata, sfields) {
							console.log(sdata);
							return true;
                        },
                        'onCreate': function(instance) {

                        }
                },

                'handlers': {
                        'errorHandler': function(error, data) {
                            switch(error) {
                                case 'ERROR_ON_PUBLISH_RESPONSE': (function(error) {
                                        alert("Hubo un error al intentar registrar la restricción: "+error);
                                })(data);
                            }
                        }
                },
            'fields': {
                'id': {},
                'name':{},
                'horary1': {},
                'horary2':{},
                'ln':{},
                'll':{},
                'vn':{},
                'vl':{}


            },
            'autopublish': false
    }
);
