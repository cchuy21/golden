

var app = {
	'load': function (url,container2, options, callback) {
        var container = jQuery(container2);

        if (jQuery.isFunction(options)) {
            callback = options;
        } else {
            if (jQuery.isFunction(options.done)) {
                callback = options.done;
            }
            if (jQuery.isFunction(options.before)) {
                options.before();
            }
        }
        container.fadeTo(250, 0.1, function () {
            container.load(url, function () {
                container.fadeTo(250, 1, function () {
                    callback();
                });
            });
        });
    },
    'exec': function (url, ops) {
        var options = {
            'type': 'POST',
            'dataType': 'json',
            'before': function () {
                return true;
            },
            'success': function () {},
            'data': {}
        };
        for (o in ops) {
            if (options[o] !== undefined) {
                options[o] = ops[o];
            }
        }
        if (jQuery.isFunction(options.before)) {
            if (options.before() === true) {
                jQuery.ajax({
                    type: options.type,
                    dataType: options.dataType,
                    url: url,
                    data: options.data
                }).done(function (d) {
                    if (jQuery.isFunction(options.success)) {
                        options.success(d);
                    }
                });
            }
        }
    }
};
jQuery(document).ready(function(){
	
	var data=jQuery(".system-data");
		var a=Systemuser.create(data);
	
	
	jQuery("#saveus").on("click",function(){
		var pass=jQuery("#act").val();
		
		jQuery.ajax({
		  type: "POST",
		  url: '/systemuser/password', 
		  data: {'d':pass}, 
		  cache: false,
		  dataType: 'json',
		  success: function(data){
				if(data=="si")
		  		a.publish();
		  		else if(data=="no")
		  		alert("Contraseña actual vacia ó incorrecta");
		  	
		  },
		  error: function(data){
		  	alert("Contraseña actual vacia ó incorrecta");
		  }
	  });
		
	});
})
