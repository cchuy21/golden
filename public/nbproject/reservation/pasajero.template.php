<script>
$('#MyTab a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})
</script>


<div class="container">
	<div class="row">
		<div class="col-md-6">
			Cliente
		</div>
		<div class="col-md-6">
			<p class="text-right"><button type="button" class="btn btn-default btn-x"><span class="glyphicon glyphicon-remove"> </span></button></p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
		<!--primera mitad-->
			<div class="col-md-12">
				<input type="text" class="form-control" placeholder="Nombre Completo, Telefono o Correo" name="" />
			</div>
			<div class="col-md-6 col-md-offset-6">						  	
				<div class="checkbox">
					<label>
			  			<label style="float: left; margin-right: 25px">
			  				Pasajero distínguido 
			  			</label>
						<input type="checkbox" class="text-right" value="distinguido">
			  		</label>
				</div>
			</div>
		<!--dat_servicios-->
		<br />
		<br />
		<div class="row" style="margin-top: 84px;">
			<div class="col-md-3">
				<small># de servicios</small>
			</div>
			<div class="col-md-4 col-md-offset-1">
				<small>Primer Servicio</small>
			</div>
			<div class="col-md-4">
				<small>Último Servicio</small>
			</div>
		</div>
		<div class="row">
			<!-- fechas de servicios  -->
			<div class="col-md-3">
				<div class="col-md-10 form-control">
					<p class="text-center">
					
					</p>
				</div>
			</div>
			<div class="col-md-4 col-md-offset-1">
				<div class="col-md-10 form-control">
				<p class="text-center">
					
					</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="col-md-10 form-control">
				<p class="text-center">
					
					</p>
				</div>
			</div>
		</div>
		<!--tablas-->
		<br />
		<div class="row">
			<div class="col-md-6">
				<table class="table table-bordered">
					<tr class="active">
						<td colspan="2">Beneficio Adicional</td>
					</tr>
					<tr>
						<td colspan="2">
							<select class="form-control">
								<option># gratuito</option>
								<option># gratuito</option>
								<option># gratuito</option>
								<option># gratuito</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Acumulados</td>
						<td> </td>
					</tr>
					<tr>
						<td>Usados</td>
						<td> </td>
					</tr>
					<tr>
						<td>Disponibles</td>
						<td> </td>
					</tr>
				</table>
				
			</div>
			<div class="col-md-6">
				<table class="table table-bordered">
					<tr class="active">
						<td colspan="2">Convenio</td>
					</tr>
					<tr>
						<td colspan="2">
							<select class="form-control" name="" >
								<option value="Sin Convenio">Sin Convenio</option>
								<option value="------------">------------</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<select class="form-control" name="" >
								<option value="Particular">Particular</option>
								<option value="----------">----------</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>RFC:</td>
						<td>
							<input type="text" class="form-control no-border" name="" />
						</td>
					</tr>
				</table>
			</div>
		</div>
		<textarea rows="4" class="form-control" name="">Notas especiales:</textarea>
	</div>
	<!--segunda parte-->
	<div class="col-md-6">
		<div class="col-md-12 form-control" style="height: auto !important" >
			<br />
			<div class="row">
				<div class="col-md-3 col-md-offset-6">
					<small># de Reservación</small>
				</div>
				<div class="col-md-2 azul-blanco">
					<p>&nbsp;</p>
				</div>
			</div>
			<br />
			<form class="form-horizontal" method="post" action="#">
			<div class="form-horizontal">
				<div class="form-group">
					<label class="control-label col-md-2"><small>telefono:</small></label>
					<div class="col-md-10">
						<input type="text" class="form-control" name="" />
					</div>
				</div>
			</div>
			<div class="form-horizontal">
				<div class="form-group">
					<label class="control-label col-md-2"><small>email:</small></label>
					<div class="col-md-10">
						<input type="text" class="form-control" name="" />
					</div>
				</div>
			</div>
			</form>
		</div>
		<div class="col-md-12 cont_direccion">
			<div class="col-md-12  back_gray">
				<div class="row">
					<div class="col-md-6">
						Dirección
					</div>
					<div class="col-md-6">
						<div id="icon_direccion" style="float: right">(iconos)
							<img />
							<img />
							<img />
						</div>
					</div>
				</div>
			</div>
	       <div class="tabbable tabs-left">
	        <ul class="nav nav-tabs" id="lat">
	          <li class="active"><a href="#1" data-toggle="tab" class="first">Casa</a></li>
	          <li><a href="#2" data-toggle="tab">Oficina</a></li>
	          <li><a href="#3" data-toggle="tab">Oficina Norte</a></li>
	        </ul>
	        <!--contenidos de las tabs laterales-->
	        <form class="form-horizontal" method="post" action="">
	        <div class="tab-content content-lateral">
	        	
	         <div class="tab-pane active" id="1"> 
	         	<!--contenido de la primera tab-->
	         	<table class="table lat" style="border-top: 1px solid #ddd;">
	         		<tr>
	         			<td colspan="2" class="first">
	         				<input class="back_am form-control" placeholder="Calle y Numero" name="">
	         			</td>
	         		</tr>
	         		<tr>
	         			<td colspan="2">
	         				<input class="back_am form-control" placeholder="Entre que Calles" name="">
	         			</td>
	         		</tr>
	         		<tr>
	         			<td colspan="2">
	         				<input class="back_am form-control" placeholder="Colonia" name="">
	         			</td>
	         		</tr>
	         		<tr>
	         			<td>
	         				<input class="form-control" name="">
	         			</td>
	         			<td>
	         				<input class="form-control" name="">
	         			</td>
	         		</tr>
	         		<tr>
	         			<td colspan="2">
	         				<textarea rows="4" class="form-control" name="">Notas Especiales</textarea>
	         			</td>
	         		</tr>
	         	</table>
	         </div>
	         <div class="tab-pane" id="2">
	         	<!--contenido de la segunda tab-->
	         	<table class="table lat" style="border-top: 1px solid #ddd;">
	         		<tr>
	         			<td colspan="2" class="first">
	         				<input class="form-control back_am" placeholder="Calle y Numero" name="">
	         			</td>
	         		</tr>
	         		<tr>
	         			<td colspan="2">
	         				<input class="form-control back_am" placeholder="Entre que Calles" name="">
	         			</td>
	         		</tr>
	         		<tr>
	         			<td colspan="2">
	         				<input class="form-control back_am" placeholder="Colonia" name="">
	         			</td>
	         		</tr>
	         		<tr>
	         			<td>
	         				<input class="form-control" name="">
	         			</td>
	         			<td>
	         				<input class="form-control" name="">
	         			</td>
	         		</tr>
	         		<tr>
	         			<td colspan="2">
	         				<textarea rows="4" class="form-control" name="">Notas Especiales</textarea>
	         			</td>
	         		</tr>
	         	</table>
	         </div>
	         <div class="tab-pane" id="3">
	         	<!--contenido de la tercera tab-->
	         	<table class="table lat" style="border-top: 1px solid #ddd;">
	         		<tr>
	         			<td colspan="2" class="first">
	         				<input class="form-control back_am" placeholder="Calle y Numero" name="">
	         			</td>
	         		</tr>
	         		<tr>
	         			<td colspan="2">
	         				<input class="form-control back_am" placeholder="Entre que Calles" name="">
	         			</td>
	         		</tr>
	         		<tr>
	         			<td colspan="2">
	         				<input class="form-control back_am" placeholder="Colonia" name="">
	         			</td>
	         		</tr>
	         		<tr>
	         			<td>
	         				<input class="form-control" name="">
	         			</td>
	         			<td>
	         				<input class="form-control" name="">
	         			</td>
	         		</tr>
	         		<tr>
	         			<td colspan="2">
	         				<textarea rows="4" class="form-control" name="">Notas Especiales</textarea>
	         			</td>
	         		</tr>
	         	</table>
	         </div>
	        </div>
	         </form>
	      </div>      
		</div>					
	</div>
</div>
<form class="form-horizontal" method="post" action="#">
	<div class="col-md-12">
		<br />
		Datos de Servicio
	</div>
		<div class="col-md-9">
			<div class="col-md-4">
				<div class="col-md-12 back_gr" style="margin-left:-30px;">
				Tipo de Servicio
				<table class="table table-bordered">
					<tr class="back_white">
						<td colspan="2">
							  <select class="form-control" name="dom">
								<option value="Viaje">Viaje</option>
								<option value="2da opcion">--------</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<select class="form-control" name="dom">
								<option value="Redondo ADA">Redondo ADA</option>
								<option value="Redondo DAD">Redondo DAD</option>
								<option value="Sencillo DA">Sencillo DA</option>
								<option value="Sencillo AD">Sencillo AD</option>
							</select>
						</td>
					</tr>
					<tr class="back_white">
						<td>
							Dom. Adicionales
						</td>
						<td>
							<select class="form-control" name="dom">
								<option value="0">0</option>
								<option value="1">1</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>
							Pasajeros
						</td>
						<td>
							<select class="form-control" name="dom">
								<option value="0">0&nbsp;&nbsp;</option>
								<option value="1">1&nbsp;&nbsp;</option>
							</select>
						</td>
					</tr>
					<tr class="back_white">
						<td colspan="2">
							<div class="checkbox no-pad">
						    	<label>
						      		<input type="radio" name="reservacion[auto]"> Automovil
						    	</label>
							</div>
							<div class="checkbox no-pad">
						    	<label>
						      		<input type="radio" name="reservacion[auto]"> Camioneta
						    	</label>
						  	</div>
						</td>
					</tr>
				</table>
				</div>
			</div>
			<div class="col-md-8 back_gr" style="margin-left: -15px;">
				<div class="col-md-12">
					Fecha y Hora del Servicio
				</div>
				<div class="col-md-12 back_white" style="padding: 10px 20px">
					El servicio Apto a Domicilio se requiere el:
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<input type="date" class="form-control" />
							</div>
						</div>
						<div class="col-md-6 no-pad">
							<div class="form-group">
								<label class="control-label col-md-4 no-pad">a las:</label>
								<div class="col-md-5 no-pad" >
									<input type="time" class="form-control">
								</div>
								<label class="control-label no-pad col-sm-offset-1 col-md-1">hrs.</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-inline">
							<div class="groupinline inpsel form-group">
							  <select class="form-control">
							  	<option value="1">aereolina 1</option>
							  	<option value="2">aereolina 2</option>
							  	<option value="3">aereolina 3</option>
							  	<option value="4">aereolina 4</option>
							  	<option value="5">aereolina 5</option>
							  </select>
							</div>
							<div class="groupinline form-group inpunl">
								<input type="text" class="form-control" placeholder="Vuelo" />
							</div>
							<div class="groupinline inpsel form-group">
							  <select class="form-control">
							  	<option value="1">terminal 1</option>
							  	<option value="2">terminal 2</option>
							  	<option value="3">terminal 3</option>
							  	<option value="4">terminal 4</option>
							  	<option value="5">terminal 5</option>
							  </select>
							</div>
							<div class="groupinline inpsel form-group">
							  <select class="form-control">
							  	<option value="1">Nacional</option>
							  	<option value="2">Internacional</option>
							  </select>
							</div>
							<div class="groupinline form-group inpunl">
								<input type="text" class="form-control" placeholder="Hora" />
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-12" style="padding:10px 20px; margin-top: 10px;">
					El servicio Domicilio a Apto. se requiere el:
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<input type="date" class="form-control" />
							</div>
						</div>
						<div class="col-md-6 no-pad">
							<div class="form-group">
								<label class="control-label no-pad col-md-4">a las:</label>
								<div class="col-md-5 no-pad" >
									<input type="time" class="form-control">
								</div>
								<label class="control-label col-sm-offset-1 no-pad col-md-1">hrs.</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-inline">
							<div class="groupinline inpsel form-group">
							  <select class="form-control">
							  	<option value="1">aereolina 1</option>
							  	<option value="2">aereolina 2</option>
							  	<option value="3">aereolina 3</option>
							  	<option value="4">aereolina 4</option>
							  	<option value="5">aereolina 5</option>
							  </select>
							</div>
							<div class="groupinline form-group inpunl">
								<input type="text" class="form-control" placeholder="Vuelo" />
							</div>
							<div class="groupinline inpsel form-group">
							  <select class="form-control">
							  	<option value="1">terminal 1</option>
							  	<option value="2">terminal 2</option>
							  	<option value="3">terminal 3</option>
							  	<option value="4">terminal 4</option>
							  	<option value="5">terminal 5</option>
							  </select>
							</div>
							<div class="groupinline inpsel form-group">
							  <select class="form-control">
							  	<option value="1">Nacional</option>
							  	<option value="2">Internacional</option>
							  </select>
							</div>
							<div class="groupinline form-group inpunl">
								<input type="text" class="form-control" placeholder="Hora" />
							</div>
						</div>
					</div>
				</div>
			</div>
			
			
			
			<div class="row">
				<div class="col-md-12" style="padding-right:0px;">
					<div class="col-md-12">
						<div class="col-md-12  back_gr" style="margin-top:10px;">
						Anotaciones de la Reservación
				
					<textarea class="form-control" rows="3"> </textarea>
					</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 back_gr">
			Tarifa
			<table class="table table-bordered table-condensed border0">
				<tr>
					<td>
						<small>Tarifa aplicada</small>
					</td>
					<td>
						<p class="text-right"> <small>$ 456.00</small></p>
					</td>
				</tr>
				<tr class="back_white">
					<td>
						<small>Dom. adic.</small>
					</td>
					<td>
						<p class="text-right"><small>$ 0.00</small></p>
					</td>
				</tr>
				<tr>
					<td>
						<div class="checkbox">
						  <label>
						    <input type="checkbox" value="">
						    <small>Beneficio</small>
						  </label>
						</div> 
					</td>
					<td>
						<p class="text-right" style="color:red;"><small>$ -123.00</small></p>
					</td>
				</tr>
				<tr class="back_white">
					<td>
						<div class="checkbox">
						  <label>
						    <input type="checkbox" value="">
						    <small>Ajuste Manual</small>
						  </label>
						</div>  
					</td>
					<td>
						<p class="text-right"><small>$ 0.00</small></p>
					</td>
				</tr>
				<tr>
					<td class="border0">
						<p class="text-right"><small>TOTAL:</small></p>
					</td>
					<td>
						<p class="text-right"><small> $ 333.00</small></p>
					</td>
				</tr>
			</table>
			<select class="form-control" name="">
				<option>Forma de pago</option>
				<option>efectivo</option>
				<option>tarjeta</option>
			</select>
			<div class="checkbox col-md-9">
				<label>
		          <input type="checkbox"> Imprimir leyenda: Bajo Riesgo del usuario.
		        </label>
			</div>
			<div class="form-group">
			    <div class="col-lg-12">
			      <button type="submit" class="btn buttonres btn-success">Generar Reservación</button>
			    </div>
			</div>
		</div>
	
	<div class="col-md-12">
		Estado de la reservación					
	</div>
	<div class="col-md-offset-2 col-md-10" style="margin-bottom:15px" >
		<div class="row">
			<div class="col-md-2">
				Status:
			</div>
			<div class="col-md-2">
				Elaboró:
			</div>
			<div class="col-md-2">
				Creada:
			</div>
			<div class="col-md-2">
				Vía:
			</div>
			<div class="col-md-2">
				Editó:
			</div>
			<div class="col-md-2">
				Editada:
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<select class="form-control estado">
					<option>En elaboración</option>
				</select>
			</div>
			<div class="col-md-2">
				<select class="form-control estado">
					<option>En elaboración</option>
				</select>
			</div>
			<div class="col-md-2">
				<select class="form-control estado">
					<option>En elaboración</option>
				</select>
			</div>
			<div class="col-md-2">
				<select class="form-control estado">
					<option>En elaboración</option>
				</select>
			</div>
			<div class="col-md-2">
				<select class="form-control estado">
					<option>En elaboración</option>
				</select>
			</div>
			<div class="col-md-2">
				<select class="form-control estado">
					<option>En elaboración</option>
				</select>
			</div>
		</div>
	</div>
	</form>
</div>