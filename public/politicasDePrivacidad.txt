• Tenemos una política de privacidad muy simple, concebida para no complicarle la vida a nadie.

• El suministro de datos personales en este sitio es completamente opcional y requerirá la creación de una cuenta con una contraseña y una dirección de correo electrónico. Si eliges usar Facebook como medio para accesar, tus datos básicos de perfil serán utilizados para generar tu acceso.

• Si lo deseas, también puedes suministrar información personal adicional (al crear una cuenta o en tu perfil). Cualquier información que envíes se utilizará única y exclusivamente para datos estadísticos.

• Podemos usar la dirección de correo electrónico que nos has proporcionado para ponernos en contacto de vez en cuando e informarte sobre el servicio de GOLDEN TRANSPORTACIONES. Nunca daremos tu dirección a nadie para que pueda contactarte directamente y ofrecerte sus propios productos y servicios.

• Los menores de 13 años no cumplen con los requisitos para usar nuestro servicio y no deben registrarse ni enviarnos información personal. Los jóvenes entre 13 y 17 años no deben enviar ningún tipo de información sobre sí mismos a otras personas a través de Internet (ni siquiera a nosotros) sin el consentimiento previo de sus padres o tutores legales.