<?php

class authDriver extends driverBase {
    //función para iniciar sesión
    public static function login($user = null, $pass = null) {
        session_start();
        //la sesión se realiza exitosamente
        if(isset($_SESSION['loggedin']))
            if($_SESSION['loggedin'])
                return true;
        if(is_object($user)) {
            //is_array=comprueba si la variable es un array
        } elseif(is_array($user) ) {
            $u = User::find_by_username($user['username']);
            if(!$u) return false;
			//verifica que el password corresponda al usuario y sea correcto para iniciar sesión
            if($u->password == md5($user['password']) && $u->group!="ASIGNACIONES"){
                $_SESSION['loggedin'] = true;
                $_SESSION['user_id'] = $u->id;
                return true;
            }
        } else {
        	//encontrar los usuarios por su nombre de registrado
            $u = User::find_by_username($user['username']);
            if(!$u) return false;
            if($u->password == md5($pass)){
                $_SESSION['loggedin'] = true;
                $_SESSION['user_id'] = $u->id;
                return true;
            }
        }
        return false;
    }
	public static function loginemp($user = null, $pass = null) {
        session_start();
			//verifica que el password corresponda al usuario y sea correcto para iniciar sesión
			$u=User::find_by_email($user);
            if($u->password == md5($pass)){
                return true;
            }
        return false;
    }
    public static function logins2($user = null, $pass = null) {
        $computerId = $_SERVER['HTTP_USER_AGENT'].$_SERVER['LOCAL_ADDR'].$_SERVER['LOCAL_PORT'].$_SERVER['REMOTE_ADDR'];
        $se=Login::find_by_iddevice($computerId);
        if($se){
            if($se->sistema=="reservaciones"){
                return true;
            }
        }
            
        if(is_object($user)) {
            
        } elseif(is_array($user) ) {
            $u = Systemuser::find_by_username($user['username']);
            if(!$u) return false;
            if($u->password == md5($user['password']) && $u->group!="ASIGNACIONES"){
                $n= new Login();
                $n->iddevice=$computerId;
                $n->sistema="reservaciones";
                $n->data=$u->id;
                if($n->save()){
                    return true;
                }
            }
        } else {
            $u = Systemuser::find_by_username($user['username']);
            if(!$u) return false;
            if($u->password == md5($pass) && $u->group!="ASIGNACIONES"){
                $_SESSION['loggedin'] = true;
                $_SESSION['user_id'] = $u->id;
                $u->loggedin = 1;
                $u->save();
                return true;
            }
        }
        return false;
    }
    public static function logins($user = null, $pass = null) {
        session_start();
        if(isset($_SESSION['loggedin']))
            if($_SESSION['loggedin'])
                return true;
                
        if(is_object($user)) {
            
        } elseif(is_array($user) ) {
            $u = Systemuser::find_by_username($user['username']);
            if(!$u) return false;
            if($u->password == md5($user['password']) && $u->group!="ASIGNACIONES"){
                $_SESSION['loggedin'] = true;
                $_SESSION['user_id'] = $u->id;
                $u->loggedin = 1;
                $u->save();
                return true;
            }
        } else {
            $u = Systemuser::find_by_username($user['username']);
            if(!$u) return false;
            if($u->password == md5($pass) && $u->group!="ASIGNACIONES"){
                $_SESSION['loggedin'] = true;
                $_SESSION['user_id'] = $u->id;
                $u->loggedin = 1;
                $u->save();
                return true;
            }
        }
        return false;
    }
    //aqui se obtiene el id del usuario en sesión
    public static function getUser() {
        session_start();
        if($_SESSION["loggedin"]) {
            $u = User::find($_SESSION['user_id']);
            return $u;
        }
        return false;
    }
    //obtiene el usuario del sistema
    public static function getSUser() {	
        session_start();	
        if($_SESSION["loggedin"]) {
            $u = Systemuser::find($_SESSION['user_id']);
            return $u;
        }
        return false;
    }
    public static function getSUser2() {	
        $computerId = $_SERVER['HTTP_USER_AGENT'].$_SERVER['LOCAL_ADDR'].$_SERVER['LOCAL_PORT'].$_SERVER['REMOTE_ADDR'];
        $se=Login::find_by_iddevice($computerId);
        if($se){
            $u = Systemuser::find($se->data);
            return $u;
        }
        return false;
    }
    //el usuario cierra sesión
    public static function logout() {
        session_start();
       
        session_destroy();
    }
    
    public static function logout2() {
        $computerId = $_SERVER['HTTP_USER_AGENT'].$_SERVER['LOCAL_ADDR'].$_SERVER['LOCAL_PORT'].$_SERVER['REMOTE_ADDR'];
        $se=Login::find_by_iddevice($computerId);
        if($se)
            if($se->delete())
                return true;
        return false;
    }
    //se inicia sesión
    
    public static function isLoggedin() {
        session_start();
        if($_SESSION["loggedin"] && isset($_SESSION["loggedin"])) {
            return true;
        }
        return false;
    }
    public static function ver() {
        session_start();
        echo $_SESSION["user"];
    }

    public static function isLoggedin2() {
        session_start();
        // Asegurarse de aceptar ID de sesiones definidas por el usuario
        session_id("2413313");
       $_SESSION["user"]=21;
       
       
        /* 
        $computerId = $_SERVER['HTTP_USER_AGENT'].$_SERVER['LOCAL_ADDR'].$_SERVER['LOCAL_PORT'].$_SERVER['REMOTE_ADDR'];
        $ses=Login::find("all",array("conditions"=>"iddevice='".$computerId."' and sistema='reservaciones'"));
        foreach($ses as $ses2){
            $se=$ses2;
        }
        if($se){
            if($se->sistema=="reservaciones"){
                return true;
            }
        }
        return false;*/
    }
    //si el usuario no ha iniciado sesión en el sistema
    public static function chkLoggin() {
        session_start();
        return true;
        if( !$_SESSION["loggedin"] )
            responseDriver::dispatch('E', 'No se ha iniciado sesión', 'No se ha iniciado sesión en el sistema');
    }
}