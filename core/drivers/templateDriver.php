<?php
class templateDriver extends driverBase{
    private static $type = 'html';
    private static $content = null;
    private static $data = array();
	
    public static function setContent($content = null) {
        if( $content === null )
            return false;
        self::$content = $content;
    }
    public static function setData($k = null, $d = null) {
        if($k === null)
            return false;
        self::$data[$k] = $d;
    }
    public static function getData($k = null) {
        if( $k === null )
            return self::$data;
        return isset(self::$data[$k]) ? self::$data[$k] : null;
    }
    //incluye un template si esque existe sino manda mensaje de error
    public static function render($content = false, $data = null, $template = 'index') {
        if( $data )
            self::setData("content", $data);
        
        templateDriver::setContent($content ? $content : configDriver::defaultContent());
        //incluye los archivos de template, si es que existen
        if( file_exists("../app/templates/".$template.".template.php") ) 
            include("../app/templates/".$template.".template.php");
        else
            die("No existe el template especificado");
    }
    
    public static function content(){
        self::renderSection(self::$content);
    }
    
    public static function renderRaw($sect = null, $data = null){
	self::renderSection($sect, $data);
    }
	//convierte los dias de ingles a español
	public static function diasem($dias){
		switch ($dias) {
		case 'Mon':
			$dia="Lunes";
		break;
		case 'Tue':
			$dia="Martes";
		break;
		case 'Wen':
			$dia="Miercoles";
		break;
		case 'Thu':
			$dia="Jueves";
		break;
		case 'Fri':
			$dia="Viernes";
		break;
		case 'Sat':
			$dia="Sabado";
		break;
		case 'Sun':
			$dia="Domingo";
		break;
	}
		return $dia;
	}
	//cambia el formato de ingles a español de algunos meses del año
	public static function timemonth2($m){
		switch ($m) {
			case 'Dec':
					$m="Dic";
				break;
			
			case 'Jan':
					$m="Ene";
				break;
			case 'Apr':
					$m="Abr";
				break;

			case 'Aug':
					$m="Ago";
				break;
		}
		return $m;
	}
	//define el mes de número a estring
	public static function timemonth($mesant){
		switch ($mesant) {
		case 1:
			$mesant="Enero";
			break;
		case 2:
			$mesant="Febrero";
			break;
		case 3:
			$mesant="Marzo";
			break;
		case 4:
			$mesant="Abril";
			break;
		case 5:
			$mesant="Mayo";
			break;
		case 6:
			$mesant="Junio";
			break;
		case 7:
			$mesant="Julio";
			break;
		case 8:
			$mesant="Agosto";
			break;
		case 9:
			$mesant="Septiembre";
			break;
		case 10:
			$mesant="Octubre";
			break;
		case 11:
			$mesant="Noviembre";
			break;
		case 12:
			$mesant="Diciembre";
			break;
	}
		return $mesant;
	}
    public static function timelo($fechare,$type,$de=" de "){
    	$fechardiare=substr($fechare, 0,3);
		$fechardianumre=substr($fechare,5 ,2);
		$fechamesre=substr($fechare,8 ,3);
		//muestra el dia de la semana correspondiente
		if($type==1) {
		switch($fechardiare){
			case 'Sun':
				$fechardiare="Dom";
				break;
			case 'Mon':
				$fechardiare="Lun";
				break;
			case 'Tue':
			    $fechardiare="Mar";
				break;
			case 'Wed': 
			    $fechardiare="Mie";
				break;
			case 'Thu':
				$fechardiare="Jue";
				break;
			case 'Fri':
				$fechardiare="Vie";
				break;
			case 'Sat':
				$fechardiare="Sab";
				break;
		}
			switch($fechamesre){
			case 'Jan':
				$fechamesre="Ene";
				break;
			case 'Apr':
				$fechamesre="Abr";
				break;
			case 'Aug':
				$fechamesre="Ago";
				break;
			case 'Dec':
				$fechamesre="Dic";
				break;
			
			}
		}else if($type==2){
			//muestra el nombre entero de los dias de la semana
			switch($fechardiare){
			case 'Sun':
				$fechardiare="Domingo";
				break;
			case 'Mon':
				$fechardiare="Lunes";
				break;
			case 'Tue':
			    $fechardiare="Martes";
				break;
			case 'Wed': 
			    $fechardiare="Miercoles";
				break;
			case 'Thu':
				$fechardiare="Jueves";
				break;
			case 'Fri':
				$fechardiare="Viernes";
				break;
			case 'Sat':
				$fechardiare="Sabado";
				break;
		}
			//muestra los meses en español
			switch($fechamesre){
			case 'Jan':
				$fechamesre="Enero";
				break;
			case 'Feb':
				$fechamesre="Febrero";
				break;
			case 'Apr':
				$fechamesre="Abril";
				break;
			case 'Mar':
				$fechamesre="Marzo";
				break;
			case 'May':
				$fechamesre="Mayo";
				break;
			case 'Jun':
				$fechamesre="Junio";
				break;
			case 'Jul':
				$fechamesre="Julio";
				break;
			case 'Aug':
				$fechamesre="Agosto";
				break;
			case 'Sep':
				$fechamesre="Septiembre";
				break;
			case 'Oct':
				$fechamesre="Octubre";
				break;
			case 'Nov':
				$fechamesre="Noviembre";
				break;
			case 'Dec':
				$fechamesre="Diciembre";
				break;
			
			}
		}else if($type==3) {
		switch($fechardiare){
			case 'Sun':
				$fechardiare="Dom";
				break;
			case 'Mon':
				$fechardiare="Lun";
				break;
			case 'Tue':
			    $fechardiare="Mar";
				break;
			case 'Wed': 
			    $fechardiare="Mie";
				break;
			case 'Thu':
				$fechardiare="Jue";
				break;
			case 'Fri':
				$fechardiare="Vie";
				break;
			case 'Sat':
				$fechardiare="Sab";
				break;
		}
			switch($fechamesre){
			case 'Jan':
				$fechamesre="Ene";
				break;
			case 'Apr':
				$fechamesre="Abr";
				break;
			case 'Aug':
				$fechamesre="Ago";
				break;
			case 'Dec':
				$fechamesre="Dic";
				break;
			
			}
			$fechaño=substr($fechare,12 ,4);
		}
		return $fechefinalre=$fechardiare.", ".$fechardianumre.$de.$fechamesre." ".$fechaño;
	}
    public static function  timelohrs($fechare,$tmd){
	//obtiene los primeros digitos de la fecha 
		$fechardiare=substr($fechare, 0,3);
		$fechardianumre=substr($fechare,5 ,2);
		$fechamesre=substr($fechare,8 ,3);
		$fechati=substr($tmd, 0,5);
		switch($fechardiare){
			case 'Sun':
				$fechardiare="Dom";
				break;
			case 'Mon':
				$fechardiare="Lun";
				break;
			case 'Tue':
			    $fechardiare="Mar";
				break;
			case 'Wed': 
			    $fechardiare="Mie";
				break;
			case 'Thu':
				$fechardiare="Jue";
				break;
			case 'Fri':
				$fechardiare="Vie";
				break;
			case 'Sat':
				$fechardiare="Sab";
				break;
		}
			switch($fechamesre){
			case 'Jan':
				$fechamesre="Ene";
				break;
			case 'Apr':
				$fechamesre="Abr";
				break;
			case 'Aug':
				$fechamesre="Ago";
				break;
			case 'Dec':
				$fechamesre="Dic";
				break;
			
			}
		$fechefinalre=$fechardiare.", ".$fechardianumre." de ".$fechamesre." ".$fechati;
		//regresa 10 caracteres de la fecha de la posición 15 a la 5
		return $horares=substr($fechefinalre,15,5);
		}
public static function timelo2($fechare,$type,$de=" de "){
	//obtiene la fecha de 0 a 3 caracteres o etc
    	$fechardiare=substr($fechare, 0,3);
		$fechardianumre=substr($fechare,5 ,2);
		$fechamesre=substr($fechare,8 ,3);
		$hora=substr($fechare, 18,5);
		if($type==1) {
		switch($fechardiare){
			case 'Sun':
				$fechardiare="Dom";
				break;
			case 'Mon':
				$fechardiare="Lun";
				break;
			case 'Tue':
			    $fechardiare="Mar";
				break;
			case 'Wed': 
			    $fechardiare="Mie";
				break;
			case 'Thu':
				$fechardiare="Jue";
				break;
			case 'Fri':
				$fechardiare="Vie";
				break;
			case 'Sat':
				$fechardiare="Sab";
				break;
		}
			switch($fechamesre){
			case 'Jan':
				$fechamesre="Ene";
				break;
			case 'Apr':
				$fechamesre="Abr";
				break;
			case 'Aug':
				$fechamesre="Ago";
				break;
			case 'Dec':
				$fechamesre="Dic";
				break;
			
			}
		}else if($type==2){
			switch($fechardiare){
			case 'Sun':
				$fechardiare="Domingo";
				break;
			case 'Mon':
				$fechardiare="Lunes";
				break;
			case 'Tue':
			    $fechardiare="Martes";
				break;
			case 'Wed': 
			    $fechardiare="Miercoles";
				break;
			case 'Thu':
				$fechardiare="Jueves";
				break;
			case 'Fri':
				$fechardiare="Viernes";
				break;
			case 'Sat':
				$fechardiare="Sabado";
				break;
		}
			switch($fechamesre){
			case 'Jan':
				$fechamesre="Enero";
				break;
			case 'Feb':
				$fechamesre="Febrero";
				break;
			case 'Apr':
				$fechamesre="Abril";
				break;
			case 'Mar':
				$fechamesre="Marzo";
				break;
			case 'May':
				$fechamesre="Mayo";
				break;
			case 'Jun':
				$fechamesre="Junio";
				break;
			case 'Jul':
				$fechamesre="Julio";
				break;
			case 'Aug':
				$fechamesre="Agosto";
				break;
			case 'Sep':
				$fechamesre="Septiembre";
				break;
			case 'Oct':
				$fechamesre="Octubre";
				break;
			case 'Nov':
				$fechamesre="Noviembre";
				break;
			case 'Dec':
				$fechamesre="Diciembre";
				break;
			
			}
		}else if($type==3) {
		switch($fechardiare){
			case 'Sun':
				$fechardiare="Dom";
				break;
			case 'Mon':
				$fechardiare="Lun";
				break;
			case 'Tue':
			    $fechardiare="Mar";
				break;
			case 'Wed': 
			    $fechardiare="Mie";
				break;
			case 'Thu':
				$fechardiare="Jue";
				break;
			case 'Fri':
				$fechardiare="Vie";
				break;
			case 'Sat':
				$fechardiare="Sab";
				break;
		}
			switch($fechamesre){
			case 'Jan':
				$fechamesre="Ene";
				break;
			case 'Apr':
				$fechamesre="Abr";
				break;
			case 'Aug':
				$fechamesre="Ago";
				break;
			case 'Dec':
				$fechamesre="Dic";
				break;
			
			}
			$fechaño=substr($fechare,12 ,4);
		}
		
		return $fechefinalre=$fechardiare.", ".$fechardianumre.$de.$fechamesre." ".$fechaño." a las ".$hora;
		}
	public static function tim($sect){
		return substr($sect, 0,5);
	}
    public static function renderSection($sect = null, $data = null, $overwrite = true){
		if (session_status() !== PHP_SESSION_ACTIVE) {session_start();}
    
	$section = "../app/templates";
        $key = null;
		//separa los valores de sect por medio de los puntos
	foreach (explode('.', $sect) as $value) {
            $section = $section . "/" . $value;
            $key = $value;
	}
        if( $data !== null ) {
            if( !$overwrite ) {
                $i = 2;
                while(isset(self::$data[$key]))
                    $key = $key.$i++;
            }
            self::setData($key, $data);
        }
		//verifica si el archivo .php existe en el sistema
        if( file_exists($section.".php") ) 
            $section = $section.".php"; 
        elseif (file_exists($section.".template.php")) //busca el archivo en template
            $section = $section.".template.php";
        else //sino encuentra el archivo solicitado
            die("No existe la sección especificada"); 
        include($section);
    }
    
}
