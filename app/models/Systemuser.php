<?php
class Systemuser extends ActiveRecord\Model {

	
	
	static $has_one=array(
	array('unity')
	);
	
	public function validate() {
		//si al momento de agregar un usuario, este ya esta registrado
		if ($this->is_new_record() && static::exists(array('conditions' => array('username' => $this->username)))) {
			$this->errors->add('username', 'Este nombre de usuario ya existe');
		}
	}
}



