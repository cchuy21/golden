<?php

class Registration extends ActiveRecord\Model {
    static $validates_presence_of = array(
    //valida que los siguientes campos no sean nullos y no esten vacios
        array("user_id"),
        array("address_id"),
        array("vehicle_id"),
        array("passengers"),
        array("terminal"),
        array("airline"),
        array("flight_number"),
        array("destiny"),
        array("revenue"),
        array("leave_time"),
        array("leave_date"),
    );
}