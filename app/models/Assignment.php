<?php

class Assignment extends ActiveRecord\Model {
	//una assignment contiene los siguientes datos
    static $has_one = array( 
		array('tickets'),
        array('reservations'),
        array('unities'),
        array('operators')
    );

}