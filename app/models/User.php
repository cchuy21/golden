<?php

class User extends ActiveRecord\Model {
	//valida que los siguientes campos no sean nullos y no esten vacios
    static $validates_presence_of = array(
        array("name"),
        array("distinguished"),
    );
    //que el usaurio tenga de uno a muchos servicios y direcciones
    static $has_many = array(
        array('addresses'),
        array('services')
    );
    //que tenga solo un servicio y una reservación
    static $has_one = array(
		//array('services', 'limit' => 10),
        array('reservations')
    );
    
     public function getFirstService() {
     	//mostrar el primer servicios
        $options = array('conditions' => "user_id='{$this->id}'", 'order' => 'created_at asc', 'limit' => 1);
        $r = Service::find('all', $options);
        return isset($r[0]) ? $r[0] : null;
    }
    
    public function getLastService() {
    	//obtener el ultimo servicio
        $options = array('conditions' => "user_id='{$this->id}'", 'order' => 'created_at desc', 'limit' => 1);
        $r = Service::find('all', $options);
        return isset($r[0]) ? $r[0] : null;
    }
	public function getsLastService() { 
    	//obtener el ultimo servicio
        $options = array('conditions' => "user_id='{$this->id}' and (pagado=1 or pagado =0)", 'order' => 'id desc');
        $r = Service::find('all', $options);
        return isset($r) ? $r : null;
    }
}

