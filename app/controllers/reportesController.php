<?php
class reportesController extends controllerBase {
    
    public function cargarcolonias($id){
        $ex = explode(", ", $id);
        $like = '';
        foreach($ex as $k => $v) {
            $like .= "REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(LOWER(suburb),'á','a'),'é','e'),'í','i'),'ó','o'),'ú','u') like '%".strtolower($v)."%' and ";
        }
        $like  = substr($like, 0, -4);
        $sub=Suburb::find('all', array('conditions' =>'enable = 1 and '.$like,"order"=>"suburb asc"));
		$cot=0;
		
		foreach($sub as $s){
		    $cot++;
			echo '<tr>
			<td>'.$cot.'</td>
			<td>'.$s->suburb.'</td>
			<td>'.$s->id_zone.'</td>
			</tr>';
        }
    }
    public function excelcargarcolonias($id){
        include 'PHPExcel/PHPExcel.php'; //libreria de excel		
		$prueba = new PHPExcel();  //se crea un achivo de excel
		$titulo="hoja de calculo";
		
        $ex = explode(", ", $id);
        $like = '';
        foreach($ex as $k => $v) {
            $like .= "REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(LOWER(suburb),'á','a'),'é','e'),'í','i'),'ó','o'),'ú','u') like '%".strtolower($v)."%' and ";
        }
        $like  = substr($like, 0, -4);
        $sub=Suburb::find('all', array('conditions' =>'enable = 1 and '.$like,"order"=>"suburb asc"));
		$cot=0;
		
		foreach($sub as $s){
		    $cot++;
			$prueba->setActiveSheetIndex(0)->setCellValue("A".$cot, $cot);
        	$prueba->setActiveSheetIndex(0)->setCellValue("B".$cot, $s->suburb);
        	$prueba->setActiveSheetIndex(0)->setCellValue("C".$cot, $s->id_zone);
        		
        }
        header('Content-type: application/vnd.ms-excel');

        // It will be called file.xls
        header('Content-Disposition: attachment; filename="reportecolonias.xlsx"');
        $objWriter = PHPExcel_IOFactory::createWriter($prueba, 'Excel2007');
		$objWriter->save('php://output'); 
    }
    public function excelalpa(){
        //include 'PHPExcel/PHPExcel.php'; //libreria de excel		
		//$prueba = new PHPExcel();  //se crea un achivo de excel
		$titulo="hoja de calculo";
		//$prueba->setActiveSheetIndex(0)->setCellValue("A1","# Reservación");
		$alphas = range('A', 'Z');
		print_r($alphas);
    }
    public function getexcelsuburbs(){
    	include 'PHPExcel/PHPExcel.php'; //libreria de excel		
		$prueba = new PHPExcel();  //se crea un achivo de excel
		$titulo="hoja de calculo";
    	$systemuser=Suburb::all();
    	$cont=1;
    	$prueba->setActiveSheetIndex(0)->setCellValue("A".$cont,"Nombre");
		$prueba->setActiveSheetIndex(0)->setCellValue("B".$cont,"Zona");
		$prueba->setActiveSheetIndex(0)->setCellValue("C".$cont,"Automovil");
		$prueba->setActiveSheetIndex(0)->setCellValue("D".$cont,"Redondo Automovil");
		$prueba->setActiveSheetIndex(0)->setCellValue("E".$cont,"Camioneta");
		$prueba->setActiveSheetIndex(0)->setCellValue("F".$cont,"Camioneta Redondo");
    	 foreach($systemuser as $sys){
    	 	
		    if($sys->enable!=0){
		        $cont++;
				$zo=Zone::find($sys->id_zone);
				$zones=Zone::find_by_id($sys->id_zone);
	            $prueba->setActiveSheetIndex(0)->setCellValue("A".$cont,$sys->suburb);
        		$prueba->setActiveSheetIndex(0)->setCellValue("B".$cont,$sys->id_zone);
        		$prueba->setActiveSheetIndex(0)->setCellValue("C".$cont,"".$zones->cost_car);
        		$prueba->setActiveSheetIndex(0)->setCellValue("D".$cont,"".((2)*($zones->cost_car)-$zo->des_car));
        		$prueba->setActiveSheetIndex(0)->setCellValue("E".$cont,"".$zones->cost_van);
        		$prueba->setActiveSheetIndex(0)->setCellValue("F".$cont,"".((2)*($zones->cost_van)-$zo->des_van));
        		
				
			}
		}
		$objWriter = PHPExcel_IOFactory::createWriter($prueba, 'Excel2007');
		$objWriter->save('excels/costoscolonias.xlsx'); 
		echo "<a href='/excels/costoscolonias.xlsx'>Descargar</a>";
    }
    public function getexcelbas(){
    	include 'PHPExcel/PHPExcel.php'; //libreria de excel		
		$prueba = new PHPExcel();  //se crea un achivo de excel
		$titulo="hoja de calculo";
    	$systemuser=User::find('all',array("conditions" => "business_id='46892'"));
    	$cont=1;
    	$prueba->setActiveSheetIndex(0)->setCellValue("A".$cont,"Nombre");
    	$prueba->setActiveSheetIndex(0)->setCellValue("B".$cont,"TIPO");

    	 foreach($systemuser as $sys){
    	 	
		    if($sys->enable!=0){
		        $cont++;
		        
	            $prueba->setActiveSheetIndex(0)->setCellValue("A".$cont,$sys->name);
	            if($sys->isbusiness == 2){
    	            $prueba->setActiveSheetIndex(0)->setCellValue("B".$cont, "usuario del sistema");
	            } else if($sys->isbusiness == 0){
    	            $prueba->setActiveSheetIndex(0)->setCellValue("B".$cont, "usuario de servicio");
	            }
			}
		}
		$objWriter = PHPExcel_IOFactory::createWriter($prueba, 'Excel2007');
		$objWriter->save('excels/usuariosbas.xlsx'); 
		echo "<a href='/excels/usuariosbas.xlsx'>Descargar</a>";
    }
	public function rep($id){
		include 'PHPExcel/PHPExcel.php'; //libreria de excel		
		$prueba = new PHPExcel();  //se crea un achivo de excel
		$titulo="hoja de calculo";
		$prueba->setActiveSheetIndex(0)->setCellValue("A1","# Reservación");
		$prueba->setActiveSheetIndex(0)->setCellValue("B1","Nombre");
		$prueba->setActiveSheetIndex(0)->setCellValue("C1","Fecha(YYYY-MM-DD)");
		$users=User::find("all",array("select"=>"id,name","conditions"=>"business_id='".$id."'"));
		$c=1;
		foreach($users as $user){
			$sers=Service::find("all",array("select"=>"id,reservation1,reservation2","conditions"=>"user_id=".$user->id));
			foreach($sers as $ser){
				
				$r=Reservation::find_by_id($ser->reservation1,array("select"=>"status,reservation_date"));
				$fres=strtotime($r->reservation_date);
				$fene=strtotime("2018-07-01 00:00:00");
				$fmay=strtotime("2018-12-31 23:59:59");
				
				if($r->status==1||$r->status==5){
					if($fres >= $fene && $fres <= $fmay){
					//aqui imprime
						$c++;
						if($r->reservation_date==""){
							$prueba->setActiveSheetIndex(0)->setCellValue("A".$c,$ser->id);
							$prueba->setActiveSheetIndex(0)->setCellValue("B".$c,$user->name);
							$prueba->setActiveSheetIndex(0)->setCellValue("C".$c,"Abierto");
						}else{
							$prueba->setActiveSheetIndex(0)->setCellValue("A".$c,$ser->id);
							$prueba->setActiveSheetIndex(0)->setCellValue("B".$c,$user->name);
							$prueba->setActiveSheetIndex(0)->setCellValue("C".$c,date("Y-m-d",strtotime($r->reservation_date)));
						}
					}
				}else{
					if($ser->reservation2){
						$r=Reservation::find_by_id($ser->reservation2,array("select"=>"status,reservation_date"));
						$fres=strtotime($r->reservation_date);
						if($r->status==1||$r->status==5){
							if($fres >= $fene && $fres <= $fmay){
							//aqui imprime
								$c++;
								if($r->reservation_date==""){
									$prueba->setActiveSheetIndex(0)->setCellValue("A".$c,$ser->id);
									$prueba->setActiveSheetIndex(0)->setCellValue("B".$c,$user->name);
									$prueba->setActiveSheetIndex(0)->setCellValue("C".$c,"Abierto");
								}else{
									$prueba->setActiveSheetIndex(0)->setCellValue("A".$c,$ser->id);
									$prueba->setActiveSheetIndex(0)->setCellValue("B".$c,$user->name);
									$prueba->setActiveSheetIndex(0)->setCellValue("C".$c,date("Y-m-d",strtotime($r->reservation_date)));
								}
							}
						}
					}
				}
			}
		}
		$fecha=date("Y-m-d");
		$objWriter = PHPExcel_IOFactory::createWriter($prueba, 'Excel2007');
		$objWriter->save('excels/rep'.$fecha.'.xlsx'); 
		echo "<a href='/excels/rep".$fecha.".xlsx'>Descargar</a>";
		
	}
	public static function xlsbit($typ,$fecha){
		include 'PHPExcel/PHPExcel.php'; //libreria de excel		
		$prueba = new PHPExcel();  //se crea un achivo de excel
		$titulo="hoja de calculo";
		$prueba->getActiveSheet()->setTitle($titulo);
		$prueba->setActiveSheetIndex(0)->setCellValue("A1","#");
		$prueba->setActiveSheetIndex(0)->setCellValue("B1","HORA");
		$prueba->setActiveSheetIndex(0)->setCellValue("C1","RES.");
		$prueba->setActiveSheetIndex(0)->setCellValue("D1","CLIENTE");
		$prueba->setActiveSheetIndex(0)->setCellValue("E1","CALLE Y NUM.");
		$prueba->setActiveSheetIndex(0)->setCellValue("F1","COLONIA");
		$prueba->setActiveSheetIndex(0)->setCellValue("G1","EMP.");
		$prueba->setActiveSheetIndex(0)->setCellValue("H1","T. DE V.");
		$prueba->setActiveSheetIndex(0)->setCellValue("I1","COSTO");
		$prueba->setActiveSheetIndex(0)->setCellValue("J1","U. DE SERVICIO");
		$prueba->setActiveSheetIndex(0)->setCellValue("K1","PAGO");
		$prueba->setActiveSheetIndex(0)->setCellValue("L1","TELEFONO");
		$prueba->setActiveSheetIndex(0)->setCellValue("M1","TIPO");
		
		/*<th>ID</th>
          <th>HORA</th>
          <th>RES.</th>
          <th>CLIENTE</th>
          <th>CALLE Y NUM.</th>
          <th>COLONIA</th>
          <th>EMPRESA</th>
          <th>VEHICULO</th>
          <th>COSTO</th>
          <th>U. DE SERVICIO</th>
          <th>F. DE PAGO</th>
          <th>TELEFONO</th>
          <th>TIPO</th>*/			
			$hoy=date('Y-m-d');
			$diac=strtotime('-1day', strtotime($hoy));
			$diac=date('Y-m-d', $diac);
			$diag=strtotime('+1 day', strtotime($diac));
			$diag=date('Y-m-d', $diag);
			if(!empty($fecha)){
				$reser=Reservation::find('all', array('order' => 'reservation_time','conditions' =>' reservation_date = {ts "'.$fecha.'"}'));
			}else{
				$reser=Reservation::find('all', array('order' => 'reservation_time','conditions' =>' reservation_date BETWEEN {ts "'.$diac.'"} AND {ts "'.$diag.'"}'));
			}
			$cot=1;
			$ct=0;
			foreach($reser as $value){
				
				if($value->id){
				if($value->type==$typ){
					if($value->vehicle_id==1){
						$vel="AUT";
					}else{
						$vel="CAM";
					}
				$assig=Assignment::find_by_reservation_id($value->id);
					$unity=Unity::find_by_id($assig->unity_id);
					$oper=Operator::find_by_id($assig->operator_id);
					$query='select * from services where reservation1='.$value->id;
					$ser=Service::find_by_sql($query);
					if(!$ser){
						//selecciona todos los servicios de la reservacion2 de un usuario definiendo si el servicio es redondo o sencillo
						$query2='select * from services where reservation2='.$value->id;
						$ser=Service::find_by_sql($query2);
						if($typ=="MTY-APTO"){
							$serredsen='ADA';
						}else{
							$serredsen='DAD';
						}
					}else{
						if($ser->reservation2!=""){
							if($typ=="MTY-APTO"){
								$serredsen='DAD';	
							}else{
								$serredsen='ADA';
							}
						}else{
							if($typ=="MTY-APTO"){
								$serredsen='DA';
							}else{
								$serredsen='AD';
							}
						}
			 	   	}			
				$direccion = '';
				$direccion=$value->addresses;
				//separa las direcciones apartir de una coma
				$direc=explode(",", $direccion);
					for($i=0;$i<count($direc);$i++){
						if(substr($direc[$i],0,1)=='a'){
							$address=Address::find_by_id(substr($direc[$i],1));
							$sub=Suburb::find_by_id($address->suburb_id);
							$a='d';
							break;
					}else{
							$a='c';
							$sub=Suburb::find_by_id(substr($direc[$i],1));
			
					}
		
				}
								
				foreach($ser as $s){
					$u=User::find_by_id($s->user_id);
					if(!$u){
				     	continue;
				     }
					if($s->id>0 && $s->id!=null){
						$cot++;
						$ct++;
						if($s->tnueva>0){
        				    $prueba->setActiveSheetIndex(0)->getStyle('A'.$cot)->applyFromArray(
                                array(
                                    'fill' => array(
                                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                        'color' => array('rgb' => '428BCA')
                                    )
                                )
                            );
        				}
						$prueba->setActiveSheetIndex(0)->setCellValue("A".$cot,$ct);
						// <!--muestra la fecha en 00:00-->
						$prueba->setActiveSheetIndex(0)->setCellValue("B".$cot,substr($value->reservation_time,0,5)); 
						$prueba->setActiveSheetIndex(0)->setCellValue("C".$cot,$s->id);									
						$namefull=$u->name;
						
							if($u->business_id){
									//si el usuario tiene convenio se selecciona el servicio particular	
									$emp2=User::find("all",array("conditions"=>"id='".$u->business_id."'")); 
									if(!count($emp2)){
										$emp="PAR";
									}else{
										$emp=User::find($u->business_id);
										if($emp->id==55752){
    										/*$prueba->setActiveSheetIndex(0)->getStyle('A'.$cot)->applyFromArray(
                                                array(
                                                    'fill' => array(
                                                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                                        'color' => array('rgb' => '428BCA')
                                                    )
                                                )
                                            );*/
										}
										$emp=$emp->name;
									}	
							}
							else
								$emp="PAR"; 
						$contname=strlen($namefull);
						$prueba->setActiveSheetIndex(0)->setCellValue("D".$cot,$namefull);
						
					    if($s->user_id=='53380'){
						        $streetfull=explode("Entre Calles:",explode("Calle:",$s->annotations)[1])[0];
						    }else{
								$streetfull = '';
						        $streetfull=$address->street;
							    $contstreet=strlen($streetfull);
							    //si la calle de la dirección tiene más de 25 caracteres, solo se muestran los 25 principales
							   
						    }
					    //si la calle de la dirección tiene más de 25 caracteres, solo se muestran los 25 principales
						$prueba->setActiveSheetIndex(0)->setCellValue("E".$cot,$streetfull);
						$subfull=$sub->suburb;
					    $contsub=strlen($subfull);
					   
						   if ($contsub>=29) {
								$prueba->setActiveSheetIndex(0)->setCellValue("F".$cot,substr($subfull, 0, 29).'.');	
						   }else{
								$prueba->setActiveSheetIndex(0)->setCellValue("F".$cot,$subfull);	
						   }
						   if(substr($s->cost, 0,1)=="m"){
						    $s->cost=substr($s->cost,1);
					    }
						$prueba->setActiveSheetIndex(0)->setCellValue("G".$cot,substr($emp,0,3));
					
						$prueba->setActiveSheetIndex(0)->setCellValue("H".$cot,$vel);	
						$prueba->setActiveSheetIndex(0)->setCellValue("I".$cot,"$".$s->cost);	
						$prueba->setActiveSheetIndex(0)->setCellValue("J".$cot,$unity->economic."-".$oper->name." ".$oper->lastname);
						$pay=$s->payment;
						if($pay=='Tarjeta de Credito/Debito')$pay='TC';
						else if($pay=='Cortesia')$pay='CO';
						else if($pay=='Firma por cobrar')$pay='FxC';
						else if($pay=='Efectivo MN')$pay='EF';
						$prueba->setActiveSheetIndex(0)->setCellValue("K".$cot,$pay);	
						if($a=='d'){
							$prueba->setActiveSheetIndex(0)->setCellValue("L".$cot,$address->telephones);
						}
						$prueba->setActiveSheetIndex(0)->setCellValue("M".$cot,$serredsen);	
						}
					}	
				}
            }
        }
            
		$objWriter = PHPExcel_IOFactory::createWriter($prueba, 'Excel2007');
		if("MTY-APTO"==$typ){
			$objWriter->save('excels/bitacora'.$fecha.'.xlsx'); 
			echo "<a href='/excels/bitacora".$fecha.".xlsx'>Descargar</a>";
		}else{
			$objWriter->save('excels/bitacora2'.$fecha.'.xlsx'); 
			echo "<a href='/excels/bitacora2".$fecha.".xlsx'>Descargar</a>";
		}
	}
	public static function xlscan($fecha1,$fecha2){
		include 'PHPExcel/PHPExcel.php'; //libreria de excel
		$prueba = new PHPExcel();  //se crea un achivo de excel
		$titulo="hoja de calculo";
		$prueba->getActiveSheet()->setTitle($titulo);
		/*$prueba->setActiveSheetIndex(0)->setCellValue("A".$c,"TC") //se llenan los campos de un archivo de excel
									->setCellValue("B".$c,$serw->id)
									->setCellValue("C".$c,$serw->cost); */
		
		$fecha1 = date("Y-m-d", strtotime($fecha1));
		$fecha2 = date("Y-m-d", strtotime($fecha2));
		
		$ope=Operator::find('all',array("conditions"=>"enable=1"));
		$cof=array(0=>array('Cancelados','Sencillas','Redondas'));
		$c=1;
		$system=Systemuser::find('all');
		foreach($system as $s){
			if($s->group=='SUPER RESERVACIONES'||$s->group=='RESERVACIONES'||$s->group=='ADMINISTRADOR'){
				$n=$s->name;
				$n2=$s->name." ".$s->lastname;
				$reser=Canceled::find("all", array("conditions" =>"(canceled_by='".$n."' or canceled_by='".$n2."') AND created_at BETWEEN {ts '".$fecha1." 00:00:00'} AND {ts '".$fecha2." 23:59:59'}"));
				$r2=0;
				$r1=0;
				foreach($reser as $s){
					if($s->reservation2){
						$r2++;
					}else{
						$r1++;
					}
				}
				if($r1 || $r2){
					$ope=array($n, $r1,$r2);
					$cof[$c]=$ope;
					$c++;
				}
			
			}
		} 
		$c=1;
		$a=0;
		foreach($cof as $da){
			foreach($da as $d){
				switch ($a) {
					case 0:
						$er="A";
						break;
					case 1:
						$er="B";
						break;
					case 2:
						$er="C";
						break;
					
					
				}
				$a++;
				$prueba->setActiveSheetIndex(0)->setCellValue($er.$c,$d); //se llenan los campos de un archivo de excel
										
			}
			$a=0;
			$c++;
		}
		
		$objWriter = PHPExcel_IOFactory::createWriter($prueba, 'Excel2007');
		$objWriter->save('excels/cancelados.xlsx'); 
		echo "<a href='/excels/cancelados.xlsx'>Descargar</a>";
	}
	public static function xlsccall($fecha1,$fecha2){
		include 'PHPExcel/PHPExcel.php'; //libreria de excel
		$prueba = new PHPExcel();  //se crea un achivo de excel
		
		
		$titulo="hoja de calculo";
		$prueba->getActiveSheet()->setTitle($titulo);
		/*$prueba->setActiveSheetIndex(0)->setCellValue("A".$c,"TC") //se llenan los campos de un archivo de excel
									->setCellValue("B".$c,$serw->id)
									->setCellValue("C".$c,$serw->cost); */
		
		$fecha1 = date("Y-m-d", strtotime($fecha1));
		$fecha2 = date("Y-m-d", strtotime($fecha2));
		
		$ope=Operator::find('all',array("conditions"=>"enable=1"));
		$cof=array(0=>array('Call center','Sencillas','Redondas'));
		$c=1;
		$system=Systemuser::find('all');
		foreach($system as $s){
			if($s->group=='SUPER RESERVACIONES'||$s->group=='RESERVACIONES'||$s->group=='ADMINISTRADOR'){
				$n=$s->name;
				$n2=$s->name." ".$s->lastname;
				$reser=Service::find("all", array("conditions" =>"(attended='".$n."' or attended='".$n2."') AND created_at BETWEEN {ts '".$fecha1." 00:00:00'} AND {ts '".$fecha2." 23:59:59'}"));
				$r2=0;
				$r1=0;
				foreach($reser as $s){
					if($s->reservation2){
						$r2++;
					}else{
						$r1++;
					}
				}
				if($r1 || $r2){
					$ope=array($n, $r1,$r2);
					$cof[$c]=$ope;
					$c++;
				}
			
			}
		} 
		$c=1;
		$a=0;
		foreach($cof as $da){
			foreach($da as $d){
				switch ($a) {
					case 0:
						$er="A";
						break;
					case 1:
						$er="B";
						break;
					case 2:
						$er="C";
						break;	
				}
				$a++;
				$prueba->setActiveSheetIndex(0)->setCellValue($er.$c,$d); //se llenan los campos de un archivo de excel
										
			}
			$a=0;
			$c++;
		}
		$objWriter = PHPExcel_IOFactory::createWriter($prueba, 'Excel2007');
		$objWriter->save('excels/reservaciones.xlsx'); 
		echo "<a href='/excels/reservaciones.xlsx'>Descargar</a>";
	}
	public static function xlsc($fecha1,$fecha2){
		include 'PHPExcel/PHPExcel.php'; //libreria de excel
		$prueba = new PHPExcel();  //se crea un achivo de excel
		
		
		$titulo="hoja de calculo";
		$prueba->getActiveSheet()->setTitle($titulo);
		/*$prueba->setActiveSheetIndex(0)->setCellValue("A".$c,"TC") //se llenan los campos de un archivo de excel
									->setCellValue("B".$c,$serw->id)
									->setCellValue("C".$c,$serw->cost); */
		
		$fecha1 = date("Y-m-d", strtotime($fecha1));
		$fecha2 = date("Y-m-d", strtotime($fecha2));
		$ope=Operator::find('all',array("conditions"=>"enable=1"));
		$coferes=array(0=>array('Operadores','Directos','Regresos'));
		
		$c=1;
		foreach ($ope as $o) {
			$ass=Assignment::find("all",array("conditions"=>"(operator_id=".$o->id." AND (type='Directo' || type='')) AND (created_at BETWEEN {ts'".$fecha1." 00:00'} AND {ts'".$fecha2." 23:59'})"));
			$as=Assignment::find("all",array("conditions"=>"(operator_id=".$o->id." AND type='Regreso') AND (created_at BETWEEN {ts'".$fecha1." 00:00'} AND {ts'".$fecha2." 23:59'})"));
			if(count($ass) || count($as)){
				$ope=array($o->name." ".$o->lastname, count($ass),count($as));
				$coferes[$c]=$ope;
				$c++;
			}
		}
		$c=1;
		$a=0;
		foreach($coferes as $da){
			foreach($da as $d){
				switch ($a) {
					case 0:
						$er="A";
						break;
					case 1:
						$er="B";
						break;
					case 2:
						$er="C";
						break;
				}
				$a++;
				$prueba->setActiveSheetIndex(0)->setCellValue($er.$c,$d); //se llenan los campos de un archivo de excel
										
			}
			$a=0;
			$c++;
		}
		
		$objWriter = PHPExcel_IOFactory::createWriter($prueba, 'Excel2007');
		$objWriter->save('excels/vueltas.xlsx'); 
		echo "<a href='/excels/vueltas.xlsx'>Descargar</a>";
	}
	public static function scriptcan(){
		?>
	<script type="text/javascript">
	google.charts.load('visualization', '1', {'packages':['corechart']});
      
	  // Set a callback to run when the Google Visualization API is loaded.
		google.charts.setOnLoadCallback(drawChart);
      
	    function drawChart() {
    	var f1=jQuery("#can1").val();
    	var f2=jQuery("#can2").val();
    	var ur="/reportes/can/"+f1+"/"+f2;
	      var jsonData = jQuery.ajax({
	          url: ur,
	          dataType:"json",
	          async: false
	          }).responseText;
	
	          jsonData = JSON.parse(jsonData);
	      // Create our data table out of JSON data loaded from server.
	
			var opciones = {
	        title: 'Cancelados',
	
	        width:800,
	        height:500
	    };
	    var data = google.visualization.arrayToDataTable(jsonData);
	
	      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.ColumnChart(document.getElementById('grrango'));
	      chart.draw(data, opciones);
	    }
    </script>
    <?php
	}
	public static function scriptcall(){
		?>
	<script type="text/javascript">
	 
	google.charts.load('visualization', '1', {'packages':['corechart']});
      
    // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);
      
	    function drawChart() {
    	var f1=jQuery("#fech1").val();
    	var f2=jQuery("#fech2").val();
    	var ur="/reportes/call/"+f1+"/"+f2;
	      var jsonData = jQuery.ajax({
	          url: ur,
	          dataType:"json",
	          async: false
	          }).responseText;
	
	          jsonData = JSON.parse(jsonData);
	      // Create our data table out of JSON data loaded from server.
	
			var opciones = {
	        title: 'Reservaciones',
	
	        width:800,
	        height:500
	    };
	    var data = new google.visualization.arrayToDataTable(jsonData);
	
	      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.ColumnChart(document.getElementById('grrango'));
	      chart.draw(data, opciones);
	    }
    </script>
    <?php
	}
	public static function script(){
		?>
	<script type="text/javascript">
	 
	 google.charts.load('visualization', '1', {'packages':['corechart']});
      
	  // Set a callback to run when the Google Visualization API is loaded.
		google.charts.setOnLoadCallback(drawChart);
	      
	    function drawChart() {
    	var f1=jQuery("#fecha1").val();
    	var f2=jQuery("#fecha2").val();
    	var ur="/reportes/choferes/"+f1+"/"+f2;
	      var jsonData = jQuery.ajax({
	          url: ur,
	          dataType:"json",
	          async: false
	          }).responseText;
	
	          jsonData = JSON.parse(jsonData);
	      // Create our data table out of JSON data loaded from server.
	
			var opciones = {
	        title: 'Vueltas',
	
	        width:600,
	        height:2000
	    };
	    var data = google.visualization.arrayToDataTable(jsonData);
	
	      // Instantiate and draw our chart, passing in some options.
	      var chart = new google.visualization.BarChart(document.getElementById("grrango"));
	      chart.draw(data, opciones);
	    }
    </script>
    <?php
	}
	public static function choferes($fecha1,$fecha2){
		$fecha1 = date("Y-m-d", strtotime($fecha1));
		$fecha2 = date("Y-m-d", strtotime($fecha2));
		$ope=Operator::find('all',array("conditions"=>"enable=1"));
		$coferes=array(0=>array('Operadores','Directos','Regresos'));
		
		$c=1;
		foreach ($ope as $o) {
			$ass=Assignment::find("all",array("conditions"=>"(operator_id=".$o->id." AND (type='Directo' || type='')) AND (created_at BETWEEN {ts'".$fecha1." 00:00'} AND {ts'".$fecha2." 23:59'})"));
			$as=Assignment::find("all",array("conditions"=>"(operator_id=".$o->id." AND type='Regreso') AND (created_at BETWEEN {ts'".$fecha1." 00:00'} AND {ts'".$fecha2." 23:59'})"));
			if(count($ass) || count($as)){
				$ope=array($o->name." ".$o->lastname, count($ass),count($as));
				$coferes[$c]=$ope;
				$c++;
			}
		}
		echo json_encode($coferes);
		
	}
	public static function can($fecha1,$fecha2){
		$fecha1 = date("Y-m-d", strtotime($fecha1));
		$fecha2 = date("Y-m-d", strtotime($fecha2));
		$cof=array(0=>array('Call center','Sencillas','Redondas'));
		$c=1;
		$system=Systemuser::find('all');
		foreach($system as $s){
			if($s->group=='SUPER RESERVACIONES'||$s->group=='RESERVACIONES'||$s->group=='ADMINISTRADOR'){
				$n=$s->name;
				$n2=$s->name." ".$s->lastname;
				
				$reser=Canceled::find("all", array("conditions" =>"(attended='".$n."' or attended='".$n2."') AND created_at BETWEEN {ts '".$fecha1." 00:00:00'} AND {ts '".$fecha2." 23:59:59'}"));
				$r2=0;
				$r1=0;
				foreach($reser as $s){
					if($s->reservation2){
						$r2++;
					}else{
						$r1++;
					}
				}
				if($r1 || $r2){
					$ope=array($n, $r1,$r2);
					$cof[$c]=$ope;
					$c++;
				}
			
			}
		}
		//$reser=Reservation::find("all", array("conditions" =>"reservation_date BETWEEN ={ts '".$fecha1." 00:00:00'} AND ={ts '".$fecha2." 23:59:59'}"));
		echo json_encode($cof);
		
		
		
	}
		public static function call($fecha1,$fecha2){
		$fecha1 = date("Y-m-d", strtotime($fecha1));
		$fecha2 = date("Y-m-d", strtotime($fecha2));
		$cof=array(0=>array('Call center','Sencillas','Redondas'));
		$c=1;
		$system=Systemuser::find('all');
		foreach($system as $s){
			if($s->group=='SUPER RESERVACIONES'||$s->group=='RESERVACIONES'||$s->group=='ADMINISTRADOR'){
				$n=$s->name;
				$n2=$s->name." ".$s->lastname;
				$reser=Service::find("all", array("conditions" =>"(attended='".$n."' or attended='".$n2."') AND created_at BETWEEN {ts '".$fecha1." 00:00:00'} AND {ts '".$fecha2." 23:59:59'}"));
				$r2=0;
				$r1=0;
				foreach($reser as $s){
					if($s->reservation2){
						$r2++;
					}else{
						$r1++;
					}
				}
				if($r1 || $r2){
					$ope=array($n, $r1,$r2);
					$cof[$c]=$ope;
					$c++;
				}
			
			}
		}
		//$reser=Reservation::find("all", array("conditions" =>"reservation_date BETWEEN ={ts '".$fecha1." 00:00:00'} AND ={ts '".$fecha2." 23:59:59'}"));
		echo json_encode($cof);
		
		
		
	}
	public function pdf($tipo,$fecha){
		if($tipo=='APTO-MTY'){
			$bittip= '(BITACORA LLEGADAS)';
		}else{
			$bittip= '(BITACORA SALIDAS)';
		}
		$hoy=date('Y-m-d');
		$html='<style>
			tr,th,td{
				border-bottom: 1px solid #dddddd;
				padding-right: 5px;
				padding-bottom: 3.9px;
				padding-top: 3.9px;
			}
			</style>
			<table class="table table-bordered" style="font-size: 10px;">
						<thead>
						<tr><td colspan="4" style="margin=0px;float:left;padding-bottom: 0px;"></td><td colspan="4"><table style="text-align: center;width: 100%;"><tbody><tr><td style="font-size:20px;border-bottom: 2px solid black;">Asignacion de Servicios</td></tr><tr><td style="text-aling:center;border-bottom: 0px;">';
			$html=$html.$bittip.'</td></tr></tbody></table></td>
			<td colspan="5" style="text-aling:right!important;vertical-aling:bottom!important;font-size: 10px;"> Para el dia: ';
			$html=$html.$fecha.' </td>
			</tr>
						
				  <tr style="text-align: center">
						<th>ID</th>
						<th>HORA</th>
						<th>RES.</th>
						<th>CLIENTE</th>
						<th>CALLE Y NUM.</th>
						<th>COLONIA</th>
						<th>EMP.</th>
						<th>T. DE V.</th>
						<th>COSTO</th>
						<th>U. DE SERVICIO</th>
						<th>PAGO</th>
						<th>TELÉFONO</th>
						<th>TIPO</th>
			        </tr>
			      </thead>
			<tbody>';
			$diac=strtotime('-1day', strtotime($hoy));
			$diac=date('Y-m-d', $diac);
			$diag=strtotime('+1 day', strtotime($diac));
			$diag=date('Y-m-d', $diag);
			if(!empty($fecha)){
				$reser=Reservation::find('all', array('order' => 'reservation_time','conditions' =>' reservation_date = {ts "'.$fecha.'"} AND type="'.$tipo.'"'));
			}else{
				$reser=Reservation::find('all', array('order' => 'reservation_time','conditions' =>' reservation_date = {ts "'.$diac.'"} AND type="'.$tipo.'"'));
			}
			$cot=0;
			foreach($reser as $value){
				if($value->id){
				if($value->type==$tipo){
					if($value->vehicle_id==1){
						$vel="AUT";
					}else{
						$vel="CAM";
					}
					$assig=Assignment::find_by_reservation_id($value->id);
					$unity=Unity::find_by_id($assig->unity_id);
					$oper=Operator::find_by_id($assig->operator_id);
					$query='select * from services where reservation1='.$value->id;
					$ser=Service::find_by_sql($query);
					if(!$ser){
						$query2='select * from services where reservation2='.$value->id;
						$ser=Service::find_by_sql($query2);
						if($tipo=="MTY-APTO"){
							$serredsen='ADA';
						}else{
							$serredsen='DAD';
						}
					}else{
						if($ser->reservation2==""){
							if($tipo=="MTY-APTO"){
								$serredsen='DA';	
							}else{
								$serredsen='AD';
							}
						}else{
							if($tipo=="MTY-APTO"){
								$serredsen='DAD';
							}else{
								$serredsen='ADA';
							}
						}
			 	   	}	
			 	   	$direccion = '';
					$direccion=$value->addresses;
					$direc=explode(",", $direccion);
					for($i=0;$i<count($direc);$i++){
						if(substr($direc[$i],0,1)=='a'){
							$address=Address::find_by_id(substr($direc[$i],1));
							$sub=Suburb::find_by_id($address->suburb_id);
							$a='d';
							break;
						}else{
							$a='c';
							$sub=Suburb::find_by_id(substr($direc[$i],1));
						}
		
					}		
					foreach($ser as $s){
						$u=User::find_by_id($s->user_id);
						if(!$u){
					     	continue;
					     }
						if($s->id>0 && $s->id!=null){
							$cot++;
							$html=$html."<tr><td style='padding-right: 10px;'>".$cot."</td><td>".substr($value->reservation_time,0,5)."</td><td style='padding-right: 10px;'>".$s->id."</td><td style='padding-right: 10px;'>";
									$pay=$s->payment;
									if($pay=='Tarjeta de Credito/Debito')$pay='TC';
									else if($pay=='Cortesia')$pay='CO';
									else if($pay=='Firma por cobrar')$pay='FxC';
									else if($pay=='Efectivo MN')$pay='EF';
									$namefull=$u->name;
									if($u->business_id){
									//si el usuario tiene convenio se selecciona el servicio particular	
											$emp2=User::find("all",array("conditions"=>"id='".$u->business_id."'")); 
											if(!count($emp2)){
												$emp="PAR";
											}else{
												$emp=User::find($u->business_id); 
												$emp=$emp->name;
											}	
									}
									else
										$emp="PAR"; 
									$contname=strlen($namefull);
										if ($contname>=20) {
											$html=$html.substr($namefull, 0, 20).'.';
										}else{
											$html=$html.$namefull;
										}
        						$html=$html."</td><td style='padding-right: 10px;'>";
        						if($s->user_id=='53380'){
        						         $html=$html.explode("Entre Calles:",explode("Calle:",$s->annotations)[1])[0];
        						    }else{
        								$streetfull = '';
        							    $streetfull=$address->street;;
        							    $contstreet=strlen($streetfull);
        							    //si la calle de la dirección tiene más de 25 caracteres, solo se muestran los 25 principales
        							   if ($contstreet>=33) {
        										   $html=$html.substr($streetfull, 0, 33).'.';
        							   }else{
        									   	   $html=$html.$streetfull;
        							   }
        						    }
							 
							 $html=$html." </td><td style='padding-right: 10px;'>";
							$subfull=$sub->suburb;
								  $contsub=strlen($subfull);
									   if ($contsub>=33) {
										   $html=$html.substr($subfull, 0, 33).'.';
									   }else{
									   	   $html=$html.$subfull;
									   }
									    if(substr($s->cost, 0,1)=="m"){
										$s->cost=substr($s->cost,1);
								       }
									   
						$html=$html."</td><td>".substr($emp,0,3)."</td><td style='padding-right: 10px;'>".$vel."</td><td style='padding-right: 10px;'> $".htmlentities($s->cost)."</td><td style='padding-right: 10px;'>".$unity->economic."-";
						$fulloper=$oper->name ." ". $oper->lastname ;
								  $contoper=strlen($fulloper);
									   if ($contoper>=15) {
										   $html=$html.substr($fulloper, 0, 15).'.';
									   }else{
									   	   $html=$html.$fulloper;
									   }
						      $html=$html."</td><td style='padding-right: 10px;'>";
						 $html=$html.$pay;
						$html=$html."</td><td style='padding-right: 10px;'>";
							if($a=='d'){
									$contphone=strlen($address->telephones);
								if($contphone>10){
									$html=$html.substr($fulloper, 0, 10);
								}else{
									$html=$html.$address->telephones;
								}
							}
							$html=$html."</td><td style='padding-right: 10px;'>".$serredsen."</td></tr>";
						}
					}
				
				}
            }
        }
		$html=$html."</tbody></table>";
    	pdfDriver::doPDFG('ejemplo',$html,false,"",false,"L");
	}
	
	function bitaco($fecha,$page,$tem){
		templateDriver::setData("fecha",  $fecha);
		templateDriver::setData("page",  $page);
		templateDriver::setData("temp",  $tem);
		templateDriver::renderSection("reports.".$tem);
	}
	function cargar($temp,$tipo,$id=-1){
		templateDriver::setData("modo", array("modo" => $tipo));
		templateDriver::setData("id", array("id" => $id));
		templateDriver::renderSection("reports.".$temp);
	}
	function template($tmp,$page=1){
		templateDriver::setData("temp", $tmp);
		templateDriver::setData("page", $page);
		templateDriver::renderSection("reports.".$tmp);
	}
	function sevendays($modo,$id){
		$arra=array();
		$hoy=date("Y-m-d");
		if($modo=="dia"){
			
			$user=Systemuser::find('all',array('conditions'=>'enable=1'));
			$cu=1;
			$arra[0][0]="dia";
			foreach($user as $u){
				$arra[0][$cu]=$u->name.' '.$u->lastname;
				$co=1;
				for($i=6;$i>=0;$i--){
					$diac=strtotime("-".$i."day", strtotime($hoy));
					$diac=date("Y-m-d", $diac);
					$diag=strtotime("+1 day", strtotime($diac));
					$diag=date("Y-m-d", $diag);
					
					//muestra los servicios de un día anterior y uno despues con el nombre de por quien fue atendido
					$query='select * from services where  (created_at BETWEEN {ts "'.$diac.'"} AND {ts "'.$diag.'"}) AND (attended="'.$u->name.' '.$u->lastname.'" or attended="'.$u->name.'")';
					$ser=Service::find_by_sql($query);
					
					$c=count($ser);
					$diac = new DateTime($diac);
					$diac=$diac->format('D');
					switch ($diac) {
						case 'Mon':
							$d="Lunes";
							break;
						case 'Tue':
							$d="Martes";
							break;
						case 'Wed':
							$d="Miercoles";
							break;
						case 'Thu':
							$d="Jueves";
							break;
						case 'Fri':
							$d="Viernes";
							break;
						case 'Sat':
							$d="Sabado";
							break;
						case 'Sun':
							$d="Domingo";
							break;
						
					}
					$arra[$co][0]=$d;
					$arra[$co][$cu]=$c;
					$cont++;
					$co++;
				}
				if($co==8)
				$co=0;
				$cu++;
			}
		}else if($modo=="can"){
			$user=Systemuser::find('all',array('conditions'=>'enable=1'));
			$cu=1;
			$arra[0][0]="dia";
			foreach($user as $u){
				$arra[0][$cu]=$u->name.' '.$u->lastname;
				$co=1;
				for($i=6;$i>=0;$i--){
					$diac=strtotime("-".$i."day", strtotime($hoy));
					$diac=date("Y-m-d", $diac);
					$diag=strtotime("+1 day", strtotime($diac));
					$diag=date("Y-m-d", $diag);
					
					//muestra las canceladas con el nombre del usuario que lo realizo
					$query='select * from canceleds where  (created_at BETWEEN {ts "'.$diac.'"} AND {ts "'.$diag.'"}) AND (attended="'.$u->name.' '.$u->lastname.'" or attended="'.$u->name.'")';
					$ser=Service::find_by_sql($query);
					
					$c=count($ser);
					$diac = new DateTime($diac);
					$diac=$diac->format('D');
					switch ($diac) {
						case 'Mon':
							$d="Lunes";
							break;
						case 'Tue':
							$d="Martes";
							break;
						case 'Wed':
							$d="Miercoles";
							break;
						case 'Thu':
							$d="Jueves";
							break;
						case 'Fri':
							$d="Viernes";
							break;
						case 'Sat':
							$d="Sabado";
							break;
						case 'Sun':
							$d="Domingo";
							break;
						
					}
					$arra[$co][0]=$d;
					$arra[$co][$cu]=$c;
					$cont++;
					$co++;
				}
				if($co==8)
				$co=0;
				$cu++;
			}
		}else{
				$diac=strtotime("-6day", strtotime($hoy));
				$diac=date("Y-m-d", $diac);
				$hoy=strtotime("+1day", strtotime($hoy));
				$hoy=date("Y-m-d", $hoy);
				
			if($modo=="user"){
				if($id!=="-1"){
					
				array_push($arra,array(" "," "));
				//selecciona los servicios fueron atendidos de hoy a seis dias átras
				$query='select attended from services where created_at BETWEEN {ts "'.$diac.'"} AND {ts "'.$hoy.'"} AND attended= '.$id;
				}else 
					$query='select attended from services where created_at BETWEEN {ts "'.$diac.'"} AND {ts "'.$hoy.'"}';
				

				$ser=Service::find_by_sql($query);
				$categorias=array("Usuario","Servicios");
				$arra=array($categorias);
				foreach ($ser as $key) {
					$d=0;
					for($i=0;$i<count($ser);$i++){
								
						if($key->attended==$ser[$i]->attended){
							$d++; //cuantos servicios se han atendido por el mismo usuario
						}
					}
					$user=Systemuser::find_by_id($key->attended);
					//si el valor NO existe en el array, SE GUARDA
					if(!in_array(array("#".$key->attended." ".$user->name." ".$user->lastname,$d),$arra))
					array_push($arra,array("#".$key->attended." ".$user->name." ".$user->lastname,$d));// se guarda
					
				}
			
			}else if($modo=="col"){
				$query='select * from services where created_at BETWEEN {ts "'.$diac.'"} AND {ts "'.$hoy.'"}';
				$ser=Service::find_by_sql($query);
				$s=array();
				$suburbs=array();
				$categorias=array("Colonia","Servicios");
				$arra=array($categorias);
				foreach ($ser as $key) {
					
					array_push($suburbs,$this->extcolon($key->reservation1));
					if(!empty($key->reservation2)){
						array_push($suburbs,$this->extcolon($key->reservation2));
					}
				}
				for($i=0;$i<count($suburbs);$i++){
					$d=0;
					for($j=0;$j<count($suburbs);$j++){
						if($id!="-1"){
							if($suburbs[$i][0]==$suburbs[$j][0]){ //compara un array, las filas con las columnas
								if($suburbs[$i][0]==$id)
									$d++;
								else{
									$d="nada";
								}
							}
						}else{
							if($suburbs[$i][0]==$suburbs[$j][0]){
								$d++;
							}
						}
					}
					if($d!="nada"){
						$col=Suburb::find_by_id($suburbs[$i][0]);
						if(!in_array(array($col->suburb,$d),$arra))
						array_push($arra,array($col->suburb,$d));
					}
				}
			}
			if($id!="-1"){
				array_push($arra,array(" "," "));
			} 
		} 
		echo json_encode($arra);
		
		
	}
	function fifteendays($modo,$id){
		$arra=array();
		$hoy=date("Y-m-d");
		if($modo=="dia"){
			
			$user=Systemuser::find('all',array('conditions'=>'enable=1'));
			$cu=1;
			$arra[0][0]="dia";
			foreach($user as $u){
				$arra[0][$cu]=$u->name.' '.$u->lastname;
				$co=1;
				for($i=14;$i>=0;$i--){
					$diac=strtotime("-".$i."day", strtotime($hoy));
					$diac=date("Y-m-d", $diac);
					$diag=strtotime("+1 day", strtotime($diac));
					$diag=date("Y-m-d", $diag);
					
					
					$query='select * from services where  (created_at BETWEEN {ts "'.$diac.'"} AND {ts "'.$diag.'"}) AND (attended="'.$u->name.' '.$u->lastname.'" or attended="'.$u->name.'")';
					$ser=Service::find_by_sql($query);
					
					$c=count($ser);
					$diac = new DateTime($diac);
					$diac=$diac->format('D');
					switch ($diac) {
						case 'Mon':
							$d="Lunes";
							break;
						case 'Tue':
							$d="Martes";
							break;
						case 'Wed':
							$d="Miercoles";
							break;
						case 'Thu':
							$d="Jueves";
							break;
						case 'Fri':
							$d="Viernes";
							break;
						case 'Sat':
							$d="Sabado";
							break;
						case 'Sun':
							$d="Domingo";
							break;
						
					}
					$arra[$co][0]=$d;
					$arra[$co][$cu]=$c;
					$cont++;
					$co++;
				}
				if($co==16)
				$co=0;
				$cu++;
			}
		}else if($modo=="can"){
			$user=Systemuser::find('all',array('conditions'=>'enable=1'));
			$cu=1;
			$arra[0][0]="dia";
			foreach($user as $u){
				$arra[0][$cu]=$u->name.' '.$u->lastname;
				$co=1;
				for($i=14;$i>=0;$i--){
					$diac=strtotime("-".$i."day", strtotime($hoy));
					$diac=date("Y-m-d", $diac);
					$diag=strtotime("+1 day", strtotime($diac));
					$diag=date("Y-m-d", $diag);
					
					
					$query='select * from canceleds where  (created_at BETWEEN {ts "'.$diac.'"} AND {ts "'.$diag.'"}) AND (attended="'.$u->name.' '.$u->lastname.'" or attended="'.$u->name.'")';
					$ser=Service::find_by_sql($query);
					
					$c=count($ser);
					$diac = new DateTime($diac);
					$diac=$diac->format('D');
					switch ($diac) {
						case 'Mon':
							$d="Lunes";
							break;
						case 'Tue':
							$d="Martes";
							break;
						case 'Wed':
							$d="Miercoles";
							break;
						case 'Thu':
							$d="Jueves";
							break;
						case 'Fri':
							$d="Viernes";
							break;
						case 'Sat':
							$d="Sabado";
							break;
						case 'Sun':
							$d="Domingo";
							break;
						
					}
					$arra[$co][0]=$d;
					$arra[$co][$cu]=$c;
					$cont++;
					$co++;
				}
				if($co==16)
				$co=0;
				$cu++;
			} 
				}else{
				array_push($arra,array(" "," "));
				$diac=strtotime("-14day", strtotime($hoy));
				$diac=date("Y-m-d", $diac);
				$de=date("Y-m-d G:i",strtotime($hoy." 12:34"));
				$hoy=strtotime("+1day", strtotime($hoy));
				$hoy=date("Y-m-d", $hoy);
				
			if($modo=="user"){
				
				if($id!=="-1"){
					$query='select attended from services where created_at BETWEEN {ts "'.$diac.'"} AND {ts "'.$hoy.'"} AND attended= '.$id;
				array_push($arra,array(" "," "));
				}else 
					$query='select attended from services where created_at BETWEEN {ts "'.$diac.'"} AND {ts "'.$hoy.'"}';
				
				$ser=Service::find_by_sql($query);
				$categorias=array("Usuario","Servicios");
				$arra=array($categorias);
				foreach ($ser as $key) {
					$d=0;
					for($i=0;$i<count($ser);$i++){
						
								
						if($key->attended==$ser[$i]->attended){
							$d++;
						}
					}
					$user=Systemuser::find_by_id($key->attended);
					if(!in_array(array("#".$key->attended." ".$user->name." ".$user->lastname,$d),$arra))
					array_push($arra,array("#".$key->attended." ".$user->name." ".$user->lastname,$d));
					
				}
			
			}else if($modo=="col"){
				$query='select * from services where created_at BETWEEN {ts "'.$diac.'"} AND {ts "'.$hoy.'"}';
				$ser=Service::find_by_sql($query);
				$s=array();
				$suburbs=array();
				$categorias=array("Colonia","Servicios");
				$arra=array($categorias);
				foreach ($ser as $key) {
					
					array_push($suburbs,$this->extcolon($key->reservation1));
					if(!empty($key->reservation2)){
						array_push($suburbs,$this->extcolon($key->reservation2));
					}
				}
				for($i=0;$i<count($suburbs);$i++){
					$d=0;
					for($j=0;$j<count($suburbs);$j++){
						if($id!="-1"){
							if($suburbs[$i][0]==$suburbs[$j][0]){
								if($suburbs[$i][0]==$id)
									$d++;
								else{
									$d="nada";
								}
							}
						}else{
							if($suburbs[$i][0]==$suburbs[$j][0]){
								$d++;
							}
						}
					}
					if($d!="nada"){
						$col=Suburb::find_by_id($suburbs[$i][0]);
						if(!in_array(array($col->suburb,$d),$arra))
						array_push($arra,array($col->suburb,$d));
					}
				}
			}
			if($id!="-1"){
				array_push($arra,array(" "," "));
			} 
		} 
		echo json_encode($arra);
		
		
		
	}
	function difechas($f1,$f2,$modo,$id){
		$er=array();
		$dia=array();
		$value=array();
		$datetime1 = new DateTime($f1);
		$datetime2 = new DateTime($f2);
		$interval = $datetime1->diff($datetime2);
		$r= $interval->format('%R%a');
		
		$hoy=$datetime2->format('Y-m-d');
		
		if($modo=="dia"){

		$categorias=array("Dia","Servicios");
		$arra=array($categorias);
		$cont=0;
		for($i=$r;$i>=0;$i--){
			$diac=strtotime("-".$i."day", strtotime($hoy));
			$diac=date("Y-m-d", $diac);
			$diag=strtotime("+1 day", strtotime($diac));
			$diag=date("Y-m-d", $diag);
			
			
			$query='select * from services where created_at BETWEEN {ts "'.$diac.'"} AND {ts "'.$diag.'"}';
			$ser=Service::find_by_sql($query);
			
			$c=(double) count($ser);
			array_push($arra,array($diac,$c));
			
			$cont++;
		}
		}else if($modo=="can"){
			$categorias=array("Dia","Servicios");
		$arra=array($categorias);
		$cont=0;
		for($i=$r;$i>=0;$i--){
			$diac=strtotime("-".$i."day", strtotime($hoy));
			$diac=date("Y-m-d", $diac);
			$diag=strtotime("+1 day", strtotime($diac));
			$diag=date("Y-m-d", $diag);
			
			
			$query='select * from canceleds where created_at BETWEEN {ts "'.$diac.'"} AND {ts "'.$diag.'"}';
			$ser=Service::find_by_sql($query);
			
			$c=(double) count($ser);
			array_push($arra,array($diac,$c));
			
			$cont++;
		}
		}else{
			
			$diag=$datetime2->format('Y-m-d');
			$diac=$datetime1->format('Y-m-d');
			
			
			
			if($modo=="user"){

				$categorias=array("Usuario","Servicios");
				$arra=array($categorias);
				
					
				
				
				if($id!="-1"){
				
					$query='select attended from services where created_at BETWEEN {ts "'.$diac.'"} AND {ts "'.$hoy.'"} AND attended = '.$id;
				}else 
					$query='select * from services where created_at BETWEEN {ts "'.$diac.'"} AND {ts "'.$hoy.'"}';
				$ser=Service::find_by_sql($query);
				foreach ($ser as $key) {
					$d=0;		
					for($j=0;$j<count($ser);$j++){
						if($key->attended==$ser[$j]->attended){
							$d++;
						}
					}
						
					if(!in_array(array("#".$key->attended,$d),$arra))
					array_push($arra,array("#".$key->attended,$d));
					
					}
									
			}
			else if($modo=="col"){
				
				
				
				$query='select * from services where created_at BETWEEN {ts "'.$diac.'"} AND {ts "'.$diag.'"}';
				$ser=Service::find_by_sql($query);
				$categorias=array("Colonia","Servicios");
				$arra=array($categorias);
				$s=array();
				$suburbs=array();
				
				foreach ($ser as $key) {
					
					array_push($suburbs,$this->extcolon($key->reservation1));
					if(!empty($key->reservation2)){
						array_push($suburbs,$this->extcolon($key->reservation2));
					}
				}
				for($i=0;$i<count($suburbs);$i++){
					$d=0;
					for($j=0;$j<count($suburbs);$j++){
						if($id!="-1"){
							if($suburbs[$i][0]==$suburbs[$j][0]){
								if($suburbs[$i][0]==$id)
									$d++;
								else{
									$d="nada";
								}
							}
						}else{
							if($suburbs[$i][0]==$suburbs[$j][0]){
								$d++;
							}
						}
					}
					if($d!="nada"){
						$col=Suburb::find_by_id($suburbs[$i][0]);
						if(!in_array(array($col->suburb,$d),$arra))
						array_push($arra,array($col->suburb,$d));
					}
				}
			}
			if($id!="-1"){
				array_push($arra,array(" "," "));
			} 
		}
		echo json_encode($arra);

		
		
	}
	function extcolon($idrse){
		$reser=Reservation::find_by_id($idrse);
		$tokcol=explode(",", $reser->addresses);
		//recorre todo el array de las direcciones que contiene una reservación
		for($i=0;$i<count($tokcol);$i++){
			if(substr($tokcol[$i], 0,1)=="a"){
				$add=Address::find_by_id(substr($tokcol[$i], 1)); //extrae la primer letra de $tokcol[$i]
				$tokcol[$i]="s".$add->suburb_id;
			}
			$tokcol[$i]=substr($tokcol[$i], 1);
		}
		return($tokcol);
	}

	function datos($f1,$f2,$modo,$q){
        $q = strtolower($q);
        if (get_magic_quotes_gpc()) $q = stripslashes($q);
		$er=array();
		$items = array();
		$dia=array();
		$value=array();
		$datetime1 = new DateTime($f1);
		$datetime2 = new DateTime($f2);
		$interval = $datetime1->diff($datetime2);
		$r= $interval->format('%R%a');
		
		$hoy=$datetime2->format('Y-m-d');
		

			//determina un formato para variable fecha
			$diag=$datetime2->format('Y-m-d');
			$diac=$datetime1->format('Y-m-d');
						
			if($modo=="user"){
				
					$query='select user_id from services where created_at BETWEEN {ts "'.$diac.'"} AND {ts "'.$diag.'"}';
					$ser=Service::find_by_sql($query);
				foreach ($ser as $key) {
					$u=User::find_by_id($key->user_id);
					
					$cadena=$u->name.' '.$u->lastname.' '.$u->secondlastname;		
					$originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðòóôõöøùúûýýþÿŔŕ';
					$modificadas = 'aaaaaaaceeeeiiiidoooooouuuuybsaaaaaaaceeeeiiiidoooooouuuyybyRr';
					$cadena = utf8_decode($cadena); //traduce una cadena de caracteres a un byte de ISO-8859-1
					$cadena = strtr($cadena, utf8_decode($originales), $modificadas);
					$cadena = strtolower($cadena); //convierte en minusculas una cadena
					$q = utf8_decode($q);
					$q = strtr($q, utf8_decode($originales), $modificadas);
					$q = strtolower($q);
					//compara si $q se encuentra en $cadena
					if (strpos($cadena, $q) !== false){
						if(!in_array(array("id" => $u->id, "label" => $u->name.' '.$u->lastname.' '.$u->secondlastname, "value" => $u->name.' '.$u->lastname.' '.$u->secondlastname),$items))
                    		array_push($items, array("id" => $u->id, "label" => $u->name.' '.$u->lastname.' '.$u->secondlastname, "value" => $u->name.' '.$u->lastname.' '.$u->secondlastname));
                			
						}	
					if (count($items) > 11){
               			 break;
            			}
					}
									
			}
			else if($modo=="col"){
				//los servicios que fueron creados en fechas dadas
				$query='select * from services where created_at BETWEEN {ts "'.$diac.'"} AND {ts "'.$diag.'"}';
				$ser=Service::find_by_sql($query);
				
				$s=array();
				$suburbs=array();
				
				foreach ($ser as $key) {
					
					array_push($suburbs,$this->extcolon($key->reservation1));
					if(!empty($key->reservation2)){
						array_push($suburbs,$this->extcolon($key->reservation2));
					}
					
				}
				for($i=0;$i<count($suburbs);$i++){
					$col=Suburb::find_by_id($suburbs[$i][0]);
					
					$cadena=$col->suburb." ".$col->state;		
					$originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðòóôõöøùúûýýþÿŔŕ';
					$modificadas = 'aaaaaaaceeeeiiiidoooooouuuuybsaaaaaaaceeeeiiiidoooooouuuyybyRr';
					$cadena = utf8_decode($cadena);
					$cadena = strtr($cadena, utf8_decode($originales), $modificadas);
					$cadena = strtolower($cadena);
					$q = utf8_decode($q);
					$q = strtr($q, utf8_decode($originales), $modificadas); //convierte caracteres de $from a $to
					$q = strtolower($q);
					
					if (strpos($cadena, $q) !== false){
						//si el registro no se encuentra en el array, se guarda
						if(!in_array(array("id" => $col->id, "label" => $col->suburb." ".$col->state, "value" => $col->suburb." ".$col->state),$items))
                    		array_push($items, array("id" => $col->id, "label" => $col->suburb." ".$col->state, "value" => $col->suburb." ".$col->state));
                		}
									
					if (count($items) > 11){ 
               			 break;
            			}
				}
			}
        
        echo $json = json_encode($items);
	
	}
	function removil($mes,$anio){
		?>
		<table class="table table-striped" style="margin-top: 20px">
			<tr>
			<td>
				#
			</td>
			<td> 
				Cliente
			</td>
			<td>
				# Reservacion
			</td>
			<td>
				Tipo
			</td>
			<td>
				Vehiculo
			</td>
			<td>
				# Pasajeros
			</td>
			<td>
				Fecha Reservación
			</td>
			<td>
				Hora Reservación
			</td>
			<td>
				Destino
			</td>
			<td>
				Costo
			</td>
			<td>
				Estado
			</td>
		</tr>
			<?php
				
				$diag=date('Y-m-d', strtotime($anio.'-'.$mes.'-01'));
				$diac=date('Y-m-d', strtotime($anio.'-'.$mes.'-31'));
				$reser=Reservation::find('all', array('conditions' =>'reservation_date BETWEEN {ts "'.$diag.'"} AND {ts "'.$diac.'"} AND mobile=1'));
				$c=0;
				foreach($reser as $r1){
					$s=Service::find_by_reservation1($r1->id);
					if($s){
					
						$r1=Reservation::find_by_id($s->reservation1);
						$u=User::find($s->user_id);
						if($r1->type=="MTY-APTO")
							$tipo="Salida";
						else 
							$tipo="Llegada";
						if($r1->vehicle_id==1||$r1->vehicle_id=="1"){
			 				$ve="Automóvil";
			 			}else{
			 				$ve="Camioneta";
			 			}
						$value=str_replace("a", "", $r1->addresses);
						$dir=Address::find_by_id($value);
						$sub=Suburb::find_by_id($dir->suburb_id);
						$r2=Reservation::find_by_id($s->reservation2);
						$ms='';
							if($r2->id){
								$ms="rowspan='2'";
							}
						if($s->pagado)
							$pg="si";
						else
							$pg="no";
						if($s->status==0){
							$st="Cancelada";
							
						}else{
							if($r1->status==0){
								$st="Cancelada";
							}else if($r1->status==3){
								$st="Completada";
							}else if($r1->status==1){
								$st="Viva";
							}
						}
						
						$c++;
						echo "<tr>
						<td ".$ms.">
							$c
						</td>
						<td ".$ms.">
							$u->name
						</td>
						<td ".$ms.">
							$s->id
						</td>
						<td>
							$tipo
						</td>
						<td>
							$ve
						</td>
						<td>
							$r1->passengers
						</td>
						<td>
							".templateDriver::timelo($r1->reservation_date,1)."
						</td>
						<td>	
							$r1->reservation_time
						</td>
						<td>
							$sub->suburb
						</td>
						<td>
							$s->cost
						</td>
						<td>
							$st
						</td>
						</tr>";
							$r2=Reservation::find_by_id($s->reservation2);
							if($r2->id){
								
						if($r2->type=="MTY-APTO")
							$tipo="Salida";
						else 
							$tipo="Llegada";
						if($r2->vehicle_id==1||$r2->vehicle_id=="1"){
			 				$ve="Automóvil";
			 			}else{
			 				$ve="Camioneta";
			 			}
						$value=str_replace("a", "", $r2->addresses);
						$dir=Address::find_by_id($value);
						$sub=Suburb::find_by_id($dir->suburb_id);
						if($s->pagado)
							$pg="si";
						else
							$pg="no";
						echo "<tr>
						
						<td>
							$tipo
						</td>
						<td>
							$ve
						</td>
						<td>
							$r2->passengers
						</td>
						<td>
							".templateDriver::timelo($r2->reservation_date,1)."
						</td>
						<td>
							$r2->reservation_time
						</td>
						<td>
							$sub->suburb
						</td>
						<td>
							
						</td>
						</tr>";
							}
					}
					}
				?>
		</table>
		<?php
	}
}

?>