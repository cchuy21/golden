<?php
class costController extends controllerBase {
    //OFFLINE precio2 es el chido
	public function precio($tipo,$con,$array,$datetime,$dist){ //se modifica el costo dependiendo del tipo de servicio que se realiza
		$conv=User::find($con)->agreement;
		if($array=="a"|| $array=="s" || $array==null || $array==" " || $array===undefined|| empty($array)){
			exit;
		}
	    $time=explode(" ",$datetime);
	 	$fecha=$time[0];
		$hora_ser=$time[1];
		if($tipo!=2 && $tipo!=1){
			//si se ingresa un tipo de automovil incorrecto
			echo "no existe tipo de automovil";
			exit;
		}	
		//si el usuario no tiene convenio con ninguna empresa
		if($conv==1||$conv==3||$conv==2){
			$conv="SC";
		}
		$aumento=200;
		$costodesc;
		//separar los datos cuando encuentre una coma
		$array = array_unique(explode(',', $array));
		$num=count($array);
		$final = array();
		$mayor=0;
		$menor=0;
		$id=array();
		$descuento=0;
		
		
			$value=$array;
			$dir=Address::find_by_id($value);
			$sub=Suburb::find_by_id($dir->suburb_id);
			
			$zon=Zone::find_by_id($sub->id_zone);
			$con=Agreement::find_by_id($conv);
				//selecciona el costo del servicio dependiendo el tipo de automóvil y la zona  para saber si cuenta con un descuento para aplicarselo al servicio
				if($tipo=="1"){
					$costo=0;
					$costo=$zon->cost_car;
					$veh="Carro";
					$desc=$zon->des_car;
				}else if($tipo=="2"){
					$costo=$zon->cost_van;
					$veh="Camioneta";
					$desc=$zon->des_van;
					
					}
				if($costo>=$mayor){
					$tipo2=$ind;
					$idmayor=$value;
					$mayor=$costo;
					$menor=$desc;
				}

		
//se selecciona los datos del servcio, se vericia si tiene algun descuento o convenio si es asi se le aplica el descuento y sino se le asigna el precio adecuado
		$met=0;
		if($tipo2 != "s"){
			$maydir=Address::find_by_id($idmayor);
			$maysub=Suburb::find_by_id($maydir->suburb_id);
			$user=User::find($maydir->user_id,array("select"=>"business_id"));
			if($user->business_id==37035 || $user->business_id=="37035"){
				$aleman=Address::find_by_sql("SELECT id,street FROM `addresses` WHERE street like '%100 km 16%'");
				foreach($aleman as $al){
					if(!$met){
						if($maydir->id==$al->id){
							$met=1;
						}
					}
				}
			}else{
				$met=0;
			}
		}else{
			$idmayor=str_replace("s", "", $idmayor);
			$maysub=Suburb::find_by_id($value);
		}
		//el suburbio que se encuentra en una zona especifica
		$mayzon=Zone::find_by_id($maysub->id_zone);
		$maycon=Agreement::find_by_id($conv);
		$num=$num-1;
		$dom=($num*$aumento);
		$costofinal=$mayor;
		
		if($conv=="SC" || !$conv || $conv==""|| $conv=="Sindescuento"){
			$costofinal=$mayor;
		}else if($conv=="ClienteDisS20R40"){
			$costofinal=$mayor;
		}else if(($veh=="Camioneta") && ( $conv!="PCNAPORED" && $conv!="AERO415" && $conv!="AERO310" && $conv!="Aerolineas" && $conv!="Aerolíneas200" && $conv!="Aerolineas230" && $conv!="Aerolíneas260" && $conv!="Aerolineas270" && $conv!="ASA" && $conv!="GLO" && $conv!="GLO200" && $conv!="GLO250" && $conv!="GLO270" && $conv!="GLO400" && $conv!="GLO450" && $conv!="GLO420" && $conv!="GLO50" && $conv!="GLO500" && $conv!="GLO600" && $conv!="GLO700" && $conv!="Ham" && $conv!="HAM300" && $conv!="HAM350" && $conv!="HAM450" && $conv!="HAM600" && $conv!="HAM700" && $conv!="HAM840" && $conv!="HAM900" && $conv!="HAM760")){
			$costofinal=$mayor;
		} else{
			
			
			//depende del area se le da un nombre y se le asigna un costo
			if($mayzon->area==1){
				$costofinal=$maycon->metropolitan;
				$menor=0;
				$area="metropolitana";
			}else if($mayzon->area==3){
				$costofinal=$maycon->intermedia;
				$menor=0;
				$area="intermedia";
				if($maycon->intermedia==0){
				    $costofinal=$maycon->metropolitan;
				}
			}else if($mayzon->area==2){
				$costofinal=$maycon->periphery;
				$menor=0;
				$area="periferica";
			}else if($mayzon->area==4){
				$costofinal=$maycon->foraneo;
				$menor=0;
				$area="foraneo";
			} else if($mayzon->area==5){
				$costofinal=$maycon->aeropuerto;
				$menor=0;
				$area="aeropuerto";
			} else if($mayzon->area==6){
				$costofinal=$maycon->restringida;
				$menor=0;
				$area="restringida";
			}
			
		}
		
		//se calcula el costo final con el descuento aplicado
	 	$costodesc=$costofinal-$descuento;
		//se calcula el costo total junto con dscuento y aumento
		$costototal=$costofinal-$descuento+$dom;

		if($costofinal===null){
			exit;
		}
		//55831 37035
		if($met){
			$costofinal=250;
		}
		//muestra todos los datos de la consulta
		$final =array("des"=>$menor,"area" =>$area,"domicilios" =>$num,"vehiculo" => $veh, "mayor" => $costofinal,"convenio"=>$con->agreement,"descuento"=> $descuento,"costo con descuento" =>$costodesc,"extra-dom"=>$dom, "total" => $costototal);
		echo json_encode($final);
	 
	}
	//OFFLINE
	public function precio3($tipo,$con,$array,$datetime,$dist){ //se modifica el costo dependiendo del tipo de servicio que se realiza
		$conv=User::find($con)->agreement;
		if($array=="a"|| $array=="s" || $array==null || $array==" " || $array===undefined|| empty($array)){
			exit;
		}
	    $time=explode(" ",$datetime);
	 	$fecha=$time[0];
		$hora_ser=$time[1];
		if($tipo!=2 && $tipo!=1){
			//si se ingresa un tipo de automovil incorrecto
			echo "no existe tipo de automovil";
			exit;
		}	
		//si el usuario no tiene convenio con ninguna empresa
		if($conv==1||$conv==3||$conv==2){
			$conv="SC";
		}
		$aumento=200;
		$costodesc;
		//separar los datos cuando encuentre una coma
		$array = array_unique(explode(',', $array));
		$num=count($array);
		$final = array();
		$mayor=0;
		$menor=0;
		$id=array();
		$descuento=0;
		
		
			$value=$array;
			$dir=Address::find_by_id($value);
			$sub=Suburb::find_by_id($dir->suburb_id);
			
			$zon=Zone::find_by_id($sub->id_zone);
			$con=Agreement::find_by_id($conv);
				//selecciona el costo del servicio dependiendo el tipo de automóvil y la zona  para saber si cuenta con un descuento para aplicarselo al servicio
				if($tipo=="1"){
					$costo=0;
					$costo=$zon->cost_car;
					$veh="Carro";
					$desc=$zon->des_car;
				}else if($tipo=="2"){
					$costo=$zon->cost_van;
					$veh="Camioneta";
					$desc=$zon->des_van;
					
					}
				if($costo>=$mayor){
					$tipo2=$ind;
					$idmayor=$value;
					$mayor=$costo;
					$menor=$desc;
				}

		
//se selecciona los datos del servcio, se vericia si tiene algun descuento o convenio si es asi se le aplica el descuento y sino se le asigna el precio adecuado
		$met=0;
		if($tipo2 != "s"){
			$maydir=Address::find_by_id($idmayor);
			$maysub=Suburb::find_by_id($maydir->suburb_id);
			$user=User::find($maydir->user_id,array("select"=>"business_id"));
			if($user->business_id==37035 || $user->business_id=="37035"){
				$aleman=Address::find_by_sql("SELECT id,street FROM `addresses` WHERE street like '%100 km 16%'");
				foreach($aleman as $al){
					if(!$met){
						if($maydir->id==$al->id){
							$met=1;
						}
					}
				}
			}else{
				$met=0;
			}
		}else{
			$idmayor=str_replace("s", "", $idmayor);
			$maysub=Suburb::find_by_id($value);
		}
		//el suburbio que se encuentra en una zona especifica
		
		$mayzon=Zone::find_by_id($maysub->id_zone);
		$maycon=Agreement::find_by_id($conv);
		$num=$num-1;
		$dom=($num*$aumento);
		$costofinal=$mayor;
		
		if($conv=="SC" || !$conv || $conv==""|| $conv=="Sindescuento"){
			$costofinal=$mayor;
			echo "entro 4";
		}else if($conv=="ClienteDisS20R40"){
			$costofinal=$mayor;
			echo "entro 3";
		}else if(($veh=="Camioneta") && ($conv!="AERO415" && $conv!="AERO310" && $conv!="Aerolineas" && $conv!="Aerolíneas200" && $conv!="Aerolineas230" && $conv!="Aerolíneas260" && $conv!="Aerolineas270" && $conv!="ASA" && $conv!="GLO" && $conv!="GLO200" && $conv!="GLO250" && $conv!="GLO270" && $conv!="GLO400" && $conv!="GLO450" && $conv!="GLO420" && $conv!="GLO50" && $conv!="GLO500" && $conv!="GLO600" && $conv!="GLO700" && $conv!="Ham" && $conv!="HAM300" && $conv!="HAM350" && $conv!="HAM450" && $conv!="HAM600" && $conv!="HAM700" && $conv!="HAM840" && $conv!="HAM900" && $conv!="HAM760")){
			$costofinal=$mayor;
			
		} else{
			
			//depende del area se le da un nombre y se le asigna un costo
			if($mayzon->area==1){
				$costofinal=$maycon->metropolitan;
				$menor=0;
				$area="metropolitana";
			}else if($mayzon->area==3){
				$costofinal=$maycon->intermedia;
				$menor=0;
				$area="intermedia";
				if($maycon->intermedia==0){
				    $costofinal=$maycon->metropolitan;
				}
			}else if($mayzon->area==2){
				$costofinal=$maycon->periphery;
				$menor=0;
				$area="periferica";
			}else if($mayzon->area==4){
				$costofinal=$maycon->foraneo;
				$menor=0;
				$area="foraneo";
			}
			
		}
		
		//se calcula el costo final con el descuento aplicado
	 	$costodesc=$costofinal-$descuento;
		//se calcula el costo total junto con dscuento y aumento
		$costototal=$costofinal-$descuento+$dom;

		if($costofinal===null){
			exit;
		}
		//55831 37035
		if($met){
			$costofinal=250;
		}
		//muestra todos los datos de la consulta
		$final =array("des"=>$menor,"area" =>$area,"domicilios" =>$num,"vehiculo" => $veh, "mayor" => $costofinal,"convenio"=>$con->agreement,"descuento"=> $descuento,"costo con descuento" =>$costodesc,"extra-dom"=>$dom, "total" => $costototal);
		echo json_encode($final);
	 
	}
	//CHIDO
	public function precio2($tipo,$con,$array,$datetime,$dist,$red='cdapto'){ //se modifica el costo dependiendo del tipo de servicio que se realiza
		$conv=User::find($con)->agreement;
		if($array=="a"|| $array=="s" || $array==null || $array==" " || $array===undefined|| empty($array)){
			exit;
		}
	    $time=explode(" ",$datetime);
	 	$fecha=$time[0];
		$hora_ser=$time[1];
		if($fecha=='abierto' || $fecha==''){
		    $hora_ser = '00:00';
		} else {
		    $hora_ser = date("H:i",strtotime($datetime));
		}
		$rhora = strtotime($hora_ser);
		
		if($tipo!=2 && $tipo!=1){
			//si se ingresa un tipo de automovil incorrecto
			echo "no existe tipo de automovil";
			exit;
		}	
		//si el usuario no tiene convenio con ninguna empresa
		if($conv==1||$conv==3||$conv==2){
			$conv="SC";
		}
		$aumento=200;
		$costodesc;
		//separar los datos cuando encuentre una coma
		$array = array_unique(explode(',', $array));
		$num=count($array);
		$final = array();
		$mayor=0;
		$menor=0;
		$id=array();
		$descuento=0;
		
		foreach ($array as $value) {
			$lon=strlen($value);
			$lon=($lon-1)*(-1);
			$ind=substr($value, 0,$lon);
			if($ind == "a"){
			
			$value=str_replace("a", "", $value);
			$dir=Address::find_by_id($value);
			$sub=Suburb::find_by_id($dir->suburb_id);
			}else if($ind == "s"){
				$value=str_replace("s", "", $value);
				
				$sub=Suburb::find_by_id($value);	
			}
			$zon=Zone::find_by_id($sub->id_zone);
			$con=Agreement::find_by_id($conv);
				//selecciona el costo del servicio dependiendo el tipo de automóvil y la zona  para saber si cuenta con un descuento para aplicarselo al servicio
				if($tipo=="1"){
					$costo=0;
					$veh="Carro";
    				$desc=0;
					if($red=='cdapto'){
    					$costo=$zon->cdapto_auto;
    					if($rhora >= strtotime("09:00") && $rhora <= strtotime("22:59")){
    					$costo=$zon->promo;
					    }
					}else{
    					$costo=$zon->aptocd_auto;
					}
					
				}else if($tipo=="2"){
					$costo=0;
					$veh="Camioneta";
					$desc=0;
					if($red=='cdapto'){
    					$costo=$zon->cdapto_cam;
					}else{
    					$costo=$zon->aptocd_cam;
					}
				}
				
				if($costo>=$mayor){
					$tipo2=$ind;
					$idmayor=$value;
					$mayor=$costo;
					$menor=$desc;
				}

		}
//se selecciona los datos del servcio, se vericia si tiene algun descuento o convenio si es asi se le aplica el descuento y sino se le asigna el precio adecuado
		$met=0;
		if($tipo2 != "s"){
			$maydir=Address::find_by_id($idmayor);
			$maysub=Suburb::find_by_id($maydir->suburb_id);
			$user=User::find($maydir->user_id,array("select"=>"business_id"));
			if($user->business_id==56355 || $user->business_id=="56355"){//56355
				$aleman=Address::find_by_sql("SELECT id,street FROM `addresses` WHERE street like '%km 16%'");
				foreach($aleman as $al){
					if(!$met){
						if($maydir->id==$al->id){
							$met=1;
						}
					}
				}
			}else{
				$met=0;
			}
		}else{
			$idmayor=str_replace("s", "", $idmayor);
			$maysub=Suburb::find_by_id($value);
		}
		//el suburbio que se encuentra en una zona especifica
		$mayzon=Zone::find_by_id($maysub->id_zone);
		$maycon=Agreement::find_by_id($conv);
		$num=$num-1;
		$dom=($num*$aumento);
		$costofinal=$mayor;
		
		if($conv=="SC" || !$conv || $conv==""|| $conv=="Sindescuento"){
			$costofinal=$mayor;
		}else if($conv=="ClienteDisS20R40"){
			$costofinal=$mayor;
		}else{
			$fora = 0;
			//depende del area se le da un nombre y se le asigna un costo
			if($mayzon->area==0){
				$costofinal=$maycon->metropolitan;
				$menor=0;
				$area="metropolitana";
			}else if($mayzon->area==3){
				$costofinal=$maycon->intermedia;
				$menor=0;
				$area="intermedia";
				if($maycon->intermedia==0){
				    $costofinal=$maycon->metropolitan;
				}
			}else if($mayzon->area==1){
				$costofinal=$maycon->periphery;
				$menor=0;
				$area="periferica";
			}else if($mayzon->area==2){
				//$costofinal=$maycon->foraneo;
				$menor=0;
				$area="foraneo";
				$fora = 1;
			} else if($mayzon->area==5){
				$costofinal=$maycon->aeropuerto;
				$menor=0;
				$area="aeropuerto";
			} else if($mayzon->area==6){
				$costofinal=$maycon->restringida;
				$menor=0;
				$area="restringida";
			}
			
			if(($veh=="Camioneta") && ($conv=="GLO850" || $conv=="GLO675" || $conv=="HAM700822" || $conv=="HAMI940" || $conv=="HYU863" || $conv=="HYU" || $cov=="PCNAPOSEN" || $conv=="PCNAPOYO" || $conv=="PCNTE" || $conv=="PCNSENDSEN" || $conv=="PCNSENDRED" || $conv=="PCNGPESEN" || $conv=="PCNGPERED" || $conv=="PCNEXT" || $conv=="PCNAPOSEN" || $conv=="PCNAPORED" || $conv=="AERO415" || $conv=="AERO350-400" || $conv=="AERO350-450" || $conv=="AERO310" || $conv=="Aerolineas" || $conv=="Aerolíneas200" || $conv=="Aerolineas230" || $conv=="Aerolíneas260" || $conv=="Aerolineas270" || $conv=="ASA" || $conv=="GLO" || $conv=="GLO200" || $conv=="GLO250" || $conv=="GLO270" || $conv=="GLO400" || $conv=="GLO450" || $conv=="GLO420" || $conv=="GLO50" || $conv=="GLO500" || $conv=="GLO600" || $conv=="GLO700" || $conv=="Ham" || $conv=="HAM300" || $conv=="HAM350" || $conv=="HAM450" || $conv=="HAM600" || $conv=="HAM700" || $conv=="HAM840" || $conv=="HAM900" || $conv=="HAM760" || $conv=="HAM450540")){
    			$costofinal=$maycon->camioneta;
    		}else if(($veh=="Camioneta") && $maycon->promo == 1){
    			$costofinal=$maycon->camioneta;
    		}else if($veh=="Camioneta"){
    		    $costofinal=$mayor;
    		}
			
		}
		
		
		//se calcula el costo final con el descuento aplicado
	 	$costodesc=$costofinal-$descuento;
		//se calcula el costo total junto con dscuento y aumento
		$costototal=$costofinal-$descuento+$dom;

		if($costofinal===null){
			exit;
		}
		//55831 37035
		if($met){
			$costofinal=250;
		}
		//muestra todos los datos de la consulta
		$final =array("des"=>$menor,"area" =>$area,"domicilios" =>$num,"vehiculo" => $veh, "mayor" => $costofinal,"convenio"=>$con->agreement,"descuento"=> $descuento,"costo con descuento" =>$costodesc,"extra-dom"=>$dom, "total" => $costototal);
		echo json_encode($final);
	 
	}
			/*fin*/
} 
	