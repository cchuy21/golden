<?php
class reservationController extends controllerBase {
    
    public function index($id = null) {
        templateDriver::renderSection("reservation.base", $id);
    }
    public function masivePublish() { 
        $data = inputDriver::getVar(__POST__);
		
        $returnData = array();
		unset($data['id']);
		$ids=array();
		$c=0;
        foreach ( $data as $id => $fields) {
        	if(!$fields['id']){
	       unset($fields['payment']);
			if($fields["reservation_date"]=="abierto"){
				//destruye la variable especifica localmente
				unset($fields["reservation_date"]); 
				$fields["abierto"]=1;
			}else{
				$fields['abierto']=0;
			}		
			$pasa=(int) $fields['passengers'];
			//si el numero de pasajeros es mayor a 4 se cambia de automovil a camioneta
			if($pasa>4 && $fields['vehicle_id']==1){ 
				$fields['vehicle_id']=2;
			}
            $R = new Reservation($fields);
            $R->save();
            $fields['id'] = $R->id;
			$ids[$c]=$R->id;
			$fecha=date("Y-m-d");
			$returnData[$id] = $fields;
		   if(strtotime($R->reservation_date)==strtotime($fecha)){
		   		$last=Last::find_by_id(1);
			    $last->reservation_id=$R->id;
				$last->save();
		   }
		   $c++;
		}else{
			 $R = Reservation::find_by_id($fields['id']);
                if($R) {
                	unset($fields['payment']);
				 if($fields["reservation_date"]=="abierto"){
					unset($fields["reservation_date"]);
					$fields["abierto"]=1;
				}else{
					$fields['abierto']=0;
				}		
				$pasa=(int) $fields['passengers'];
				if($pasa>4&&$fields['vehicle_id']==1){
					$fields['vehicle_id']=2;
				}
				
                    $R->update_attributes($fields); //modifica los datos de una reservación
                    $returnData[$id] = $fields;
                }
            $fecha=date("Y-m-d");
          
		   if(strtotime($R->reservation_date)==strtotime($fecha)){ //si el dia de una reservaciin es iguak¡l a hoy
		   		$last=Last::find_by_id(1);
			    $last->reservation_update=$R->id;
				$last->save();
		   }
			$ids[$c]=$R->id;
		   
           $c++;
		}
        }
           

		//codigo para el doble 0
		if($ids[0]!=0)
        responseDriver::dispatch('D', $returnData);
    }
    
    public function service() { //se actualiza la información del servicio
        $data = inputDriver::getVar(__POST__);
		$hoy=date("Y-m-d G:i");
        if($data['id'] !== '') {
            $S = Service::find_by_id($data['id']);
			$data['user_id']= $S->user_id;
			
            if($S) {
            	unset($data['attended']);
            	//guarda el usuario final que ha modifcado el servicio
				$data['edited']=authDriver::getSUser()->name." ".authDriver::getSUser()->lastname; 
				//guarda el día en que se modifico el servicio
				$data['updated_at']=date('d-m-Y'); 
				//ingresa en el campo log quien lo edito y cuando
				$data['log']=$S->log."||'Editado','".authDriver::getSUser()->name." ".authDriver::getSUser()->lastname."','".$hoy."'";
                $S->update_attributes($data);
                responseDriver::dispatch('M', "Cambios guardados", "Informacion actualizada correctamente en la dirección.");
            }
        } else {
			$data['edited']="No ha sido editado";
			$data['attended']=authDriver::getSUser()->name." ".authDriver::getSUser()->lastname; //nombre del personal que atendio el servicio
			
			$data['log']="'Creado','".authDriver::getSUser()->name." ".authDriver::getSUser()->lastname."','".$hoy."'";
            $service = new Service($data);
            if($service->is_valid()) {
            	//codigo para el doble 0
            	if(($data['reservation1']!=0 && $data['reservation2']!=0)||$data['reservation2']!=0||$data['reservation1']!=0 ){
                $service->save();
                responseDriver::dispatch('D', array('reservation1'=>$service->reservation1,'reservation2'=>$service->reservation2,'id'=>$service->id, 'service_id'=> sprintf("%06d", $service->id)));
				}else{
                responseDriver::dispatch('E', "Error", "Error al intentar ingresar el servicio.");
				}
		    } else {
                //Error
                responseDriver::dispatch('E', "Error", "Error al intentar ingresar el servicio.");
            }
        }
    }

	function cancelassig(){ //se elimina una asignación
		$id= inputDriver::getVar("id");
		$as=Assignment::find_by_reservation_id($id);
		if($as->delete()){
			$ser=Service::find_by_reservation1($id);
			if(!$ser)
				$ser=Service::find_by_reservation2($id);
			$this->cancel($ser->id);
		}
	}
	
    public function cancel($data=0) { //se cancela una asignación
    	if(!$data)
       $data = inputDriver::getVar("id");
		$canceledby = inputDriver::getVar("canceled");
        $service = Service::find_by_id($data);
        if($service) {
        	
			templateDriver::setData("tipo", array("id" => 2));
			templateDriver::setData("servicio", array("id" => $service->id));
			templateDriver::renderSection("pruebas.mail");
        	$cn= new Canceled();
			$cn->canceled_by=$canceledby;
			$cn->id=$service->id;
			$cn->user_id=$service->user_id;
            $r1 = $service->reservation1;
            $r2 = $service->reservation2;
            $r1 = Reservation::find_by_id($r1);
            if( $r2 !== null )
                $r2 = Reservation::find_by_id($r2);
            if($r1){
            	$r1->status=0;
            	$r1->save();
				$cn->reservation1=$service->reservation1;
            }
                
            if($r2){
            	$r2->status=0;
				$r2->delete();
				$cn->reservation2=$service->reservation2;
            }
			
			$cn->requested=$service->requested;
			$cn->annotations=$service->annotations;
			$cn->cost=$service->cost;
			$cn->payment=$service->payment;
			$cn->attended=$service->attended;
			$cn->log=$service->log;
			$l=Last::find_by_id(1);
			$l->reservation_id=$service->reservation1;
			$l->save();
			$service->delete();
			if($cn->save()){
				echo json_encode(1);			
			}
			
			
        }
    }
 
	   public function ca($data) { //se cancela un servicio
    	
       
        $service = Service::find_by_id($data);
        if($service) {
        	
        	$cn= new Canceled();
			$cn->id=$service->id;
			$cn->user_id=$service->user_id;
            $r1 = $service->reservation1;
            $r2 = $service->reservation2;
            $r1 = Reservation::find_by_id($r1);
            if( $r2 !== null )
                $r2 = Reservation::find_by_id($r2);
            if($r1){
            	$r1->status=0;
            	$r1->save();
				$cn->reservation1=$service->reservation1;
				
            }
                
            if($r2){
            	$r2->status=0;
				$r2->save();
				$cn->reservation2=$service->reservation2;
            }
			
			$cn->requested=$service->requested;
			$cn->annotations=$service->annotations;
			$cn->cost=$service->cost;
			$cn->payment=$service->payment;
			$cn->attended="SisAnt";
			$dt=date("Y-m-d G:i");
			$cn->log="||'Cancelado','".authDriver::getSUser()->name." ".authDriver::getSUser()->lastname."','".$dt."'".$service->log;
			$service->delete(); //se cancela el servicio
			$cn->save(); //se guardan los datos de la cancelación
						
        }
		
    }
     
    public function hide() {
        $data = inputDriver::getVar("service");
        $service = Reservation::find_by_id($data['id']);
        if($service) {
            $service->hide = true;
            $service->save();
        }
    } 
}