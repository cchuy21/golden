<?php

class restrictionController extends controllerBase {
    public function add() {
        //Validar login
        $data = inputDriver::getVar(__POST__);
        
        $address = new Restriction($data);
        if( $address->is_valid() ) {
            $address->save();
            responseDriver::dispatch('D', array('id'=>$address->id));
        } else {
            //Error
            responseDriver::dispatch('E', "Error", "Error al intentar guardar la nueva dirección.");
        }
    }

		//agrega el dato de una zona o lo modifica 
    public function publish(){ 
        $data = inputDriver::getVar(__POST__);
        $isnew = false;
		
        if($data['id'] == '' || $data['id'] == '-1' || $data['id'] == -1) {
        	//destruye la variable local
            unset($data['id']); 
            $this->add($data);
        } else {
            $this->update($data);
        }
    }
	//modifica una dirección si se encuentra en zonas
    public function update() {
        //Validar login
        $data = inputDriver::getVar(__POST__);
        //busca la zona dependiendo del id
        $address = Restriction::find_by_id($data['id']); 
        if($address) {
            unset($data['id']);
            $address->update_attributes($data);
            //sino hay ningún error
            if(!$address->errors) { 
        		responseDriver::dispatch('M', "Cambios guardados", "Informacion actualizada correctamente en la dirección.");
            } else {
                //Error
            }
        } else {
            $this->add($data);
        }
    }
	//deshabilita una zona
	public function disable() {
        //Validar login
        $data = inputDriver::getVar("aid");
        $z = Restriction::find_by_id($data);
        if($z){
        	//deshabilita la zona
            $z->delete();
            responseDriver::dispatch('D', array('message'=>"zona deleted"));
        }
    }
	function creater($fecha){
		$c=0;
		for($i=1;$i<=8;$i++){
			$r=Restriction::find("all",array("conditions"=>"fecha={ ts'".$fecha."'} AND schedule_id=".$i));
			if(count($r)){
				continue;
			}
			$sc=Schedule::find($i);
			$di=date("N",strtotime($fecha));
			$r=new Restriction();
			if($di==1){
				$r->libres=$sc->ln;
			}else{
				$r->libres=$sc->vn;
			}
			$r->fecha=$fecha;
			$r->hechas=0;
			$r->alt=0;
			$r->amx=0;
			$r->int=0;
			$r->extra=0;
			$r->extra_name="";
			$r->schedule_id=$i;
			if($r->save()){
				$c++;
			}
		}
	}
	function busq(){
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$fecha=$_POST["fecha"];
		$hora=$_POST["hora"];
		$hoy=date("Y-m-d",strtotime($fecha));
		$ahora=date("H:i:s",strtotime($hora));
		$dia=date('w',strtotime($fecha));
		$rest=Schedule::find('all');
		foreach ($rest as $res) {
			$rst=$res;
		}
		$sche=Schedule::find("all",array("limit"=>1,"conditions"=>"hour1 <= {ts '".$ahora."'} AND hour2 >= {ts '".$ahora."'}"));
		foreach ($sche as $sch) {
			$sc=$sch;
		}
		$rest=Restriction::find('all', array("conditions"=>"fecha ='".$hoy."'"));
		if(!count($rest)){
			$this->creater($fecha);
			echo json_encode("");
			exit;
		}
		$sche2=Schedule::find("last");
		if(strtotime($ahora)>strtotime($sche2->hour2)){
			echo json_encode("");
			exit;
		}
		$sche2=Schedule::find("first");
		if(strtotime($ahora)<strtotime($sche2->hour1)){
			echo json_encode("");
			exit;
		}
		$rest=Restriction::find('all', array("conditions"=>"fecha ='".$hoy."' and schedule_id='".$sc->id."'"));
		foreach ($rest as $res) {
			$rst=$res;
		}
		$maximon=$rst->libres;
		if($maximon>0){
			echo json_encode("");
		}else{
			$st="Lo sentimos, sólo tenemos disponibles: <br /><table style='margin: 0 auto;'><tr><td>";
			if($sc->id!=1){
				$maximon=0;
				$sc2=Schedule::find($sc->id);
				while($maximon<=0 && $sc2->id>=2){
					$restant=Restriction::find('all', array("limit"=>1,"conditions"=>"schedule_id=".(intval($sc2->id)-1)." AND fecha={ ts '".$hoy."'}"));
					$sc2=Schedule::find(($sc2->id-1));
					foreach ($restant as $resant) {
						$rstant=$resant;
					}
					$maximon=$rstant->libres;
					if($maximon>0){
						$st=$st.$maximon." - ".$sc2->hour."</td></tr><tr><td>";
						break;
					}
				}
			}
			if($sc->id!=8){
				$maximon=0;
				$sc2=Schedule::find($sc->id);
				while($maximon<=0 && $sc2->id<=7){
					$restant=Restriction::find('all', array("limit"=>1,"conditions"=>"schedule_id=".(intval($sc2->id)+1)." AND fecha={ ts '".$hoy."'}"));
					$sc2=Schedule::find(($sc2->id+1));
					foreach ($restant as $resant) {
						$rstant=$resant;
					}
					$maximon=$rstant->libres;
					
					if($maximon>0){
						$st=$st.$maximon." - ".$sc2->hour."</td></tr>";
						break;
					}
				}
			}
			if($st=="Lo sentimos, sólo tenemos disponibles: <br /><table style='margin: 0 auto;'><tr><td>"){
				$st="Lo sentimos, sólo tenemos disponibles: desde las 8:30";
			}
			echo json_encode($st);
		}
	}
	function busq3(){
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$fecha=$_POST["fecha"];
		$hora=$_POST["hora"];
		$hoy=date("Y-m-d",strtotime($fecha));
		$ahora=date("H:i:s",strtotime($hora));
		$dia=date('w',strtotime($fecha));
		$sche=Schedule::find("all",array("limit"=>1,"conditions"=>"hour1 <= {ts '".$ahora."'} AND hour2 >= {ts '".$ahora."'}"));
		
		if(!count($sche)){
			echo json_encode("");
			exit;
		}
		foreach ($sche as $sch) {
			$sc=$sch;
		}
		$rest=Restriction::find_by_schedule_id_and_fecha($sc->id,$hoy);
		if(!count($rest)){
			$this->creater($fecha);
			echo json_encode("");
			exit;
		}
		$hechas=Reservation::find("all",array("select"=>"id","conditions"=>"status<>'1' and reservation_date = {ts '".$hoy."'} AND reservation_time BETWEEN {ts '".$sc->hour1."'} AND {ts '".$sc->hour2."'} AND type='MTY-APTO'"));
		$h=count($hechas);
		
		$total=intval($rest->alt)+intval($rest->amx)+intval($rest->int)+intval($rest->vol)+intval($rest->extra)+intval($h);
		$w=date("w",strtotime($hoy));
		$b=Benefit::find(2);
		
		
		if(isset($rest->cambio)){
			$max=$rest->cambio;
		}else{
			if($b->benefit=="0"){
				if($w!=1){
					$max=$sc->vn;
				}else{
					$max=$sc->ln;
				}
			}else{
				if($w!=1){
					$max=$sc->vl;
				}else{
					$max=$sc->ll;
				}
			}
		}
		$lb=$max-$total;
		if($lb>0){
			echo json_encode("");
			exit;
		}else{
			echo json_encode("Lo sentimos no hay disponibles para este horario");
			exit;
		}
	}
	function busq2(){
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		$fecha=$_POST["fecha"];
		$hora=$_POST["hora"];
		$hoy=date("Y-m-d",strtotime($fecha));
		$ahora=date("H:i:s",strtotime($hora));
		$dia=date('w',strtotime($fecha));
		$sche=Schedule::find("all",array("limit"=>1,"conditions"=>"hour1 <= {ts '".$ahora."'} AND hour2 >= {ts '".$ahora."'}"));
		
		if(count($sche)<1){
			
			echo json_encode("");
			exit;
		}
		foreach ($sche as $sch) {
			$sc=$sch;
		}
		$rest=Restriction::find_by_schedule_id_and_fecha($sc->id,$hoy);
		if(!count($rest)){
			$this->creater($fecha);
			echo json_encode("");
			exit;
		}
		$hechas=Reservation::find("all",array("select"=>"id","conditions"=>"status<>'0' and reservation_date = {ts '".$hoy."'} AND reservation_time BETWEEN {ts '".$sc->hour1."'} AND {ts '".$sc->hour2."'} AND type='MTY-APTO'"));
		$h=count($hechas);
		$total=intval($rest->alt)+intval($rest->amx)+intval($rest->int)+intval($rest->vol)+intval($rest->extra)+intval($h);
		$w=date("w",strtotime($hoy));
		
		
		if($rest->cambio!="" && $rest->cambio!=NULL){
			$max=$rest->cambio;
		}else{
			if($w!=1){
				$max=$sc->vn;
			}else{
				$max=$sc->ln;
			}
		}
		$lb=$max-$total;
		
		if($lb>0){
			echo json_encode("");
			exit;
		}else{
			echo json_encode("Lo sentimos no hay disponibles para este horario");
			exit;
		}
	}
	public function lluvia(){
		$ben=Benefit::find(2);
		$ben->benefit=$_POST["id"];
		
		if($ben->save()){
			if($_POST["id"]=="1"){
				echo json_encode("Lluvia");
			}else{
				echo json_encode("Normal");
			}
		}
	}
	public function lluvia2(){
		$ben=Benefit::find(2);
		$ben->benefit=$_POST["id"];
		for($i=1;$i<=8;$i++){
			$res=Restriction::find("all",array("conditions"=>"fecha={ ts '".$_POST["fecha"]."'} AND schedule_id='".$i."'"));
			foreach($res as $rs){
				$R=$rs;
			}
			$sc=Schedule::find($i);
			$f=date("w",strtotime($_POST["fecha"]));
			if($f!=1){
				if($_POST["id"]=="1"){
					$R->cambio=$sc->vl;
				}else{
					$R->cambio=$sc->vn;
				}
			}else{
				if($_POST["id"]=="1"){
					$R->cambio=$sc->ll;
				}else{
					$R->cambio=$sc->ln;
				}
			}
			$R->save();
		}
		if($ben->save()){
			if($_POST["id"]=="1"){
				echo json_encode("Lluvia");
			}else{
				echo json_encode("Normal");
			}
		}
	}
}