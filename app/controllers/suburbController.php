<?php
//usuarios-direcciones-colonias
class suburbController extends controllerBase {
	     public function add($data) {
        //Validar login
        $data['suburb']=strtoupper($data['suburb']);
        $col = new Suburb($data);
        if( $col->save() ) {
             //guarda la dirección de un suburb
            responseDriver::dispatch('D', array('id'=>$col->id));
        } else {
            //Error
            responseDriver::dispatch('E', "Error", "Error al intentar guardar la nueva dirección.");
        }
    
    }
    public function publish(){
        $data = inputDriver::getVar(__POST__);
		$data['enable']=1;
        if($data['id'] == '' || $data['id'] == '-1' || $data['id'] == -1) {
            unset($data['id']);
            $this->add($data);
        } else {
            $this->update($data);	
				}
    }
	
	//modifica la dirección de un suburb
    public function update($data) {
        //Validar login
        //dia y hora  de modificación 
       	$hoy=date("Y-m-d G:i");
						
        $data = inputDriver::getVar(__POST__);
		$address = Suburb::find_by_id($data['id']);
        if($address) {
        	unset($data['id']);
            $address->suburb=$data['suburb'];
			$address->id_zone=$data['id_zone'];
			$address->log=$data['log'];
			$address->log="||'Editado','".authDriver::getSUser()->name." ".authDriver::getSUser()->lastname."','".$hoy."'";
		if($address->save()) {
                        responseDriver::dispatch('M', "Cambios guardados", "Informacion actualizada correctamente en la dirección.");  
            } 
        } else {
            //Error
        }
		
    }
	public function xmls(){
		$su=Suburb::find('all',array("order"=>"suburb asc"));
		header('Content-type: text/xml');
			echo '<?xml version="1.0" encoding="UTF-8" ?>';
			echo "<Message>";
			foreach($su as $s){
				$s->id=strval($s->id);
				$s->id_zone=strval($s->id_zone);
				echo "<colonia>";
				echo "<id>{$s->id}</id>";
	            echo "<nombre>".strtoupper($s->suburb)."</nombre>";
	            echo "<zona>{$s->id_zone}</zona>
	            </colonia>";
			}
			echo "</Message>";
	}
	public function disable() { //deshabilita una colonia en la bdd
        //Validar login
        $data = inputDriver::getVar("aid");
        $sub = Suburb::find_by_id($data);
        if($sub){        	
            $sub->update_attributes(array('enable' => 0));
            responseDriver::dispatch('D', array('message'=>"colonia deleted"));
        }
    }
}
