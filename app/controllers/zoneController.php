<?php
class zoneController extends controllerBase {
	     public function add() {
        //Validar login
        $data = inputDriver::getVar(__POST__);
        
        $address = new Zone($data);
        if( $address->is_valid() ) {
            $address->save();
            responseDriver::dispatch('D', array('id'=>$address->id));
        } else {
            //Error
            responseDriver::dispatch('E', "Error", "Error al intentar guardar la nueva dirección.");
        }
    
    }
		//agrega el dato de una zona o lo modifica 
    public function publish(){ 
        $data = inputDriver::getVar(__POST__);
        $isnew = false;
		
        if($data['id'] == '' || $data['id'] == '-1' || $data['id'] == -1) {
        	//destruye la variable local
            unset($data['id']); 
            $this->add($data);
        } else {
            $this->update($data);
        }
    }
	//modifica una dirección si se encuentra en zonas
    public function update() { 
        //Validar login
        $data = inputDriver::getVar(__POST__);
        //busca la zona dependiendo del id
        $address = Zone::find_by_id($data['id']); 
        if($address) {
            unset($data['id']);
            $address->update_attributes($data);
            //sino hay ningún error
            if(!$address->errors) { 
        		responseDriver::dispatch('M', "Cambios guardados", "Informacion actualizada correctamente en la dirección.");
            } else {
                //Error
            }
        } else {
            $this->add($data);
        }
    }
	//deshabilita una zona
	public function disable() {  
        //Validar login
        $data = inputDriver::getVar("aid");
        $z = Zone::find_by_id($data);
        if($z){
        	//deshabilita la zona
            $z->update_attributes(array('enable' => 0)); 
            responseDriver::dispatch('D', array('message'=>"zona deleted"));
        }
    }
}
