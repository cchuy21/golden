<?php

use Passbook\Pass\Field;
use Passbook\Pass\Image;
use Passbook\PassFactory;
use Passbook\Pass\Barcode;
use Passbook\Pass\Structure;
use Passbook\Type\EventTicket;

class passbookController extends controllerBase {
    
    public function index() {
        $pass = new EventTicket("1234567890", "The Beat Goes On");
        $pass->setBackgroundColor('rgb(60, 65, 76)'); //le da color y opacidad
        $pass->setLogoText('Apple Inc.'); 
        // Create pass structure
        $structure = new Structure();

        // Add primary field
        $primary = new Field('event', 'The Beat Goes On');
        $primary->setLabel('Event'); //le da el nombre de la etiqueta a la pantalla
        $structure->addPrimaryField($primary);

        // Add secondary field
        $secondary = new Field('location', 'Moscone West');
        $secondary->setLabel('Location');
        $structure->addSecondaryField($secondary);

        // Add auxiliary field
        $auxiliary = new Field('datetime', '2013-04-15 @10:25');
        $auxiliary->setLabel('Date & Time');
        $structure->addAuxiliaryField($auxiliary);

        // Add icon image
        $icon = new Image('../passbook/icon.png', 'icon');
        $pass->addImage($icon);

        // Set pass structure
        $pass->setStructure($structure);

        // Add barcode
        $barcode = new Barcode(Barcode::TYPE_QR, 'barcodeMessage');
        $pass->setBarcode($barcode);

        // Create pass factory instance
        //PASS-TYPE-IDENTIFIER
        //TEAM-IDENTIFIER
        //ORGANIZATION-NAME
        $factory = new PassFactory('pass.com.golden.reservaciones', 'ZDGK67DP98', 'Animactiva', '../passbook/Certificates.p12', 'G0ld3n961001', '../passbook/AppleWWDRCA.pem');
        $factory->setOutputPath('../passbook/output');
        $factory->package($pass);
    }
    
    
}

