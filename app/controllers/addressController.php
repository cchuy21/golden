<?php
class addressController extends controllerBase {
	public function alias1(){
		$us = Address::find('all', array('conditions' => "alias=''","order"=>"user_id asc","group"=>"user_id"));
		$uid=0;
		foreach($us as $u){
			$al=Address::find("all",array("conditions"=>"user_id='".$u->user_id."'"));
			$c=1;
			echo "id-".$u->user_id."<br />";
			foreach($al as $a){
				$a->alias="alias".$c;
				$a->save();
				$c++;
			}
		}
	}//1968
	public function dispatch($id = null) {
		if (!$id && !is_numeric($id))
			return;
		$d = Address::find('all', array('conditions' => "user_id = {$id} and status=1"));
		//print_r($direcciones); exit;

		header('Content-type: text/xml');
		echo '<?xml version="1.0" encoding="UTF-8" ?>';
		echo "<Message>";
		if (count($d)) {
			foreach ($d as $o) {
				$s = Suburb::find($o -> suburb_id);
				$z = Zone::find($s -> id_zone);
				$c = Zone::find_by_id("app");
				echo "
				<direccion>
		            <id>{$o->id}</id>  
		            <alias>{$o->alias}</alias> 
		            <calle>{$o->street}</calle>  
		            <colonia>{$s->suburb}</colonia>
		            <entre>{$o->between}</entre>
		            <colid>{$o->suburb_id}</colid>
		            <telefono>{$o->telephones}</telefono>
		            <cuadrante>{$o->quadrant}</cuadrante>
		            <notas>{$o->reference}</notas>
		            <pautomovil>{$z->cost_car}</pautomovil>
		            <pcamioneta>{$z->cost_van}</pcamioneta>		           
		            <dautomovil>{$z->des_car}</dautomovil>
		            <dcamioneta>{$z->des_van}</dcamioneta>
		            <zona>{$z->id}</zona>";
				//  echo " //cambiar c por z para costo por la zona y z por c para costo estatico
				//  <pautomovil>340</pautomovil>
				// <pcamioneta>650</pcamioneta>";
				echo "</direccion>";
			}
		} else {
			echo "<error value='No hay direcciones registradas' />";
		}

		echo "</Message>";
	}

	public function add($data) {
		//Validar login
		//isset-si la variable esta definida y tiene un valor diferente a null
		if (isset($data['id']))//    unset-destruye el valor de la variable dentro del if
			unset($data['id']);
		$address = new Address($data);
		//si la dirección es valida
		if ($address -> is_valid()) {
			//se guarda en la base de datos

			$address -> save();
			responseDriver::dispatch('D', array('id' => $address -> id));
		} else {
			//Error
			responseDriver::dispatch('E', "Error", "Error al intentar guardar la nueva dirección.");
		}

	}
	
	//se convierten los datos a mayusculas para guardarlos
	public function publish() {
		$hoy = date("Y-m-d G:i");
		$data = inputDriver::getVar(__POST__);

		$data['alias'] = strtoupper($data['alias']);
		$data['street'] = strtoupper($data['street']);
		$data['reference'] = strtoupper($data['reference']);
		$data['between'] = strtoupper($data['between']);
		$isnew = false;
		if ($data['id'] == '' || $data['id'] == '-1' || $data['id'] == -1) {
			unset($data['id']);
			//aqui se agrega nuevo
			$data['log'] = "'Creado','" . authDriver::getSUser() -> name . " " . authDriver::getSUser() -> lastname . "','" . $hoy . "'";
			$this -> add($data);
		} else {
			//aqui se actualiza
			$this -> update($data);
		}
	}

	//se actualiza la dirección
	public function appupdate() {
		$required = array('id', 'alias', 'calle', 'colonia', 'calle1');
		foreach ($required as $value) {
			if (trim($_POST[$value]) == "") {
				$this -> dispatchErrorXML("Alguno de los campos está en blanco o estás usando caracteres especiales (~,`,œ,ñ...)");
			}
		}
		$id = $_POST['id'];
		$alias = $_POST['alias'];
		$calle = $_POST['calle'];
		$colonia = $_POST['colonia'];
		$calle1 = $_POST['calle1'];
		$idLogin = $_POST['idLogin'];

		$add = Address::find($id);
		$add -> alias = $alias;
		$add -> user_id = $idLogin;
		$add -> street = $calle;
		$add -> suburb_id = $colonia;
		$add -> between = $calle1;

		if ($add -> save()) {
			$this -> dispatchMessageXML("Dirección actualizada correctamente");
		} else {
			$this -> dispatchErrorXML("No se pudo actualizar la direccion intentelo de nuevo más tarde." . $add -> user_id);
		}
	}

	public function update($data) {
		$hoy = date("Y-m-d G:i");
		//Validar login
		$address = Address::find_by_id($data['id']);
		if ($address) {
			unset($data['id']);
			$address -> log = $address -> log."||'Editado','" . authDriver::getSUser() -> name . " " . authDriver::getSUser() -> lastname . "','" . $hoy . "'";
			$address -> update_attributes($data);
			if (!$address -> errors) {

			} else {
				//Error
			}
		} else {
			//Error
		}
		responseDriver::dispatch('M', "Cambios guardados", "Informacion actualizada correctamente en la dirección.");
	}

	//se deshabilita una direccion
	public function disable() {
		//Validar login
		$data = inputDriver::getVar("aid");
		$address = Address::find_by_id($data);
		if ($address) {

			$address -> update_attributes(array('status' => 0));
			responseDriver::dispatch('D', array('message' => "address deleted"));
		}
	}

	//se elimina la direccion existente
	public function delete() {
		//Validar login
		$data = inputDriver::getVar("aid");
		$address = Address::find_by_id($data);
		if ($address) {
			$address -> delete();
			responseDriver::dispatch('D', array('message' => "address disabled"));
		}
	}

	public function getFields($uid = null, $tmpAlias = '') {
		if ($tmpAlias == '0') {
			//manda el valor de fields a un id especifico
			templateDriver::setData("fields", array("id" => $uid));
		} else {
			templateDriver::setData("temporal", array("alias" => $tmpAlias, "uid" => $uid));
		}
		templateDriver::renderSection("address.fields");
	}

	public function getBase($id) {
		templateDriver::setData("user", array("id" => $id));
		templateDriver::renderSection("address.base");
	}

	public function appdelete() {
		if (isset($_POST['id'])) {
			$id = $_POST['id'];
			$r = Address::find_by_id($id);
			//$r = dbDriver::execQuery("UPDATE direcciones SET eliminado='1' WHERE id='{$id}'");
			if ($r) {
				$r -> status = 0;
				if ($r -> save())
					$this -> dispatchMessageXML("Dirección actualizada correctamente");
				else
					$this -> dispatchErrorXML("No se pudo actualizar la direccion intentelo de nuevo más tarde.");

			} else {
				$this -> dispatchErrorXML("No se pudo actualizar la direccion intentelo de nuevo más tarde.");
			}
		}

	}

	private function dispatchMessageXML($mensaje = null) {
		header('Content-type: text/xml');
		echo '<?xml version="1.0" encoding="UTF-8" ?>';
		echo "<Message>";
		echo "<mensaje value='{$mensaje}' />";
		echo "</Message>";
		exit ;
	}

	private function dispatchErrorXML($mensaje = null) {
		header('Content-type: text/xml');
		echo '<?xml version="1.0" encoding="UTF-8" ?>';
		echo "<Message>";
		echo '<error value="' . $mensaje . '"></error>';
		echo "</Message>";
		exit ;
	}

	public function insert() {
		$required = array('alias', 'calle', 'colonia', 'calle1', 'idLogin');
		foreach ($required as $value) {
			if (trim($_POST[$value]) == "") {
				$this -> dispatchErrorXML("Alguno de los campos está en blanco o estás usando caracteres especiales (~,`,œ,ñ...)");
			}
		}
		$alias = $_POST['alias'];
		$calle = $_POST['calle'];
		$colonia = $_POST['colonia'];
		$calle1 = $_POST['calle1'];
		$idLogin = $_POST['idLogin'];
		$add = new Address();
		$add -> alias = $alias;
		$add -> user_id = $idLogin;
		$add -> street = $calle;
		$add -> suburb_id = $colonia;
		$add -> between = $calle1;
		if ($add -> save()) {
			$this -> dispatchMessageXML("Dirección agregada correctamente");
		} else {
			$this -> dispatchErrorXML("No se pudo crear la direccion intentelo de nuevo más tarde.");
		}
	}

}
