<?php

class upreservationController extends controllerBase {//cuando la reservación es abierta

	public function masivePublish(){
        $dat = inputDriver::getVar(__POST__);
		$data=array();
		foreach ( $dat as $fields) {
			$data=array_merge($data,$fields);
		}
		
        $returnData = array();
		$ids=array();
		$c=0;
		$ct=$c1+$c2;
		
		
		//separa arrreglos
		for($i=1;$i<=2;$i++){
			if(($data['tipservice']=="red" && $i==2) || $i==1){
				$ar=array();
				$ar['id']=$data['id'.$i];
				$ar['addresses']=$data['addresses'.$i];
				$ar['vehicle_id']=$data['vehicle_id'.$i];
				$ar['passengers']=$data['passengers'.$i];
				$ar['terminal']=$data['terminal'.$i];
				$ar['airline_id']=$data['airline_id'.$i];
				$ar['flight_number']=$data['flight_number'.$i];
				$ar['flight_hour']=$data['flight_hour'.$i];
				$ar['cost']=$data['cost'.$i];
				$ar['reservation_date']=$data['reservation_date'.$i];
				$ar['reservation_time']=$data['reservation_time'.$i];
				$ar['annotations']=$data['annotations'.$i];
				
				if($data["reservation_date".$i]=="abierto"){
					//destruye la variable especifica en el post cuando el servicio es abierto para no tener una fecha
					
					$ar["reservation_date"]=null;
					$ar["reservation_time"]="23:59";
					$ar["abierto"] = 1;
					
				}else{
					$ar['abierto']=0;
				}
				$ar['flight_type']=$data['flight_type'.$i];
				$ar['type']=$data['type'.$i];
				if($i==1){
					$r1=$ar;
				}else{
					if($data['tipservice']=="red")
					$r2=$ar;
				}
			}
		}
		if($data['tipservice']=="red"){
			$id1=$this->editr($r1);
			$id2=$this->editr($r2);
		}else{
			$id1=$this->editr($r1);
			
			
		}
		$s=array();
		$s['tnueva']=$data['tnueva'];
		$s['payment']=$data['payment'];
		$s['id']=$data['service_id'];
		$s['user_id']=$data['user_id'];
		$s['especial']=$data['especial'];
		$s['reservation1']=$id1;
		if($id2){
			$s['reservation2']=$id2;
		}else{
			$s['reservation2']=null;
		} 
		$s['requested']=$data['requested'];
		$s['annotations']=$data['annotations'];
		
		//si el tipo de servicio es redondo y no es costo manual
		//si el costo que viene en post es mayor o igual al costo creado
		if($data['tipservice']=="red" && substr($data['cost'], 0, 1)!="m")
			if($data['cost']>=$ct)
				$s['cost']=$data['cost'];
			else
				$s['cost']=$ct;
		else
			$s['cost']=$data['cost'];
		//echo $s['cost'];			
				
		$this->creater($s);		
	}
	public function editr($fields){
		if (!$fields['id']) {
			unset($fields['payment']);
			if ($fields["reservation_date"] == "abierto") {

				$fields["reservation_date"]=null;
				$fields["reservation_time"]="23:59";
				$fields["abierto"] = 1;
			} else {
				$fields['abierto'] = 0;
			}
			$pasa = (int)$fields['passengers'];
			//valor entero de los pasajeros
			if ($pasa > 4 && $fields['vehicle_id'] == 1) {//si el numero total de pasajeros es mayor a cuatro lo cambia a automòvil
				$fields['vehicle_id'] = 2;
			}

			$R = new Reservation($fields);
			//se crea una nueva reservación
			if ($R -> is_valid()) {
				$R -> save();
				$fields['id'] = $R -> id;
			}
		} else {
			
			$R = Reservation::find_by_id($fields['id']);
			if ($R) {
				unset($fields['payment']);
				if ($fields["reservation_date"] == "abierto" || $fields["reservation_date"] == null) {
					$fields["reservation_date"]=null;
					$fields["reservation_time"]="23:59";
					$fields["abierto"] = 1;
				} else {
					$fields['abierto'] = 0;
				}
				$pasa = (int)$fields['passengers'];
				if ($pasa > 4 && $fields['vehicle_id'] == 1) {

					$fields['vehicle_id'] = 2;
				}
				//si los datos de una reservación se han modificado se guarda en el campo log quien modifico, que se modifico y la hora y dia
				$add = "se ha modificado ";
				if ($R -> passengers != $fields['passengers']) {
					$add = $add . "el número de pasajeros; ".$R -> passengers;
				}
				if ($R -> vehicle_id != $fields['vehicle_id']) {
					if($R -> vehicle_id==1){
						$aut="Automóvil";
					}else
						$aut="Camioneta";
					$add = $add . " el tipo de vehiculo; ".$aut;
				}
				if ($R -> addresses != $fields['addresses']) {
					$add = $add . " la dirección;";
				}
				if ($R -> terminal != $fields['terminal']) {
					$add = $add . " la terminal; ".$R -> terminal;
				}
				if ($R -> airline_id != $fields['airline_id']) {
					$add = $add . " la aerolinea;";
				}
				if ($R -> flight_number != $fields['flight_number ']) {
					$add = $add . " el número de vuelo; ".$R -> flight_number;
				}					
				if ($R -> cost != $fields['cost']) {
					$add = $add . " el costo;";
				}
				if ($R -> annotations != $fields['annotations']) {
					$add = $add . " una anotación; ".$R -> annotations;
				}
				if (date("Y-m-d",strtotime($R -> reservation_date)) != date("Y-m-d",strtotime($fields['reservation_date']))){
					$add = $add . " fecha de reservación; ".$R -> reservation_date;
				}
				if(!($fields['reservation_date']=='abierto' && $R->abierto==1)){
				if ($R -> flight_hour != $fields['flight_hour'].":00") {
					$add = $add . " la hora del vuelo; ".$R -> flight_hour;
				}
				if ($R -> reservation_time != $fields['reservation_time'].":00") {
					$add = $add . " la hora de la reservación; ".$R -> reservation_time;
				}
				if ($R -> flight_type != $fields['flight_type']) {
					$add = $add . " el tipo de vuelo;";
				}	
				}	
				if ($R -> type != $fields['type']) {
					$add = $add . " el tipo de reservación; ".$R -> type;
				}

				$hoy = date("Y-m-d G:i");
				$fields['log'] = $R -> log . "||'Editado por','" . authDriver::getSUser() -> name . "','" . $hoy . ",'$add";
				$R -> update_attributes($fields);
				//modifica los datos de una reservación
			}
			$fecha = date("Y-m-d");

			if (strtotime($R -> reservation_date) == strtotime($fecha)) {//si el dia de una reservaciin es iguak¡l a hoy
				$last = Last::find_by_id(1);
				$last -> reservation_update = $R -> id;
				$last -> save();
			}

		}
		return $R -> id;
	}
		public function creater($data){
		$hoy=date("Y-m-d G:i");
        if($data['id'] !== '') {
            $S = Service::find_by_id($data['id']);
			$data['user_id']= $S->user_id;
			
            if($S) {
            	unset($data['attended']);
            	//guarda el usuario final que ha modifcado el servicio
				$data['edited']=authDriver::getSUser()->name; 
				//guarda el día en que se modifico el servicio
				$data['updated_at']=date('d-m-Y');
				if($S->payment==$data["payment"]){
					$data['log']=$S->log;
				}else{
					$data['log']=$S->log."||'Payment','".authDriver::getSUser()->name."','".$hoy."','".$S->payment."-".$data['payment']."'";
				}
				//ingresa en el campo log quien lo edito y cuando
				$data['log']=$data['log']."||'Editado','".authDriver::getSUser()->name."','".$hoy."'";
                $S->update_attributes($data);
                responseDriver::dispatch('D', array('reservation1'=>$S->reservation1,'reservation2'=>$S->reservation2, 'service_id'=> sprintf("%06d", $S->id)));
            }
        }
	}
	public function masivePublishant() {
		$data = inputDriver::getVar(__POST__);
		$returnData = array();
		foreach ($data as $id => $fields) {

			if (!$fields['id']) {
				unset($fields['payment']);
				if ($fields["reservation_date"] == "abierto") {

					unset($fields["reservation_date"]);
					$fields["abierto"] = 1;
				} else {
					$fields['abierto'] = 0;
				}
				$pasa = (int)$fields['passengers'];
				//valor entero de los pasajeros
				if ($pasa > 4 && $fields['vehicle_id'] == 1) {//si el numero total de pasajeros es mayor a cuatro lo cambia a automòvil
					$fields['vehicle_id'] = 2;
				}

				$R = new Reservation($fields);
				//se crea una nueva reservación
				if ($R -> is_valid()) {
					$R -> save();
					$fields['id'] = $R -> id;
				}
			} else {
				$R = Reservation::find_by_id($fields['id']);
				if ($R) {
					unset($fields['payment']);
					if ($fields["reservation_date"] == "abierto") {
						$fields["reservation_date"]=null;
						$fields["abierto"] = 1;
					} else {
						$fields['abierto'] = 0;
					}
					$pasa = (int)$fields['passengers'];
					if ($pasa > 4 && $fields['vehicle_id'] == 1) {

						$fields['vehicle_id'] = 2;
					}
					//si los datos de una reservación se han modificado se guarda en el campo log quien modifico, que se modifico y la hora y dia
					$add = "se ha modificado ";
				if ($R -> passengers != $fields['passengers']) {
					$add = $add . "el número de pasajeros; ".$R -> passengers;
				}
				if ($R -> vehicle_id != $fields['vehicle_id']) {
					if($R -> vehicle_id==1){
						$aut="Automóvil";
					}else
						$aut="Camioneta";
						
					$add = $add . " el tipo de vehiculo; ".$aut;
				}
				if ($R -> addresses != $fields['addresses']) {
					$add = $add . " la dirección;";
				}
				if ($R -> terminal != $fields['terminal']) {
					$add = $add . " la terminal; ".$R -> terminal;
				}
				if ($R -> airline_id != $fields['airline_id']) {
					$add = $add . " la aerolinea;";
				}
				if ($R -> flight_number != $fields['flight_number ']) {
					$add = $add . " el número de vuelo; ".$R -> flight_number;
				}					
				if ($R -> cost != $fields['cost']) {
					$add = $add . " el costo;";
				}
				if ($R -> annotations != $fields['annotations']) {
					$add = $add . " una anotación; ".$R -> annotations;
				}
				if (date("Y-m-d",strtotime($R -> reservation_date)) != date("Y-m-d",strtotime($fields['reservation_date']))){
					$add = $add . " fecha de reservación; ".$R -> reservation_date;
				}
				if(!($fields['reservation_date']=='abierto' && $R->abierto==1)){
				if ($R -> flight_hour != $fields['flight_hour'].":00") {
					$add = $add . " la hora del vuelo; ".$R -> flight_hour;
				}
				if ($R -> reservation_time != $fields['reservation_time'].":00") {
					$add = $add . " la hora de la reservación; ".$R -> reservation_time;
				}
				if ($R -> flight_type != $fields['flight_type']) {
					$add = $add . " el tipo de vuelo;";
				}	
				}	
				if ($R -> type != $fields['type']) {
					$add = $add . " el tipo de reservación; ".$R -> type;
				}

					$hoy = date("Y-m-d G:i");
					$fields['log'] = $R -> log . "||'Editado por','" . authDriver::getSUser() -> name .  "','" . $hoy . ",'$add";
					$R -> update_attributes($fields);
					//modifica los datos de una reservación
				}
				$fecha = date("Y-m-d");

				if (strtotime($R -> reservation_date) == strtotime($fecha)) {//si el dia de una reservaciin es iguak¡l a hoy
					$last = Last::find_by_id(1);
					$last -> reservation_update = $R -> id;
					$last -> save();
				}

			}
			$returnData[$id] = $fields;
		}
		responseDriver::dispatch('D', $returnData);
	}

}
