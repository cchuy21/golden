<?php
class androidController extends controllerBase {
	function unidades(){
		$un=Unity::find("all",array("order"=>"economic asc"));
		foreach($un as $u){
			echo $u->economic." <br />".md5($u->id."_anim")." <br />";
		}
	}
	function numfol($folio) {
		$i = strlen($folio);
		switch ($i) {
			case 0 :
				$folio = 'xxxx';
				break;
			case 1 :
				$folio = '000' . $folio;
				break;
			case 2 :
				$folio = '00' . $folio;
				break;
			case 3 :
				$folio = '0' . $folio;
				break;
			case 4 :
				$folio = $folio;
				break;
			default :
				$folio = $folio;
		}
		return $folio;
	}
	public function check($id){
		$cho=Unity::find("all",array("conditions"=>"enable='1'"));
		foreach($cho as $c){
			if(md5($c->id."_anim")==$id){
				
				$dri=Operator::find("all",array("conditions"=>"id='".$c->driver."'"));
				if(count($dri)){
					foreach($dri as $dr){
						$d=$dr;
						break;
					}
					echo json_encode(array(array("idu"=>''.$c->id,"economic"=>''.$c->economic,"nombre"=>$d->username,"ido"=>''.$d->id)));
					exit;
				}else{
					echo json_encode(array(array("idu"=>'0',"economic"=>''.$c->economic,"nombre"=>"","ido"=>"")));
					exit;
				}
			}
		}
		echo json_encode(array(array("idu"=>"0","economic"=>"0","nombre"=>"0","ido"=>"0")));
	}
	function direc($array, $cond = false) {
		$r = "";
		//separa el array por ,
		$array = explode(",", $array);
		foreach ($array as $value) {
			//obtiene la longitud de la variable
			$lon = strlen($value);
			$lon = ($lon - 1) * (-1);
			//mostrar la letra que esta en una posición especifica de una variable
			$ind = substr($value, 0, $lon);

			if ($ind == "a") {
				//si el valor del caracter es a, se reemplaza por un espacio en blanco de lo que contiene la variable
				$value = str_replace("a", "", $value);
				$dir = Address::find_by_id($value);
				$sub = Suburb::find_by_id($dir -> suburb_id);
				//se determina la dirección a la que va el servicio
				if ($cond) {
					$r = "{$sub->suburb}";
				} else {
					$r = "{$sub->suburb}";
				}
			} else if ($ind == "s") {
				$value = str_replace("s", "", $value);
				$sub = Suburb::find_by_id($value);
				if ($cond) {
					$r = "{$sub->suburb}";
				} else
					$r = "{$sub->suburb}";
			}

			return $r;
		}
	}

	function llegadastodas() {
		$aressl=array();
        $aressl=$this->reserv("APTO-MTY",1,$aressl);
		$aresss=array();
		$aresss=$this->reserv2("MTY-APTO",2,$aresss);
		$ares=array_merge($aressl, $aresss);
		$hoy = date("Y-m-d");
		$hoym = date("Y-m-d", strtotime($hoy));
		$otra = strtotime('+1 day', strtotime($hoym));
		$otra = date("Y-m-d G:i", $otra);
		//LOS TICKETs de hoy y mañana
		$reser = Ticket::find("all", array("order" => "time asc", "conditions" => "status=1 AND created_at BETWEEN {ts '" . $hoy . "'} AND {ts '" . $otra . "'}"));
		if ($reser)
			foreach ($reser as $r) {
				$info = array();
				$bolfol = $this -> numfol($r -> folio);
				$info['id'] = $r -> id;
				$info['infoid'] = $bolfol;
				$tr="";
				if($r->terminal=="TERM_C"){
					$r->terminal="Term C";
				}else if($r->terminal=="TERM_B"){
					$r->terminal="Term B";
				}else if($r->terminal=="NAC"){
					$r->terminal="Term A";
				}
				if($r->zone=='ZONA HOTELERA AEROPUERTO'){ $zone='ZONA H';}
				if(substr($r->zone, 0,4)=="ZONA"){
					//muestra los primeros 5 caracteres de la zona
					$r->zone=substr($r->zone, 5,1);
				}else if($r->zone=="CAMBIO")
					$r->zone="C";
				else if($r->zone=="especial"){
					$r->zone="E";
				}
				$info['nombre'] = $r->terminal." ".$r->zone;
				$info['direccion'] = $r -> zone;
				$info['vehiculo'] = 1;
				$info['tipo'] = 0;
				$info['hoy'] = 1;
				array_push($ares, $info);

			}

		echo json_encode($ares);


	}
	function llegadastodasapp() {
		$ares=array();
		for($i=0;$i<=4;$i++){
		$info['nombre'] = "Proyecto ".$i;
		$info['direccion'] = "1";
		$info['vehiculo'] = 1;
		$info['tipo'] = 1;
		$info['hoy'] = 1;
		array_push($ares, $info);
					
		}
		for($i=0;$i<=4;$i++){
		$info['nombre'] = "Proyecto ".$i;
		$info['direccion'] = "1";
		$info['vehiculo'] = 1;
		$info['tipo'] = 0;
		$info['hoy'] = 0;
		array_push($ares, $info);
					
		}
		echo json_encode($ares);


	}
	function reserv($tipor,$tiporb,$ares){
		$hoy = date("Y-m-d");
		$hoym = date("Y-m-d", strtotime($hoy));
		$otra = strtotime('+1 day', strtotime($hoym));
		$otra = date("Y-m-d", $otra);
		$reser = Reservation::find("all", array('select' => 'id', "order" => "reservation_time asc", "conditions" => "type='".$tipor."' AND status=1 AND reservation_date ='" . $hoy . "'"));
		if(count($reser)){
		$id = '('; 
		$c = 0;
		foreach ($reser as $r) {
			$id .= $r -> id . ",";
		}

		$id = substr($id, 0, -1) . ")";
		$ser = Service::find('all', array('conditions' => array('reservation1 in ' . $id, "order" => "reservation_time asc")));

		$ser2 = Service::find('all', array('conditions' => array('reservation2 in ' . $id, "order" => "reservation_time asc")));
		$ul=0;
		foreach ($ser as $s) {
			$re = Reservation::find($s -> reservation1); 
			if ($re -> vehicle_id == 1) {
				$a = "Automóvil";
			} else {
				$a = "Camioneta";
			}
			
			$ares[$c]['id'] = $s -> reservation1;
			$ares[$c]['infoid'] = $s -> id;
			$ares[$c]['tipo'] = $tiporb;
			$ares[$c]['direccion'] = substr($re->reservation_time,0,5)." ".$this -> direc($re -> addresses);
			$ares[$c]['nombre'] = $s -> user -> name;
			$ares[$c]['time'] = $re->reservation_time;
			$ares[$c]['vehiculo'] = $a;
			
			if (strtotime($re -> reservation_date) == strtotime($hoy)) {
				$ares[$c]['hoy'] = 1;
			} else {
				$ares[$c]['hoy'] = 0;
			}
			$c++;
		}
		
		foreach ($ser2 as $s) {
			$ul=$c-1;
			$re = Reservation::find($s -> reservation2);

			if ($re -> vehicle_id == 1) {
				$a = "Automóvil";
			} else {
				$a = "Camioneta";
			}
			
			
				$ares[$c]['id'] = $s -> reservation2;
				$ares[$c]['infoid'] = $s -> id;
				$ares[$c]['tipo'] = $tiporb;
				$ares[$c]['direccion'] = substr($re->reservation_time,0,5)." ".$this -> direc($re -> addresses);
				$ares[$c]['vehiculo'] = $a;
				$ares[$c]['nombre'] = $s -> user -> name;
				$ares[$c]['time'] = $re->reservation_time;
				if (strtotime($re -> reservation_date) == strtotime($hoy)) {
					$ares[$c]['hoy'] = 1;
				} else {
					$ares[$c]['hoy'] = 0;
	
				}
			
			$c++;
		}
		}
		$ares2=array();
		foreach($ares as $ar1){
			$t1=strtotime(date("G:i",strtotime($ar1['time'])));
			$c=0;
			foreach($ares as $ar2){
				$t2=strtotime(date("G:i",strtotime($ar2['time'])));
				if($t1>$t2){
					$c++;
				}
			}
			//son iguales
 			while(strtotime(date("G:i",strtotime($ares2[$c]["time"])))==$t1)
				$c++;
			
			
			$ares2[intval($c)]=$ar1;	
		}
		ksort($ares2);
		$ares=array();
		$c=0;
		foreach($ares2 as $ar){
			$ares[$c]=$ar;
			$c++;
		}
                ksort($ares);

		return $ares;
		}	
	function reserv2($tipor,$tiporb,$ares){
		$hoy = date("Y-m-d");
		$hoym = date("Y-m-d", strtotime($hoy));
		$otra = strtotime('+1 day', strtotime($hoym));
		$otra = date("Y-m-d", $otra);
		$reser = Reservation::find("all", array('select' => 'id', "order" => "reservation_time asc", "conditions" => "type='".$tipor."' AND status=1 AND reservation_date ='" . $hoy . "'"));
		if(count($reser)){
		$id = '(';
		$c = 0;
		$r1=0;
		$arr=array();
		foreach ($reser as $r) {
			$id .= $r -> id . ",";
		    $arr[$r1]=$r -> id;
		    $r1++; 
		}
		$id = substr($id, 0, -1) . ")";
		$ser = Service::find('all', array('conditions' => array('reservation1 in ' . $id, "order" => "reservation_time asc")));

		$ser2 = Service::find('all', array('conditions' => array('reservation2 in ' . $id, "order" => "reservation_time asc")));
		$ul=0;
		foreach ($ser as $s) {
			$re = Reservation::find($s -> reservation1); 
			if ($re -> vehicle_id == 1) {
				$a = "Automóvil";
				$a1="Auto";
			} else {
				$a = "Camioneta";
				$a1="Cam";
			}
			
			$ares[$c]['id'] = $s -> reservation1;
			$ares[$c]['infoid'] = $s -> id;
			$ares[$c]['tipo'] = $tiporb;
			$ares[$c]['direccion'] = substr($re->reservation_time,0,5)." ".$this -> direc($re -> addresses);
			if(!$re->flight_number || $re->flight_number==""||$re->flight_number==" "){
				if(!$re->terminal || $re->terminal==""||$re->terminal==" ")
					$ares[$c]['nombre'] = $s -> user -> name." V:XXX ".$a1." ".$re->passengers." X";
				else
					$ares[$c]['nombre'] = $s -> user -> name." V:XXX ".$a1." ".$re->passengers." ".$re->terminal;
			} else {
				if(!$re->terminal || $re->terminal==""||$re->terminal==" ")
					$ares[$c]['nombre'] = $s -> user -> name." V:".$re->flight_number." ".$a1." ".$re->passengers." X";
				else
					$ares[$c]['nombre'] = $s -> user -> name." V:".$re->flight_number." ".$a1." ".$re->passengers." ".$re->terminal;
			}
			$ares[$c]['time'] = $re->reservation_time;
			$ares[$c]['vehiculo'] = $a;
			
			if (strtotime($re -> reservation_date) == strtotime($hoy)) {
				$ares[$c]['hoy'] = 1;
			} else {
				$ares[$c]['hoy'] = 0;
			}
			$c++;
		}
		
		foreach ($ser2 as $s) {
			$ul=$c-1;
			$re = Reservation::find($s -> reservation2);

			if ($re -> vehicle_id == 1) {
				$a = "Automóvil";
				$a1="Auto";
			} else {
				$a = "Camioneta";
				$a1="Cam";
			}
			
			
				$ares[$c]['id'] = $s -> reservation2;
				$ares[$c]['infoid'] = $s -> id;
				$ares[$c]['tipo'] = $tiporb;
				$ares[$c]['direccion'] = substr($re->reservation_time,0,5)." ".$this -> direc($re -> addresses);
				$ares[$c]['vehiculo'] = $a;
				/*$ares[$c]['nombre'] = $s -> user -> name;
				if(!$re->flight_number || $re->flight_number==""||$re->flight_number==" "){
					if(!$re->terminal || $re->terminal==""||$re->terminal==" ")
						$ares[$c]['inf'] = "V:XXX ".$a1." ".$re->passengers." X";
					else
						$ares[$c]['inf'] = "V:XXX ".$a1." ".$re->passengers." ".$re->terminal;
				} else {
					if(!$re->terminal || $re->terminal==""||$re->terminal==" ")
						$ares[$c]['inf'] = "V:".$re->flight_number." ".$a1." ".$re->passengers." X";
					else
						$ares[$c]['inf'] = "V:".$re->flight_number." ".$a1." ".$re->passengers." ".$re->terminal;
				}	*/
				if(!$re->flight_number || $re->flight_number==""||$re->flight_number==" "){
					if(!$re->terminal || $re->terminal==""||$re->terminal==" ")
						$ares[$c]['nombre'] = $s -> user -> name." V:XXX ".$a1." ".$re->passengers." X";
					else
						$ares[$c]['nombre'] = $s -> user -> name." V:XXX ".$a1." ".$re->passengers." ".$re->terminal;
				} else {
					if(!$re->terminal || $re->terminal==""||$re->terminal==" ")
						$ares[$c]['nombre'] = $s -> user -> name." V:".$re->flight_number." ".$a1." ".$re->passengers." X";
					else
						$ares[$c]['nombre'] = $s -> user -> name." V:".$re->flight_number." ".$a1." ".$re->passengers." ".$re->terminal;
				}				
				$ares[$c]['time'] = $re->reservation_time;
				if (strtotime($re -> reservation_date) == strtotime($hoy)) {
					$ares[$c]['hoy'] = 1;
				} else {
					$ares[$c]['hoy'] = 0;
	
				}
			
			$c++;
		}
		}
		$ares2=array();
		foreach($ares as $ar1){
			$t1=$ar1['id'];
			foreach($arr as $key=>$ar2){
            	if($t1==$ar2){
			        $ares2[$key]=$ar1;	
                 }
            }
		}
		ksort($ares2);
		$ares=array();
		$c=0;
		foreach($ares2 as $ar){
			$ares[$c]=$ar;
			$c++;
		}
        ksort($ares);
		return $ares;
	}

	function reserv3($tipor,$tiporb,$ares){
		$hoy = date("Y-m-d");
		$hoym = date("Y-m-d", strtotime($hoy));
		$otra = strtotime('+1 day', strtotime($hoym));
		$otra = date("Y-m-d", $otra);
		$reser = Reservation::find("all", array('select' => 'id', "order" => "reservation_time asc", "conditions" => "type='".$tipor."' AND status=1 AND reservation_date ='" . $hoy . "'"));
		if(count($reser)){
		$id = '(';
		$c = 0;
		$r1=0;
        $arr=array();
		foreach ($reser as $r) {
			$id .= $r -> id . ",";
			$arr[$r1]=$r -> id;
			$r1++;
		}
		$id = substr($id, 0, -1) . ")";
		$ser = Service::find('all', array("select"=>"reservation1,id,user_id",'conditions' => array('reservation1 in ' . $id, "order" => "reservation_time asc")));
		$ser2 = Service::find('all', array("select"=>"reservation2,id,user_id",'conditions' => array('reservation2 in ' . $id, "order" => "reservation_time asc")));
		$ul=0;
		foreach ($ser as $s) {
		    if(isset($s->user->name)){
				$re = Reservation::find($s->reservation1,array("select"=>"vehicle_id,reservation_time,addresses,flight_number,terminal,passengers,reservation_date")); 
				if ($re -> vehicle_id == 1) {
					$a = "Automóvil";
					$a1="Auto";
				} else {
					$a = "Camioneta";
					$a1="Cam";
				}
				
				$ares[$c]['id'] = $s->reservation1;
				$ares[$c]['infoid'] = $s -> id;
				$ares[$c]['tipo'] = $tiporb;
				$ares[$c]['direccion'] = substr($re->reservation_time,0,5)." ".$this -> direc($re -> addresses);
				$ares[$c]['nombre'] = $s -> user -> name;
					if(!$re->flight_number || $re->flight_number==""||$re->flight_number==" "){
						if(!$re->terminal || $re->terminal==""||$re->terminal==" ")
							$ares[$c]['inf'] = "V:XXX ".$a1." ".$re->passengers." X";
						else
							$ares[$c]['inf'] = "V:XXX ".$a1." ".$re->passengers." ".$re->terminal;
					} else {
						if(!$re->terminal || $re->terminal==""||$re->terminal==" ")
							$ares[$c]['inf'] = "V:".$re->flight_number." ".$a1." ".$re->passengers." X";
						else
							$ares[$c]['inf'] = "V:".$re->flight_number." ".$a1." ".$re->passengers." ".$re->terminal;
					}
				$ares[$c]['time'] = $re->reservation_time;
				$ares[$c]['vehiculo'] = $a;
				
				if (strtotime($re -> reservation_date) == strtotime($hoy)) {
					$ares[$c]['hoy'] = 1;
				} else {
					$ares[$c]['hoy'] = 0;
				}
				$c++;
			}
		}
		
		foreach ($ser2 as $s) {
			$ul=$c-1;
			$re = Reservation::find($s -> reservation2,array("select"=>"vehicle_id,reservation_time,addresses,flight_number,terminal,passengers,reservation_date"));

			if ($re -> vehicle_id == 1) {
				$a = "Automóvil";
				$a1="Auto";
			} else {
				$a = "Camioneta";
				$a1="Cam";
			}
			
			
				$ares[$c]['id'] = $s -> reservation2;
				$ares[$c]['infoid'] = $s -> id;
				$ares[$c]['tipo'] = $tiporb;
				$ares[$c]['direccion'] = substr($re->reservation_time,0,5)." ".$this -> direc($re -> addresses);
				$ares[$c]['vehiculo'] = $a;
				$ares[$c]['nombre'] = $s -> user -> name;
				if(!$re->flight_number || $re->flight_number==""||$re->flight_number==" "){
					if(!$re->terminal || $re->terminal==""||$re->terminal==" ")
						$ares[$c]['inf'] = "V:XXX ".$a1." ".$re->passengers." X";
					else
						$ares[$c]['inf'] = "V:XXX ".$a1." ".$re->passengers." ".$re->terminal;
				} else {
					if(!$re->terminal || $re->terminal==""||$re->terminal==" ")
						$ares[$c]['inf'] = "V:".$re->flight_number." ".$a1." ".$re->passengers." X";
					else
						$ares[$c]['inf'] = "V:".$re->flight_number." ".$a1." ".$re->passengers." ".$re->terminal;
				}				
				$ares[$c]['time'] = $re->reservation_time;
				if (strtotime($re -> reservation_date) == strtotime($hoy)) {
					$ares[$c]['hoy'] = 1;
				} else {
					$ares[$c]['hoy'] = 0;
				}
			
			$c++;
		}
		}
		$ares2=array();
		foreach($ares as $ar1){
			$t1=$ar1['id'];
			foreach($arr as $key=>$ar2){
            	if($t1==$ar2){
			        $ares2[$key]=$ar1;	
                 }
            }
		}
		ksort($ares2);
		$ares=array();
		$c=0;
		foreach($ares2 as $ar){
			$ares[$c]=$ar;
			$c++;
		}
        ksort($ares);
		return $ares;
	}
	function llegadastodas3() {
		$aressl=array();
        $aressl=$this->reserv3("APTO-MTY",1,$aressl);
		$aresss=array();
		$aresss=$this->reserv3("MTY-APTO",2,$aresss);
		$ares=array_merge($aressl, $aresss);
		$hoy = date("Y-m-d");
		$hoym = date("Y-m-d", strtotime($hoy));
		$otra = strtotime('+1 day', strtotime($hoym));
		$otra = date("Y-m-d G:i", $otra);
		//LOS TICKETs de hoy y mañana
		$reser = Ticket::find("all", array("select"=>"folio,id,terminal,zone","order" => "time asc", "conditions" => "status=1 AND created_at BETWEEN {ts '" . $hoy . "'} AND {ts '" . $otra . "'}"));
		if ($reser)
			foreach ($reser as $r) {
				$info = array();
				$bolfol = $this -> numfol($r -> folio);
				$info['id'] = $r -> id;
				$info['infoid'] = $bolfol;
				$tr="";
				if($r->terminal=="TERM_C"){
					$r->terminal="Term C";
				}else if($r->terminal=="TERM_B"){
					$r->terminal="Term B";
				}else if($r->terminal=="NAC"){
					$r->terminal="Term A";
				}
				if($r->zone=='ZONA HOTELERA AEROPUERTO'){ $zone='ZONA H';}
				if(substr($r->zone, 0,4)=="ZONA"){
					//muestra los primeros 5 caracteres de la zona
					$r->zone=substr($r->zone, 5,1);
				}else if($r->zone=="CAMBIO")
					$r->zone="C";
				else if($r->zone=="especial"){
					$r->zone="E";
				}
				$info['nombre'] = $r->terminal." ".$r->zone;
				$info['direccion'] = $r->zone;
				$info['vehiculo'] = 1;
				$info['tipo'] = 0;
				$info['hoy'] = 1;
				array_push($ares, $info);
			}
		echo json_encode($ares);
	}
	function llegadastodas2() {
		$aressl=array();
        $aressl=$this->reserv2("APTO-MTY",1,$aressl);
		$aresss=array();
		$aresss=$this->reserv2("MTY-APTO",2,$aresss);
		$ares=array_merge($aressl, $aresss);
		$hoy = date("Y-m-d");
		$hoym = date("Y-m-d", strtotime($hoy));
		$otra = strtotime('+1 day', strtotime($hoym));
		$otra = date("Y-m-d G:i", $otra);
		//LOS TICKETs de hoy y mañana
		$reser = Ticket::find("all", array("order" => "time asc", "conditions" => "status=1 AND created_at BETWEEN {ts '" . $hoy . "'} AND {ts '" . $otra . "'}"));
		if ($reser)
			foreach ($reser as $r) {
				$info = array();
				$bolfol = $this -> numfol($r -> folio);
				$info['id'] = $r -> id;
				$info['infoid'] = $bolfol;
				$tr="";
				if($r->terminal=="TERM_C"){
					$r->terminal="Term C";
				}else if($r->terminal=="TERM_B"){
					$r->terminal="Term B";
				}else if($r->terminal=="NAC"){
					$r->terminal="Term A";
				}
				if($r->zone=='ZONA HOTELERA AEROPUERTO'){ $zone='ZONA H';}
				if(substr($r->zone, 0,4)=="ZONA"){
					//muestra los primeros 5 caracteres de la zona
					$r->zone=substr($r->zone, 5,1);
				}else if($r->zone=="CAMBIO")
					$r->zone="C";
				else if($r->zone=="especial"){
					$r->zone="E";
				}
				$info['nombre'] = $r->terminal." ".$r->zone;
				$info['direccion'] = $r -> zone;
				$info['vehiculo'] = 1;
				$info['tipo'] = 0;
				$info['hoy'] = 1;
				array_push($ares, $info);
			}
		echo json_encode($ares);
	}
	function choferesdis() {
		header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		header('Content-Type: application/json');
		$un = Unity::find("all",array("conditions"=>"driver<>''"));
		$chof = array();
		foreach ($un as $u) {
			$info = array();
			$op = Operator::find_by_id($u -> driver);
			$info['ido'] = $op -> id;
			$info['idu'] = $u -> id;
			/*$info['ido'] = $op -> id;
			$info['idu'] = $u -> id;*/
			$info['economic'] = $u -> economic;
			$info['nombre'] = $op -> username;
			array_push($chof, $info);
		}
		echo json_encode($chof);
	}
	function choferesdis2() {
		header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		header('Content-Type: application/json');
		$un = Unity::find("all",array("select"=>"driver,economic,id","conditions"=>"driver<>''"));
		$chof = array();
		foreach ($un as $u) {
			$info = array();
			$op = Operator::find_by_id($u -> driver,array("select"=>"id,username"));
			$info['idu'] = $op -> id;
			$info['ido'] = $u -> id;
			/*$info['ido'] = $op -> id;
			$info['idu'] = $u -> id;*/
			$info['economic'] = $u -> economic;
			$info['nombre'] = $op -> username;
			array_push($chof, $info);
		}
		echo json_encode($chof);
	}
	//funcion para asignar
	function asignar($reserva = null, $chofer = null, $tipo = null) {
		//$reserva=$_REQUEST['reserva'];
		//$chofer=$_REQUEST['chofer'];
		//$tipo=$_REQUEST['tipo'];
		//si cualquier campo esta vacio no realiza ninguna operación
		header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		if (!isset($reserva) || !isset($chofer) || !isset($tipo)) {
			echo json_encode(array( array("status" => 0, "msg" => "No se pudo asignar, error datos faltantes")));
			exit ;
		}
		//tipo vale 1 si es reservación
		//solo cambio al campo que se vaya a guardar
		
		if ($tipo) {
			$data['reservation_id'] = $reserva;
			$r = Reservation::find($reserva);
			$data['operator_id'] = $chofer;
			$data['type'] = "Directo";
			$hor=date("G:i");
			if($r->type=="APTO-MTY"){
				$r->reservation_time=$hor;
			}
		} else {
			$data['ticket_id'] = $reserva;
			$r = Ticket::find_by_id($data['ticket_id']);
			$data['operator_id'] = $chofer;
			$data['type'] = "Directo";
			
		}
		$o = Operator::find_by_id($chofer);
		if ($o) {
			$data['operator_id'] = $o -> id;
			$un = Unity::find_by_driver($o -> id);

		} else {
			echo json_encode(array( array("status" => 0, "msg" => "No se pudo asignar, error en el operador y / o unidad")));
			exit ;
		}

		if ($un) {
			$data['unity_id'] = $un -> id;
			$un -> status = 3;
			$un -> save();
		} else {
			echo json_encode(array( array("status" => 0, "msg" => "No se pudo asignar, error en el operador y / o unidad")));
			exit ;
		}
		//se agrego para evitar reservar duplicadas
		if ($tipo)
			$as = Assignment::find_by_reservation_id($data['reservation_id']);
		else
			$as = Assignment::find_by_ticket_id($data['ticket_id']);

		if ($as) {//si existe el registro entra
			$as->update_attributes($data); //si existe actualiza datos
			echo json_encode(array( array("status" => 0, "msg" => "ya estaba asignada")));
			if($tipo){
				$s=Service::find_by_reservation1($r -> id);
				if(!$s)
					$s=Service::find_by_reservation2($r -> id);
				$s->log=$s->log."||'Asignado','".$un->economic." ".$o->username."','".date("Y-m-d G:i")."'";
				$s->save();
			}
			$r -> save();
			if ($tipo) {
				$this->correoasing($r, $un, $o);
			} else {
				//$this->alertbol($r, $un, $o);
			}
			exit;
		} else {//si no existe crea uno nuevo
			$data["starttime"]=date("Y-m-d G:i:s");
			$assigment = new Assignment($data);
			if ($assigment -> save()) {
				$r -> status = 2;
				if($tipo){
					$s=Service::find_by_reservation1($r -> id);
					if(!$s)
						$s=Service::find_by_reservation2($r -> id);
					$s->log=$s->log."||'Asignado','".$un->economic." ".$o->username."','".date("Y-m-d G:i")."'";
					$s->save();
				}
				$r -> save();
				echo json_encode(array( array("status" => 1, "msg" => "Confirmado")));
				if ($tipo)
					$this->correoasing($r, $un, $o);
				else{
					//$this->alertbol($r, $un, $o);
				}
				exit;
			}else{
				echo json_encode(array( array("status" => 0, "msg" => "Error")));
				exit;
			}
		}//cierre de la condicional de si existe o no el registro*/
	}
		//funcion para asignar
	function asignar2($reserva = null, $unidad = null, $tipo = null) {
		//$reserva=$_REQUEST['reserva'];
		//$chofer=$_REQUEST['chofer'];
		//$tipo=$_REQUEST['tipo'];
		//si cualquier campo esta vacio no realiza ninguna operación
		header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		if (!isset($reserva) || !isset($unidad) || !isset($tipo)) {
			echo json_encode(array( array("status" => 0, "msg" => "No se pudo asignar, error datos faltantes")));
			exit ;
		}
		//tipo vale 1 si es reservación
		//solo cambio al campo que se vaya a guardar
		
		if ($tipo) {
			$data['reservation_id'] = $reserva;
			$r = Reservation::find($reserva);
			$data['unity_id'] = $unidad;
			$data['type'] = "Directo";
			$hor=date("G:i");
			if($r->type=="APTO-MTY"){
				$r->reservation_time=$hor;
			}
		} else {
			$data['ticket_id'] = $reserva;
			$r = Ticket::find_by_id($data['ticket_id']);
			$data['unity_id'] = $unidad;
			$data['type'] = "Directo";
		}
		$un=Unity::find($unidad);
		if($un){
			$data['unity_id'] = $un -> id;
			if($un->driver){
				$data['operator_id'] = $un -> driver;
				$o = Operator::find_by_id($un -> driver);
			}else{
				echo json_encode(array( array("status" => 0, "msg" => "No se pudo asignar, error en el operador y / o unidad")));
				exit;	
			}				
		}else{
		    echo json_encode(array( array("status" => 0, "msg" => "No se pudo asignar, error en el operador y / o unidad")));
		    exit;
		}
		
		
		//anterior
		/*$o = Operator::find_by_id($chofer);
		if ($o) {
			$data['operator_id'] = $o -> id;
			$un = Unity::find_by_driver($o -> id);

		} else {
			echo json_encode(array( array("status" => 0, "msg" => "No se pudo asignar, error en el operador y / o unidad")));
			exit ;
		}

		if ($un) {
			$data['unity_id'] = $un -> id;
			$un -> status = 3;
			$un -> save();
		} else {
			echo json_encode(array( array("status" => 0, "msg" => "No se pudo asignar, error en el operador y / o unidad")));
			exit ;
		}*/
		//se agrego para evitar reservar duplicadas
		if($tipo){
			$as = Assignment::find_by_reservation_id($data['reservation_id']);
		}else{
			$as = Assignment::find_by_ticket_id($data['ticket_id']);
		}
		if ($as) {//si existe el registro entra
			$as->update_attributes($data); //si existe actualiza datos
			echo json_encode(array( array("status" => 0, "msg" => "ya estaba asignada")));
			
			if($tipo){
					$s=Service::find_by_reservation1($r -> id);
					if(!$s)
						$s=Service::find_by_reservation2($r -> id);
					$s->log=$s->log."||'Asignado','".$un->economic." ".$o->username."','".date("Y-m-d G:i")."'";
					$s->save();
				}
				$r -> save();
			$this->sendnot($un->phone_id);
			if ($tipo) {
				$this->correoasing($r, $un, $o);
			} else {
				//$this->alertbol($r, $un, $o);
			}
			exit;
		} else {//si no existe crea uno nuevo
			$data["starttime"]=date("Y-m-d G:i:s");
			$assigment = new Assignment($data);
			if ($assigment -> save()) {
				$r -> status = 2;
				if($tipo){
					$s=Service::find_by_reservation1($r -> id);
					if(!$s)
						$s=Service::find_by_reservation2($r -> id);
					$s->log=$s->log."||'Asignado','".$un->economic." ".$o->username."','".date("Y-m-d G:i")."'";
					$s->save();
				}
				$r -> save();

				echo json_encode(array( array("status" => 1, "msg" => "Confirmado")));
				$this->sendnot($un->phone_id);
				if ($tipo)
					$this->correoasing($r, $un, $o);
				else{
					//$this->alertbol($r, $un, $o);
				}
				exit ;
			}else{
				echo json_encode(array( array("status" => 0, "msg" => "Error")));
				exit ;
			}
		}//cierre de la condicional de si existe o no el registro*/
	}
	public function sendnot($phe){
		$server_key='AIzaSyDmVM6ZPCPIJiPpDbfjK4w9_Air_xUd3IU';
		$headers = array(
			'Authorization:key='.$server_key,
			'Content-Type:application/json'
		);
		$ph=Phone::find("all",array("conditions"=>"id='".$phe."'"));
		if(count($ph)){
			$p=Phone::find($phe);
			//key token
			$key=$p->token;
			$fields = array('to'=>$key,'data'=>array('prueba'=>'nuevo'));
			$payload = json_encode($fields);
			$curl_session = curl_init();
			$path_to_fcm='https://fcm.googleapis.com/fcm/send';
			curl_setopt($curl_session, CURLOPT_URL,$path_to_fcm);
			curl_setopt($curl_session, CURLOPT_POST,true);
			curl_setopt($curl_session, CURLOPT_HTTPHEADER,$headers);
			curl_setopt($curl_session, CURLOPT_RETURNTRANSFER,true);
			curl_setopt($curl_session, CURLOPT_SSL_VERIFYPEER,false);
			curl_setopt($curl_session, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
			curl_setopt($curl_session, CURLOPT_POSTFIELDS,$payload);
			$result = curl_exec($curl_session);
			curl_close($curl_session);
		}
	}
	//funcion para desasignar desde la app
	function desasignar($reserva) {
		//busca la reservacion
		$reser = Reservation::find_by_id($reserva);
		if ($reser) {
			$reser -> status = 1;
			//guardar reservacion
			//$reser -> save();
			$as = Assignment::find_by_reservation_id($data['reservation_id']);
			if ($as) {

			}
		}
	}
	function alertbol($r, $un, $o){
		
		//http://gps.goldentransportaciones.com/services/addservice/?fecha=2016-05-02&nombre_pasajero=EPSA&direccion=ZONA%203&observaciones=boleto&telefono=boleto&chofer_external_id=60&external_id=220300316050200000001&aeropuertociudad=1&numero_reservacion=220300316050200000001
		$hoy=date("Y-m-d G:i");
		$ch = curl_init();
		$url='http://gps.goldentransportaciones.com/services/addservice/?fecha='.str_replace(" ","%20",$hoy).'&nombre_pasajero=EPSA&direccion='.str_replace(" ","%20",$r -> zone).'&observaciones=boleto&telefono=boleto&chofer_external_id='.$o->id.'&external_id='.trim($r->code, " \t\n\r\0\x0B").'&aeropuertociudad=1&numero_reservacion='.trim($r->code, " \t\n\r\0\x0B");
		curl_setopt_array($ch, array(
    		CURLOPT_RETURNTRANSFER => 1,
    		CURLOPT_URL => $url,
		));
		// Turn off the server and peer verification (TrustManager Concept).
		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		//curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		
		echo $httpResponse = curl_exec($ch);
		if(!$httpResponse) {
			exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
		}
	} 
		
	function correoasing($r,$un,$op,$ti="ASIGNACIÓN DE RESERVACIÓN",$tii="RESERVATION ASSIGNMENT"){
						//email
		$s=Service::find_by_reservation1($r->id);
		if(!$s)
			$s=Service::find_by_reservation2($r->id);
		$ass=Assignment::find_by_reservation_id($r->id);
		$hoy=date("Y-m-d");
		$fecha=date("Y-m-d", strtotime($ass->updated_at));
		if($hoy==$fecha){
			if(strpos($s->user->email, "taxiweb")===true){
				$para="servicios@goldentransportaciones.com";
			}else{
				$para=$s->user->email;
			}
					//$para="chuy@chivasentada.com";
			$titulo="Asignado - Asigned";	
					// Cabeceras adicionales
			$cabeceras = array("From" => 'Asignaciones - Golden - Asignment' ,"To"=> $para, "MIME-Version"=>"1.0", "Content-type" =>"text/html");
			$msj="
			<div width='650' style='font-family:helvetica!important;'>
				<img  width='650' src='http://resv.goldentransportaciones.net/static/images/top.jpg' />
				<!--cabecera-->
				<br /> <br /> 
				<table width='650'>
					<tr>
						<td>
							<center><span style='color:#26143e; font-size:20px;font-family: helvetica;'><strong>".htmlentities($ti)."</strong></span></center>
							<center><span style='color:#26143e; font-size:20px;font-family: helvetica;'><strong>".htmlentities($tii)."</strong></span></center>
						</td>
						<td>
							<center><span style='color:#c69b10; font-size:50px;font-family: helvetica;'><strong>".htmlentities($s->id)."</strong></span></center>
						</td>
					</tr>
				</table>
				<br />
				<table width='550' style='margin-left:50px;    border-collapse: collapse!important;'>
					<tr bgcolor='#26143e'>
						<td height='33' width='650' colspan='3' >
							<center><span style='color:#fff; font-size:14px;font-family: helvetica;font-weight: 300;'>NOMBRE / NAME</span></center>
						</td>
						<td rowspan=4>
							<center><img src='http://resv.goldentransportaciones.net/static/images/fotos/".$op->picture."' width='100' style='padding: 5px;' ></center>
						</td> 
					</tr>
					<tr height='33' bgcolor='#e6e6e6'>
						<td colspan='3'>
							<center><span style='color:#26143e; font-size:18px;font-family: helvetica;'><strong>".htmlentities($s->user->name)."</strong></span></center>
						</td>
					</tr>
					<tr height='33' bgcolor='#26143e'>
						<td>
							<center><span style='color:#fff; font-size:14px;font-family: helvetica; font-weight: 300;'>ID / ID</span></center>
						</td>
						<td>
							<center><span style='color:#fff; font-size:14px;font-family: helvetica; font-weight: 300;'>MODELO / MODEL</span></center>
						</td>
						<td>
							<center><span style='color:#fff; font-size:14px;font-family: helvetica; font-weight: 300;'>CHOFER / DRIVER</span></center>
						</td>
					</tr>
					<tr height='33' bgcolor='#e6e6e6'>
						<td>
							<center><span style='color:#26143e;font-size:18px;font-family: helvetica;'><strong>".htmlentities($un->economic)."</strong></span></center>
						</td>
						<td>
							<center><span style='color:#26143e;font-size:18px;font-family: helvetica;'><strong>".htmlentities($un->model)."</strong></span></center>
						</td>
						<td>
							<center><span style='color:#26143e;font-size:18px;font-family: helvetica;'><strong>".htmlentities($op->name." ".$op->lastname)."</strong></span></center>
						</td>
					</tr>
				</table>
				<br /> 
				<img width='650' src='http://resv.goldentransportaciones.net/static/images/bottom.jpg' />
			</div>"; 
			//se envia los datos del email para el usuario correspondiente
			mailDriver::send2($para, $titulo, $msj, $cabeceras);
			//mail($para, $titulo, $mensaje, $cabeceras);
			$hoy=date("Y-m-d G:i");
			if($r->type=="APTO-MTY"){
				$ae=1;
			}else{
				$ae=0;
			}
			$ch = curl_init();
			$url='http://gps.goldentransportaciones.com/services/addservice/?'.str_replace(" ","%20",'fecha='.$hoy.'&nombre_pasajero='.$s->user->name.'&direccion='.$this -> direc($r -> addresses).'&observaciones='.$r->annotations.'&telefono=' . $s->user->telephones . '&chofer_external_id='.$op->id.'&external_id='.$r->id.'&aeropuertociudad='.$ae.'&numero_reservacion='.$s->id);
			curl_setopt_array($ch, array(
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_URL => $url,
			));
		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		//curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		
			echo $httpResponse = curl_exec($ch);
			if(!$httpResponse) {
				exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
			}else{
				curl_close($ch);
			}
			// Turn off the server and peer verification (TrustManager Concept).
			/*echo '
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
			<script>
					jQuery(document).ready(function(){
						jQuery.ajax({
							url : "http://gps.goldentransportaciones.com/services/addservice/?fecha='.$hoy.'&nombre_pasajero='.$s->user->name.' '.$s->user->lastname.'&direccion='.$this -> direc($r -> addresses).'&observaciones='.$r->annotations.'&telefono=' . $s->user->telephones . '&chofer_external_id='.$op->id.'&external_id='.$r->id.'&aeropuertociudad='.$ae.'&numero_reservacion='.$s->id.',
							type : "GET",
							crossDomain : true,
							dataType : "jsonp",
							"contentType" : "application/x-www-form-urlencoded; charset=UTF-8",
							success : function(source) {
								data = source;
		
							},
							error : function(dato) {
		
							}
						});
					});
					
			</script>';*/
		}
	}
}
?>