<?php

class upreservationController extends controllerBase {//cuando la reservación es abierta
	public function masivePublish() {
		$data = inputDriver::getVar(__POST__);
		$returnData = array();
		foreach ($data as $id => $fields) {

			if (!$fields['id']) {
				unset($fields['payment']);
				if ($fields["reservation_date"] == "abierto") {

					unset($fields["reservation_date"]);
					$fields["abierto"] = 1;
				} else {
					$fields['abierto'] = 0;
				}
				$pasa = (int)$fields['passengers'];
				//valor entero de los pasajeros
				if ($pasa > 4 && $fields['vehicle_id'] == 1) {//si el numero total de pasajeros es mayor a cuatro lo cambia a automòvil
					$fields['vehicle_id'] = 2;
				}

				$R = new Reservation($fields);
				//se crea una nueva reservación
				if ($R -> is_valid()) {
					$R -> save();
					$fields['id'] = $R -> id;
				}
			} else {
				$R = Reservation::find_by_id($fields['id']);
				if ($R) {
					unset($fields['payment']);
					if ($fields["reservation_date"] == "abierto") {
						unset($fields["reservation_date"]);
						$fields["abierto"] = 1;
					} else {
						$fields['abierto'] = 0;
					}
					$pasa = (int)$fields['passengers'];
					if ($pasa > 4 && $fields['vehicle_id'] == 1) {

						$fields['vehicle_id'] = 2;
					}
					//si los datos de una reservación se han modificado se guarda en el campo log quien modifico, que se modifico y la hora y dia
					$add = "se ha modificado ";
					if ($R -> passengers != $fields['passengers']) {
						$add = $add . "el número de pasajeros;";
					}
					if ($R -> vehicle_id != $fields['vehicle_id']) {
						$add = $add . " el tipo de vehiculo;";
					}
					if ($R -> addresses != $fields['addresses']) {
						$add = $add . " la dirección;";
					}
					if ($R -> terminal != $fields['terminal']) {
						$add = $add . " la terminal;";
					}
					if ($R -> airline_id != $fields['airline_id']) {
						$add = $add . " la aerolinea;";
					}
					if ($R -> flight_number != $fields['flight_number ']) {
						$add = $add . " el número de vuelo;";
					}					
					if ($R -> cost != $fields['cost']) {
						$add = $add . " el costo;";
					}
					if ($R -> annotations != $fields['annotations']) {
						$add = $add . " una anotación;";
					}					
					if(!($fields['reservation_date']=='abierto' && $R->abierto==1)){
					if ($R -> flight_hour != $fields['flight_hour'].":00") {
						$add = $add . " la hora del vuelo;";
					}
					if ($R -> reservation_time != $fields['reservation_time'].":00") {
						$add = $add . " la hora de la reservación;";
					}
					if ($R -> flight_type != $fields['flight_type']) {
						$add = $add . " el tipo de vuelo;";
					}	
					}	
					if ($R -> type != $fields['type']) {
						$add = $add . " el tipo de reservación;";
					}

					$hoy = date("Y-m-d G:i");
					$fields['log'] = $R -> log . "||'Editado por','" . authDriver::getSUser() -> name . " " . authDriver::getSUser() -> lastname . "','" . $hoy . ",'$add";
					$R -> update_attributes($fields);
					//modifica los datos de una reservación
				}
				$fecha = date("Y-m-d");

				if (strtotime($R -> reservation_date) == strtotime($fecha)) {//si el dia de una reservaciin es iguak¡l a hoy
					$last = Last::find_by_id(1);
					$last -> reservation_update = $R -> id;
					$last -> save();
				}

			}
			$returnData[$id] = $fields;
		}
		responseDriver::dispatch('D', $returnData);
	}

}
