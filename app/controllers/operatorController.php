<?php
class operatorController extends controllerBase {
	
	 public function add($data) { //agrega dirección de un nuevo operador
        if($data['id']==-1 || $data['id']=="-1" || $data['id']=="") 
        unset($data['id']);
		$hoy=date("Y-m-d G:i");
		$data["picture"]=substr($data["picture"],12);
        $address = new Operator($data);
        if( $address->is_valid() ) {
        	$address->log="'Creado','".authDriver::getSUser()->name." ".authDriver::getSUser()->lastname."','".$hoy."'";
            $address->save();
			
            responseDriver::dispatch('D', array('id'=>$address->id));
        } else {
            //Error
            responseDriver::dispatch('E', "Error", "Error al intentar guardar la nueva dirección.");
        }
    
    }
	 public function oper() { //agrega dirección de un nuevo operador
        
    
    }
    public function publish(){
        $data = inputDriver::getVar(__POST__);
        
		$op=Operator::find_by_id($data['id']);
        if(!$op) {
            $this->add($data);
        } else {
            $this->update($data);
        }
    }
	
    public function update($data) {
        //Validar login
        $hoy=date("Y-m-d G:i"); 
        $address = Operator::find_by_id($data['id']);
        if($address) {
            unset($data['id']);
			$address->enable=1;
			$address->status=1;
			 $address->log="||'Editado','".authDriver::getSUser()->name." ".authDriver::getSUser()->lastname."','".$hoy."'";
            $address->update_attributes($data);
            if(!$address->errors) {
                
            } else {
                //Error
            }
        } else {
            //Error
        }
        responseDriver::dispatch('M', "Cambios guardados", "Informacion actualizada correctamente en la dirección.");
    }
	
	public function disable() { //elimina a un operador 
        //Validar login
        $data = inputDriver::getVar("aid");
        $z = Operator::find_by_id($data);
        if($z){
        	
            $z->update_attributes(array('enable' => 0,"unity" =>0)); //modifica los parametros enable y unity a 0 para deshabilitar el operador
            responseDriver::dispatch('D', array('message'=>"operador deleted"));
        }
    }
	public function id(){
		
			//muestra operadores habilitados					     		
 		$op=Operator::all(array('conditions'=> 'enable = 1'));
 		foreach ($op as $p) {
			 echo $p->id."<br />";
		 }
								     
	}
	
}