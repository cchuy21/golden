<?php
class businessController extends controllerBase {
	
	     public function add($data) { // guardar una nueva dirección
        //Validar login
        $data = inputDriver::getVar(__POST__);
		if($data["password"]==""){
			unset($data["password"]);
		}else{
			$data["password"]=md5($data["password"]);
		}
        if(isset($data['id'])) unset ($data['id']);
		$data['isBusiness']=1;
        $address = new User($data);
        if( $address->is_valid() ) {
            $address->save();
            $address->business_id=$address->id;
            $address->save();
			
            responseDriver::dispatch('D', array('id'=>$address->id));
        } else {
            //Error
            responseDriver::dispatch('E', "Error", "Error al intentar guardar la nueva dirección.");
        }
    
    }
    public function publish(){ // agregar o modificar un dato sobre la direccion
    
        $data = inputDriver::getVar(__POST__);
		if($data["password"]==""){
			unset($data["password"]);
		}else{
			$data["password"]=md5($data["password"]);
		}
	    if($data['id'] == '' || $data['id'] == '-1' || $data['id'] == -1) {
            unset($data['id']);
            $this->add($data);
        } else {
            $this->update($data);
        }
    }
	public function unir($id){
		 $emp=User::find($id);
		 $empr=explode(",",$emp->bus_id);
		 
		 echo '<!DOCTYPE HTML>
<html> <!--archivo principal del servicio, se mandan a llamar los estilos css y js-->
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <title>Golden Transportaciones</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	
        <!-- Bootstrap -->
        <link href="/static/css/bootstrap.css" rel="stylesheet">
        <link href="/static/css/animate.css" rel="stylesheet">
        <link href="/static/css/selectize.css" rel="stylesheet">
        <link href="/static/css/selectize.bootstrap3.css" rel="stylesheet">
        <link href="/static/css/jquery.datetimepicker.css" rel="stylesheet">
        <link href="/static/css/base/jquery.ui.all.css" rel="stylesheet">
        <link href="/static/css/bootstrap-editable.css" rel="stylesheet">
        <link href="/static/css/shadowbox.css" rel="stylesheet">
        <link href="/static/css/style.css" rel="stylesheet">
        <link href="/static/css/estilo.css" rel="stylesheet">
        <link href="/static/css/jquery.fs.stepper.css" rel="stylesheet">
        <link href="/static/css/notifIt.css" rel="stylesheet">
        <link href="/static/tags/bootstrap-tagsinput.css" rel="stylesheet">
        <link href="/static/js/sorter/styles.css" rel="stylesheet" type="text/css" />
      
    </head>
    <body>
	<div class="container contnew" id="content">';
	$tic=User::find("all",array("conditions"=>"isBusiness=1 and id<>".$id,"order"=>"name asc"));
	
	echo "<table class='table table-bordered'>";
		 $c=0;
		 foreach($tic as $ti){
				if(!$c){
				echo "<tr>";
				}
				foreach($empr as $e){
				  
				  if($e==$ti->id){
						   $che="checked";
						   break;
				  }
				  $che="";
				}
				echo "<td><input ".$che." value='".$ti->id."' class='ch' type='checkbox' name='juntar' /></td>";
				
				echo "<td>".$ti->name."</td>";
				$c++;
				if($c==4){
				  $c=0;
				}
		 }
	echo "</table><center><button class='btn btn-success btn-save'>Guardar</button>";
	
	
	echo '</center></div></body>
	<script src="/static/js/jquery.js" type="text/javascript"></script>
	<script>
	jQuery(document).ready(function(){
		 var va="";
		 jQuery(".btn-save").on("click",function(){
				  jQuery(".ch").each(function(){
						   if(jQuery(this).is(":checked")){
								if(va=="")
									va=jQuery(this).val();	
								else
									va=va+","+jQuery(this).val();	
						   }
				  });
				  alert(va);
				  jQuery.ajax({
				  type : "POST",
				  url : "/business/svunion",
				  data : {
				      union:va,id:'.$id.'
				  },
				  dataType : "json",
				  success : function(data) {
						   window.close();
				  }
			  });
		 });
	});
		 
	</script>
';
	}
	public function svunion(){
		 $u=User::find($_POST["id"]);
		 $u->bus_id=$_POST["union"];
		 $u->save();
		 echo json_encode(1);
	}
    public function update($data) {
        //Validar login
        $data = inputDriver::getVar(__POST__);
		if($data["password"]==""){
			unset($data["password"]);
		}else{
			$data["password"]=md5($data["password"]);
		}
        $address = User::find_by_id($data['id']);
        if($address) {
            unset($data['id']);
            $address->update_attributes($data);
            if(!$address->errors) {
                
            } else {
                //Error
            }
        } else {
            //Error
        }
        responseDriver::dispatch('M', "Cambios guardados", "Informacion actualizada correctamente en la dirección.");
    }
	
	public function disable() { //deshabilta una zona
        //Validar login
        $data = inputDriver::getVar("aid");
       
        $z = User::find_by_id($data);
        if($z){
        	$z->delete();
            responseDriver::dispatch('D', array('message'=>"zona deleted"));
        }
    }
	
	public function conv(){ //
		$id=$_POST['id'];
		if($id){
			$emp=User::find($id);
			$agr=explode(",", $emp->agreements_bus);
			$ar=array();
			foreach($agr as $arq){
				$a=Agreement::find_by_id($arq);
				if($a)
				array_push($ar,array($a->id,$a->agreement));
				
			}
			echo json_encode($ar);
		}
	}
}
