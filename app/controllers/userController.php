<?php

class userController extends controllerBase {
	public function testi(){
		$this -> dispatchErrorXML("Usuario vacío");
	}
	public function appregister(){
        if (isset($_POST['correo']) && $this -> exist($_POST['correo'])==2)
            $this -> dispatchErrorXML("Esta dirección de email ya ha sido registrada con facebook");
		if (isset($_POST['correo']) && $this -> exist($_POST['correo']))
			$this -> dispatchErrorXML("Esta dirección de email ya ha sido registrada.");
		if (isset($_POST['correo']) && isset($_POST['password'])) {
			$nombre = isset($_POST['nombre']) ? $_POST['nombre'] : "NULL";
			$telefono = isset($_POST['telefono']) ? $_POST['telefono'] : "NULL";
			$pass = $_POST['password'];
			$para = $_POST['correo'];
			$token = base64_encode($para . '\\' . $pass . '\\' . $nombre . '\\' . $telefono);
			$titulo = 'Confirmar cuenta Golden Transportaciones - No responder';
			$mensaje = "Para confirmar su cuenta de click en el siguiente link: \r<br />http://resv.goldentransportaciones.net/user/validate/{$token}\r<br /> \r<br />\tNombre: {$nombre}\r<br />\tCorreo: {$para}\r<br />\tTeléfono: {$telefono}\r<br /> \r<br />Si usted no solicitó crear una cuenta en Golden Transportaciones o cree que recibió este mensaje por error, simplemente haga caso omiso.\r<br /> \r\nAtentamente\r\nGolden Transportaciones\r\n \r\nPor favor no responda a este correo.";
			$cabeceras = array("From" => "registroApp@goldentransportaciones.com", "Reply-To" => "registroApp@goldentransportaciones.com", "X-Mailer" => 'PHP/' . phpversion());
			if (mailDriver::send($para, $titulo, $mensaje, $cabeceras))
				$this -> dispatchMessageXML("Te enviamos un email, da click en el link incluido para terminar el proceso de registro");
			else
				$this -> dispatchErrorXML("Error al enviar correo electrónico.");
		}
	}
//login aplicacion
	public function applogin($username = null, $facebook = false) {
		//$correo = base64_decode($correo);
		//echo $this->basename;
		
		if (!$username){
			$username = $_POST['correo'];
		}
		
        if (!$this -> exist($_POST['correo']))
			$this -> dispatchErrorXML("Asegúrate de haber confirmado el email que te enviamos o estar usando un correo registrado");

		$user = User::find_by_email($username,array(limit=> 1));
		if (!$user)
			$this -> dispatchErrorXML("El usuario o la contraseña que se intenta usar es incorrecto.");
        if($user && strlen($user -> password) < 5)
            $this -> dispatchErrorXML("Este usuario está dado de alta con facebook, por favor ingresa con facebook.");
		$password=$_POST['password'];
			
		
		
		if (($user -> password == $password) || $facebook) {
			//$user -> token = md5($user -> id . date('m/d/Y h:i:s a', time()));
			//$view -> dispatchUserXML($user);
			//$view = $this->getView();
			$this->dispatchXML($user);
		} else {
			$this -> dispatchErrorXML("El usuario o la contraseña que se intenta usar es incorrecto.");
		}
	}

	public function validate($token){

		if ($token != null) {
			$campos = explode('\\', base64_decode($token));

				$data['email'] = $campos[0];
				if ($this -> exist($data['email'])) { echo "Petición no valida.";
					exit ;
				}
				$data['password'] = $campos[1];
				$data['name'] = $campos[2];
				$data['telephones'] = $campos[3];
				$data['username'] = $campos[0];
				$data['distinguished']=0;
				$data['benefit_id']=0;
				$data['agreement']="SC";
				$data['user_type']=0;
				$data['badges']=0;
				$data['business_id']=5529;
				$data['isBusiness']=0;
				$data['created_by']="App";
				$data['enable']=1;
				$data['log']="App";
				$hoy=date("Y-m-d G:i");
      
            
			//insertar quien creo el usuario
				$data['log']="'CREADO','Aplicación','".$hoy."'";

				$user=new User($data);
		
        if($user->is_valid()) {
        	$user->save();
        	$re=array();
			//crear registro con 0 de 11 gratuito si no tiene convenio
			$re['joined']=0;
			$re['used']=0;
			$re['available']=0;
			$re['services']=0;
    		
    		$re['user_id']=$user->id;
			$r=new Revenue($re);//si el usuario tiene convenio
			if($user->business_id==5529) 
				$r->save();
            echo "usuario creado";  
        } else {
			echo "error al crear el usuario";
        }
		} else {
			echo "Petición no valida.";
		}
	}
	public function appupdate() {
		if (isset($_POST['facebook']) && $_POST['facebook'] == 1) {
			$this -> fupdate();
		} else {
			$id = $_POST['id'];
			$user=User::find_by_id($id);
//			$user = dbDriver::execQueryObject("SELECT * FROM logins where id = '{$id}' limit 1");

//			$view = $this -> getView();

			if (!$user) {
				//El usuario no existe
				$this -> dispatchErrorXML("No existe el usuario.");
			}

			$password = $_POST['password'];

			if ($password == $user -> password) {
				if (isset($_POST['correo'])) {
					if ($user -> email != $_POST['correo'])
						if ($this -> exist($_POST['correo'])) {
							//El usuario ya habia sido registrado
							$this -> dispatchErrorXML("Esta dirección de email ya ha sido registrada.");
						}

					//Procede a hacer el update
					$correo = $_POST['correo'];
					$nombre = isset($_POST['nombre']) ? $_POST['nombre'] : $user -> nombre;
					$telefono = isset($_POST['telefono']) ? $_POST['telefono'] : $user -> telefono;
					$facebook = isset($_POST['facebook']) ? $_POST['facebook'] : $user -> facebook;
					$password = $user -> password;
					if (isset($_POST['newPassword']) && trim($_POST['newPassword']) != '') {
						$password = $_POST['newPassword'];
					}
					/*$r = dbDriver::execQuery("UPDATE logins
							SET
							username='{$correo}',
							correo='{$correo}',
							nombre='{$nombre}',
							edad='{$edad}',
							genero='{$genero}',
							telefono='{$telefono}',
							password='{$password}'
							where
							id='{$id}'
						");*/
						$user->username=$correo;
						$user->email=$correo;
						$user->name=$nombre;
						$user->telephones=$telefono;
						$user->password=$password;
						$user->facebook=$facebook;
						
					if ($user->save())
						$this -> dispatchMessageXML("Información actualizada correctamente.");
					else
						$this -> dispatchErrorXML("Error al actualizar la información.");
				} else {
					//Campo correo vacio
					$this -> dispatchErrorXML("Campo de correo vacío.");
				}
			} else {
				//No se tiene permiso para hacer update
				$this -> dispatchErrorXML("Contraseña incorrecta.");
			}
		}
	}
    public function pass( $correo = null, $tel = null) {
        //$view = $this -> getView(); 
        $correo = isset($_POST['correo']) ? $_POST['correo'] : null;
        if(!$correo){
           $this -> dispatchErrorXML("El campo correo está en blanco");                                        
        }
        $tel = isset($_POST['tel']) ? $_POST['tel'] : null;
        if(!$tel){
           $this -> dispatchErrorXML("El campo teléfono está en blanco");
        }
        $user = User::find("all",array("conditions"=>"email = '" . $correo . "' and telephones='{$tel}'",'limit' => 1));
        if (!$user){
            $this -> dispatchErrorXML("El correo o el teléfono son incorrectos");
        }
       /* if(strlen($user -> password) < 5){
            $this -> dispatchErrorXML("Las cuentas registradas con facebook no pueden restablecer contraseñas, inicia sesión con facebook.");
        }*/
        
        $newPass = '';
        $charset="abcdefghijklmnopqrstuvwxyz0123456789";
        $count = strlen(charset);
        $length = 4;
        while ($length--) {
             $newPass .= $charset[mt_rand(0, $count-1)];
        }
        $para = $correo;
        $titulo = 'Restablecimiento de contrasena - No responder';
        $mensaje = utf8_decode("La nueva contraseña para tu cuenta \"{$correo}\" es: \r <br /> \r<br />{$newPass}\r<br /> \r<br />Si usted no solicitó restablecer su contraseña en Golden Transportaciones \r<br />preste atención a su cuenta ya que podría ser victima de falsificación o fraude. \r<br />Si usted cree que recibió este mensaje por error, simplemente haga caso omiso.\r<br /> \r<br />Atentamente\r<br />Golden Transportaciones\r<br /> \r<br />Por favor no responda a este correo.");
        $cabeceras = array("From" => "registroApp@goldentransportaciones.com", "Reply-To" => "registroApp@goldentransportaciones.com", "BCC" => "sergio@animactiva.mx,ajp@expertsys.com.mx", "X-Mailer" => 'PHP/' . phpversion());
                                                   
        if (mailDriver::send($para, $titulo, $mensaje, $cabeceras)){
            $newPass = md5($newPass);
	        $u = User::find_by_email($correo, array("conditions"=>"telephones='{$tel}'",'limit' => 1));
            $u -> password=$newPass;
			$u->save();
            if ($u->save()){
                 $this -> dispatchMessageXML("Te enviamos un email con la nueva contraseña para tu cuenta, inicia sesión y después personalízala.");
            }else{
                 $this -> dispatchErrorXML("Error al actualizar la información.");
            }
        } else{
            $this -> dispatchErrorXML("Hubo un error al enviar el correo electrónico.");
        }
    }
	private function dispatchXML($user){
		//$user=User::find_by_id($usr);
		header('Content-type: text/xml');
			echo '<?xml version="1.0" encoding="UTF-8" ?>';
			echo "<Message>";
			if($user){ 
				echo "<id>{$user->id}</id>";
				echo "<username>{$user->name}</username>";
	            echo "<nombre>{$user->name}</nombre>";
	            echo "<correo>{$user->email}</correo>";
	            echo "<telefono>{$user->telephones}</telefono>";
				echo "<token></token>";
	            echo "<edad></edad>";
	            echo "<genero></genero>";
				echo "<fb>{$user->facebook}</fb>";
				echo "<fblogin>{$user->fblogin}</fblogin>";
			}else{
				echo " <error value='Usuario vacio.' /> ";
			}
			echo "</Message>"; 
	}
	private function dispatchMessageXML($mensaje = null){
					header('Content-type: text/xml');
		        echo '<?xml version="1.0" encoding="UTF-8" ?>';
				echo "<Message>";
				echo "<mensaje value='{$mensaje}' />";				
				echo "</Message>";
				exit;
	}
	private function dispatchErrorXML($mensaje = null){
					header('Content-type: text/xml');
		        echo '<?xml version="1.0" encoding="UTF-8" ?>';
				echo "<Message>";
				echo '<error value="'.$mensaje.'"></error>';
				echo "</Message>";
				exit;
	}
	private function exist($correo) {
		$user = User::find_by_email($correo);
		if (!$user)
			return false;
		return true;
	}
    public function direcciones(){ // se guarda una dirección 
    	$user = inputDriver::getVar("user");
		$direcciones = inputDriver::getVar("ids");
		echo $user;
		
		foreach ($direcciones as $dir) { //recorre todas las direcciones 
			$ad=Address::find_by_id($dir);
			$ad->user_id=$user; 
			$ad->save();	
		}
    }
	
    public function validatePass($pwd = null) {
        $strength = array("Blank","Very Weak","Weak","Medium","Strong","Very Strong");
        $score = 1;
        if (strlen($pwd) < 1) { //checa la longitud de la variable 
            return 0;
        }
        if (strlen($pwd) < 4) {
            return $score;
        } 
        if (strlen($pwd) >= 8) {
            $score++;
        }
        if (strlen($pwd) >= 10) {
            $score++;
        }
        if (preg_match("/[a-z]/", $pwd) && preg_match("/[A-Z]/", $pwd)) { //que la varianle solo contenga letras
            $score++;
        }
        if (preg_match("/[0-9]/", $pwd)) {
            $score++;
        }
        if (preg_match("/.[!,@,#,$,%,^,&,*,?,_,~,-,£,(,)]/", $pwd)) {
            $score++;
        }
        return $score;
    }
    
    public function publish(){
        $data = inputDriver::getVar(__POST__);
		$hoy=date("Y-m-d G:i");
        if($data['id'] == '' || $data['id'] == '-1' || $data['id'] == -1) {
            unset($data['id']);
			//insertar quien creo el usuario
			$data['log']="'Creado','".authDriver::getSUser()->name." ".authDriver::getSUser()->lastname."','".$hoy."'";
            $this->register($data);
			//$data['username']=preg_replace('/\s+/', ' ', $data['username']); //add 4/02/2015
        } else {
        	
            $this->update($data);
			
        }
    }
	
	public function notrepeat($da){ //si existe el nombre de un usuario, no se puede agregar

		$cd=$da['name'];
		$us=User::all();
		$c=0;
		foreach($us as $demas){
			
			$com=$demas->name;
			if($cd==$com){
				$c++;
			}
			
		}
		
		if($c==1){
			return true;
		
		}else{
			return false;
			
		}
	}
	
	public function validdireccion($id){
		$u=Address::find_by_user_id($id);
		if(!$u){
			echo json_encode(1);
		}else{
			echo json_encode(0);
		}
	}
	
    public function valid($correo="ninguno",$tel){
    	
    	$em = User::find_by_email($correo);
		$te = User::find_by_telephones($tel);
		$item=array();
		 //si las dos variables estan definidas
		if(isset($em) || isset($te) ){
			//tiene un correo registrado en la bdd
			if(isset($em)){
				$item=array("correo"=>"si"); 
			}else{
				$item=array("correo"=>"no");	
				}
				//tiene un telefono registrado en la bdd
			if(isset($te)){
				$item+=array("telefono"=>"si");				
			}else{
				$item+=array("telefono"=>"no");
			}
			
			echo json_encode($item); 
		}
		
    }
	//cuando se modifica un registro es necesario que ingrese todos los datos
   public function update($data = null) {
        if($data == null) exit;
        //authDriver::chkLoggin();
        $data = inputDriver::getVar(__POST__);
		$hoy=date("Y-m-d G:i");
		$em=$data['email'];
		$conv=$data['agreement'];
        $data=$this->touppe($data);
        //reemplaza de $em los ; por ,
		$em = preg_replace('/;/', ',', $em); 
		$data['email']=$em;
		$data['agreement']=$conv;
		
		$data['name']=trim($data['name']); //add 4/02/2015
		$data['name']=preg_replace('/\s+/',' ', $data['name']); //add 4/02/2015 quitar doble espacios entre palabras
		
		//AGREGA EN EL CAMPO DE LOG QUIEN EDITO Y A QUE HORA
		$data['log']="||'Editado','".authDriver::getSUser()->name." ".authDriver::getSUser()->lastname."','".$hoy."'";;
		
        if(!isset($data['id'])) exit;         
        $user = User::find_by_id($data['id']);
        
        if(isset($data['telephones'])){
            if(trim($data['telephones']) === '') {
                responseDriver::dispatch('E', 'Faltan campos', 'El campo telefono es requerido'); 
            }
        }
		//al momento de determinar una contraseña si no coincide manda error
        if(isset($data['password'])){ 
            if($data['password'] !== ''){
                if($data['password'] !== $data['password2']) {
                    responseDriver::dispatch('E', "Las contraseñas no coinciden", "Debes usar la misma contraseña en los dos campos");
                } else{
                    unset($data['password2']);
                }
                //si la longitud de la contraseña es muy pequeña
                if($this->validatePass($data['password']) < 3){ 
                    responseDriver::dispatch('E', "Contraseña debil", "Tu contraseña debe contener al menos 8 caracteres, letras (mayusculas y minusculas) y números. ");
                } 
                $data['password'] = md5($data['password']);
            }
        }
        $user->update_attributes($data);
        responseDriver::dispatch('M', "Cambios guardados", "Informacion actualizada correctamente.");
		
    }	
	//para registrar a un nuevo usuario
    public function register($data = null) { 
        if($data == null) exit;
        $em=$data['email'];
		$conv=$data['agreement'];
        $data=$this->touppe($data);
        $em = preg_replace('/;/', ',', $em);
		$data['email']=$em;
		$data['agreement']=$conv;
		
		$data['name']=trim($data['name']); //add 4/02/2015 quita espacios laterales de la cadena
		$data['name']=preg_replace('/\s+/',' ', $data['name']); //add 4/02/2015 quitar doble espacios entre palabras
		
        if(!isset($data['fbid'])) {
        	 //se deben de llenar los dos campos de contraseñas
            if(!isset($data['password']) || !isset($data['password2'])) { 
                if( inputDriver::getVar('isMobile') ) {
                    responseDriver::dispatch('E', "Falta contraseña", "Campo de contraseña obligatorio");
                } else {
                	//Generar una contraseña temporal
                    $data['password'] = 'tem_Pass1';
                    $data['password2'] = 'tem_Pass1';
                   
                }
            }
            if($data['password'] !== $data['password2']) {
                responseDriver::dispatch('E', "Las contraseñas no coinciden", "Debes usar la misma contraseña en los dos campos");
            } else{
                unset($data['password2']);
            }
            if($this->validatePass($data['password']) < 3){
                responseDriver::dispatch('E', "Contraseña debil", "Tu contraseña debe contener al menos 8 caracteres, letras (mayusculas y minusculas) y números. ");
            } 
        } else {
            $data['username'] = $data['fbid'];
            $data['email'] = $data['fbemail'];
			$data['username']=$data['email'];
			
            unset($data['fbid']);
            unset($data['fbemail']);
        }
        $data['password'] = md5($data['password']);        
        $data['created_by']=authDriver::getSUser()->name;
		$user=new User($data);
		
        if($user->is_valid()) {
        	$re=array();
			//crear registro con 0 de 11 gratuito si no tiene convenio
			$re['joined']=0;
			$re['used']=0;
			$re['available']=0;
			$re['services']=0;
    		$user->save();
    		$re['user_id']=$user->id;
			$r=new Revenue($re);//si el usuario tiene convenio
			if($user->business_id==5529) 
				$r->save();
            responseDriver::dispatch('D', array('id'=>$user->id));
           
        } else {
            responseDriver::dispatch('E', 'Error', 'Un error al intentar crear la cuenta, por favor intentalo nuevamente.');
        }
        
    }
    //se utiliza para cuando se quiere borrar un usario
    public function disable(){ 
    	$id=inputDriver::getVar('id');
		$u=User::find($id);
		$u->enable=0; //se deshabilita
		if($u->is_valid()){
			$u->save();
       		responseDriver::dispatch('D', 'Borrado');
   		}else{
            responseDriver::dispatch('E', 'Error', 'Un error al borrar el usuario');
   		}
    }
	//se activa una cuenta de usuario ya existente pero deshabilitada
    public function activate($id = null) { 
        if($id === null) exit;
        $r = Registration::find_by_id($id); 
        if($r) {
            $auxr = $r;
            $r = json_decode($r->json);
            $arr = array();
            foreach($r as $key => $val) {
                $arr[$key] = $val;
            }
            $r = User::create($arr);
            $auxr->delete();
            echo "La cuenta se ha activado correctamente";
        } else {
            echo "No existe este registro";
        }    
    }
    //buscas el servicio del usuario    
    public function latest() { 
        authDriver::chkLoggin();
        $id = inputDriver::getVar('id');
        $user = User::find_by_id($id);
        $services = $user->services;
    }
    
    public function passys() {
        //Olvido contraseña
        $data = inputDriver::getVar("recover");
        if(!isset($data['email'])) //si el emal es null 
            responseDriver::dispatch('E', "Email", "No se ha llenado el campo email.");
     }
	
    

    
    public function badges() {
       authDriver::chkLoggin();
       $id = isset($_POST['id']) ? $_POST['id'] : exit;
       $user = User::find_by_id($id);
       if (!$user) {
          responseDriver::dispatch('E', "Error", "No existe el usuario espcificado.");
       }
       $user->badges = 0;
       if ($user->save())
           responseDriver::dispatch('M', "Actualizado", "Información actualizada correctamente.");
       else
           responseDriver::dispatch('E', "Error", "Error al actualizar la información.");
    }
    
    public function login() {
        if( authDriver::isLoggedin() ) {
            responseDriver::dispatch('M', "Sesión iniciada", "Ya se ha iniciado sesión en el sistema.");
        }
        $user = inputDriver::getVar("user");
        if( authDriver::login($user) ) {
            responseDriver::dispatch('M', "Login succeed", "El usuario se ha logeado correctamente");
        } else {
            responseDriver::dispatch('E', "Incorrect data", "La informacion de inicio de sesión es incorrecta");
        }
    }
    
    public function logout() {
        authDriver::chkLoggin(); //para iniciar sesión 
        authDriver::logout(); //cierra la sesión
       
    }
    
    public function filter() {
        //Return a JSON string wich contains all the matches 
    }
	
    public function raw($view) {
        viewBase::raw("user.testblock");
    }
	
	public function getdata($id = null){
		if(!$id) return;
        if (empty($_GET['nocache'])) exit ;	
		$user=User::find_by_id($id);
		$items=array("id" => $user->id,"name"=>$user->name,"email"=>$user->email,"telephones"=>$user->telephones);
		echo json_encode($items);
	}
	
	public function countser($id){  //se cuentan los servicios que tiene un usuario
		$cou=Service::find( 'all' , array ( 'conditions' => array ( ' user_id =? ' , $id )));
		$count = count($cou);
		echo $count."<br/>";
		$co=(int)($count / 11);
		echo $co."<br/>";
		$rev=Revenue::find(array ( 'conditions' => array ( ' user_id =? ' , $id )));
		$usados=$rev->used;
		$dis=$co-$usados;
		$rev->available=$dis;
		$rev->joined = $co;
		$rev->save();
	}
	 //convierte todo el array en mayusculas 
	public function touppe($array){
		//devuelve todas las claves del array
		$ke=array_keys($array); 
		
		for($i=0;$i<count($array);$i++){
			$array[$ke[$i]]=strtoupper($array[$ke[$i]]);
		}
		return $array;
		
	}
	
	public function gf(){
		echo "<img src='/static/img/cargando.gif' width='50px'>";
	}
	
    public function gettemplate($temp){
    	templateDriver::renderSection($temp);
    }
    public function index($id = null,$tipo) {
    	if($tipo=="41" || $tipo=="42"){
			if( authDriver::isLoggedin() ) {
			templateDriver::setData("dat",$tipo);
			 $this->getView()->window($id);
			
			 } else {
            exit;
	        }
		}else
    	if($tipo!="0" && $tipo!="4" && $tipo!="3" && $tipo!="5" && $tipo!="6" && $tipo!="7" && $tipo!="9"){
    		
	        if( authDriver::isLoggedin() ) {
	            $this->getView()->window($id);
				
	        } else {
	            exit;
	        }
		}else if($tipo=="0"){
			if( authDriver::isLoggedin() ) {
	             templateDriver::renderSection("user.register");
	        } else {
	            exit;
	        }
		}else if($tipo=="3"){
			if( authDriver::isLoggedin() ) {
				
				
				$ser=Service::find_by_id($id);
				$u = User::find_by_id($ser->user_id);
				templateDriver::setData("window",$u->id);
	            templateDriver::renderSection("user.data");
				templateDriver::setData("id",array("id" => $id));
                ?>
                <div class="container" id="content" style="margin-top: 20px">
                <div id="editar" class="cancel-bt">
                <?php
                templateDriver::renderSection("service.editar");
                ?>
                </div>
                </div>
            <?php
            } else {
                exit;
            }
        }else if($tipo=="9"){
			if( authDriver::isLoggedin() ) {
				$ser=Service::find_by_id($id);
				$u = User::find_by_id($ser->user_id);
				templateDriver::setData("window",$u->id);
	            templateDriver::renderSection("user.data");
				templateDriver::setData("id",array("id" => $id));
                ?>
                <div class="container" id="content" style="margin-top: 20px">
                <div id="editarn" class="cancel-bt">
                <?php
                templateDriver::renderSection("service.editarn");
                ?>
                </div>
                </div>
            <?php
            } else {
                exit;
            }
        }else if($tipo=="5"){
            //si el usuario a iniciado sesión
            if( authDriver::isLoggedin() ) {
                $ser=Service::find_by_id($id);
                $u = User::find_by_id($ser->user_id);
                templateDriver::setData("window",$u->id);
                templateDriver::renderSection("user.data");
                templateDriver::setData("id",array("id" => $id));
                ?>
                <div class="container" id="content" style="margin-top: 20px">
                <div id="editar" class="cancel-bt">
                <?php
                 templateDriver::renderSection("service.consultar");
                ?>
                </div>
                </div>
                <?php
            } else {
                exit;
            }
        }else if($tipo=="7"){
            if( authDriver::isLoggedin() ) {
                //se muestran las canceladas
                $ser=Canceled::find_by_id($id);
                $u = User::find_by_id($ser->user_id);
                templateDriver::setData("window",$u->id);
                templateDriver::renderSection("user.data");
                templateDriver::setData("id",array("id" => $id));
                ?>
                <div class="container" id="content" style="margin-top: 20px">
                <div id="editar" class="cancel-bt">
                <?php
                //manda a consultar los servicios cancelados
                templateDriver::renderSection("service.consultarc");
                ?>
                </div>
                </div>
                <?php
            } else {
                exit;
            }
        }else if($tipo=="6"){
            if( authDriver::isLoggedin() ) {
                //eliminar a un usuario del sistema
                templateDriver::setData("window",$id);
                templateDriver::renderSection("user.data");
                echo '<button type="button" class="btn btn-danger" id="user-disable">Borrar Usuario</button>';
            } else {
                exit;
            }
        }else{
            if( authDriver::isLoggedin() ) {
                //cancelación de un servicio
                $ser=Service::find_by_id($id);
                $u = User::find_by_id($ser->user_id);
                //seleccionar los servicios del usuario
                templateDriver::setData("window",$u->id);
                templateDriver::renderSection("user.data");
                templateDriver::setData("id",array("id" => $id));
                ?>
                <div class="container" id="content" style="margin-top: 20px">
                <div id="editar" class="cancel-bt">
                <?php
                templateDriver::renderSection("service.cancelar");
                ?>
                </div>
                </div>
                <?php
            } else {
                exit;
            }
        }
    }
public function dispatchUserXML($user = null)
{
header('Content-type: text/xml');
echo '<?xml version="1.0" encoding="UTF-8" ?>';
echo "<Message>";
if($user)
{

echo "<id>{$user->id}</id>";
echo "<token>{$user->token}</token>";
echo "<username>{$user->username}</username>";
echo "<nombre>{$user->nombre}</nombre>";
echo "<correo>{$user->correo}</correo>";
echo "<edad>{$user->edad}</edad>";
echo "<genero>{$user->genero}</genero>";
echo "<telefono>{$user->telefono}</telefono>";
echo "<fb>{$user->facebook}</fb>";
}
else
{
echo " <error value='Usuario vacio.' /> ";
}
echo "</Message>";
}
public function applatest($id = null) {
if (!$id)
exit ;
$u=User::find_by_id($id);
$s2=$u->getsLastService();
header('Content-type: text/xml');
echo '<?xml version="1.0" encoding="UTF-8" ?>';
echo "<reservaciones>";

foreach ($s2 as $s) {
$r=Reservation::find($s->reservation1);
if($r->status!=0){
if($s->reservation2)
$this->latestv($s->reservation1,0,$u,$s->id,$s);
else
$this->latestv($s->reservation1,0,$u,-1,$s);

if($s->reservation2)
$this->latestv($s->reservation2,1,$u,-1,$s);

}
}
echo "</reservaciones>";

exit;

}
public function latestv($res,$k,$u,$r2,$s){
$r=Reservation::find_by_id($res);
$direccion=$r->addresses;
$direc=explode(",", $direccion);

for($i=0;$i<count($direc);$i++){
if(substr($direc[$i],0,1)=="a"){
$a=Address::find_by_id(substr($direc[$i],1));
$sub=Suburb::find_by_id($a->suburb_id);
break;
}else{
$sub=Suburb::find_by_id(substr($direc[$i],1));
}
}

$hora=date("G:i",strtotime($r->reservation_time));
if(strlen($hora)==4)
$hora="0".$hora;
//$hora = explode(":", $o->hora_solicitada);
//              $h = sprintf('%2u',($hora[0] + 2));
//            $hora = $h . ":" .$hora[1];
if($r->vehicle_id==1)
$v="Automovil";
else {
$v="Camioneta";
}
if($r->airline_id){
$air=User::find($r->airline_id);
}
if($r->type=="MTY-APTO"){
$ti=1;
}else{
$ti=2;
}
if($s->status!=3)
echo "<reservacion>
<id>{$s->id}</id>
<status>{$s->status}</status>
<alias>{$a->alias}</alias>
<redondo>{$k}</redondo>
<nombre>{$u->name}</nombre>
<correo>{$u->email}</correo>
<telefono>{$u->telephones}</telefono>
<hora>{$hora}</hora>
<tipo>{$ti}</tipo>
<calle>{$a->street}</calle>
<colonia>{$sub->suburb}</colonia>
<entre1>{$a->between}</entre1>
<fecha>".$this->cambiofec(date("D d M Y",strtotime($r->reservation_date)))."</fecha>
<hora2>{$hora}</hora2>
<pasajeros>{$r->passengers}</pasajeros>
<vehiculo>".$v."</vehiculo>
<terminal>{$r->terminal}</terminal>
<aerolinea>{$air->name}</aerolinea>
<nvuelo>{$r->flight_number}</nvuelo>
<horasal>{$r->flight_hour}</horasal>
<precio>{$s->cost}</precio>
<motivo>{$r->annotations}</motivo>
<pagado>{$s->pagado}</pagado>
<linked>{$r2}</linked>
</reservacion>";

}
function cambiofec($fecha){
$dia=substr($fecha, 0,3);
$numdia=substr($fecha, 4,2);
$mes=substr($fecha, 7,3);
$año=substr($fecha, 11,4);
switch($dia){
case "Mon":
$diae="Lun";
break;
case "Tue":
$diae="Mar";
break;
case "Wed":
$diae="Mie";
break;
case "Thu":
$diae="Jue";
break;
case "Fri":
$diae="Vie";
break;
case "Sat":
$diae="Sab";
break;
case "Sun":
$diae="Dom";
break;
}
switch ($mes) {
case 'Jan':
$mes="Ene";
break;
case 'Apr':
$mes="Abr";
break;
case 'Aug':
$mes="Ago";
break;
case 'Dec':
$mes="Dic";
break;
}
$fecha=$diae." ".$numdia." ".$mes." ".$año;
return $fecha;
}
public function fallidos ($id = null, $trans = null, $status = null, $date = null, $res){
$id = isset($_POST['id']) ? $_POST['id'] : null;
$trans = isset($_POST['trans']) ? $_POST['trans'] : null;
$status = isset($_POST['status']) ? $_POST['status'] : null;
$date = isset($_POST['date']) ? $_POST['date'] : null;
$res = isset($_POST['res']) ? $_POST['res'] : null;
//  $user = dbDriver::execQueryObject("SELECT * FROM logins where id = '{$id}' limit 1");
$user=User::find(all,array("conditions"=>"id=".$id,"limit"=>1));
if (!$user) {
$this -> dispatchErrorXML("No existe el usuario.");
}
$pf = new Failed();
$r->user_id=$user->id;
$r->payKey=$trans;
$r->status=$status;
$r->service_id=$res;
//dbDriver::execQuery("INSERT INTO pagosFallidos ( user,payKey,status,date, reservacion) VALUES ('".($user->id)."','".$trans."','".$status."','".$date."','".$res."');");
/*$r=new Pagosfallidos();
* $r->user=$user->id;
* $r->payKey=$trans
* $r->status=$status;
* $r->date=$date;
* $r->reservacion=1
*
*
*
* if($r->save())
* */
$s=Service::find($res);
if ($r->save()){
if($status == "COMPLETED" || $status == "approved"){
$s->status=0;
$s->pagado=1;
}
$this -> dispatchMessageXML("Pago registrado correctamente.");
} else
$this -> dispatchMessageXML("Pago registrado correctamente.");
}
public function pagos ($id = null, $trans = null, $status = null, $date = null, $amount = null, $res = null){
if($_POST['id'])
$id = $_POST['id'];
if($_POST['trans'])
$trans = $_POST['trans'];
if($_POST['status'])
$status = $_POST['status'];
if($_POST['date'])
$date = $_POST['date'];
if($_POST['amount'])
$amount = $_POST['amount'];
if($_POST['res'])
$res = $_POST['res'];
$user=User::find_by_id($id);
//$resP = dbDriver::execQueryObject("SELECT * FROM reservaciones where id = '{$reservacion}' limit 1");

if (!$user) {
$this -> dispatchErrorXML("No existe el usuario.");
}
// $r = dbDriver::execQuery("INSERT INTO pagos ( user,transaction,status,date, amount, reservacion) VALUES ('".($user->id)."','".$trans."','".$status."','".$date."','".$amount."','".$reservacion."');");
$p=new Payment();

$p->service_id=$res;
$p->user_id=$user->id;
$p->transaction=$trans;
$p->cost=$amount;
$p->status=$status;

$s= Service::find_by_id($res);

if ($p->save()){
if($status == "completed" || $status == "approved"){
if ($s->status== '5') {
$s->status=0;
$s->pagado=1;
}else{
$s->pagado=1;
}
$s->save();
$r=Reservation::find($s->reservation1);
$para = "reservacionesapp@goldentransportaciones.com,rmireles@goldentransportaciones.com";

$titulo = 'Nueva solicitud de servicio '.$r->type.' >>>> PAGADO';
$mensaje = "Se ha generado una nueva solicitud de servicio. R:".$s->id." ".$r->type." el cual ya fue PAGADO, ingresa a la siguiente dirección para confirmarlo.\r<br /> \r<br /> http://resv.goldentransportaciones.net/";
$cabeceras = array("From" => "reservacionesApp@goldentransportaciones.com", "Reply-To" => "reservacionesApp@goldentransportaciones.com", "BCC" => "sergio@animactiva.mx, nnarvaez@goldentransportaciones.com,ajp@expertsys.com.mx", "X-Mailer" => 'PHP/' . phpversion());
//mailDriver::send($para, $titulo, $mensaje, $cabeceras);
}
$this -> dispatchMessageXML("Pago registrado correctamente.");
} else
$this -> dispatchErrorXML("Error al registrar el pago.");
}
public function flogin() {
$username = $_POST['uid'];
$device = isset($_POST['dev']) ? $_POST['dev'] : null;
$user = User::find_by_fblogin($username);
if (!$user) {
$this -> dispatchErrorXML("Para completar tu registro con facebook, te pedimos llenar algunos campos adicionales. nserver");
} else {
$user -> token = md5($user -> id . date('m/d/Y h:i:s a', time()));
if($user->save()){
$dev=Device::find_by_did($device);
if($dev){

$dev->login=1;
$dev->save();
}
$this -> dispatchXML($user);
}
}
}
	public function fupdate() {
		$username = $_POST['uid'];

		$user = User::find_by_fblogin($username);

		$correo = isset($_POST['correo']) ? $_POST['correo'] : $user -> correo;
		$nombre = isset($_POST['nombre']) ? $_POST['nombre'] : $user -> nombre;
		$telefono = isset($_POST['telefono']) ? $_POST['telefono'] : $user -> telefono;
		if (!$user) {
			$required = array("correo", "nombre", "telefono");
			foreach ($required as $value) {
				if (trim($_POST[$value]) == "") {
					$view -> dispatchErrorXML("Alguno de los campos está en blanco");
				}
			}
			$user=new User();

			$user->email=$correo;
			$user->name=$nombre;
			$user->username=$correo;
			$user->telephones=$telefono;
			$user->distinguished=0;
			$user->agreement="SC";
			$user->user_type;
			$user->business_id=5529;
			$user->created_by=0;
			$user->enable=1;
			$user->facebook=1;
			$user->fblogin=$username;
			//VALUES('{$username}','{$correo}','{$nombre}','{$edad}','{$genero}','{$telefono}',1)");
			if ($user->save()) {
				$this -> dispatchMessageXML("Se ha guardado correctamente. no debe");
				//$this -> dispatchMessageXMLfacebook($idtoken -> id, $idtoken -> token);
			} else {
				$this -> dispatchErrorXML("Error al actualizar la información.");
			}
		} else {
			$telefono=(int) $telefono;
			$user->email=$correo;
			$user->name=$nombre;
			$user->telephones=$telefono;
			$user->user_type;
			$user->facebook=1;
			if (1) {
				$this -> dispatchMessageXML("Se ha guardado correctamente. guardar");
				//$this -> dispatchMessageXMLfacebook($idtoken -> id, $idtoken -> token);
			} else {
				$this -> dispatchErrorXML("Error al actualizar la información.");
			}
		}
	}
	function dispatchMessageXMLfacebook($ms){
		header('Content-type: text/xml');
		echo '<?xml version="1.0" encoding="UTF-8" ?>';
		echo "<message>{$ms}</message>";
		return 1;
	}
	function xlslist($user){
		include 'PHPExcel/PHPExcel.php'; //libreria de excel
		$prueba = new PHPExcel();
		$u = User::find_by_id($user);

		$ser=Service::all(array('conditions' => 'user_id = '.$u->id,"limit"=>300, "order"=>"id desc"));
		$c=1;
		$prueba->setActiveSheetIndex(0)->setCellValue("A".$c,"#") 
			->setCellValue("B".$c,"Folio")
			->setCellValue("C".$c,"Fecha")
			->setCellValue("D".$c,"Hora")
			->setCellValue("E".$c,"Dirección")
			->setCellValue("F".$c,"Pasajeros")
			->setCellValue("G".$c,"Unidad")
			->setCellValue("H".$c,"Chofer")
			->setCellValue("I".$c,"Costo");
		$contador=1;
		foreach($ser as $s){
			$c++;
			$showc=0;
			$test=0;
			if(!empty($s->reservation1)){
				$r1=Reservation::find_by_id($s->reservation1);
				$as=Assignment::find_by_reservation_id($r1->id);
				$unid=Unity::find_by_id($as->unity_id);
				$oper=Operator::find_by_id($as->operator_id);
				if($r1->abierto){
					$fecg="Abierto";
				}else{
					$fecg=templateDriver::timelo($r1->reservation_date,3);
				}
				$prueba->setActiveSheetIndex(0)->setCellValue("A".$c,$contador) 
					->setCellValue("B".$c,$s->id)
					->setCellValue("C".$c,$fecg)
					->setCellValue("D".$c,substr($r1->reservation_time,0,5))
					->setCellValue("E".$c,$this->direccion($r1->addresses))
					->setCellValue("F".$c,$r1->passengers)
					->setCellValue("G".$c,$unid->economic)
					->setCellValue("H".$c,$oper->name." ".$oper->lastname)
					->setCellValue("I".$c,$s->cost);
				$showc++;	
				$contador++;
				$test=1;	
			}
			if(!empty($s->reservation2)){
				$c++;
				$r2=Reservation::find_by_id($s->reservation2);
			
				//si tiene una segunda reservación, se muestran los datos
				if($r2->abierto){
					$fecg="Abierto";
				}else{
					$fecg=templateDriver::timelo($r2->reservation_date,3);
				}
				$prueba->setActiveSheetIndex(0)->setCellValue("A".$c,"") 
					->setCellValue("B".$c,$s->id)
					->setCellValue("C".$c,$fecg)
					->setCellValue("D".$c,substr($r2->reservation_time,0,5))
					->setCellValue("E".$c,$this->direccion($r2->addresses))
					->setCellValue("F".$c,$r2->passengers)
					->setCellValue("G".$c,$unid->economic)
					->setCellValue("H".$c,$oper->name." ".$oper->lastname)
					->setCellValue("I".$c,$s->cost);
				
				if($showc)
					$prueba->setActiveSheetIndex(0)->setCellValue("I".$c,"0"); 
				else
					$prueba->setActiveSheetIndex(0)->setCellValue("I".$s->cost); 
			}
			
		}
		$titulo="hoja de calculo";
		$prueba->getActiveSheet()->setTitle($titulo); 
		
		$objWriter = PHPExcel_IOFactory::createWriter($prueba, 'Excel2007');
		$objWriter->save('excels/list.xlsx'); 
		echo "<a href='/excels/list.xlsx'>Descargar</a>";
	}

	function direccion($direccion){
		//extrae el número de direcciones 
		$direc=explode(",", $direccion);
		for($i=0;$i<count($direc);$i++){
			if(substr($direc[$i],0,1)=="a"){
				$ad=Address::find_by_id(substr($direc[$i],1));
				$col=Suburb::find_by_id($ad->suburb_id);
				return $ad->alias.": ".$ad->street.' '.$col->suburb;
				break;
			}
		}
	}
	function vehiculo($veh){ //determina el tipo de vehiculo del servicio
		if($veh==1)	$vehi="Automovil";else if($veh==2) $vehi="Camioneta";
		return $vehi;
	}
	function aerolinea($aer){ //nombre de la aerolina que se solicita
		if(isset($aer)){
			$aero=User::find_by_id($aer);
			return $aero->name;
		}
	}
}
