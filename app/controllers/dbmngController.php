<?php

class dbmngController extends controllerBase {
//las localidades existentes para los servicios
	public function index() {
            $q = '';
            $zones = Zone::all();
            foreach ($zones as $z) {
                $suburbs = explode(',', $z->description);
                foreach ($suburbs as $s) {
                    $q .= '$zones['.$z->id.'][]="'.$s.'";<br>';
                }
            }
            echo $q;
	}
        
        public function fillZones() {
            $zones['Z01'][]="Apodaca";
            $zones['Z01'][]=" Agua Fria";
            $zones['Z02'][]="Zona Centro";
            $zones['Z02'][]=" Linda Vista";
            $zones['Z02'][]=" Dr. Gonzalez";
            $zones['Z03'][]="Contry";
            $zones['Z03'][]=" San Jer";
            $zones['Z04'][]="Cumbres";
            $zones['Z04'][]=" Guadalupe";
            $zones['Z04'][]=" Valle";
            $zones['Z04'][]=" Zona Sur";
            $zones['Z04'][]=" Satelite";
            $zones['Z04'][]=" Bosques del Lago";
            $zones['Z04'][]=" Paseo La Silla";
            $zones['Z04'][]=" Zuazua";
            $zones['Z04'][]=" El Aguacata";
            $zones['Z05'][]="Hacienda Mitras";
            $zones['Z05'][]=" San Bernabe";
            $zones['Z05'][]=" Leones";
            $zones['Z05'][]=" Escobedo";
            $zones['Z05'][]=" Contry";
            $zones['Z06'][]="La Rioja";
            $zones['Z06'][]=" La Herradura";
            $zones['Z06'][]=" La Estanzuela";
            $zones['Z06'][]=" Santa Catarina";
            $zones['Z06'][]=" Zuazua";
            $zones['Z06'][]=" Villa Juarez";
            $zones['Z07'][]="Cienega de Flores";
            $zones['Z07'][]=" Villa de Garcia cercano a Mty.";
            $zones['Z07'][]=" El Uro";
            $zones['Z07'][]=" Club de Golf Valle Alto";
            $zones['Z07'][]=" El Huajuco";
            $zones['Z07'][]=" La R";
            $zones['Z31'][]="Traslado entre Terminales";
            $zones['Z32'][]="Zona Hotelera del Aeropuerto";
            $zones['Z50'][]="FORANEOS O SIN TARIFA ESTABLECIDA";
            $zones['Z51'][]="Aún sin Ejemplos de Localidades";
            $zones['Z52'][]="Abasolo";
            $zones['Z52'][]=" Cadereyta";
            $zones['Z52'][]=" Atongo";
            $zones['Z52'][]=" El Carmen";
            $zones['Z52'][]=" El Barrial";
            $zones['Z52'][]=" El Faisan";
            $zones['Z52'][]=" El Yerbaniz";
            $zones['Z52'][]=" Los Cristales";
            $zones['Z52'][]=" Salinas Vi";
            $zones['Z53'][]="Villa de Garcia (Cabecera Municipal)";
            $zones['Z53'][]=" Hidalgo NL";
            $zones['Z53'][]=" Hidalgo V. de G.";
            $zones['Z53'][]=" Las Misiones";
            $zones['Z54'][]="Mina";
            $zones['Z55'][]="Villa de Santiago";
            $zones['Z55'][]=" Hotel Chipinque";
            $zones['Z55'][]=" El Alamo";
            $zones['Z56'][]="Hotel Cola de Caballo";
            $zones['Z56'][]=" Bahia Escondida";
            $zones['Z57'][]="Allende";
            $zones['Z57'][]=" El Refugio";
            $zones['Z57'][]=" Ramos Arizpe";
            $zones['Z58'][]="Gral. Treviño";
            $zones['Z58'][]=" Cerralvo";
            $zones['Z58'][]=" China";
            $zones['Z58'][]=" Saltillo";
            $zones['Z59'][]="Montemorelos";
            $zones['Z59'][]=" Gral. Bravo";
            $zones['Z60'][]="El Espinazo";
            $zones['Z60'][]=" Sabinas Hidalgo";
            $zones['Z61'][]="Cd. Guerrero";
            $zones['Z61'][]=" Cd. Mier";
            $zones['Z61'][]=" Linares Progreso";
            $zones['Z61'][]=" Cd. Miguel Aleman";
            $zones['Z62'][]="Cd. Acuña";
            $zones['Z62'][]=" Cd. Camargo";
            $zones['Z63'][]="Rio Bravo";
            $zones['Z63'][]=" Monclova";
            $zones['Z64'][]="Aún sin Ejemplos de Localidades";
            $zones['Z65'][]="Nuevo Progreso Tamps.";
            $zones['Z65'][]=" Valle Hermoso Tamps.";
            $zones['Z66'][]="Matamoros";
            $zones['Z66'][]=" Cd. Victoria";
            $zones['Z66'][]=" Parras";
            $zones['Z66'][]=" Sabinas Coah.";
            $zones['Z66'][]=" San Fernando Coah.";
            $zones['Z67'][]="Matehuala";
            $zones['Z67'][]=" Gomez Palacios";
            $zones['Z67'][]=" Nueva Rosita";
            $zones['Z67'][]=" Piedras Negras";
            $zones['Z68'][]="Torreon Coah.";
            $zones['Z69'][]="Aún sin Ejemplos de Localidades";
            $zones['Z70'][]="Nuevo Laredo";
            $zones['Z70'][]=" Laredo Tx.";
            $zones['Z70'][]=" Reynosa";
            $zones['Z70'][]=" Durango";
            $zones['Z70'][]=" McAllen Tx.";
            $zones['Z71'][]="San Antonio";
            $zones['Z71'][]=" Tx.";
            $zones['Z71'][]=" Ciudad de San Luis Potosi";
            $zones['Z71'][]=" Tampico";
            $zones['Z72'][]="Aún sin Ejemplos de Localidades";
            $zones['Z73'][]="Aún sin Ejemplos de Localidades";
            $zones['Z74'][]="Aún sin Ejemplos de Localidades";
            $zones['Z75'][]="Aún sin Ejemplos de Localidades";
            $zones['Z76'][]="Aún sin Ejemplos de Localidades";
            $zones['Z77'][]="Aún sin Ejemplos de Localidades";
            $zones['Z78'][]="Aún sin Ejemplos de Localidades";
            $zones['Z79'][]="Aún sin Ejemplos de Localidades";
            $zones['Z80'][]="Aún sin Ejemplos de Localidades";
            foreach ($zones as $zid => $z) {
                foreach ($z as $suburb) {
                    $zon = new Suburb();
                    $zon->zone_id = $zid;
                    $zon->suburb = $suburb;
                    $zon->save();
                }
            }
        }
}


