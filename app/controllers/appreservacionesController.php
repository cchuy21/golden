<?php

class appreservacionesController extends controllerBase {
		public function price() {
		$precios = dbDriver::execQueryObject("SELECT * FROM costos", true);

		header('Content-type: text/xml');
		echo '<?xml version="1.0" encoding="UTF-8" ?>';
		echo "<Precios>";
		foreach ($precios as $value) {
			$value -> costo = number_format($value -> costo, 2);
			echo "<{$value->descripcion} value='{$value->costo}' />";
		}
		echo "</Precios>";
		exit ;
	}		
}