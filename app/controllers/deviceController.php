<?php
class deviceController extends controllerBase {


	public function insert(){
		$did = $_POST['did'];
		$token = $_POST['token'];
		$r=Device::find("all",array("conditions"=>"did='{$did}'"));
	//	$r=dbDriver::execQueryObject("select * from devices where did='{$did}'");
		if ($r)
		{
			$this->dispatchMessageXML("El dispositivo ya se encuentra registrado.");
		}else{
			 $d = new Device();
			 $d->did=$did;
			 $d->token=$token;
			//if(dbDriver::execQuery("INSERT INTO devices (did, token) VALUES('{$did}','{$token}')"))
			if($d->save())
				$this->dispatchMessageXML("Registro correcto.");
			else
				$this->dispatchErrorXML("Error al registrar el dispositivo.");
		}
		
	} 
	  
	public function update(){
		$did = $_POST['did'];
		$token = $_POST['token'];
		$r=Device::find_by_did($did);
		$r->token=$token;
//		$r = dbDriver::execQuery("UPDATE devices SET token='{$token}' where did='{$did}'");
			
		if($r->save())
		
			$this->dispatchMessageXML("Información actualizada correctamente.");
		else
			$this->dispatchErrorXML("Error al actualizar la información.");
	}
	
	
	public function assoc(){
		$did = $_POST['did'];
		$uid = $_POST['uid'];
        $r=Device::find_by_did($did);
		if ($r)
		{
			$r->userid=$uid;
			$r->login=1;
			
			//$r = dbDriver::execQuery("UPDATE devices SET userid='{$uid}', login='1'  where did='{$did}'");
            if($r->save())
                $this->dispatchMessageXML("Información actualizada correctamente.");
            else
                $this->dispatchErrorXML("Error al actualizar la información.");
		}else{
			 $d = new Device();
			 $d->did=$did;
			 $d->userid=$uid;
			 $d->login=1;
            if($d->save())
				$this->dispatchMessageXML("Registro correcto.");
			else
				$this->dispatchErrorXML("Error al registrar el dispositivo. device '{$did}',uid '{$uid}'");
        }
        
		
	}	public function asso(){
		$did = $_POST['did'];
		$uid = $_POST['uid'];
        $r=Device::find("all",array("conditions"=>"did='{$did}'"));
		if ($r)
		{
			$r->userid=$uid;
			$r->login=1;
			
			//$r = dbDriver::execQuery("UPDATE devices SET userid='{$uid}', login='1'  where did='{$did}'");
            if($r->save())
                $this->dispatchMessageXML("Información actualizada correctamente.");
            else
                $this->dispatchErrorXML("Error al actualizar la información.");
		}else{
			 $d = new Device();
			 $d->did=$did;
			 $d->userid=$uid;
			 $d->login=1;
            if($d->save())
				$this->dispatchMessageXML("Registro correcto.");
			else
				$this->dispatchErrorXML("Error al registrar el dispositivo. device '{$did}',uid '{$uid}'");
        }
        
		
	}
		public static function getDevices($uid = null)
	{
		if(!$uid) exit;
		return Device::find("all",array("conditions"=>"userid='{$uid}' and login='1' and token IS NOT NULL"));
//		return dbDriver::execQueryObject("SELECT * FROM devices where userid='{$uid}' and login='1' and token IS NOT NULL", true);
	}
	private function dispatchMessageXML($mensaje = null){
		header('Content-type: text/xml');
        echo '<?xml version="1.0" encoding="UTF-8" ?>';
		echo "<Message>";
		echo "<mensaje value='{$mensaje}' />";				
		echo "</Message>";
		exit;
	}
	private function dispatchErrorXML($mensaje = null){
		header('Content-type: text/xml');
		echo '<?xml version="1.0" encoding="UTF-8" ?>';
		echo "<Message>";
		echo '<error value="'.$mensaje.'"></error>';
		echo "</Message>";
		exit;
	}
	public function badges ($id = null){
       $id = isset($_POST['id']) ? $_POST['id'] : null;
       $user = User::find_by_id($id);
       if (!$user) {
          $this -> dispatchErrorXML("No existe el usuario.");
       }
       $user->badges = 0;
       if ($user->save())
          $this -> dispatchMessageXML("Información actualizada correctamente.");
       else
          $this -> dispatchErrorXML("Error al actualizar la información.");
    }
    
}