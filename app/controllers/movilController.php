<?php

class movilController extends controllerBase {
	public function appcosto() {
		$costocar = $_POST['costoauto'];
		$costocam = $_POST['costocam'];

		$z = Zone::find_by_id("app");
		if ($z) {
			$z -> cost_car = $costocar;
			$z -> cost_van = $costocam;
			$z -> save();
		}
	}

	private function dispatchMessageXML($mensaje = null) {
		header('Content-type: text/xml');
		echo '<?xml version="1.0" encoding="UTF-8" ?>';
		echo "<Message>";
		echo "<mensaje value='{$mensaje}' />";
		echo "</Message>";
		exit ;
	}
	
	private function dispatchErrorXML($mensaje = null) {
		header('Content-type: text/xml');
		echo '<?xml version="1.0" encoding="UTF-8" ?>';
		echo "<Message>";
		echo '<error value="' . $mensaje . '"></error>';
		echo "</Message>";
		exit ;
	}
	
	function appinsertdouble() {
		$tipo = $_POST['type'];
		if ($tipo == 1) {
			$tip = "MTY-APTO";
		} else {
			$tip = "APTO-MTY";
		}
		$direc = $_POST['direccion'];
		$hserv = $_POST['horaServi'];
		$fecha = $_POST['fecha'];
		$pasajeros = $_POST['pasajeros'];
		$vehiculo = $_POST['vehiculo'];
		if ($vehiculo)
			$vehiculo = 1;
		else
			$vehiculo = 2;
		$terminal = $_POST['terminal'];
		$aerolinea = $_POST['aerolinea'];
		$nvuelo = $_POST['noVuelo'];
		$hsalida = $_POST['horaSalida'];
		$fsalida = $this -> revfecha($_POST['fechaSalida']);
		$dest = $_POST['destino'];
		$precio = intval($_POST['precio']);
		$id_user = $_POST['idLogin'];
		$status = $_POST['status'];

		$tipo2 = $_POST['type2'];
		if ($tipo2 == 1) {
			$tip2 = "MTY-APTO";
		} else {
			$tip2 = "APTO-MTY";
		}
		$direc2 = $_POST['direccion2'];
		$hserv2 = $_POST['horaServi2'];
		$fecha2 = $_POST['fecha2'];
		$pasajeros2 = $_POST['pasajeros2'];
		$vehiculo2 = $_POST['vehiculo2'];
		if ($vehiculo2)
			$vehiculo2 = 1;
		else
			$vehiculo2 = 2;

		$terminal2 = $_POST['terminal2'];
		$aerolinea2 = $_POST['aerolinea2'];
		$nvuelo2 = $_POST['noVuelo2'];
		$hsalida2 = $_POST['horaSalida2'];
		$dest2 = $_POST['destino2'];
		$precio2 = intval($_POST['precio2']);

		$r1 = new Reservation();
		$r1 -> addresses = "a" . $direc;
		$r1 -> vehicle_id = $vehiculo;
		$r1 -> passengers = $pasajeros;
		$r1 -> terminal = $terminal;
		$r1 -> flight_hour = $hsalida;
		$r1 -> flight_number = $nvuelo;
		$r1 -> cost = $precio;
		$r1 -> mobile = 1;
		$fecha = $this -> revfecha($fecha);
		if ($tipo == 1) {
			//vie 26 Jun 2015
			$r1 -> reservation_date = date("Y-m-d", strtotime($fecha));
			$r1 -> reservation_time = date("G:i", strtotime($hserv));
		} else if ($tipo == 2) {
			$r1 -> reservation_date = date("Y-m-d", strtotime($fsalida));
			$r1 -> reservation_time = date("G:i", strtotime($hsalida));
		}
		$r1 -> abierto = 0;
		$r1 -> status = -1;
		$r1 -> type = $tip;
		$r1 -> save();
		$r2 = new Reservation();
		$r2 -> addresses = "a" . $direc2;
		$r2 -> vehicle_id = $vehiculo2;
		$r2 -> passengers = $pasajeros2;
		$r2 -> terminal = $terminal2;
		$r2 -> flight_hour = $hsalida2;
		$r2 -> flight_number = $nvuelo2;
		$r2 -> cost = $precio2;
		$fecha2 = $this -> revfecha($fecha2);
		$fsalida2 = $this -> revfecha($_POST['fechaSalida2']);
		if ($tipo == 1) {
			//vie 26 Jun 2015
			$r2 -> reservation_date = date("Y-m-d", strtotime($fecha2));
			$r2 -> reservation_time = date("G:i", strtotime($hserv2));
		} else if ($tipo == 2) {
			$r2 -> reservation_date = date("Y-m-d", strtotime($fsalida2));
			$r2 -> reservation_time = date("G:i", strtotime($hsalida2));
		}
		$r2 -> mobile = 1;
		$r2 -> abierto = 0;
		$r2 -> status = -1;
		$r2 -> type = $tip2;
		$r2 -> save();
		$s = new Service();
		$s -> user_id = $id_user;
		$s -> reservation1 = $r1 -> id;
		$s -> reservation2 = $r2 -> id;
		$s -> requested = "El mismo";
		$s -> cost = $precio + $precio2;
		$s -> payment = "Tarjeta de Credito/Debito";
		$s -> status = $status;
		$s -> received="movil";
		if ($s -> save()) {
			if ($status != "5") {
				$para = "rmireles@goldentransportaciones.com, sergio@animactiva.mx";
				$titulo = 'Nueva solicitud de servicio';
				$mensaje = "Se ha generado una nueva solicitud de servicio R:".$s->id." es ".$r1->type.", ingresa a la siguiente dirección para poder confirmarlo.\r\n \r\n http://resv.goldentransportaciones.net/";
				$cabeceras = array("From" => "reservacionesApp@goldentransportaciones.com",  "BCC" => "chuyflamerazul21@gmail.com,sergio@animactiva.mx, nnarvaez@goldentransportaciones.com,ajp@expertsys.com.mx");

				//$cabeceras = 'From: reservacionesApp@goldentransportaciones.com' . "\r\n" . 'Bcc: sergio@animactiva.mx, nnarvaez@goldentransportaciones.com,ajp@expertsys.com.mx' . "\r\n" . 'X-Mailer: PHP/' . phpversion();
				if ($_POST['pagar']) {
					$this -> dispatchMessageXML($s -> id);

				} else if (1) {
					$this -> dispatchMessageXML($s -> id);
				} else {
					$this -> dispatchErrorXML("Error al enviar correo electronico.");
				}
			} else {
				$this -> dispatchMessageXML($s -> id);
			}
		} else {
			$this -> dispatchErrorXML("Error al enviar correo electronico.");
		}
	}
	
	function appinsert() {
		$tipo = $_POST['type'];
		if ($tipo == 1) {
			$tip = "MTY-APTO";
		} else {
			$tip = "APTO-MTY";
		}
		$direc = $_POST['direccion'];
		$hserv = $_POST['horaServi'];
		$fecha = $_POST['fecha'];
		$pasajeros = $_POST['pasajeros'];
		$vehiculo = $_POST['vehiculo'];
		$status = $_POST['status'];
		if ($vehiculo)
			$vehiculo = 1;
		else
			$vehiculo = 2;

		$terminal = $_POST['terminal'];
		$aerolinea = $_POST['aerolinea'];
		$nvuelo = $_POST['noVuelo'];
		$hsalida = $_POST['horaSalida'];
		$fsalida = $this -> revfecha($_POST['fechaSalida']);
		$dest = $_POST['destino'];
		$precio = intval($_POST['precio']);
		$id_user = $_POST['idLogin'];
		$r = new Reservation();
		$r -> addresses = "a" . $direc;
		$r -> vehicle_id = $vehiculo;
		$r -> passengers = $pasajeros;
		$r -> terminal = $terminal;
		$r -> flight_hour = $hsalida;
		$r -> flight_number = $nvuelo;
		$r -> cost = $precio;
		$fecha = $this -> revfecha($fecha);
		if ($tipo == 1) {
			//vie 26 Jun 2015
			$r -> reservation_date = date("Y-m-d", strtotime($fecha));
			$r -> reservation_time = date("G:i", strtotime($hserv));
		} else if ($tipo == 2) {
			$r -> reservation_date = date("Y-m-d", strtotime($fsalida));
			$r -> reservation_time = date("G:i", strtotime($hsalida));
		}
		$r -> abierto = 0;
		$r -> status = -1;
		$r -> type = $tip;
		$r -> mobile = 1;
		$r -> save();
		$s = new Service();
		$s -> user_id = $id_user;
		$s -> reservation1 = $r -> id;
		$s -> requested = "El mismo";
		$s -> cost = $precio;
		$s -> payment = "Tarjeta de Credito/Debito";
		$s -> status = $status;
		$s -> received="movil";
		if ($s -> save()) {
			if ($status != "5") {
				//$u=User::find_by_id($id_user);
				$para = "rmireles@goldentransportaciones.com, sergio@animactiva.mx";
				//$para = $u->email;
				$titulo = 'Nueva solicitud de servicio';
				$mensaje = "Se ha generado una nueva solicitud de servicio R:".$s->id." es ".$r->type.", ingresa a la siguiente dirección para poder confirmarlo.\r\n \r\n http://resv.goldentransportaciones.net/ en configuraciones submenu  de Reservaciones App";
				$cabeceras = array("From" => "reservacionesApp@goldentransportaciones.com",  "BCC" => "chuyflamerazul21@gmail.com,sergio@animactiva.mx, nnarvaez@goldentransportaciones.com,ajp@expertsys.com.mx");
				//$cabeceras = 'From: reservacionesApp@goldentransportaciones.com' . "\r\n" . 'Bcc: sergio@animactiva.mx, nnarvaez@goldentransportaciones.com,ajp@expertsys.com.mx' . "\r\n" . 'X-Mailer: PHP/' . phpversion();
				if ($_POST['pagar']) {
					if($tip=="APTO-MTY"){
						$s -> status =1;
						$s -> save();
					}
					$this -> dispatchMessageXML($s -> id);
				} else if (1) {
					//$this -> dispatchMessageXML("Hemos recibido tu solicitud, te enviaremos un email con la confirmación en unos minutos. Una vez confirmado el servicio podrás pagarlo en \"Mis Reservaciones\" mediante PayPal, es completamente seguro y fácil, o bien, puedes pagar en caseta en el aeropuerto.");
					$this -> dispatchMessageXML($s -> id);

				} else {
					$this -> dispatchErrorXML("Error al enviar correo electronico.");
				}
			} else {
				$this -> dispatchMessageXML($s -> id);
			}
		} else {
			$this -> dispatchErrorXML("Error al enviar correo electronico.");
		}
	}

	function pemail() {
		$para = "chuy_flamerazul@hotmail.com";
		//$para = $u->email;
		$titulo = 'Nueva solicitud de servicio';
		$mensaje = "Se ha generado una nueva solicitud de servicio, ingresa a la siguiente dirección para poder confirmarlo.\r\n \r\n http://resv.goldentransportaciones.net/ en configuraciones submenu  de Reservaciones App";
		$cabeceras = 'From: reservacionesApp@goldentransportaciones.com' . "\r\n" . 'Bcc: sergio@animactiva.mx, nnarvaez@goldentransportaciones.com,ajp@expertsys.com.mx' . "\r\n" . 'X-Mailer: PHP/' . phpversion();
		mail($para, $titulo, $mensaje, $cabeceras);
	}

	function revfecha($fecha) {
		$fechad = substr($fecha, 4, 2);
		$fecham = substr($fecha, 7, 3);
		$fechaa = substr($fecha, 11, 4);
		switch ($fecham) {
			case 'Ene' :
				$fecham = "Jan";
				break;
			case 'Abr' :
				$fecham = "Apr";
				break;
			case 'Ago' :
				$fecham = "Aug";
				break;
			case 'Dic' :
				$fecham = "Dec";
				break;
		}
		return $fechad . " " . $fecham . " " . $fechaa;
	}
	public function apphab() {
		$id = $_POST['id'];
		$s = Service::find($id);
		$s -> status = 1;
		$s->attended=authDriver::getSUser() -> name . " " . authDriver::getSUser() -> lastname;
		$s -> save();
		$r1 = Reservation::find($s -> reservation1);
		$r1 -> status = 1;
		$r1 -> save();
		$tipo = "Viaje sencillo";
		if ($s -> reservation2) {
			$r2 = Reservation::find($s -> reservation2);
			$r2 -> status = 1;
			$r2 -> save();
			$tipo = "Viaje redondo";
		}
		$dev = Device::find_by_userid($s -> user_id);
		$u = User::find_by_id($s -> user_id);
		$this->notify($dev->token,$tipo,$r1->type. " - " . templateDriver::timelo($r1 -> reservation_date,1) . " - " . $r1 -> reservation_time . " - " . "confirmado",$u->badges);
		$cabeceras = array("From" => "reservacionesApp@goldentransportaciones.com", "X-Mailer" => 'PHP/' . phpversion());
		$mensaje = "Reservacion Habilitada";
		$titulo = "Reservacion Golden";
		$para = $u -> email;
header("Location: http://resv.goldentransportaciones.net/service/correo/".$s->id."/0");		
//mailDriver::send($para, $titulo, $mensaje, $cabeceras);
		echo json_encode(1);
	}
	public function appdel() {
		$id = $_POST['id'];
		$s = Service::find($id);
		$r1 = Reservation::find($s -> reservation1);
		$r1 -> status = 0;
		$r1 -> save();
		$tipo = "Viaje sencillo";
		if ($s -> reservation2) {
			$r2 = Reservation::find($s -> reservation2);
			$r2 -> status = 0;
			$r2 -> save();
			$tipo = "Viaje redondo";
		}
		$dev = Device::find_by_userid($s -> user_id);
		$u = User::find_by_id($s -> user_id);
		$this->notify($dev->token,$tipo,$r1->type. " - " . templateDriver::timelo($r1 -> reservation_date,1) . " - " . $r1 -> reservation_time . " - " . "cancelado",$u->badges);
		templateDriver::setData("tipo", array("id" => 2));
		templateDriver::setData("servicio", array("id" => $s->id));
		templateDriver::renderSection("pruebas.mail");
		
	}

	public function validateUser($id = null, $token = null) {
		if ($id != null && $token != null) {
			$user = User::find_by_id($id);
			if ($user)
				return true;
		}
		return false;
	}

	public function hide() {
		$id = isset($_POST['id']) ? $_POST['id'] : null;
		$token = isset($_POST['token']) ? $_POST['token'] : null;
		if ($this -> validateUser($id, $token)) {
			$idr = isset($_POST['idr']) ? $_POST['idr'] : null;
			if (!$idr)
				$this -> dispatchErrorXML("No se seleccionó ninguna reservación.");

			$s = Service::find_by_reservation1($idr);
			if ($s) {
				$s -> status = 3;
			}
			if ($s -> save())
				$this -> dispatchMessageXML("La reservación se ha borrado de tu historial correctamente.");
		}
		$this -> dispatchErrorXML("Error al borrar la reservación de tu historial.");
	}

	public function comment() {

		$id = isset($_POST['id']) ? $_POST['id'] : null;
		$token = isset($_POST['token']) ? $_POST['token'] : null;
		if ($this -> validateUser($id, $token)) {
			$comment = isset($_POST['comentario']) ? $_POST['comentario'] : null;
			if (!$comment)
				$view -> dispatchErrorXML("El comentario esta vacio.");
			date_default_timezone_set('America/Mexico_City');
			setlocale(LC_ALL, "es_ES");
			$fecha = utf8_encode(strftime("%A %d de %B del %Y a las %H:%m"));
			$rev = new Review();
			$rev -> user_id = $id;
			$rev -> comment = $comment;
			$rev -> fecha = $fecha;
			if ($rev -> save())
				$this -> dispatchMessageXML("Tu comentario se ha enviado correctamente.");
		}
		$this -> dispatchErrorXML("Error al enviar el comentario.");
	}

	public static function notify($token = null, $title = null, $message = null, $badges = null) {
		//open connection
		$ch = curl_init();
		$url = "timytinna.com/notify2.php";

		//set the url, number of POST vars, POST data
		if (!$token) {
			$token = urlencode($token);
			$title = urlencode($title);
			$message = urlencode($message);
			$badges = urlencode($badges);
		}
		if (!$token)
			exit ;

		curl_setopt($ch, CURLOPT_URL, $url);
		$fields = array('token' => $token, 'title' => $title, 'message' => $message, 'badges' => $badges);

		foreach ($fields as $key => $value) {
			$fields_string .= $key . '=' . $value . '&';
		}
		rtrim($fields_string, '&');
		$fields_string;
		curl_setopt($ch, CURLOPT_POST, count($fields));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

		//execute post
		$result = curl_exec($ch);

		//close connection
		curl_close($ch);
		//print_r($result);
	}

	/*public function notify($user=null,$token=null,$title=null,$message=null,$badges){
	 echo '<script src="/static/js/jquery.js" type="text/javascript"></script>
	 <script>
	 jQuery(document).ready(function(){
	 jQuery.ajax({
	 url: "timytinna.com/Porticoweb/'.$user.'/'.$token.'/'.title.'/'.message.'/'.badges.'",
	 })
	 });
	 </script>';

	 }*/
}
