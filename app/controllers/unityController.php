<?php
class unityController extends controllerBase {

		public function touppe($array){
		$ke=array_keys($array);

		for($i=0;$i<count($array);$i++){
			$array[$ke[$i]]=strtoupper($array[$ke[$i]]);
		}
		return $array;

	}
	//dar de alta una unidad
	public function register($data) {
	  	$data=$this->touppe($data);
			unset($data['id']);

		$unit= new Unity($data);
		//si se guarda correctamente el registro en la base de datos
		if($unit->save()){
			responseDriver::dispatch('M', 'Success', 'la unidad se ha dado de alta');
		}else{
			responseDriver::dispatch('E', 'Error', 'Se ha generado un errror al dar de alta a la unidad por favor intentelo de nuevo');
		}
	  }

//busca una unidad en la base de datos
	public function publish(){
        $data = inputDriver::getVar(__POST__);
        $isnew = false;
		$op=Unity::find_by_id($data['id']);
        if(!$op) {
            $this->register($data);
        } else {
            $this->update($data);
        }
    }


//se actualizan los datos de una unidad
	public function update($data){

		$unity = Unity::find_by_id($data['id']);
		if(!isset($data['id'])) exit;
		$data=$this->touppe($data);
		 $unity->update_attributes($data);
		 //si existe un error al momento de modificar
		if(!$unity->errors){
			responseDriver::dispatch('E', 'Error', 'Se ha generado un errror al actualizar los datos de la unidad por favor intentelo de nuevo');
		}else{
			responseDriver::dispatch('M', 'Success', 'Se han actualizado los datos de la unidad');
		}
	}

	//deshabilita una unidad
	  public function disable() {
        //Validar login
        $data = inputDriver::getVar("aid");
        $u = Unity::find_by_id($data);
        if($u){
        	//se elimina la unidad
            $u->delete();
            responseDriver::dispatch('D', array('message'=>"unidad deleted"));
        }
    }
    public function serv(){
    	$uni=Unity::find("all");

    	?>
		<table>
			<thead>

			</thead>
			<tbody>
				<?php
				$c=1;
				foreach($uni as $u){
					if(!empty($u->driver)){
					$o=Operator::find($u->driver);
					echo '<tr class="unidad">
							<td class="number">'.$c.'<td>
							<td class="id">'.$u->economic.'<td>
							<td class="driver">'.$o->id.'</td>
							<td class="name">'.$o->name.' '.$o->lastname.'</td>
						</tr>';
						$c++;
					}
				}
				?>
			</tbody>
		</table>
		<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
		<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery(".unidad").each(function(){
					var t=jQuery(this);
					var id=t.find(".id").html();
					var idchofer=t.find(".driver").html();
					var name=t.find(".name").html();
					jQuery.ajax({
			  			url:"http://gps.goldentransportaciones.com/units/updatecreatevehicle/",
			  			 type: "GET",
			  			data:{
						     chofer_external_id : idchofer,
						     chofer_name:name,
						     unidad:id
						   },
			            crossDomain: true,
    					dataType: 'jsonp',
			            "contentType": 'application/x-www-form-urlencoded; charset=UTF-8',
			            success: function (source) {
			                data=source;

			            },
			            error: function (dato) {

			            }
			  		});
				});
			});
		</script>
		<?php
    }
}
