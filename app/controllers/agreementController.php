<?php
class agreementController extends controllerBase {
	     public function add($data) {
	     $hoy=date("Y-m-d G:i"); 
        //Validar login
       
        $data = inputDriver::getVar(__POST__);
  		$data['id']=str_replace(' ', '', $data['agreement']);
  		$data['id']=str_replace('/', '-', $data['id']);
  		$u=Agreement::find("all",array("conditions"=>"id='".$data["id"]."'"));
        if(count($u)){
            $digits = 3;
            $rnd=rand(pow(10, $digits-1), pow(10, $digits)-1); 
            $data["id"]=$data["id"].$rnd;
        }
        $address = new Agreement($data);
        if( $address->is_valid() ) { //se agrega una nueva direccion 
        $address->log="'Creado','".authDriver::getSUser()->name." ".authDriver::getSUser()->lastname."','".$hoy."'";
        
            $address->save();
            responseDriver::dispatch('M',"Mensaje","mensaje");
        } else {
            //Error
            responseDriver::dispatch('E', "Error", "Error al intentar guardar la nueva dirección.");
        }
    
    }
    public function publish(){
        $data = inputDriver::getVar(__POST__);
        $isnew = false;
		$data['enable']=1;
		
        if($data['id'] == '' || $data['id'] == '-1' || $data['id'] == -1) {
            $this->add($data);
        } else {
            $this->update($data);
        }
    }
    public function update($data) { //se actualiza la dirección 
        //Validar login
        $hoy=date("Y-m-d G:i"); 
        $data = inputDriver::getVar(__POST__);
		
        $address = Agreement::find_by_id($data['id']);
		
        if($address) {
            $address->log=$address->log."||'Editado','".authDriver::getSUser()->name." ".authDriver::getSUser()->lastname."','".$hoy."'";
			$address->agreement=$data['agreement'];
			$address->aeropuerto=$data['aeropuerto'];
			$address->restringida=$data['restringida'];
			$address->metropolitan=$data['metropolitan'];
			$address->periphery=$data['periphery'];
			$address->foraneo=$data['foraneo'];
			$address->intermedia=$data['intermedia'];
			$address->camioneta=$data['camioneta'];
			$address->description=$data['description'];
			$address->promo=$data['promo'];
            if($address->save()) {
				  responseDriver::dispatch('M', "Cambios guardados", "Informacion actualizada correctamente en la convenio.");
				
            }
        } else {
            //Error
            $data['enable']=1;
			$this->add($data);
        }
    }
	public function disable() {
        //Validar login
        $data = inputDriver::getVar("aid");
        $z = Agreement::find_by_id($data);
		if($z->business!=0){
		$emp=User::find($z->business);
			if($emp && $emp->id!=0){
	        $user=User::find("all",array("conditions" => array("business_id= {$emp->id} ")));
	            foreach($user as $u){
	            	if($u->agreement==$z->id){
	            		$u->agreement="SC";
						$u->save();	
	            	}
	            }
			}
		}
		$z->delete();
			
        responseDriver::dispatch('D', array('message'=>"zona deleted"));
       
    }
}
