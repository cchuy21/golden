<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class userView extends viewBase {
	public function dispatchUserXML($user = null)
	{
		header('Content-type: text/xml');
		echo '<?xml version="1.0" encoding="UTF-8" ?>';
		echo "<Message>";
		if($user)
		{

			echo "<id>{$user->id}</id>";
			echo "<token>{$user->token}</token>";
			echo "<username>{$user->username}</username>";
            echo "<nombre>{$user->nombre}</nombre>";
            echo "<correo>{$user->correo}</correo>";
            echo "<edad>{$user->edad}</edad>";
            echo "<genero>{$user->genero}</genero>";
            echo "<telefono>{$user->telefono}</telefono>";
			echo "<fb>{$user->facebook}</fb>";
		}
		else 
		{
			echo " <error value='Usuario vacio.' /> ";
		}
		echo "</Message>";
	}
    public function index() {
        templateDriver::render();
    }
    
    public function window($id = null) {
        templateDriver::renderSection("user.window", $id);
    }
	  public function winreser($id = null) {
        templateDriver::renderSection("user.windowr", $id);
    }
    
    
}
