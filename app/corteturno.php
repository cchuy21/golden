public function corteturno($hor){
		<?php
		//SE IMPRIME EL CORTE DE TURNO DE LA MAÑANA O TARDE DEPENDIENDO DEL HORARIO QUE SE SOLICITA
		$cortet='
		
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
 		 <head>
  		  <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
 		   <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
		   <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
			<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    		<title>P. corte de turno</title>
  		 </head>
  		<body>
    		<div style="width: 325px; text-align: center;padding-left: 10px;">
    			<small> GOLDEN TRANSPORTACIONES S.A de C.V</small><br />
    			Corte de turno<br />
    			Servicios por operador
      			<table style="text-align: left;border-top: 5px solid black;border-bottom: 5px solid black;width: 100%">
      			 <tbody>
       				<tr>
       	   			 <td>'.
       	   			 	
							$hoy=date("Y-m-d");

							$date = strtotime("-1 day", strtotime($hoy));
						    $date =date('Y-m-d', $date);
							$hora=date('G:i');
							 $hora=date("G:i",strtotime($hor));
													
							
							if(strtotime($hora)<strtotime(date("G:i",strtotime("03:15")))&&strtotime($hora)>strtotime(date("G:i",strtotime("00:00")))){
								
								$assi=Assignment::find("all",array("conditions"=>"(created_at BETWEEN {ts'".$date." 03:15:00'} AND {ts'".$hoy." 03:14:59'})"));
								
							}else{
								
								$assi=Assignment::find("all",array("conditions"=>"created_at BETWEEN {ts'".$hoy." 03:15:00'} AND {ts'".$hoy." 23:59:59'}"));
								
							}	
							
							$hora=date("G:i");
							
							
							$r=0;
						
						foreach($assi as $as){
		  					$res=Reservation::find_by_id($as->reservation_id);
							if($res && $res->status==3){
								$r++;
							}else{
								$tick=Ticket::find_by_id($as->ticket_id);
								if($tick && $tick->status==3){
									$r++;
								}
							}
						}
						
							$diasem=date("D");
							$di=date("d");
							$me=date("m");
							$yea=date("Y");
							switch ($me) {
								case 1:
									$mes="Enero";
									break;
								
								case 2:
									$mes="Febrero";
									break;
								case 3:
									$mes="marzo";
									break;
								case 4:
									$mes="Abril";
									break;
								case 5:
									$mes="Mayo";
									break;
								case 6:
									$mes="Junio";
									break;
								case 7:
									$mes="Julio";
									break;
								case 8:
									$mes="Agosto";
									break;
								case 9:
									$mes="Septiembre";
									break;
								case 10:
									$mes="Octubre";
									break;
								case 11:
									$mes="Noviembre";
									break;
								case 12:
									$mes="Diciembre";
									break;
							}
							switch ($diasem) {
								case 'Mon':
									$dia="Lunes";
									break;
								case 'Tue':
									$dia="Martes";
									break;
								case 'Wen':
									$dia="Miercoles";
									break;
								case 'Thu':
									$dia="Jueves";
									break;
								case 'Fri':
									$dia="Viernes";
									break;
								case 'Sat':
									$dia="Sabado";
									break;
								case 'Sun':
									$dia="Domingo";
									break; 
									
							}
							
       	   			 	'
       	 				<small>Total de servicios:</small>'.$r.'
       	   			 </td>
       				</tr>
       				<tr>
       			     <td>
       					"<small>Impreso el'.$dia.' '.$di.' de '.$mes.' del '.$yea.' a las '.$hora.'</small>";
       				 </td>
       				</tr>
       			 </tbody>
      		   </table>'.
      		   
      		   
      		
     			 	$operator=Operator::find('all',array('order' => 'id asc'));
					$cont=0;
					foreach ($operator as $o) {
													 $hora=date("G:i",strtotime($hor));
						
							if(strtotime($hora)<strtotime(date("G:i",strtotime("03:15")))&&strtotime($hora)>strtotime(date("G:i",strtotime("00:00")))){
								$assig=Assignment::find("all",array("conditions"=>"operator_id =".$o->id." AND (created_at BETWEEN {ts'".$date." 03:15:00'} AND {ts'".$hoy." 03:14:59'})"));
							}else{
								$assig=Assignment::find("all",array("conditions"=>"operator_id =".$o->id." AND (created_at BETWEEN {ts'".$hoy." 03:15:00'} AND {ts'".$hoy." 23:59:59'})"));
							}
					if ($assig) {
						$cr=0;
						$cb=0;
						$cd=0;
						$cre=0;
						
						foreach($assig as $as){
		  					$res=Reservation::find_by_id($as->reservation_id);
							if($res && $res->status==3){
								$cr++;
							
								if($as->type=="Directo"){
									$cd++;
								}else if($as->type="Regreso"){
									$cre++;
								}
							}else{
								$res=Ticket::find_by_id($as->ticket_id);
								if($res && $res->status==3){
									$cb++;
								}
							}
						}
				
					if($cr!=0 || $cb!=0){
				
	 				
     			 '<div class="row" style="margin-left: 0px;margin-right: 0px;"'>'
      				'.$o->id.' <strong>'. $o->name.'</strong><br />
      				Total de servicios:'. ($cb+$cr).'<br />
  					<div class="col-md-6" style="border-bottom: 1px solid black;text-align: left;padding-left: 45px;padding-top: 10px;padding-bottom: 5px;">
  						<small>Boletos:</small> '. $cb.'  <br />
  						<small>Reservaciones:</small>'.$cr.'<br />
  						<small>Directas:</small>'.$cd.'<br />
  						<small>Regresos:</small>'. $cre.'<br />
  					</div>
  					<div class="col-md-6" style="border-bottom: 1px solid black;text-align: center;padding-top: 10px;padding-bottom: 7px;">
  					<small>Pagables:</small>'.($cb+$cr).'<br />
  					<table style="border: 2px solid black;text-align: center;margin-top: 12px;margin-left: 20px;"> 
  						<tbody>
  							<tr>
  								<td style="border-bottom: 2px solid black;">
  									Comisión Base
  								</td>
  							</tr>
  							<tr>
  								<td>
  									$85.50
  								</td>
  							</tr>
  						</tbody>
  					</table>
  					</div>
			  	</div>'.
	 			
	  				$z=0;
	  				foreach ($assig as $a) {
	  					
				
	  					$resr=Reservation::find_by_id($a->reservation_id);
						
						if($resr && $resr->status==3){
								$z++;
							$cont++;
						$fechamesre=substr($resr->reservation_date,8 ,3);
	   					$horares=substr($resr->reservation_time, 0 ,5);
	   					$fechadia=substr($resr->reservation_date, 5 ,2);
						$ser=Service::find_by_reservation1($resr->id);
			
						
						
						if(!$ser){
						$ser=Service::find_by_reservation2($resr->id);
							
						}				
							'
				<table style="width: 100%">
      				<tbody>
      					<tr style="border-bottom: 1px solid black;">
      						<td>
      						'.$z.'<br>&nbsp;
      						</td>
      						<td>
      							U: '.$a->unity_id.'<br>&nbsp;
      						</td>
      						<td>
      							R:'. $ser->i; $resr->type.'<br /> 
      							'. $a->type.'
      							 | LOCAL
      						</td>
      						<td>
      							   '. $fechamesre.'-'.$fechadia.' '. $horares.' <br>&nbsp;
      						</td>
      					</tr>
      				</tbody>
      			</table>';							
													
						}else{
							
							$resr=Ticket::find_by_id($a->ticket_id);
							if($resr && $resr->status==3){
								$z++;
								$cont++;
							$fechamesre=substr($resr->created_at,8 ,3);
	   						$horares=substr($resr->time, 0 ,5);
	   						$fechadia=substr($resr->created_at, 5 ,2);
							
							
				$cortet=$cortet.'<table style="width: 100%">
      				<tbody>
      					<tr style="border-bottom: 1px solid black;">
      						<td>'.$z.'<br>&nbsp;
      						</td>
      						<td>
      							U: '.$a->unity_id.'<br>&nbsp;
      						</td>
      						<td width="156">
      							B:'.$resr->folio.' <br>&nbsp;
      						</td>
      						<td>
      							'.$fechamesre.'-'.$fechadia.' '.$horares.' <br>&nbsp;
      						</td>
      					</tr>
      				</tbody>
      		</table>';
      									
		
		/*si se activa la siguiente linea, se envia al correo del usuario el corte de turno cuando se esta realizando
		ACTUALMENTE NO ESTA EN USO POR QUE EL SISTEMA SE SATURA AL REALIZARLO 			
		 * BCC-> COPIA PARA:	
									
		//	 mailDriver::send("fernanda_monsalvo25@hotmail.com","Corte Turno",$cort,array("From" => "reservaciones@goldentransportaciones.com", "Content-type" =>"text/html",  "BCC" => "chuy_flamerazul@hotmail.com"));	

		*/				
						}
	  				
					}
				}
				}
			}
		}
      
      
   				$cortet=$cortet.'</div>
  			</body>
		</html>'?>