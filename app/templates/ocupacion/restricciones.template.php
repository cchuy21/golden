<?php
	$fecha=templateDriver::getData("fecha");
	$normal="NORMAL";
	$nb="checked";
	$lb="";
	
?>
<style>
	.inptocu{
		width:40px;
		margin: 0 auto 0;
    	text-align: center;
	}
	.brd{
		border: none!important;
	}
</style>
Normal
<table class="table table-bordered">
	<thead>
	<tr>
		<td colspan="2" rowspan="3">
			HORARIO
		</td>
		<td colspan="8">
			<center>RESERVACIONES</center>
		</td>
	</tr>
	<tr>
		<?php
			$w=date("w",strtotime($fecha));
			if($w!=1){
				$dias="MARTES - VIERNES";
			}else{
				$dias="LUNES";
			}
		?>
		<td colspan="2">
			<?php echo $dias; ?>
		</td>
		<td rowspan="2">
			HECHAS
		</td>
		<td rowspan="2">
			ALT
		</td>
		<td rowspan="2">
			AMX
		</td>
		<td rowspan="2">
			INT
		</td>
		<td rowspan="2">
			VOL
		</td>
		<td rowspan="2">
			
		</td>
		<td rowspan="2">
			LIBRES
		</td>
	</tr>
	<tr>
		<td colspan="2"><?php echo $normal;?></td>
	</tr>
	</thead>
	<tbody>
		<?php
		$sc=Schedule::find("all",array("select"=>"id,vn,vl,ln,ll,hour,hour1,hour2"));
		$c=0;
		$sn=0;
		$sh=0;
		$sl=0;
		$tant=0;
		foreach ($sc as $s) {
			$c++;
			if($w>1 && $w<6){
				$normal=$s->vn;
				$nc=$s->vn;
				
			}else{
				$normal=$s->ln;
				$nc=$s->ln;
			}
			$res=Reservation::find("all",array("select"=>"id", "conditions"=>"status <> 0 and reservation_date='".$fecha."' AND reservation_time BETWEEN {ts '".$s->hour1."'} AND {ts '".$s->hour2."'} AND type='MTY-APTO'"));
			$cant=0;
			foreach($res as $r){
				$ser=Service::find_by_reservation1($r->id,array("select"=>"id"));
				if(!$ser)
					$ser=Service::find_by_reservation2($r->id,array("select"=>"id"));
				//$u=$ser->user;
				if($ser)
					$cant++;
			}
			$rest=Restriction::find("all",array("conditions"=>"fecha ={ts '".$fecha."'} and schedule_id=".$s->id));
			foreach($rest as $re){
				$rst=$re;
				break;
			}
			$tant=$tant+intval($nc);
			echo "<tr>
				<td>
					".date("G:i",strtotime($s->hour1))." - ".date("G:i",strtotime($s->hour2))."
				</td>
				<td>
					<strong>".$s->hour."</strong>
				</td>
				<td><strong>".$nc."</strong></td>
				<td>";
				if($rst->cambio>0){
					echo "<input id='rtotal".$c."' name='cambio".$c."' class='form-control inptocu' value='".$rst->cambio."' type='text' />";
					$normal=$rst->cambio;
				}else{
					echo "<input id='rtotal".$c."' name='cambio".$c."' class='form-control inptocu' value='".$normal."' type='text' />";
				}
				$cnt=$cant;
				$canti=$cant+intval($rst->alt)+intval($rst->vol)+intval($rst->amx)+intval($rst->int)+intval($rst->extra);
				$dis=intval($normal)-intval($canti);
				$sl=$sl+intval($dis);
				$sh=$sh+intval($cant);
				$sn=$sn+intval($normal);
				echo "</td>
				<td>
					<span id='rhechas".$c."'>".$cant."</span>
					<input type='hidden' id='hchas".$c."' value='".$cnt."' />
					<input type='hidden' name='libres".$c."' id='libres".$c."' class='form-control inptocu' value='".$dis."' />
					<input name='id".$c."' data-lugar='".$c."' type='hidden' class='form-control inptocu' value='".$rst->id."'>
				</td>
				<td>
					<input name='alt".$c."' data-lugar='".$c."' class='form-control inptocu' value='".$rst->alt."'>
				</td>
				<td>
					<input name='amx".$c."' data-lugar='".$c."' class='form-control inptocu' value='".$rst->amx."'>
				</td>
				<td>
					<input name='int".$c."' data-lugar='".$c."' class='form-control inptocu' value='".$rst->int."'>
				</td>
				<td>
					<input name='vol".$c."' data-lugar='".$c."' class='form-control inptocu' value='".$rst->vol."'>
				</td>
				<td>
					<input name='extra".$c."' data-lugar='".$c."' class='form-control inptocu' value='".$rst->extra."'>
					
				</td>
				<td id='rextra".$c."'>
					".$dis."
				</td>
			</tr>";
		}
		echo "<tr class='brd'>
		<td class='brd'></td>
		<td class='brd'></td>
		<td><strong>".$tant."</strong></td>
		<td id='sumtotal'>".$sn."</td>
		<td>".$sh."</td>
		<td colspan='5' class='brd'></td>
		<td id='sumlibres'>".$sl."</td>
		</tr>";
		?>
	</tbody>
</table>
<center><button class="btn btn-success btn-saveres">Guardar</button></center>