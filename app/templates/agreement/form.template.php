<?php
    $d = templateDriver::getData('id');
	if(!$d){
		
	}else{
		$agre=Agreement::find_by_id($d['id']);
	}



?>
<!--muestra los datos en la pantalla para poder realizarle modificaciones-->
<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Convenio</h4>
			</div>
	<div class="modal-body" style="padding: 10px 15px;">
	
		
  	<table class="tablamarar"> 
  		<tbody>
	  		<tr>
	  			<td class="tdstatico">
	  				<span class="tool glyphicon glyphicon-globe iconoesl" data-toggle="tooltip" data-placement="top" title="Suburb"></span>
	  			</td>
	  			<td colspan='2'>
	  			 <div class="form-inline">
	  			 	<input name="id" type="hidden" placeholder="Nombre" value="<?php echo isset($agre->id) ? $agre->id : ''; ?>" class="form-control system_data confnombre">

		  			<input name="agreement" style="width: 100%;" type="text" placeholder="Nombre" value="<?php echo isset($agre->agreement) ? $agre->agreement : ''; ?>" class="form-control system_data confnombre">
	  			 
		         </div>              
	  		    </td>
	  		</tr>

  			<tr>
	  			<td class="tdstatico">
	  				<span class="tool glyphicon glyphicon-usd iconoesl" data-toggle="tooltip" data-placement="top" title="E-mail"></span>
	  			</td>
	  			<td>
	  			    Zona Aeropuerto
	  			</td>
	  			<td>
	  			   <div class="form-inline">
		  		    <input name="aeropuerto" style="width: 100%" type="text" placeholder="Costo Área Aeropuerto" value="<?php echo isset($agre->aeropuerto) ? $agre->aeropuerto : ''; ?>" class="form-control system_data">
		           </div>
	  			</td>
	  		</tr>
			<tr>
	  			<td class="tdstatico">
	  				<span class="tool glyphicon glyphicon-usd iconoesl" data-toggle="tooltip" data-placement="top" title="E-mail"></span>
	  			</td>
	  			<td>
	  			    Zona Inmediata
	  			</td>
	  			<td>
	  			   <div class="form-inline">
		  		    <input name="metropolitan" style="width: 100%" type="text" placeholder="Costo Área Inmediata" value="<?php echo isset($agre->metropolitan) ? $agre->metropolitan : ''; ?>" class="form-control system_data">
		           </div>
	  			</td>
	  		</tr>
	  		<tr>
	  			<td class="tdstatico">
	  				<span class="tool glyphicon glyphicon-usd iconoesl" data-toggle="tooltip" data-placement="top" title="E-mail"></span>
	  			</td>
	  			<td>
	  			    Zona Intermedia
	  			</td>
	  			<td>
	  			   <div class="form-inline">
		  		    <input name="intermedia" style="width: 100%" type="text" placeholder="Costo Área Intermedia" value="<?php echo isset($agre->intermedia) ? $agre->intermedia : ''; ?>" class="form-control system_data">
		           </div>
	  			</td>
	  		</tr>
  	  		<tr>
	  			<td class="tdstatico">
	  				<span class="tool glyphicon glyphicon-usd iconoesl" data-toggle="tooltip" data-placement="top" title="E-mail"></span>
	  			</td>
	  			<td>
	  			    Zona Periferica
	  			</td>
	  			<td>
	  			   <div class="form-inline">
		            <input name="periphery" style="width: 100%" type="text" placeholder="Costo Área Foranea" value="<?php echo isset($agre->periphery) ? $agre->periphery : ''; ?>" class="form-control system_data">
		           </div>
	  			</td>
	  		</tr>
  	  		<tr>
	  			<td class="tdstatico">
	  				<span class="tool glyphicon glyphicon-usd iconoesl" data-toggle="tooltip" data-placement="top" title="E-mail"></span>
	  			</td>
	  			<td>
	  			    Zona Foranea
	  			</td>
	  			<td>
	  			   <div class="form-inline">
		            <input name="foraneo" style="width: 100%" type="text" placeholder="Costo Área Periferica" value="<?php echo isset($agre->foraneo) ? $agre->foraneo : ''; ?>" class="form-control system_data">
		           </div>
	  			</td>
	  		</tr>
	  		<tr>
	  			<td class="tdstatico">
	  				<span class="tool glyphicon glyphicon-usd iconoesl" data-toggle="tooltip" data-placement="top" title="E-mail"></span>
	  			</td>
	  			<td>
	  			    Zona Restringida
	  			</td>
	  			<td>
	  			   <div class="form-inline">
		  		    <input name="restringida" style="width: 100%" type="text" placeholder="Costo Área Restringida" value="<?php echo isset($agre->restringida) ? $agre->restringida : ''; ?>" class="form-control system_data">
		           </div>
	  			</td>
	  		</tr>
	  		<tr>
	  		    <td>
	  		        <div class="form-inline">
	  		            Camioneta: 
	  		        </div>
	  		    </td>
	  			<td colspan='2'>
	  		        <div class="form-inline">
	  		            <input name="camioneta" style="width: 100%" type="text" placeholder="Costo por camioneta" value="<?php echo isset($agre->camioneta) ? $agre->camioneta : ''; ?>" class="form-control system_data">
	  		        </div>
	  		    </td>
	  		</tr>
	  		<tr>
	  			<td class="tdstatico">
	  			</td>
	  			<td colspan='2'>
	  			 <div class="form-inline">
		            <textarea name="description" type="text" placeholder="Descripción" class="form-control system_data"><?php echo isset($agre->description) ? $agre->description : ''; ?></textarea>
		         </div>              
	  		    </td>
	  		</tr>
	  		<tr>
	  		    <td>
	  		        <div class="form-inline">
	  		            Promoción Camioneta: 
	  		        </div>
	  		    </td>
	  			<td colspan='2'>
	  		        <div class="form-inline">
	  		            <select name="promo" style="width:49%;" class="form-control system_data">
		       			<?php
			  				$opciones=array(0,1);
							foreach($opciones as $opc){
								switch ($opc) {
									case 0:
										$tex="No";
										break;
									case 1:
										$tex="Si";
										break;
								}
				  					if($opc==$agre->promo){
				  						echo "<option value='".$opc."' selected>".$tex."</option>";
				  					}else{
				  						echo "<option value='".$opc."'>".$tex."</option>";
				  					}
				  				
							}
			  				?>
		       		</select>
	  		        </div>
	  		    </td>
	  		</tr>
		</tbody>
	</table> 
	</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-danger close-user" data-dismiss="modal">Cancelar</button>
			<button type="button" class="btn-usersave btn btn-primary">Guardar cambios</button>
		</div>
		</div>
	</div>