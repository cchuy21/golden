<!--se muestra los datos que tiene los convenios actuales que tiene la empresa -->
<div class="panel-default systemuser-block">
    <div class="panel-body" style="background: white; padding: 0px; height: 500px; overflow: auto;padding-left: 10px;">
    	<button class="btn btn-success tool system-add" data-typ="agreement" data-toggle='tooltip' data-placement='top' title='Agregar' style="float:right;">
 		   <span class="btn-address glyphicon glyphicon-plus" style="cursor: pointer;"><strong>Agregar</strong></span>
		</button>
		<!--muestra la tabla con todos los convenios que existen-->
    	<table class="table table-hover">
    		<tr>
    			<th>
    				#
    			</th>
    			<th>
    				Convenio
    			</th>
    			<th>
    				Aeropuerto
    			</th>
    			<th>
    				Inmediata
    			</th>
    			<th>
    				Intermedia
    			</th>
    			<th>
    				Periférico
    			</th>
    			<th>
    				Foráneo
    			</th>
    			<th>
    				Restringida
    			</th>
    			<th>
    				Camioneta
    			</th>
    			<th>
    				
    			</th>
    			
    		</tr>
    	<?php
    	//busca todos los acuerdos de manera ascendente
    	$systemuser=Agreement::find('all',array("order"=>"agreement asc"));
    	$cont=1;
		$class="spn-dire sa";
    	 foreach($systemuser as $sys){
    	 		$nombr="Sin Empresa";
    	 		$u=0;
    	 		//si el acuerdo esta habilitado
				 if($sys->enable!=0){
				//si tiene un negocio
						if($sys->business!=0)
						//se extrae el usuario relacionado
						$u=User::find($sys->business);
						//si tiene convenio, se visualiza el nombre de la empresa
						if($u)
							$nombr=$u->name;
						else 
							$nombr="Sin Empresa";
					//se muestra en pantalla los nombres y precios de cada convenio	
					//al final se muestra los botones para editar y borrar un convenio especifico
				 	echo "<tr>
    	 			<td>{$cont}</td> 
     	 			<td>".$sys->agreement."</td>
    	 			<td>".$sys->aeropuerto."</td>
    	 			<td>".$sys->metropolitan."</td>
    	 			<td>".$sys->intermedia."</td>
    	 			<td>".$sys->periphery."</td>
    	 			<td>".$sys->foraneo."</td>
     	 			<td>".$sys->restringida."</td>
    	 			<td>".$sys->camioneta."</td>
    	 			
    	 			<td>
    	 			<div class='sys' data-typ='agreement' data-system='".$sys->id."'><button type='button' data-aid='{$sys->id}' class='system-delete btn exsm addr btn-default btns-golden btn-sm tool' data-toggle='tooltip' data-placement='top' title='Borrar'><span class='glyphicon glyphicon-remove-circle'></span></button><button type='button' data-aid='{$sys->id}' class='system-edit btn exsm addr btn-default btns-golden btn-sm tool' data-toggle='tooltip' data-placement='top' title='Editar'><span class='glyphicon glyphicon-pencil'></span></button></div></td>";
	             	//se va enumerando los convenios
					$cont++; 
				
				}
			} 
		 
    	?>
    	</table>
    </div>
</div>
<div class="modal fade"id="systemuser-modal">
	
</div>
  