<?php

$d=templateDriver::getData("data");
$da=templateDriver::getData("da");

$ser=Service::find_by_user_id($d['data']);
$c=count($ser); //total de servicios 

?>
<div class="container" id="content">
<div class="panel panel-default" id="pnl-res">
	<div class="panel-heading top-bar container align-normal" style="padding: 0!important; border: 1px solid #ddd;">
	    <div id="app-breadcrumbs" class="panel-heading top-bar container align-normal">
	        <div id="cab">
	        	<div id="menu_user">
					<ul class="nav nav-tabs" id="tabsRes">
						<li class="active"><a href="#nueva" data-toggle="tab">Nueva Reservación</a></li>
						<?php if($da){
							
						}else{
							//echo '<li><a href="#editar" data-toggle="tab">Última Reservación</a></li>';
						}
						?>
						
					</ul>
				</div>
	    	</div>
    	</div>
    </div>
	<div class="panel-body">
		<div class="tab-content">
		  <div class="tab-pane active" id="nueva">
		  	<?php 
		  	$hoy=date("Y-m-d");
												
		  	if($da){
		  		templateDriver::setData("da",$da);
				}
				templateDriver::setData("user",$d['data']);

		  	templateDriver::renderSection("service.service"); ?>
		  </div>
		  <!--<div class="tab-pane" id="editar">
		  	<div id="ef">
		  		<?php 		  		
	  		/*	$reser=Reservation::find_by_id($ser->reservation2);
				if(!$reser){
		  			$reser=Reservation::find_by_id($ser->reservation1);
				}
			   $fecha=date("Y-m-d",strtotime($reser->reservation_date));
				$fech=strtotime($fecha);
				$hoy = strtotime($hoy);
		  	if($c==0 || $fech < $hoy){
		  		if($fech< $hoy){
					templateDriver::setData("id", array("id" => $d['data'] ));
					$ser=Service::find_by_user_id($d['data'],array('limit' => 1,'order' => 'id desc'));
		  			templateDriver::renderSection("service.service"); 
		  			//opciones de imprimir los tickets de las reservaciones
					if($ser->reservation1) 
						echo "<div class='col-md-12'><a target='_blank' href='http://resv.goldentransportaciones.net/assignment/imp/".$ser->reservation1."'><button style='float:left' class='btn btn-primary'>Imprimir Ticket</button></a>";
					if($ser->reservation2)
						echo "<a target='_blank' href='http://resv.goldentransportaciones.net/assignment/imp/".$ser->reservation2."'><button style='float:left; margin-left:20px;' class='btn btn-primary'>Imprimir Complemento</button></a></div>";		
					else
						echo "</div>";
		  		}else
		  		echo $p="aún no ha realizado ningún otro servicio valido o actual";	
				//si el servicio esta organizado para hoy
		  	}else if($fech==$hoy){
		  		echo "Se realizará el servicio hoy";
				templateDriver::setData("id", array("id" => $d['data'] ));
				//manda a llamar al archivo services y muestra los datos de la reservación
		  		templateDriver::renderSection("service.service"); 
		  	}else{
		  		templateDriver::setData("id", array("id" => $d['data'] ));
				
		  		templateDriver::renderSection("service.service"); 
				}
				*/?>
		  	</div>
		  </div>-->
		</div>
	</div>
</div>
</div>
<!--mensaje de cuando se crea una nueva reservación y datos relevantes-->
<div class="modal fade" id="serviceModal" tabindex="-1" role="dialog" aria-labelledby="serviceModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Creando servicio</h4>
      </div>
      <div class="modal-body">
          <p class="creating-service">Creando reservación</p>
          <ul class="list-group">
            <li class="list-group-item">
              <span class="badge bdg-stp1"><span class="glyphicon glyphicon-ok "></span></span>
              Generando reservaciones
            </li>
            <li class="list-group-item">
              <span class="badge bdg-stp2"><span class="glyphicon glyphicon-ok"></span></span>
              Notificando al usuario
            </li>
          </ul>
          <span class="service_text" style="display:none;">El servicio se ha generado con el siguiente folio:</span>
          <h3 class="service_id_container" style="display:none;"><span class="label label-default label-primary service_id"></span></h3>
      </div>
      <div class="modal-footer">
      	<div style="display: none; float: left" class="imprimir"></div>
		<a href="#" style="display: none;" type="button" class="btn btn-success finish-btn">Terminar</a>
		<a href="#" type="button" class="btn btn-danger btn-again">cerrar para intentar de nuevo</a>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
