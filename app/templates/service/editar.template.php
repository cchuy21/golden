<?php
//se utiliza para modificar los datos de una reservación ya registrada en la base de datos
 $d = templateDriver::getData('id');
$blo="";

    if(!$d) {
		$opciones='<option selected="selected" value="MTY-APTO">
						MTY-APTO
					</option>
					<option value="APTO-MTY">
						APTO-MTY
					</option>';
		$round='';
		$blo="bloqueada"; 
		$accion='Generar';
		$button='saveService"';
		$check='';
		$check2='';
		$cos='';
		
		
    } else {
    	$atendio=''; 
    	$round='';
		$accion='Editar';
		$button='updateService"';
		 $ser=Service::find_by_id($d['id']);

		$u = User::find_by_id($ser->user_id);
		
                	 $log=$ser->log;
                $lo=explode("||", $log);
				
                	 foreach($lo as $l){
                	 	$atendio.=$l."<br>";
                	 }
				
		$costo=$ser->cost; 
		if(substr($costo, 0,1)=="m"){
			$check='';
			$cos=substr($costo, 1);
			if($ser->tnueva > 0){
				$cos=(intval($cos))-$ser->tnueva;
			}
		}else{
			$check='checked';
			$cos='';
		}
		$cos_ima=0;
		if($ser->tnueva){
			$check2='checked';
			
			$cos_ima=$ser->tnueva;

		}
		if(!empty($ser->reservation1)){
			$serv="s";
			$reser1=Reservation::find_by_id($ser->reservation1);
			templateDriver::setData("reser1", array("id" => $reser1->id));
			if($reser1->type=="MTY-APTO"){
				$opciones='<option selected="selected" value="MTY-APTO">
						MTY-APTO
					</option>
					<option value="APTO-MTY">
						APTO-MTY
					</option>';
			}else{
				$opciones='<option value="MTY-APTO">
						MTY-APTO
					</option>
					<option selected="selected" value="APTO-MTY">
						APTO-MTY
					</option>';
			}
			if(!empty($ser->reservation2)){
				$reser2=Reservation::find_by_id($ser->reservation2);
				$serv="r";
				$round='checked';
				$blo="";
			}
			else 
				{
				$var='visibility: hidden';	//ADD
					
				}
				
			
			templateDriver::setData("typ", array("typ" => $serv));
		}
    }
    


?>

<div id="service">
	<table class="table table-bordered tb-reservacion">
		<tr>
			<td class="active pasajero" colspan="2">
				<strong class="text-serv">Datos de la reservación:</strong>
				
			</td>
		</tr>
		<tr>
			<td class="servictext">
				Servicio:
			</td>
			<td class="brleft">
				<select class="form-control"  id="service_type" style="width: 21%;">
					<?php echo $opciones;?>
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="2"> 
				 <div id="serviceInfo">
                    <input type="hidden" name="" value="">
                    <input type="hidden" name="" value="">
                    <input type="hidden" name="" value="">
                    <input type="hidden" name="" value="">
                </div>
				<div class="reservation" id="reservation1" rid="1">
					<input name="tnueva" id="tnuevat" type="hidden" class="inpama form-control reservation_field" value="<?php echo isset($ser->tnueva) ? $ser->tnueva : null ?>">
					<input name="cost" id="costt" type="hidden" class="inpama form-control reservation_field" value="<?php echo isset($ser->cost) ? $ser->cost : null ?>">
					<input name="user_id" id="s-user-id" type="hidden" class="inpama form-control reservation_field" value="<?php echo $d['id'];?>">
                    <input type="hidden" value="<?php echo isset($ser->payment) ? $ser->payment : null ?>" name="payment" class="reservation_field" />
					<input type="hidden" value="<?php echo isset($ser->especial) ? $ser->especial : 0 ?>" name="especial" id="hdespecial" class="reservation_field" />

					<?php
					$esp="";
					if($ser->especial){
						$esp="checked";
					}else{
						$esp="";
					}
			    	if(!empty($ser->reservation2)){
			    		$ti="red";
			    	}else{
			    		$ti="sen";
			    	}
					?>
			    	<input type="hidden" name="tipservice" value="<?php echo $ti;?>" class="reservation_field" id="valtip" />
                    
					<?php
					
					if($reser1->status!=1){
						echo '<div class="sobre"></div>';
					}
					?>
                    <?php 
                     templateDriver::setData("num", 1);
                     templateDriver::setData("user", $ser->user_id);
                     templateDriver::renderSection("service.form"); ?>
                    <input type="hidden" value="<?php echo isset($ser->payment) ? $ser->payment : null ?>" name="payment" class="reservation_field" />
                </div>
			</td>
        </tr>
        <tr>              	 
        	<td colspan="2">	
        		<div class="row round">
	                <div class="col-md-12">
	                    	<div class="checkbox">
							    <label>
							    	 <input id="roundtrip"type="checkbox" <?php echo $round;  ?> /> Servicio Redondo<!--muestra marcado-->
							    </label>
						  </div>
	                </div>
	           </div>
	           <br>	
	         	 <div class="reservation " id="reservation2" rid="2" style="<?php echo $var; echo $blo;?>"> <!--muestra bloqueado el echo-->
	                <?php 
	                
	                 if(!$d) {
	                 }else{
	                 	templateDriver::setData("reser1", array("id" => null));
		                if(!empty($ser->reservation2)){
		                	
		                	if($reser2->status)
							templateDriver::setData("reser2", array("id" => $reser2->id));
						}
					 }
 					 templateDriver::setData("num", 2);
					 
	                templateDriver::renderSection("service.form"); ?>
	            </div>   
            </td>   
        </tr>
        
        <tr style="margin-top: 1px;">
        	<td class="servictext" style="width: 13.5%;">
				Notas de la Reservación:
			</td>
        	<td class="brleft" style="padding-top: 2px; width: 76.5%;">
				<textarea style="width: 100%" name="annotations" id="user-annotations" class=" inpama form-control service_field"><?php echo isset($ser->annotations) ? $ser->annotations : '' ?></textarea>
				<input type="hidden" value="<?php echo isset($ser->id) ? $ser->id : '' ?>" name="id" class="service_field" >
        	</td>
        	</tr>        	
        	 <tr>
  				<td colspan="2">
        			<?php
        			templateDriver::renderSection("reservation.historialreservaciones");
				?>
        	</td>
        	</tr>	
       	
	</table>
	<table class="table table-bordered tb-tarifa">
		<tr>
			<td class="active pasajero" style="padding: 0" colspan="2">
				<strong>Tarifa</strong>
				<?php
				if($d){
					?>
					<div class="folio-res" style="float: right">
						<?php
						echo $ser->id;
						?>
					</div>
					<?php
				}
				
				?>
				
			</td>
		</tr>
        <tr>
            <td class="chan">Tarifa aplicada :</td>
            <td>
            	<p id="final_cost" class="margin0">0</p>
            	<input type="hidden" id="final-first" value="0" />
            	<input type="hidden" id="final-last" value="0" />
            </td>
        </tr>
        <tr class="gosthwhite">
            <td class="chan">Domicilios adicionales :</td>
            <td>
            	<p id="dom_cost" class="margin0">0</p>
            	<input type="hidden" id="dom-first" value="0" />
            	<input type="hidden" id="dom-last" value="0" />
            </td>
        </tr>
        <tr>
            <td class="chan" id="ben_des">
            	
            	<div class="form-group col-md-8">
                	<label style="font-weight: normal; cursor: pointer;">
					 <input type="checkbox" id="ben" style="margin-right: 5px;">Beneficio :
					
					</label>
					
				</div>
				<div class="col-md-4">
					 <select style='margin-left:5px; height: 26px;' class='selectben form-control'><option value="0"></option></select>
				</div>
			</td>
            <td id="ben_cost" class="margin0">0</td>
        </tr>
        <tr class="gosthwhite">
            <td class="backgray manual">
            	<div class="margin0 form-group">
                	<label style="font-weight: normal; cursor: pointer;">
					 <input type="checkbox" id="man" <?php echo $check;  ?> style="margin-right: 10px;">Ajuste Manual :
					</label>
				</div>
			</td>
            <td id="man_cos">
                <div class="form-group">
                    <input type="text" class="inpama form-control" id="ajus_man" style="display:none" disabled="" value="<?php echo $cos; ?>">
                </div>
            </td>
        </tr>
		<?php
		$dsp="display:none";
		if($check2=="checked"){
			$dsp="display:block";

		}else{
			$dsp="display:none";

		}
		echo '
		<tr>
			<td>
            	<div class="margin0 form-group">
                	<label style="font-weight: normal; cursor: pointer;">
					 <input type="checkbox" id="cost_mimagen" '.$check2.' style="margin-right: 10px;">Nueva Imagen :
					</label>
				</div>
			</td>
            <td id="ima_cos">
                <div class="form-group">
                    <input type="text" class="inpama form-control service_field" name="tnueva" id="ima_man" style="'.$dsp.'" value="'.$cos_ima.'">
                </div>
            </td>
		';
		?>
        <tr>
            <td class="chan">TOTAL :</td>
            <td>
            	<p id="total_cost" class="margin0">0</p>
            	<input type="hidden" id="total-first" value="0" />
            	<input type="hidden" id="total-last" value="0" />
                <input type="hidden" class="service_field" name="cost" id="total-manual" value="0" />
                <input type="hidden" id="des-first" value="0" />
            		<input type="hidden" id="des-last" value="0" />
            </td>
        </tr>
        <tr>	
        	<td colspan="2">
        		<div class="form-group margin0">
        			<center>Formas de pago:</center> 
                    <input type="text" value="<?php echo isset($ser->payment) ? $ser->payment : null ?>" name="payment" class="inpama service_field ejpagar form-control" placeholder="Forma de pago" />
                </div>
        	</td>
        </tr>
        <tr>
        	<td style="text-align: left" colspan="2">
        		<div class="checkbox">
					<label>
                		<input type="checkbox" id="leyenda" style="margin-right: 10px;" />Imprimir leyenda bajo riesgo del usuario   
                	</label>
                </div>         
        	</td>
        </tr>
        <tr>
        	<td colspan="2">
        		<div id="saveServiceDiv">
        			<button class="btn btn-success" id="<?php echo $button ?>>
        				<?php echo $accion ?> Reservación
    				</button>
    				<button class="btn btn-primary" id="remail">
        				Reenviar correo
    				</button> <br /> 
    				<?php
    				$r1=$ser->reservation1;
					$sr="http://resv.goldenmty.com/assignment/imp2/";
					$sr1=$sr.$r1;
                	echo '<br /><center><a href="'.$sr1.'" target="_blank" ><button class="btn btn-primary" id="">
        				Imprimir ticket
    				</button> </a></center><br />';
					if($ser->reservation2){
						$r2=$ser->reservation2;
					$sr2=$sr.$r2;
                	echo '<center><a href="'.$sr2.'" target="_blank" ><button class="btn btn-primary" id="">
        				Imprimir Complemento
    				</button> </a></center><br />';
					}
						echo $atendio;
					
    				?>
    			</div>
    			
        	</td>
        </tr>
		<tr>
			<td colspan='2'>
			<div class="checkbox" style="margin:0">
					<label>
                		<input type="checkbox" <?php echo $esp; ?> id="nespecial" style="margin-right: 10px;" /><b style='font-size: 18px;float: left;'>Notas Especiales</b>
                	</label>
                </div>   
			
			</td>
		</tr>
     </table>   
	
				
</div>