<?php

    $u = templateDriver::getData('user');
    $s = templateDriver::getData('ser');
    $r = templateDriver::getData('res');
    $abr="";  
    $dis="";  
    $h="";  
    $r1="";  
?>

<div class="row">
   	<div class="col-xs-2">
   	    Pasajeros:
   	</div>
   	<div class="col-xs-2">
   	    <input type="number" placeholder="1" min="1" max="15" value="1" name="passenger" class="form-control reservation_fl">
   	</div>
   	<div class="col-xs-6">
        <div class="input-group">
			<input type="hidden" value="<?php echo isset($r->reservation_date) ? $r->reservation_date : $abr; ?>" class="<?php echo $dis; ?> idate" />
            <input name="reservation_date<?php echo $h; ?>" type="text" value="<?php echo isset($r->reservation_date) ? $r->reservation_date : $abr; ?>" class="resdate inpama <?php echo $dis; ?> form-control res-date <?php echo $r1; ?>">
        	<span style="padding: 4px 10px;" class="input-group-addon tool" data-toggle="tooltip" data-placement="top" title="Fecha que se requiere el servicio"><span class="glyphicon glyphicon-calendar"></span></span>
    	</div>
   	</div>
   	<div class="col-xs-2" style="padding:0px">
   	    <span class="col-xs-6" style="padding:0px">a las:</span>
   	    <span class="col-xs-6" style="padding:0px"><input name="reservation_time" type="text" class="form-control masked-time reservation_fl" placeholder="Hora" value="00:00:00"  style="padding:0px" /></span>
   	</div>
</div>
