<?php
//esta la seccion de consulta no extrae nada del form todo lo genera y lo configura aqui
//al momento de consultar una reservación, muestra los datos correspondiente especificando quien lo creo y cuando asi como tambien la fecha de consulta si es el caso
 $d = templateDriver::getData('id');  
$ser=Service::find_by_id($d);  
$res=Reservation::find_by_id($ser->reservation1);  
$ter=0;
if($res->status==3){
	$ter++;
}
function di($re){
	$direccion=$re;
	$direc=explode(",", $direccion);
	for($i=0;$i<count($direc);$i++){
		if(substr($direc[$i],0,1)=="a"){
			$a=Address::find_by_id(substr($direc[$i],1));
			$sub=Suburb::find_by_id($a->suburb_id);
			$d=$a->alias.": ".$a->street.", ".$sub->suburb;
			break;
		}else{
			
		$sub=Suburb::find_by_id(substr($direc[$i],1));
			$d=$sub->suburb;
		}
	}
	return $d;
}		


$respad2=Reservation::find_by_id($ser->reservation2);
		if($respad2){
			$respad='margin-top: 37px;';
		}else{
			$respad='margin-top: -27px;';
		}
				
$dom=$res->addresses;
$domnum = explode(",", $dom);
$contdom1=count($domnum);
$contdom1=$contdom1-1;
$costodom1=$contdom1*200;
?>

<div id="servic">
<div class="row" style="margin: 0">
	<div class="col-md-9" style="padding: 0px;">
	<div class="col-md-12" style="padding: 0px;border: 1px #E4E4E4 solid;padding-bottom: 5px;">
		<table class="table consulta" style="margin-bottom: 3px!important;">
		<tr>
			<td colspan="8" class="active pasajero"  style="border-right: 1px #eee solid;">
				<strong class="text-serv">Datos de la reservación:</strong>
			</td>
		</tr>
		</table>
		
			<table class="table servic sol">
				<tbody>
					
					<tr>
						<td class="servictext" style="width: 128px;">
							Solicitó:
						</td>
						<td>
							<input name="requested" style="background-color: rgb(255, 255, 153)!important; font-weight:bold; width: 400px!important;  " type="text" id="disabledInput" class="form-control pasagocp form-controlo" placeholder="<?php echo strtoupper($ser->requested);?>" id="disabledInput" disabled>
						</td>
						
					</tr>
					
					<tr>
						<td class="servictext">
							Servicio:
						</td>
						<td>
							<input name="requested" style="background-color: rgb(255, 255, 153)!important; font-weight:bold;  " type="text" id="disabledInput" class="form-control pasagocp form-controlo" placeholder="<?php echo $res->type; ?>" id="disabledInput" disabled>
						</td>
						
						
					</tr>
				</table>
				<div class="col-md-4" style="padding: 0;">
				<table class="table servic sol">
					<tr>
						<td class="servictext" style="width: 50%;">
							Pasajeros:
						</td>
						<td>
							 <input type="text" style="background-color: rgb(255, 255, 153)!important; font-weight:bold;  " id="disabledInput" class="form-control pasagocp form-controlo" placeholder="<?php echo $res->passengers;?>" disabled>
						</td>
						<td>
						</td>
					</tr>
					<tr>
						<td class="servictext">
							Vehículo:
						</td>
						<td>
							 <input type="text" style="background-color: rgb(255, 255, 153)!important; font-weight:bold;  " class="form-control pasagocp form-controlo" placeholder="<?php
								if($res->vehicle_id==1){
									echo "Automóvil";
								}else{
									echo "camioneta";
								}
						 ?>"id="disabledInput" disabled>
						</td>
						<td>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-md-8" style="padding: 0;">
			<table class="table servic" style="<?php echo $respad; ?>">
				<tbody>
					<tr>
						<td colspan="4">
							<div class="col-md-12 ser-tex">
			 					<div></div>El servicio <span class="servicetype"><?php echo $res->type; ?></span> se requiere el dia:
							</div>
						</td>
						<td colspan="2">
							
						</td>
					</tr>
					<tr>
						<td  colspan="3">
							<div class="input-group">
								<?php
								if($res->abierto=="1" || $res->abierto){
                                	echo '<input style="background-color: rgb(255, 255, 153)!important; font-weight:bold;  " name="reservation_date" class="form-control" type="text" placeholder="abierto" id="disabledInput" disabled>';
								}else{
								?>
                                <input style="background-color: rgb(255, 255, 153)!important; font-weight:bold;  " name="reservation_date" class="form-control pasagocpc form-controlo" type="text" placeholder="<?php echo templateDriver::timelo($res->reservation_date,2); 	?>" id="disabledInput" disabled>
                                <?php
								}
                                ?>
	                        	<span class="input-group-addon tool" data-toggle="tooltip" data-placement="top" title="Fecha que se requiere el servicio" style="padding: 4px 12px!important;" id="disabledInput" disabled><span class="glyphicon glyphicon-calendar" id="disabledInput" disabled></span></span>
	                    	</div>
	                    </td>
	                    <td class="servictext" style="text-align: center">
	                    	a las: 
	                    </td>
	                    <td>
	                    	<div class="form-group">
	                    		<input style="background-color: rgb(255, 255, 153)!important; font-weight:bold;  " name="reservation_time" type="text"  class="form-control pasagocph form-controlo" placeholder="<?php	echo templateDriver::tim($res->reservation_time);	?>" id="disabledInput" disabled>				
	                		</div>
	                    </td>
	                     <td class="servictext">
	                    	<div class="hrs">hrs.</div>
	                    </td>
					</tr>
					<tr>
						<td class="dvuelo padocupr" style="width: 118px;">
							<div class="form-group">
			                    <input style="background-color: rgb(255, 255, 153)!important; font-weight:bold; " type="text"  class="form-control pasagocpd form-controlo " placeholder="<?php	echo User::find_by_id($res->airline_id)->name;?>" id="disabledInput" disabled>   
			                </div>          
						</td>
						<td class="dvuelo padocupl"style="width: 112px;">
							<div class="form-group">
			                    <input style="background-color: rgb(255, 255, 153)!important; font-weight:bold; " type="text"  class="form-control pasagocpd form-controlo " placeholder="<?php	echo $res->flight_type;?>" id="disabledInput" disabled>   
			                </div>
						</td>
						<td class="dvuelo"style="width: 86px;"> 
							 <div class="form-group">
			                    <input style="background-color: rgb(255, 255, 153)!important; font-weight:bold; " type="text"  class="form-control pasagocpp form-controlo" placeholder="<?php	echo $res->terminal;?>" id="disabledInput" disabled>   
			                </div>
						</td>
						<td>
							<input style="background-color: rgb(255, 255, 153)!important; font-weight:bold; " name="flight_number" class="form-control pasagocpp form-controlo" type="text"  placeholder="<?php	echo $res->flight_number;?>"  id="disabledInput" disabled>
						</td>
						<td>
							 <input style="background-color: rgb(255, 255, 153)!important; font-weight:bold; " type="text" class="form-control pasagocph form-controlo" name="flight_hour" placeholder="<?php echo templateDriver::tim($res->flight_hour);?>"  id="disabledInput" disabled>
						</td>
						<td class="servictext">
							 <div class="hrs">hrs.</div>
						</td>
					</tr>
					
				</tbody>
			</table>
		</div>
		<div class="col-md-12" style="padding-left: none;">
			<table class="table table-bordered dic">
				<tbody>
					<tr>
						<td class="servictext" style="width: 124px!important; vertical-align: top!important; padding-top: 4px;padding-left: 47px;">
							Dirección:
						</td>
						<td style="width: 81%;" colspan="2">
							<input type="text" class="form-control pasagocpdi form-controlo" name="address" placeholder="<?php echo di($res->addresses);?>" style="height: 26px; background-color: rgb(255, 255, 153)!important; font-weight:bold; " id="disabledInput" disabled>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		</div>
		<?php 
		$costodom2=0;
		$res=Reservation::find_by_id($ser->reservation2);
		if($res){
			if($res->status==3){
				$ter++;
			}
			$dom=$res->addresses;
			$domnum = explode(",", $dom);
			$contdom2=count($domnum);
			$contdom2=$contdom2-1;
			//para convertir el servicio a sencillo y despues restarle la diferencia
			$costodom2=$contdom2*200;
			
			if($res->status==0)
			echo "<br /><span style='font-weight:bold'>Convertido a sencillo en cobranza</span>";
		?>
	<div class="col-md-12"  style="padding: 0px;border: 1px #E4E4E4 solid;padding-bottom: 5px;">
		<div class="col-md-4" style="padding: 0;">
			<table class="table servic sol" style="margin-top: 0px!important;">
				<tbody>
					
					<tr>
						<td class="servictext">
							Solicitó:
						</td>
						<td>
							<input name="requested" type="text" id="disabledInput" style="background-color: rgb(255, 255, 153);" class="form-control pasagocp form-controlo" placeholder="<?php echo strtoupper($ser->requested);?>" id="disabledInput" disabled>
						</td>
					</tr>
					<tr>
						<td class="servictext" style="width: 50%;">
							Pasajeros:
						</td>
						<td>
							 <input type="text" id="disabledInput" style="background-color: rgb(255, 255, 153);" class="form-control pasagocp form-controlo" placeholder="<?php echo $res->passengers;?>" disabled>
						</td>
					</tr>
					<tr>
						<td class="servictext">
							Vehículo:
						</td>
						<td>
							 <input type="text" style="background-color: rgb(255, 255, 153);" class="form-control pasagocp form-controlo" placeholder="<?php
								if($res->vehicle_id==1){
									echo "Automóvil";
								}else{
									echo "camioneta";
								}
						 ?>"id="disabledInput" disabled>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-md-8" style="padding: 0;">
			<table class="table servic" style="margin-top: 5px;">
				<tbody>
					<tr>
						<td colspan="4">
							<div class="col-md-12 ser-tex">
			 					<div></div>El servicio <span class="servicetype"><?php echo $res->type; ?></span> se requiere el dia:
							</div>
						</td>
						<td colspan="2">
							
						</td>
					</tr>
					<tr>
						<td  colspan="3">
							<div class="input-group">
								<?php
								if($res->abierto=="1" || $res->abierto){
                                	echo '<input style="background-color: rgb(255, 255, 153);" name="reservation_date" class="form-control" type="text" placeholder="abierto" id="disabledInput" disabled>';
								}else{
								?>
                                <input name="reservation_date" class="form-control pasagocpc form-controlo" type="text" style="background-color: rgb(255, 255, 153);" placeholder="<?php echo templateDriver::timelo($res->reservation_date,2); 	?>" id="disabledInput" disabled>
	                        	<?php
								}
	                        	?>
	                        	<span class="input-group-addon tool" data-toggle="tooltip" data-placement="top" title="Fecha que se requiere el servicio" style="padding: 4px 12px!important;" id="disabledInput" disabled><span class="glyphicon glyphicon-calendar" id="disabledInput" disabled></span></span>
	                    	</div>
	                    </td>
	                    <td class="servictext" style="text-align: center">
	                    	a las: 
	                    </td>
	                    <td>
	                    	<div class="form-group">
	                    		<input name="reservation_time" type="text" style="background-color: rgb(255, 255, 153);" class="form-control pasagocph form-controlo" placeholder="<?php	echo templateDriver::tim($res->reservation_time);	?>" id="disabledInput" disabled>				
	                		</div>
	                    </td>
	                     <td class="servictext">
	                    	<div class="hrs">hrs.</div>
	                    </td>
					</tr>
					<tr>
						<td class="dvuelo padocupr" style="width: 118px;">
							<div class="form-group">
			                    <input type="text" style="background-color: rgb(255, 255, 153);" class="form-control pasagocpd form-controlo " placeholder="<?php	echo User::find_by_id($res->airline_id)->name;?>" id="disabledInput" disabled>   
			                </div>          
						</td>
						<td class="dvuelo padocupl"style="width: 112px;">
							<div class="form-group">
			                    <input type="text" style="background-color: rgb(255, 255, 153);" class="form-control pasagocpd form-controlo " placeholder="<?php	echo $res->flight_type;?>" id="disabledInput" disabled>   
			                </div>
						</td>
						<td class="dvuelo"style="width: 86px;"> 
							 <div class="form-group">
			                    <input type="text" style="background-color: rgb(255, 255, 153);" class="form-control pasagocpp form-controlo" placeholder="<?php	echo $res->terminal;?>" id="disabledInput" disabled>   
			                </div>
						</td>
						<td>
							<input name="flight_number" style="background-color: rgb(255, 255, 153);" class="form-control pasagocpp form-controlo" type="text"  placeholder="<?php	echo $res->flight_number;?>"  id="disabledInput" disabled>
						</td>
						<td>
							 <input type="text" style="background-color: rgb(255, 255, 153);" class="form-control pasagocph form-controlo" name="flight_hour" placeholder="<?php echo templateDriver::tim($res->flight_hour);?>"  id="disabledInput" disabled>
						</td>
						<td class="servictext">
							 <div class="hrs">hrs.</div>
						</td>
					</tr>
					
				</tbody>
			</table>
		</div>
		<div class="col-md-12" style="padding-left: none;">
			<table class="table table-bordered dic">
				<tbody>
					<tr>
						<td class="servictext" style="width: 124px!important; vertical-align: top!important; padding-top: 4px;padding-left: 47px;">
							Dirección:
						</td>
						<td style="width: 81%;">
							<input type="text" class="form-control pasagocpdi form-controlo" name="address" placeholder="<?php echo di($res->addresses);?>" style="height: 26px; background-color: rgb(255, 255, 153);" id="disabledInput" disabled>
			       
						</td>
						<td style="width: 6.6%; vertical-align: top; ">
							
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	
		</div>
		
		
	<?php
		}else{
			$res=Reservation::find_by_id($ser->reservation1);
			if($res->status==3){
				
	$ter=$ter+2;
}
			
		}
	$adddom=$costodom2+$costodom1;
	//tarifa final aplicada al servicio
	$taridaap=$ser->cost-$adddom;


	?>
		<div class="col-md-12" style="padding-left: none;">
			<table class="table table-bordered dic">
				<tbody>
					<tr>
						<td class="servictext" style="width: 124px!important; vertical-align: top!important; padding-top: 4px;padding-left: 47px;">
						Notas de la reservacion:
						</td>
						<td style="width: 81%;">
							<textarea class="form-controlo" rows="3" style="height: 42px!important;cursor: not-allowed;display: block;width: 100%;background: rgb(255,204,102)!important;" id="disabledInput" disabled><?php echo strtoupper($ser->annotations);?></textarea>
						</td>
						<td style="width: 6.6%; vertical-align: top; ">		
						</td>	
					</tr>
					 <tr>
		  				<td colspan="2" style="text-align: left!important;">
		        			<?php
		        			
		        			templateDriver::renderSection("reservation.historialreservaciones");
						?>
		        		</td>
		        	</tr>	
				</tbody>
			</table>
		</div>
	</div>
	
	<div class="col-md-3"  style="padding: 0px;">
			<table class="table table-bordered tb-tarifa" style="width: 100%!important;">
				<tbody>
		<tr>
			<td class="active pasajero" colspan="2">
				<strong>Tarifa</strong>
				<?php
				if($d){
					?>
					<div class="folio-res" style="float: right">
						<?php
						echo $ser->id;
						?>
					</div>
					<?php
				}
				
				?>
				
			</td>
		</tr>
        <tr>
            <td >Tarifa aplicada :</td>
            <td>
            	<p class="margin0" style="width: 70px;text-align: center;"><?php echo $taridaap;?></p>
            </td>
        </tr>
        <tr class="gosthwhite">
            <td >Domicilios adicionales :</td>
            <td>
            	<p class="margin0" style="width: 70px;text-align: center;" ><?php echo $adddom;?></p>
            </td>
        </tr>
        <tr>
            <td class="chan" id="ben_des">
            	
            	<div class="form-group col-md-8">
                	<label style="font-weight: normal; cursor: pointer;">
					 <input type="checkbox" id="ben" style="margin-right: 5px;">Beneficio :
					
					</label>
					
				</div>
				<div class="col-md-4">
					 <select style='margin-left:5px; height: 26px; background-color: rgb(255, 255, 153);' class='selectben form-control'><option value="0"></option></select>
				</div>
			</td>
            <td id="ben_cost" class="margin0">0</td>
        </tr>
        <?php
        
       if(substr($ser->cost, 0,1)=="m"){
       	$check="checked";
		$ser->cost=substr($ser->cost,1);
       }
        ?>
        <tr class="gosthwhite">
            <td class="backgray manual">
            	<div class="margin0 form-group">
                	<label style="font-weight: normal; cursor: pointer;">
					 <input type="checkbox" id="man" <?php echo $check;  ?> style="margin-right: 10px;">Ajuste Manual :
					</label>
				</div>
			</td>
            <td id="man_cos">
                <div class="form-group">
                    <input type="text" class="form-control" id="ajus_man" style="display:none;background-color: rgb(255, 255, 153);" disabled="" value="<?php echo $cos; ?>">
                </div>
            </td>
        </tr>
        <tr>
            <td class="chan">TOTAL :</td>
            <td>
            	<p id="total_cost" class="margin0" style="width: 70px;text-align: center;"><?php echo $ser->cost;?></p>
            </td>
        </tr>
        <tr>	
        	<td colspan="2">
        		<div class="form-group margin0">
        			<center>Formas de pago:</center> 
                    <input type="text" name="payment" style="width: 100%;background-color: rgb(255, 255, 153)!important; font-weight:bold;  " class="form-controlo" placeholder="<?php echo strtoupper($ser->payment);?>" id="disabledInput" disabled>   
                	<br /><?php
                	 $log=$ser->log;
                	$lo=explode("||", $log); 
				
                	 foreach($lo as $l){ //muestra quien creo, modifico, y que edito
                	 	echo $l."<br>";
                	 }
             	$estado=$res->status;
		
				$hoy=strtotime(date("d-m-Y")); 
				$fecha=strtotime($reservation_date);
				/*if($res->status=="2"||$res->status=="3"||$res->status=="5"){
					$as=Assignment::find_by_reservation_id($res->id);
					$op=Operator::find_by_id($as->operator_id);
					$un=Unity::find_by_id($as->unity_id);
					echo "<br />Asiganda a ".$op->name." ".$op->lastname." en la unidad: ".$un->economic;
				}
				if($respad2)
				if($respad2->status=="2" || $respad2->status=="3"|| $respad2->status=="5"){
					$as2=Assignment::find_by_reservation_id($respad->id);
					$op=Operator::find_by_id($as2->operator_id);
					$un=Unity::find_by_id($as2->unity_id);
					echo "<br />Asigando el complemento a ".$op->name." ".$op->lastname." en la unidad: ".$un->economic;
				}*/
				
				echo "<br />";
				
				if ($hoy>$fecha && $estado==0)
				{
					$tes=4;
				}	
			//se selecciona el estado de la reservación dependiendo su status 0-no se ha realizado 1-cuando solo se ha atendido una reservación de 2 2-cuando se realizo una reservación redonda 3-cuando se realizo el servicio 4- cuando el servicio no se realizo
        		switch ($ter) {
					case 0:
						echo "En elaboración"; //no se ha realizando el servicio
						break;
					case 1:
						echo "Atendida parcial"; //cuando existen viaje redondo 
						break;
					case 2:
						echo "Atendida completa"; //de un viaje redondo
						break;
					case 3:
						echo "Atendida completa"; //de un viaje sencillo 
						break;
					case 4:
						echo  "Tirado";
						break;	
				}
														
                	$r1=$ser->reservation1;
					$sr="http://resv.goldenmty.com/assignment/imp2/";
					$sr1=$sr.$r1;
                	echo '<br /><center><a href="'.$sr1.'" target="_blank" ><button class="btn btn-primary" id="">
        				Imprimir ticket
    				</button> </a></center><br />';
					if($ser->reservation2){
						$r2=$ser->reservation2;
					$sr2=$sr.$r2;
                	echo '<center><a href="'.$sr2.'" target="_blank" ><button class="btn btn-primary" id="">
        				Imprimir Complemento
    				</button> </a></center><br />';
					}
					echo '
					<center><button class="btn btn-primary" id="rmail">
        					Reenviar correo
    				</button></center>';
    				?>
    			
    				</div>
        	</td>
        </tr>
        </tbody>
        </table>
	</div>
</div>
</div>
</div>	