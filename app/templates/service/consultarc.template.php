<?php
$d = templateDriver::getData('id');
$ser=Canceled::find_by_id($d);
$res=Reservation::find_by_id($ser->reservation1);
$r2=Reservation::find_by_id($ser->reservation2);
$log=$ser->log;
function di($re){
	$direccion=$re;
	$direc=explode(",", $direccion);
	for($i=0;$i<count($direc);$i++){
		if(substr($direc[$i],0,1)=="a"){
			$a=Address::find_by_id(substr($direc[$i],1));
			$sub=Suburb::find_by_id($a->suburb_id);
			$d=$a->alias.": ".$a->street.", ".$sub->suburb;
			break;
		}else{
			$sub=Suburb::find_by_id(substr($direc[$i],1));
			$d=$sub->suburb;
		}
	}
	return $d;
}		



$respad=Reservation::find_by_id($ser->reservation2);
		if($respad){
			$respad='margin-top: 37px;';
		}else{
			$respad='margin-top: 47px;';
		}
		
		
$dom=$res->addresses;
$domnum = explode(",", $dom);
$contdom1=count($domnum);
$contdom1=$contdom1-1;
$costodom1=$contdom1*200;
?>

<div id="service">
<div class="row" style="margin: 0">
	<div class="col-md-9" style="padding: 0px;">
	<div class="col-md-12" style="padding: 0px;border: 1px #E4E4E4 solid;padding-bottom: 5px;">
		<table class="table consulta" style="margin-bottom: 3px!important;">
		<tr>
			<td colspan="8" class="active pasajero"  style="border-right: 1px #eee solid;">
				<strong class="text-serv">Datos de la reservación:</strong>
			</td>
		</tr>
		</table>
		<div class="col-md-4" style="padding: 0;">
			<table class="table servic sol">
				<tbody>
					
					<tr>
						<td class="servictext">
							Servicio:
						</td>
						<td>
							<input name="requested" type="text" id="disabledInput" class="form-control pasagocp form-controlo" placeholder="<?php echo $res->type; ?>" id="disabledInput" disabled>
						</td>
					</tr>
					<tr>
						<td class="servictext">
							Solicitó:
						</td>
						<td>
							<input name="requested" type="text" id="disabledInput" class="form-control pasagocp form-controlo" placeholder="<?php echo strtoupper($ser->requested);?>" id="disabledInput" disabled>
						</td>
					</tr>
					<tr>
						<td class="servictext" style="width: 50%;">
							Pasajeros:
						</td>
						<td>
							 <input type="text" id="disabledInput" class="form-control pasagocp form-controlo" placeholder="<?php echo $res->passengers;?>" disabled>
						</td>
					</tr>
					<tr>
						<td class="servictext">
							Vehículo:
						</td>
						<td>
							 <input type="text" class="form-control pasagocp form-controlo" placeholder="<?php
																	if($res->vehicle_id==1){
																		echo "Automóvil";
																	}else{
																		echo "camioneta";
																	}
															 ?>"id="disabledInput" disabled>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-md-8" style="padding: 0;">
			<table class="table servic" style="<?php echo $respad; ?>">
				<tbody>
					<tr>
						<td colspan="4">
							<div class="col-md-12 ser-tex">
			 					<div></div>El servicio <span class="servicetype"><?php echo $res->type; ?></span> se requiere el dia:
							</div>
						</td>
						<td colspan="2">
							
						</td>
					</tr>
					<tr>
						<td  colspan="3">
							<div class="input-group">
                                <input name="reservation_date" class="form-control pasagocpc form-controlo" type="text" placeholder="<?php echo templateDriver::timelo($res->reservation_date,2); 	?>" id="disabledInput" disabled>
	                        	<span class="input-group-addon tool" data-toggle="tooltip" data-placement="top" title="Fecha que se requiere el servicio" style="padding: 4px 12px!important;" id="disabledInput" disabled><span class="glyphicon glyphicon-calendar" id="disabledInput" disabled></span></span>
	                    	</div>
	                    </td>
	                    <td class="servictext" style="text-align: center">
	                    	a las: 
	                    </td>
	                    <td>
	                    	<div class="form-group">
	                    		<input name="reservation_time" type="text"  class="form-control pasagocph form-controlo" placeholder="<?php	echo templateDriver::tim($res->reservation_time);	?>" id="disabledInput" disabled>				
	                		</div>
	                    </td>
	                     <td class="servictext">
	                    	<div class="hrs">hrs.</div>
	                    </td>
					</tr>
					<tr>
						<td class="dvuelo padocupr" style="width: 118px;">
							<div class="form-group">
			                    <input type="text"  class="form-control pasagocpd form-controlo " placeholder="<?php	echo User::find_by_id($res->airline_id)->name;?>" id="disabledInput" disabled>   
			                </div>          
						</td>
						<td class="dvuelo padocupl"style="width: 112px;">
							<div class="form-group">
			                    <input type="text"  class="form-control pasagocpd form-controlo " placeholder="<?php	echo $res->flight_type;?>" id="disabledInput" disabled>   
			                </div>
						</td>
						<td class="dvuelo"style="width: 86px;"> 
							 <div class="form-group">
			                    <input type="text"  class="form-control pasagocpp form-controlo" placeholder="<?php	echo $res->terminal;?>" id="disabledInput" disabled>   
			                </div>
						</td>
						<td>
							<input name="flight_number" class="form-control pasagocpp form-controlo" type="text"  placeholder="<?php	echo $res->flight_number;?>"  id="disabledInput" disabled>
						</td>
						<td>
							 <input type="text" class="form-control pasagocph form-controlo" name="flight_hour" placeholder="<?php echo templateDriver::tim($res->flight_hour);?>"  id="disabledInput" disabled>
						</td>
						<td class="servictext">
							 <div class="hrs">hrs.</div>
						</td>
					</tr>
					
				</tbody>
			</table>
		</div>
		<div class="col-md-12" style="padding-left: none;">
			<table class="table table-bordered dic">
				<tbody>
					<tr>
						<td class="servictext" style="width: 124px!important; vertical-align: top!important; padding-top: 4px;padding-left: 47px;">
							Dirección:
						</td>
						<td style="width: 81%;" colspan="2">
							<input type="text" class="form-control pasagocpdi form-controlo" name="address" placeholder="<?php echo di($res->addresses);?>" style="height: 26px;" id="disabledInput" disabled>
			       
						</td>
						
					</tr>
				</tbody>
			</table>
		</div>
		</div>
		<?php 
		$costodom2=0;
		if($r2){
			$dom=$res2->addresses;
			$domnum = explode(",", $dom);
			$contdom2=count($domnum);
			$contdom2=$contdom2-1;
			$costodom2=$contdom2*200;
		?>
	<div class="col-md-12"  style="padding: 0px;border: 1px #E4E4E4 solid;padding-bottom: 5px;">
		<div class="col-md-4" style="padding: 0;">
			<table class="table servic sol" style="margin-top: 0px!important;">
				<tbody>
					<tr>
						<td class="servictext" style="width: 50%;">
							Pasajeros:
						</td>
						<td>
							 <input type="text" id="disabledInput" class="form-control pasagocp form-controlo" placeholder="<?php echo $res2->passengers;?>" disabled>
						</td>
					</tr>
					<tr>
						<td class="servictext">
							Vehículo:
						</td>
						<td>
							 <input type="text" class="form-control pasagocp form-controlo" placeholder="<?php
																	if($res2->vehicle_id==1){
																		echo "Automóvil";
																	}else{
																		echo "camioneta";
																	}
															 ?>"id="disabledInput" disabled>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-md-8" style="padding: 0;">
			<table class="table servic" style="margin-top: 5px;">
				<tbody>
					<tr>
						<td colspan="4">
							<div class="col-md-12 ser-tex">
			 					<div></div>El servicio <span class="servicetype"><?php echo $res2->type; ?></span> se requiere el dia:
							</div>
						</td>
						<td colspan="2">
							
						</td>
					</tr>
					<tr>
						<td  colspan="3">
							<div class="input-group">
                                <input name="reservation_date" class="form-control pasagocpc form-controlo" type="text" placeholder="<?php echo templateDriver::timelo($res2->reservation_date,2); 	?>" id="disabledInput" disabled>
	                        	<span class="input-group-addon tool" data-toggle="tooltip" data-placement="top" title="Fecha que se requiere el servicio" style="padding: 4px 12px!important;" id="disabledInput" disabled><span class="glyphicon glyphicon-calendar" id="disabledInput" disabled></span></span>
	                    	</div>
	                    </td>
	                    <td class="servictext" style="text-align: center">
	                    	a las: 
	                    </td>
	                    <td>
	                    	<div class="form-group">
	                    		<input name="reservation_time" type="text"  class="form-control pasagocph form-controlo" placeholder="<?php	echo templateDriver::tim($res2->reservation_time);	?>" id="disabledInput" disabled>				
	                		</div>
	                    </td>
	                     <td class="servictext">
	                    	<div class="hrs">hrs.</div>
	                    </td>
					</tr>
					<tr>
						<td class="dvuelo padocupr" style="width: 118px;">
							<div class="form-group">
			                    <input type="text"  class="form-control pasagocpd form-controlo " placeholder="<?php	echo User::find_by_id($res2->airline_id)->name;?>" id="disabledInput" disabled>   
			                </div>          
						</td>
						<td class="dvuelo padocupl"style="width: 112px;">
							<div class="form-group">
			                    <input type="text"  class="form-control pasagocpd form-controlo " placeholder="<?php	echo $res2->flight_type;?>" id="disabledInput" disabled>   
			                </div>
						</td>
						<td class="dvuelo"style="width: 86px;"> 
							 <div class="form-group">
			                    <input type="text"  class="form-control pasagocpp form-controlo" placeholder="<?php	echo $res2->terminal;?>" id="disabledInput" disabled>   
			                </div>
						</td>
						<td>
							<input name="flight_number" class="form-control pasagocpp form-controlo" type="text"  placeholder="<?php	echo $res2->flight_number;?>"  id="disabledInput" disabled>
						</td>
						<td>
							 <input type="text" class="form-control pasagocph form-controlo" name="flight_hour" placeholder="<?php echo templateDriver::tim($res2->flight_hour);?>"  id="disabledInput" disabled>
						</td>
						<td class="servictext">
							 <div class="hrs">hrs.</div>
						</td>
					</tr>
					
				</tbody>
			</table>
		</div>
		<div class="col-md-12" style="padding-left: none;">
			<table class="table table-bordered dic">
				<tbody>
					<tr>
						<td class="servictext" style="width: 124px!important; vertical-align: top!important; padding-top: 4px;padding-left: 47px;">
							Dirección:
						</td>
						<td style="width: 81%;">
							<input type="text" class="form-control pasagocpdi form-controlo" name="address" placeholder="<?php echo di($res2->addresses);?>" style="height: 26px;" id="disabledInput" disabled>
			       
						</td>
						<td style="width: 6.6%; vertical-align: top; ">
							
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	
		</div>
		
		
	<?php
		}
	$adddom=$costodom2+$costodom1;
	$taridaap=$ser->cost-$adddom;


	?>
		<div class="col-md-12" style="padding-left: none;">
			<table class="table table-bordered dic">
				<tbody>
					<tr>
						<td class="servictext" style="width: 124px!important; vertical-align: top!important; padding-top: 4px;padding-left: 47px;">
						Notas de la reservacion:
						</td>
						<td style="width: 81%;">
							<textarea class="form-controlo" rows="3" style="height: 42px!important;cursor: not-allowed;display: block;width: 100%;" id="disabledInput" disabled><?php echo strtoupper($ser->annotations);?></textarea>
						</td>
						<td style="width: 6.6%; vertical-align: top; ">
							
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	
	<div class="col-md-3"  style="padding: 0px;">
			<table class="table table-bordered tb-tarifa" style="width: 100%!important;">
				<tbody>
		<tr>
			<td class="active pasajero" colspan="2">
				<strong>Tarifa</strong>
				<?php
				if($d){
					?>
					<div class="folio-res" style="float: right">
						<?php
						echo $ser->id;
						?>
					</div>
					<?php
				}
				
				?>
				
			</td>
		</tr>
        <tr>
            <td >Tarifa aplicada :</td>
            <td>
            	<p class="margin0" style="width: 70px;text-align: center;"><?php echo $taridaap;?></p>
            </td>
        </tr>
        <tr class="gosthwhite">
            <td >Domicilios adicionales :</td>
            <td>
            	<p class="margin0" style="width: 70px;text-align: center;" ><?php echo $adddom;?></p>
            </td>
        </tr>
        <tr>
            <td class="chan" id="ben_des">
            	
            	<div class="form-group col-md-8">
                	<label style="font-weight: normal; cursor: pointer;">
					 <input type="checkbox" id="ben" style="margin-right: 5px;">Beneficio :
					
					</label>
					
				</div>
				<div class="col-md-4">
					 <select style='margin-left:5px; height: 26px;' class='selectben form-control'><option value="0"></option></select>
				</div>
			</td>
            <td id="ben_cost" class="margin0">0</td>
        </tr>
        <tr class="gosthwhite">
            <td class="backgray manual">
            	<div class="margin0 form-group">
                	<label style="font-weight: normal; cursor: pointer;">
					 <input type="checkbox" id="man" <?php echo $check;  ?> style="margin-right: 10px;">Ajuste Manual :
					</label>
				</div>
			</td>
            <td id="man_cos">
                <div class="form-group">
                    <input type="text" class="form-control" id="ajus_man" style="display:none" disabled="" value="<?php echo $cos; ?>">
                </div>
            </td>
        </tr>
        <tr>
            <td class="chan">TOTAL :</td>
            <td>
            	<p id="total_cost" class="margin0" style="width: 70px;text-align: center;"><?php echo $ser->cost;?></p>
            </td>
        </tr>
        <tr>	
        	<td colspan="2">
        		<div class="form-group margin0">
        			<center>Formas de pago:</center> 
                    <input type="text" name="payment" style="width: 100%;" class="form-controlo" placeholder="<?php echo strtoupper($ser->payment);?>" id="disabledInput" disabled>   
                </div>
        	</td>
        </tr>
        <tr>
        	<td colspan="2">
        		Solicitó cancelación: <?php echo strtoupper($ser->canceled_by." ".date("Y-m-d G:i",strtotime($ser->created_at)));?>
        		
        		<br /><?php
                	
                $lo=explode("||", $log);
				
                	 foreach($lo as $l){
                	 	echo $l."<br>";
                	 }
				
        		?>
        	</td>
        </tr>
        <tr>
        	<td>
        		<a href=<?php echo "http://resv.goldenmty.com/assignment/impc2/".$ser->reservation1 ?> target="_blank"><button class="btn btn-default">Imprimir</button></a>
        		<br />
        		<?php
        		if($ser->reservation2){
        			?>
        		<a href=<?php echo "http://resv.goldenmty.com/assignment/impc2/".$ser->reservation2 ?> target="_blank"><button class="btn btn-default">Imprimir Complemento</button></a>
        			<?php
        		}
        		?>
				<br />
				<button class="btn btn-primary envcan">Enviar Correo</button>

        	</td>
        </tr>
        </tbody>
        </table>
        
	</div>
</div>
</div>
</div>	