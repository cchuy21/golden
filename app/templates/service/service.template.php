<?php
//muestra los datos de una reservación, en la consulta
 $d = templateDriver::getData('id');
 $da = templateDriver::getData('da');
 $user = templateDriver::getData('user');
$blo="";

    if(!$d) {
		$opciones='<option selected="selected" value="MTY-APTO">
						MTY-APTO
					</option>
					<option value="APTO-MTY">
						APTO-MTY
					</option>';
		$round='';
		$blo="bloqueada";
		$accion='Generar';
		$button='saveService"';
		$check='';
		$cos='';
		$atendio="";
		if($da==42){
			 $opciones='<option selected="selected" value="MTY-APTO">
						MTY-APTO
					</option>';
		}else if($da==41){
			 $opciones='<option selected="selected" value="APTO-MTY">
						APTO-MTY
					</option>';
		}
    } else {
    	$round='';
    	$u = User::find_by_id($d['id']);
		$accion='Editar';
		$button='updateService"';
 		$ser=Service::find_by_user_id($u->id,array('limit' => 1,'order' => 'id desc'));
		$costo=$ser->cost;
		$atendio="Atendido por: ".$ser->attended;
		if(substr($costo, 0,1)=="m"){
			$check='';
			$cos=substr($costo, 1);
			if($ser->tnueva > 0){
				$cos=(intval($cos))-$ser->tnueva;
			}
		}else{
			$check='checked';
			$cos='';
		}
		$cos_ima=0;
		if($ser->tnueva){
			$check2='checked';
			
			$cos_ima=$ser->tnueva;

		}
		if(!empty($ser->reservation1) ){
			$serv="s";
			$reser1=Reservation::find_by_id($ser->reservation1);
			echo $reser1->id;

			templateDriver::setData("reser1", array("id" => $reser1->id));
			if($reser1->type=="MTY-APTO"){
				$opciones='<option selected="selected" value="MTY-APTO">
						MTY-APTO
					</option>
					<option value="APTO-MTY">
						APTO-MTY
					</option>';
			}else{
				$opciones='<option value="MTY-APTO">
						MTY-APTO
					</option>
					<option selected="selected" value="APTO-MTY">
						APTO-MTY
					</option>';
			}
			
			
		}
		if(!empty($ser->reservation2)){
				$reser2=Reservation::find_by_id($ser->reservation2);
				$serv="r";
				$round='checked';
				$blo="";
			}
			templateDriver::setData("typ", array("typ" => $serv));
    }
    
?>

<div id="service">
	<table class="table table-bordered tb-reservacion">
		<tr>
			<td class="active pasajero" colspan="2">
				<strong class="text-serv">Datos de la reservación:</strong>
				
			</td>
		</tr>
		<tr>
			<td class="servictext">
				Servicio:
			</td>
			<td class="brleft">
				<strong class="mnsj" style="color:red;font-size: 20px;text-align: right;"></strong>
				<select class="form-control"  id="service_type" style="width: 21%;">
					<?php echo $opciones;?>
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="2"> 
				 <div id="serviceInfo">
                    <input type="hidden" name="" value="">
                    <input type="hidden" name="" value="">
                    <input type="hidden" name="" value="">
                    <input type="hidden" name="" value="">
                </div>
				<div class="reservation" id="reservation1" rid="1">
					<input name="tnueva" id="tnuevat" type="hidden" class="inpama form-control reservation_field" value="<?php echo isset($ser->tnueva) ? $ser->tnueva : null ?>">
					<input name="cost" id="costt" type="hidden" class="inpama form-control reservation_field" value="<?php echo isset($ser->cost) ? $ser->cost : null ?>">
					<input name="user_id" id="s-user-id" type="hidden" class="inpama form-control reservation_field" value="<?php echo $d['id'];?>">
                    <input type="hidden" value="<?php echo isset($ser->payment) ? $ser->payment : null ?>" name="payment" class="reservation_field" />
                    <input type="hidden" value="<?php echo isset($ser->especial) ? $ser->especial : 0 ?>" name="especial" id="hdespecial" class="reservation_field" />
			    	<input type="hidden" name="tipservice" value="sen" class="reservation_field" id="valtip" />
                    <?php
					templateDriver::setData("num", 1);
					templateDriver::setData("idu", $user);

                     templateDriver::renderSection("service.form"); ?>
                </div>
			</td>
        </tr>
        <?php 
        if($da){
        	echo "<tr style='opacity:0;' >";
        }else{
        	echo "<tr>";
        }?>
        
        	<td colspan="2">
        		<div class="row round">
	                <div class="col-md-12">
	                    	<div class="checkbox">
							    <label>
							    	 <input id="roundtrip" <?php echo $round; ?> type="checkbox" /> Servicio Redondo
							    </label>
						  </div>
	                </div>
	           </div>
	            <div class="reservation <?php echo $blo; ?>" id="reservation2" rid="2">
	                <?php 
	                
	                 if(!$d) {
	                 	
	                 }else{
	                 	templateDriver::setData("reser1", array("id" => null));
		                if(!empty($ser->reservation2)){
							templateDriver::setData("reser2", array("id" => $reser2->id));
						}
					 }
					 templateDriver::setData("num", 2);
					 templateDriver::setData("idu", $user);

	                templateDriver::renderSection("service.form"); ?>
	            </div>
            </td>
        </tr>
        <tr style="margin-top: 1px;">
        	<td class="servictext" style="width: 13.5%;">
				Notas de la Reservación:
			</td>
        	<td class="brleft" style="padding-top: 2px; width: 76.5%;">
				<textarea style="width: 100%" name="annotations" id="user-annotations" class="inpama form-control service_field"><?php echo isset($ser->annotations) ? $ser->annotations : '' ?></textarea>
				<input type="hidden" value="<?php echo isset($ser->id) ? $ser->id : '' ?>" name="id" class="service_field" >
        	</td>
        </tr>
	</table>
	<table class="table table-bordered tb-tarifa">
		<tr>
			<td class="active pasajero" colspan="2">
				<strong>Tarifa</strong>
				<?php
				if($d){
					?>
					<div class="folio-res" style="float: right"> 
						<?php
						echo $ser->id;
						?>
					</div>
					<?php
				}
				
				?>
				
			</td>
		</tr>
        <tr>
            <td class="chan">Tarifa aplicada :</td>
            <td>
            	<p id="final_cost" class="margin0">0</p>
            	<input type="hidden" id="final-first" value="0" />
            	<input type="hidden" id="final-last" value="0" />
            </td>
        </tr>
        <tr class="gosthwhite">
            <td class="chan">Domicilios adicionales :</td>
            <td>
            	<p id="dom_cost" class="margin0">0</p>
            	<input type="hidden" id="dom-first" value="0" />
            	<input type="hidden" id="dom-last" value="0" />
            </td>
        </tr>
        <tr>
            <td class="chan" id="ben_des">
            	
            	<div class="form-group col-md-8">
                	<label style="font-weight: normal; cursor: pointer;">
					 <input type="checkbox" id="ben" style="margin-right: 5px;">Beneficio :
					
					</label>
					
				</div>
				<div class="col-md-4">
					 <select style='margin-left:5px; height: 26px;' class='selectben form-control'><option value="0"></option></select>
				</div>
			</td>
            <td id="ben_cost" class="margin0">0</td>
        </tr>
        <tr class="gosthwhite">
            <td class="backgray manual">
            	<div class="margin0 form-group">
                	<label style="font-weight: normal; cursor: pointer;">
					 <input type="checkbox" id="man" <?php echo $check;  ?> style="margin-right: 10px;">Ajuste Manual :
					</label>
				</div>
			</td>
            <td id="man_cos">
                <div class="form-group">
                    <input type="text" class="inpama form-control" id="ajus_man" style="display:none" disabled="" value="<?php echo $cos; ?>">
                	<input type="hidden" id="des-first" value="0" />
            		<input type="hidden" id="des-last" value="0" />
                </div>
            </td>
        </tr>
		<?php
		echo '
		<tr>
			<td>
            	<div class="margin0 form-group">
                	<label style="font-weight: normal; cursor: pointer;">
					 <input type="checkbox" id="cost_mimagen" '.$check2.' style="margin-right: 10px;">Nueva Imagen :
					</label>
				</div>
			</td>
            <td id="ima_cos">
                <div class="form-group">
                    <input type="text" class="inpama form-control service_field" name="tnueva" id="ima_man" style="display:none" value="'.$cos_ima.'">
                </div>
            </td>
		';
		?>
        <tr>
            <td class="chan">TOTAL :</td>
            <td>
            	<p id="total_cost" class="margin0">0</p>
            	<input type="hidden" id="total-first" value="0" />
            	<input type="hidden" id="total-last" value="0" />
                <input type="hidden" class="service_field" name="cost" id="total-manual" value="0" />
            </td>
        </tr>
        <tr>	
        	<td colspan="2">
        		<div class="form-group margin0">
        			<center>Formas de pago:</center> 
                     <input type="text" value="<?php echo isset($ser->payment) ? $ser->payment : null ?>" name="payment" class="inpama service_field ejpagar form-control" placeholder="Forma de pago" />
                </div>
        	</td>
        </tr>
        <tr>
        	<td style="text-align: left" colspan="2">
        		<div class="checkbox">
					<label>
                		<input type="checkbox" id="leyenda" style="margin-right: 10px;" />Imprimir leyenda bajo riesgo del usuario   
                	</label>
                </div>         
        	</td>
        </tr>
        <tr>
        	<td colspan="2">
        		<div id="saveServiceDiv">
        			<button class="btn btn-success" id="<?php echo $button ?>>
        				<?php echo $accion ?> Reservación
    				</button>
    				<?php
    				echo $atendio;
    				?>
    			</div>
        	</td>
		</tr>
		<tr>
		<td colspan='2'>
				<div class="checkbox" style="margin:0">
					<label>
                		<input type="checkbox" id="nespecial" style="margin-right: 10px;" /><b style='font-size: 18px;float: left;'>Notas Especiales</b>
                	</label>
                </div>   
			
			</td>
			
		</tr>
		
	</table>
	<table class="table table-bordered">
		<tr>
			<td>

			</td>
		</tr>
	</table>
</div>