 <?php
 //muestra los datos que se requieren para una reservación con los datos de su servicio
 		$disa = templateDriver::getData('dis');
   		$ty = templateDriver::getData('typ');
		$d2 = templateDriver::getData('reser2');
		$d1 = templateDriver::getData('reser1');
		$h = templateDriver::getData('num');
		$isiser = templateDriver::getData('user');

	    $openVuelta = "";
	  if($d1 && $d1['id']!==null){
    	
    	$reser=Reservation::find_by_id($d1['id']);
		$ser=Service::find_by_reservation1($d1['id']);
		$isiser=$ser->user_id;
		//print_r($ser);
		//si el servicio se selecciona como abierto
		if($reser->abierto==1) 
			$chec="checked";
		else {
			$chec="";
		}
		$r="reservation_field";
		$ids=$reser->id;
        $abr="";	
	}else 
    if($d2) { 	
    	$reser=Reservation::find_by_id($d2['id']);
        if($reser->abierto==1){
			$chec="checked";
            $abr="Abierto";
        }
		else {
            $abr="";
			$chec="";
		}
		
		$ser=Service::find_by_reservation2($d2['id']);
		$isiser=$ser->user_id;

		$r="reservation_field";
		$ids=$reser->id;
		$ch="checked";
    }
	
	else{
		//seleccionas los datos de la reservación
			$r="reservation_field";
        	$ids="";
        	$address='';
    	    $opveh='<option value="1">Automóvil</option>
				    <option value="2">Camioneta</option>';
		    $opnac='<option selected="selected" value="null">Nac./int.</option>
	   			    <option value="Nacional">Nacional</option>
	    		    <option value="Internacional">Internacional</option>';
			$opter='<option>Terminal</option>
			        <option value="A">A</option>
			        <option value="B">B</option>
			        <option value="C">C</option>'; 
    }
    
    if($d1['id']!==null || $d2){
		$address='';
		//print_r($ad);
		$veh=$reser->vehicle_id;
		//selecciona el tipo de vehiculo del servicio, si es 1 es automóvil
		if($veh==1){
			$opveh='<option selected="selected" value="1">Automóvil</option>
					<option value="2">Camioneta</option>';
		}else{
			$opveh='<option value="1">Automóvil</option>
					<option selected="selected" value="2">Camioneta</option>';
		}
		$nac=$reser->flight_type;
		//determina si el tipo de vuelo 
		if($nac=='Internacional'){
			$opnac='<option>Nac./int.</option>
			        <option value="Nacional">Nacional</option>
			        <option selected="selected" value="Internacional">Internacional</option>';
		}else if($nac=='Nacional'){
			$opnac='<option>Nac./int.</option>
			        <option selected="selected" value="Nacional">Nacional</option>
			        <option value="Internacional">Internacional</option>';
		}else{
			$opnac='<option>Nac./int.</option>
			        <option selected="selected" value="Nacional">Nacional</option>
			        <option value="Internacional">Internacional</option>';
		}
		$ter=$reser->terminal;
		//selecciona la terminal a donde va el servicio
		if(empty($ter) || $ter=="null" || !$ter){
			$opter='<option>Terminal</option>
			        <option value="A">A</option>
			        <option value="B">B</option>
			        <option value="C">C</option>';
		}else{
			$opter='<option value="'.$ter.'">'.$ter.'</option>';
		}
		//separa las direcciones del servicio por medio de una coma
		$di_id=explode(',',$reser->addresses);
		
		$con=0;
		$iddir="";
		//datos de la dirección
		
		foreach ($di_id as $dirid) {
			if(substr($dirid, 0,1)=="a"){
				$dir=Address::find_by_id(substr($dirid, 1));
				$subur=Suburb::find_by_id($dir->suburb_id);
				$iddir="a".$dir->id;
				
				if($con==0){
					if(strlen($dir->alias.': '.$dir->street.'- '.$subur->suburb)>63){
						$punt="...";
					}else{
						$punt="";
					}
					$dire=substr($dir->alias.': '.$dir->street.'- '.$subur->suburb,0,63);
					$address=$address.$dire.$punt;
					
				}else{
					if(strlen($dir->alias.': '.$dir->street.'- '.$subur->suburb)>70){
						$punt="...";
						$dire=substr($dir->alias.': '.$dir->street.'- '.$subur->suburb,0,63);
					}else{
						$punt="";
						$dire=$dir->alias.': '.$dir->street.'- '.$subur->suburb;
					}
					
					$address=$address."&%$".$dire.$punt;
				}
				
			}else if(substr($dirid, 0,1)=="s"){
				$subur=Suburb::find_by_id(substr($dirid, 1));
				if($con==0)
					$address=$address.$subur->suburb;
				else
					$address=$address."&%$".$subur->suburb;
			}
			$con++;
		}
    }

if($disa){
	$dis="disa";
}else{
	$dis="";
}

$us=User::find($isiser);
$ad=Address::all(array("conditions"=>"user_id='".$us->id."' and status=1"));

?>

<div class="row" style="margin: 0">
	<div class="col-md-12">
		<div class="col-md-4" style="padding: 0;">
			<table class="table servic sol">
				<tbody>
					<tr class="solicito">
						<td class="servictext">
							Solicitó: <!--quien solicito el servicio-->
						</td>
						<td>
							<?php
							if($h==1){
								?>
								<input name="annotations" type="hidden" id="hidannotations" class="inpama form-control reservation_field" value="<?php echo isset($ser->annotations) ? $ser->annotations : '' ?>" <?php echo $dis; ?>>
								<input name="service_id" type="hidden" class="inpama form-control reservation_field" data-val="<?php echo isset($ser->id) ? $ser->id : '' ?>" value="<?php echo isset($ser->id) ? $ser->id : '' ?>" <?php echo $dis; ?>>
								<input name="requested" type="text" class="inpama form-control reservation_field solicitante" data-val="<?php echo isset($ser->requested) ? $ser->requested : 'El mismo' ?>" value="<?php echo isset($ser->requested) ? $ser->requested : 'El mismo' ?>" placeholder="El mismo" <?php echo $dis; ?>>
							<?php
						}
						?>
						</td>
					</tr>
					<tr>
						<td class="servictext" style="width: 40%;">
							Pasajeros: <!--numero de pasajeros que van en el servicio-->
						</td>
						<td>
                             <input name="id<?php echo $h; ?>" type="hidden" value="<?php echo $ids ?>" class="<?php echo $r ?>">
							 <input type="text" placeholder="1" data-val="<?php echo isset($reser->passengers) ? $reser->passengers : '1' ?>" value="<?php echo isset($reser->passengers) ? $reser->passengers : '1' ?>" name="passengers<?php echo $h; ?>" class="inpama <?php echo $dis; ?> form-control passenger <?php echo $r ?>">
						</td>
					</tr>
					<tr>
						<td class="servictext">
							Vehículo: <!--vehiculo del servicio-->
						</td>
						<td>
							 <select type="text" name="vehicle_id<?php echo $h; ?>" class="inpama <?php echo $dis; ?> form-control <?php echo $r ?> id_vehicle" value="">
							 	<?php echo $opveh; ?>
							 </select>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-md-8" style="padding: 0;">
			<table class="table servic">
				<tbody>
					<tr>
						<td colspan="4">
							<div class="col-md-12 ser-tex">
			 					<div></div>El servicio <span class="servicetype">{service-type}</span> se requiere el dia:
			 					<input type="hidden" name="type<?php echo $h; ?>" class=<?php echo $dis; ?> "<?php echo $r ?> res_type" value="">
							</div>
						</td>
						<td colspan="2">
							<div class="checkbox abr">
						    <label>
						      <input type="checkbox" name="open" <?php echo $chec ?> style="height: 15px!important;" class="<?php echo $dis; ?> abierto" /> Abierto
						    </label>
						  </div>
						</td>
					</tr>
					<tr>
						<td class="calendar" colspan="3">
							<div class="input-group">
								<input type="hidden" value="<?php echo isset($reser->reservation_date) ? $reser->reservation_date : $abr; ?>" class="<?php echo $dis; ?> idate" />
                                <input name="reservation_date<?php echo $h; ?>" type="text" value="<?php echo isset($reser->reservation_date) ? $reser->reservation_date : $abr; ?>" class="resdate inpama <?php echo $dis; ?> form-control res-date <?php echo $r; ?>">
	                        	<span class="input-group-addon tool" data-toggle="tooltip" data-placement="top" title="Fecha que se requiere el servicio"><span class="glyphicon glyphicon-calendar"></span></span>
	                    	</div>
	                    </td>
	                    <td class="servictext">
	                    	a las: 
	                    </td>
	                    <td>
	                    	<div class="form-group">
	                    		<?php
	                    		if($disa){
	                    			?>
	                    		<input type="text" class="inpama <?php echo $dis; ?> form-control masked-time" placeholder="Hora" value="<?php echo isset($reser->reservation_time) ? $reser->reservation_time : '' ?>">
	                    			<?php
	                    		}else{
									
	                    		?> <!--tiempo de la reservación-->
	                    		<input name="reservation_time<?php echo $h; ?>" type="text" class="restime inpama form-control masked-time <?php echo $r ?>" placeholder="Hora" value="<?php echo isset($reser->reservation_time) ? $reser->reservation_time : '' ?>">
						<?php }
	                    		?>
	                		</div>
	                    </td>
	                     <td class="servictext">
	                    	<div class="hrs">hrs.</div>
	                    </td>
					</tr>
					<tr>
						<td class="dvuelo" style="width: 118px;">
							<div class="form-group">
			                    <select name="airline_id<?php echo $h; ?>" class="resair inpama <?php echo $dis; ?> form-control1 <?php echo $r ?>">
			                        <option value="null">Aerolinea</option>
			                        <?php
			                            $aerolines = User::all(array('order'=>'name asc','conditions' => 'user_type = 2'));
			                            foreach ($aerolines as $al){
			                            	if($d1['id']!==null || $d2){
			                            		
			                            		if($reser->airline_id==$al->id)
			                             	    echo "<option selected='selected' value='{$al->id}'>{$al->name}</option>";
												else
													echo "<option value='{$al->id}'>{$al->name}</option>";
											}else{
												echo "<option value='{$al->id}'>{$al->name}</option>";
											}
											
			                            }
			                        ?>
			                    </select>
			                    
			                </div>
						</td>
						<td class="dvuelo"style="width: 112px;">
							<div class="form-group">
			                    <select name="flight_type<?php echo $h; ?>" class="resftype inpama <?php echo $dis; ?> form-control1 <?php echo $r ?>">
			                        <?php echo $opnac; ?>
			                    </select>
			                    <input type='hidden' value='<?php echo $nac ?>' class='hiddennacional' >

			                </div>
						</td>
						<td class="dvuelo"style="width: 86px;"> 
							 <div class="form-group">
			                    <select name="terminal<?php echo $h; ?>" class="resfterminal inpama <?php echo $dis; ?> form-control1 <?php echo $r ?>">
			                        <?php echo $opter; ?>
			                    </select>
			                    <input type='hidden' value='<?php echo $ter ?>' class='hiddenterminal' >
			                </div>
						</td>
						<td>
							<input name="flight_number<?php echo $h; ?>" type="text" class="resfnum inpama <?php echo $dis; ?> form-control <?php echo $r ?>" placeholder="Vuelo" value="<?php echo isset($reser->flight_number) ? $reser->flight_number : '' ?>">
						</td>
						<td>
							 <input type="text" name="flight_hour<?php echo $h; ?>" class="resfhour inpama <?php echo $dis; ?> form-control masked-time <?php echo $r ?>" placeholder="Hora" value="<?php echo isset($reser->flight_hour) ? $reser->flight_hour : '' ?>">
						</td>
						<td class="servictext">
							 <div class="hrs">hrs.</div>
						</td>
					</tr>
					
				</tbody>
			</table>
		</div>
		<div class="col-md-12">
			<table class="table table-bordered dic">
				<tbody>
					<tr>
						<td class="servictext" style="width: 12.1%; vertical-align: top!important; padding-top: 4px;">
							Dirección:
						</td>
						<td style="width: 81%;">
							<input type="text" name="address" class="inpama <?php echo $dis; ?> form-control add-ac" value="" placeholder="Dirección" style="height: 26px;">
			                <input type="hidden" class="<?php echo $dis; ?> add-id <?php echo $r ?>" value="<?php echo $iddir ?>" name="addresses<?php echo $h; ?>" >
			                <input type="hidden" class="<?php echo $dis; ?> addva" value="<?php echo str_replace(",", "-", $address); ?>" >
			                <input type="hidden" class="<?php echo $dis; ?> dirhidden" value="" />
							<?php
						    /*    
						    }else{
						        ?>
							<select class="form_control inpama seldireccion <?php echo $r; ?>" name="addresses<?php echo $h; ?>" style="height: 26px!important;width: 100%;">
							<option value=0></option>
										<?php
										foreach($ad as $a){
											$sel="";
											if($d1['id']!==null || $d2){
												if($a->id==$iddir){
													$sel="selected";
												}
											}
											$subur=Suburb::find_by_id($a->suburb_id);
											echo "<option ".$sel." value='".$a->id."'>".$a->alias.': '.$a->street.'- '.$subur->suburb."</option>";
										}
										?>
							</select>
							<?php
							}*/
							?>
						</td>
						<td style="width: 6.6%; vertical-align: top; ">
							<button class="<?php echo $dis; ?> btn btn-default btn-xs add-di tool" style="padding: 5px 5px 7px 7px!important; display:none;" data-toggle="tooltip" data-placement="right" title="Agregar dirección adicional">
							    <span class="pull-right glyphicon glyphicon-plus" style="cursor: pointer;"></span>
						     </button>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<input type="hidden" class="<?php echo $dis; ?> cost-reservation <?php echo $r ?>" name="cost<?php echo $h; ?>">