<?php

	function direccion($direccion){
		//extrae el número de direcciones 
		$direc=explode(",", $direccion);
		for($i=0;$i<count($direc);$i++){
			if(substr($direc[$i],0,1)=="a"){
				$ad=Address::find_by_id(substr($direc[$i],1));
				$col=Suburb::find_by_id($ad->suburb_id);
				return '<span class="tool" data-toggle="tooltip" data-placement="top" title="'.$ad->street.', '.$col->suburb.'">'.$ad->alias.'</span>';
				break;
			}
		}
	}
	function vehiculo($veh){ //determina el tipo de vehiculo del servicio
		if($veh==1)	$vehi="Automovil";else if($veh==2) $vehi="Camioneta";
		return $vehi;
	}
	function aerolinea($aer){ //nombre de la aerolina que se solicita
		if(isset($aer)){
		$aero=User::find_by_id($aer);
		return $aero->name;
		}
	}
	$u = User::find_by_id(templateDriver::getData("window"));
	
	if($u->id==7542)
		$ser=Service::all(array('conditions' => 'user_id = '.$u->id,"limit"=>100,"order"=>"id desc"));
	else
		$ser=Service::all(array('conditions' => 'user_id = '.$u->id,"limit"=>100, "order"=>"id desc"));
	
	
	//$contador=1;
	//datos del servicio que se realiza
	echo '<table class="table table-hover"><tbody>
	   <tr>
	    	<td>#</td>
			<td>Folio</td>
			<td>Fecha</td>
			<td>Hora</td>
			<td>Dirección</td>
			<td>Pasajeros</td>
			<td>Tipo de Unidad</td>
			<td>Unidad</td>
			<td>Chofer</td>
			<td>Costo</td>
		</tr>
	  </div>
	</nav>';
	$contador=1;
	foreach($ser as $s){
		$showc=0;
		$test=0;
		if(!empty($s->reservation1)){
			$r1=Reservation::find_by_id($s->reservation1);
			if($u->id==7542){
				
				if($r1->abierto){
					$fecg="Abierto";
				}else{
					$fecg=templateDriver::timelo($r1->reservation_date,3);
				}
				$as = Assignment::find("all",array('conditions' => 'reservation_id="'.$r1->id.'"'));
				
				echo '<tr><td>'.$contador.'</td>
				<td>'.$s->id.'</td>
				<td>'.$fecg.'</td>
				<td>'.substr($r1->reservation_time,0,5).'</td>
				<td>'.direccion($r1->addresses).'</td>
				<td>'.$r1->passengers.'</td>';
				if($r1->vehicle_id == 1){
				    echo '<td>auto</td>';
				}else{
				    echo '<td>camioneta</td>';
				}
				
				if(count($as)){
				    $unit = Unity::find_by_id($as[0]->unity_id);
				    $oper = Operator::find_by_id($as[0]->operator_id);
    				echo '<td>'.$unit->economic.'</td>';
    				echo '<td>'.$oper->username.'</td>';
				}else {
				 echo '<td>N/A</td><td>N/A</td>';
				}
				if(substr($s->cost,0,1)=="m"){
			        echo '<td>'.substr($s->cost,1).'</td></tr>';
			    }else
			        echo '<td>'.$s->cost.'</td></tr>';
				$showc++;	
				$contador++;
				$test=1;
			}else{
				
				if($r1->abierto){
					$fecg="Abierto";
				}else{
					$fecg=templateDriver::timelo($r1->reservation_date,3);
				}		
				$as = Assignment::find("all",array('conditions' => 'reservation_id="'.$r1->id.'"'));
				
				echo '<tr><td>'.$contador.'</td>
				<td>'.$s->id.'</td>
				<td>'.$fecg.'</td>
				<td>'.substr($r1->reservation_time,0,5).'</td>
				<td>'.direccion($r1->addresses).'</td>
				<td>'.$r1->passengers.'</td>';
				if($r1->vehicle_id == 1){
				    echo '<td>auto</td>';
				}else{
				    echo '<td>camioneta</td>';
				}
				if(count($as)){
				    
				    $unit = Unity::find_by_id($as[0]->unity_id);
				    $oper = Operator::find_by_id($as[0]->operator_id);
    				echo '<td>'.$unit->economic.'</td>';
    				echo '<td>'.$oper->username.'</td>';
				}else {
				 echo '<td></td><td></td>';
				}
				if(substr($s->cost,0,1)=="m"){
			       echo '<td>'.substr($s->cost,1).'</td></tr>';
			    } else
			        echo '<td>'.$s->cost.'</td></tr>';
				$showc++;	
				$contador++;
			}	
		}
		if(!empty($s->reservation2)){
					$r2=Reservation::find_by_id($s->reservation2);
			if($u->id==7542){
				if($test){
					if($r2->abierto){
						$fecg="Abierto";
					}else{
						$fecg=templateDriver::timelo($r2->reservation_date,3);
					}
					$as = Assignment::find("all",array('conditions' => 'reservation_id="'.$r2->id.'"'));
				
				    echo '<tr><td>'.$contador.'</td>
    				<td>'.$s->id.'</td>
    				<td>'.$fecg.'</td>
    				<td>'.substr($r2->reservation_time,0,5).'</td>
    				<td>'.direccion($r2->addresses).'</td>
    				<td>'.$r2->passengers.'</td>';
					if($r2->vehicle_id == 1){
				        echo '<td>auto</td>';
			        }else{
				        echo '<td>camioneta</td>';
				    }
    				if(count($as)){
    				    $unit = Unity::find_by_id($as[0]->unity_id);
    				    $oper = Operator::find_by_id($as[0]->operator_id);
        				echo '<td>'.$unit->economic.'</td>';
        				echo '<td>'.$oper->username.'</td>';
    				}else {
    				 echo '<td>N/A</td><td>N/A</td>';
    				}
					if($showc)
					echo '<td>0</td></tr>';
					else{
    				    if(substr($s->cost,0,1)=="m"){
    				       echo '<td>'.substr($s->cost,1).'</td></tr>';
    				    }else
    				    echo '<td>'.$s->cost.'</td></tr>';
					}
					$contador++;
				}
			}else{
			
			//si tiene una segunda reservación, se muestran los datos
				if($r2->abierto){
					$fecg="Abierto";
				}else{
					$fecg=templateDriver::timelo($r2->reservation_date,3);
				}
				    $as = Assignment::find("all",array('conditions' => 'reservation_id="'.$r2->id.'"'));
				
				    echo '<tr><td>'.$contador.'</td>
    				<td>'.$s->id.'</td>
    				<td>'.$fecg.'</td>
    				<td>'.substr($r2->reservation_time,0,5).'</td>
    				<td>'.direccion($r2->addresses).'</td>
    				<td>'.$r2->passengers.'</td>';
					if($r2->vehicle_id == 1){
    				    echo '<td>auto</td>';
    				}else{
    				    echo '<td>camioneta</td>';
    				}
    				if(count($as)){
    				    $unit = Unity::find_by_id($as[0]->unity_id);
    				    $oper = Operator::find_by_id($as[0]->operator_id);
        				echo '<td>'.$unit->economic.'</td>';
        				echo '<td>'.$oper->username.'</td>';
    				}else {
    				 echo '<td>N/A</td><td>N/A</td>';
    				}
				if($showc)
				echo '<td>0</td></tr>';
				else {
				     if(substr($s->cost,0,1)=="m"){
				       echo '<td>'.substr($s->cost,1).'</td></tr>';
				    } else
				        echo '<td>'.$s->cost.'</td></tr>';    
				}
				
				$contador++;
			}
		}
		if($contador>=500)
		break;
	}	
	echo '</tbody></table>';
	

?>