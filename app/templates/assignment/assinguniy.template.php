<?php
//muestra los datos de una unidad especifica
    $d = templateDriver::getData('id');
	if(!$d){
		echo 'no existe';
	}else{
		
		$z=Unity::find_by_id($d['id']);
	}



?>
<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Asignación</h4>
			</div>
	<div class="modal-body assin" style="padding:40px;padding-bottom: 0px;">
	
		
  	<table class="table" style="margin-bottom: 0px;"> 
  		<tbody>
	  		<tr>
	  			<td class="tdstatico">
	  				<span class="tool glyphicon glyphicon-road iconoesl" data-toggle="tooltip" data-placement="top" title="Suburb"></span>
	  			</td>
	  			<td>
	  			 <div class="form-inline">
		  			<input type="hidden" class="idun" value='<?php echo $z->id;?>' />
		  			<input type="hidden" class="ecoun" value='<?php echo $z->economic;?>' />
		  		    <span><?php echo "Id: ".$z->economic.""; ?></span>
		         </div>              
	  		    </td>
	  		    <td>
	  		    	<?php
	  		    	echo " Tipo: {$z->type}";
	  		    	?>
	  		    </td>
	  		    <td style="width: 128px;">
	  		    	<?php
	  		    	echo " Color: {$z->color}";
	  		    	?>
	  		    </td>
	  		</tr>
	  		<tr>
	  			<td class="tdstatico">
	  				<span class="tool glyphicon glyphicon-th-list iconoesl" data-toggle="tooltip" data-placement="top" title="Suburb"></span>
	  			</td>
	  			<td colspan="3">
	  			 <table style="width: 100%;"<tbody>
	  			 	<tr>
	  			 		<td>
	  			 		<?php
	  		    			echo " Num. Serial: {$z->serial_number}";
	  		    		?>	
	  			 		</td>
	  			 		<td>
	  			 			<?php
	  		    			echo " Licencia: {$z->license_plate}";
	  		    		?>	
	  			 		</td>
	  			 	</tr>
	  			 </tbody></table>
	  		    </td>
	  		</tr>
	  		<tr>
	  			<td class="tdstatico" style="vertical-align: -webkit-baseline-middle;padding-top: 13px;">
	  				<span class="tool glyphicon glyphicon-usd iconoesl" data-toggle="tooltip" data-placement="top" title="E-mail"></span>
	  			</td>
	  			<td colspan="2"style="height: 121px;vertical-align: initial;width: 220px;padding-right: 0px;">
	  			   <div class="form-inline" style="padding-right: 0px;"><div class="row"> 
	  			   
 				 <div class="col-md-6" style="text-align: left;padding-left: 15px;">
	  			   	<select class="form-control" id="asunit" style="text-align: left">
	  			   			
		  		    <?php 
		  		    //muestra todos los operadores activos a los que se les puede asignar una unidad
		  		    $unit=Operator::all(array('conditions'=> 'enable = 1','order'=>'username asc'));
					echo "<option value='0'>Sin asignar</option>";
		  		    foreach ($unit as $u) {
						  echo "<option value='{$u->id}'>{$u->username}</option>";
					  }
		  		    ?>
		  		    </select>  </div><div class="col-md-6" style="text-align: right;padding-right: 0px;"><p style="padding-right: 13px;">Descripcion:</p> </div>
		           </div></div>
		           	<td colspan="3" id="desunit" style="text-align: left;padding-top: 9px;border-bottom: 1px solid #dddddd;">
	  				
	  			</td> 
	  		</tr>
		</tbody>
	</table> 
	</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-danger close-user" data-dismiss="modal">Cancelar</button>
			<button type="button" class="btn-asig btn btn-primary">Guardar cambios</button>
		</div>
		</div>
	</div>