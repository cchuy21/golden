<!--permite habilitar o deshabilitar una unidad y sus operadores-->

<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Estado de Unidades</h4>
			</div>
			<div class="modal-body" style="padding:40px;">
				<ul class="nav nav-tabs" id="admun" role="tablist">
				  <li class="active"><a href="#confunity" role="tab" data-toggle="tab">Unidades</a></li>
				  <li><a href="#confoperator" role="tab" data-toggle="tab">Operadores</a></li>
				</ul>
				
				<!-- Tab panes -->
				<div class="tab-content" style="height: 150px;">
				  <div class="tab-pane active stat" data-tipo="unity" id="confunity" style="padding-top: 30px;">
				  	<div class="col-md-6">
				  		<div class="col-md-4" style="padding-top: 5px;">Unidad: </div>
				  		<div class="col-md-8">
				  			<select class="form-control" id="asunit">  <?php 
				  			//seleccionar una unidad
		  		    $unit=Unity::all(array('order'=>'economic asc'));
					echo "<option value='0'>Seleccionar unidad</option>";
		  		    foreach ($unit as $u) {
		  		    	if($u->enable==1)
						  echo "<option value='{$u->id}'>{$u->economic}</option>";
					  }
		  		    ?></select></div>
				  		<label class="radio-inline">
 							 <input type="radio" name="status" id="hab" value="1"><label style="margin-top: 3px;margin-bottom: 0px;font-weight:normal;"> Habilitado</label>
						</label>
						<label class="radio-inline">
 							 <input type="radio" name="status" id="deshab" value="0"><label style="margin-top: 3px;margin-bottom: 0px;font-weight:normal;"> Inhabilitado</label>
						</label>
						<textarea class="form-control" rows="3"></textarea>
				 	</div>
				  	<div class="col-md-6" id="descr" style="text-align: left;padding-left: 50px;">
				  		Descripción:
				  	</div>
				  	<div class="col-md-12" style="text-align: left; padding-top: 20px;">
				  		<button type="button"  name="habi" class="btn-status btn btn-primary">Guardar cambios</button>
				  	</div>
				  </div>
				  <div class="tab-pane stat" data-tipo="operator" id="confoperator" style="padding-top: 30px;">
				  	<div class="col-md-6">
				  		<div class="col-md-4" style="padding-top: 5px;">Operador: </div>
				  		<div class="col-md-8"><select class="form-control" id="asunit">
				  			 <?php 
				  			 //el nombre de los operadores que estan en servicio
		  		    $unit=Operator::all(array('order'=>'username asc'));
					echo "<option value='0'>Seleccionar operador </option>";
		  		    foreach ($unit as $u) {
		  		    	if($u->enable==1)
						  echo "<option value='{$u->id}'>{$u->username}</option>";
					  }
		  		    ?>
		  		    </select></div>
				  		<label class="radio-inline">
 							 <input type="radio" name="status" id="hab" value="1"><label style="margin-top: 3px;margin-bottom: 0px;font-weight:normal;"> Habilitado</label>
						</label>
						<label class="radio-inline">
 							 <input type="radio" name="status" id="deshab" value="0"><label style="margin-top: 3px;margin-bottom: 0px;font-weight:normal;"> Inhabilitado</label>
						</label>
						<textarea class="form-control" rows="3"></textarea>
						</div>
				  	<div class="col-md-6" id="descr" style="text-align: left;padding-left: 50px;">
				  		Descripción:
				  	</div>
				  	<div class="col-md-12" style="text-align: left; padding-top: 20px;">
				  		<button type="button" name="habi" class="btn-status btn btn-primary">Guardar cambios</button>
				  	</div> </div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger close-user" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
