<?php
    $d = templateDriver::getData('id');
	$m=0;
	if(!$d){
		
	}else{
		$s=Reservation::find_by_id($d['id']);
	}
		$tipo=$s->vehicle_id;
		//separar las direcciones ante una ,
		$array = array_unique(explode(',', $s->addresses));
			foreach ($array as $value) {
			//obtener la longitud de la variable
			$lon=strlen($value);
			$lon=($lon-1)*(-1);
			//obtiene el primer caracter de la variable
			$ind=substr($value, 0,$lon);
			//si es un automovil, se obtiene la dirección y suburbio a donde se va a dirigir
			if($ind == "a"){

			$value=str_replace("a", "", $value);
			$dir=Address::find_by_id($value);
			$sub=Suburb::find_by_id($dir->suburb_id);
			}else if($ind == "s"){
				$value=str_replace("s", "", $value);
				$sub=Suburb::find_by_id($value);	
			}
			
			$zon=Zone::find_by_id($sub->id_zone);
			$con=Agreement::find_by_id($conv);
				//selecciona el costo de la zona, dependiendo del tipo de vehiculo
				if($tipo=="1"){
					$costo=0;
					$costo=$zon->cost_car;
					$veh="Carro";
					$desc=$zon->des_car;
				}else if($tipo=="2"){
					$costo=$zon->cost_van;
					$veh="Camioneta";
					$desc=$zon->des_van;
					}
				if($costo>=$mayor){
					$idmayor=$value;
					$mayor=$costo;
					$menor=$desc;
				}
	
		}
			
		if($ind != "s"){
			$maydir=Address::find_by_id($idmayor);
			$maysub=Suburb::find_by_id($maydir->suburb_id);
			}else{
				$idmayor=str_replace("s", "", $idmayor);
			$maysub=Suburb::find_by_id($value);
			}
			$mayzon=Zone::find_by_id($maysub->id_zone);
			if($tipo=="1"){
				$des=$mayzon->des_car;
				$cost2=$mayzon->cost_car;
			}else if($tipo=="2"){
				$des=$mayzo->des_van;
				$cost2=$mayzon->cost_van;
			}
		//convertir el servicio en sencillo o redondo
		$r=Service::find_by_reservation1($s->id);
		$u=User::find($r->user_id);
		if($r->reservation2){
			$che="<td><input type='checkbox' class='convsen'>  Convertir a sencillo</td>";
		}else{
			$cost2=(($r->cost*2)-$des);
			$che="<td><input type='checkbox' class='convred'>  Convertir a redonda</td>";
		}
	if(substr($r->cost, 0,1)=="m"){
			$m=1;
			$r->cost=substr($r->cost,1);
			$cost2=$r->cost*2;
	       }
	
?>
<div class="modal-dialog" style="width: 300px;">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">Cobranza R:<?php echo $r->id; ?></h4>
		</div>
		<div class="modal-body assin" style="padding:40px;padding-bottom: 40px;">
		<input type="hidden" id="cobreserid" value="<?php echo $s->id ?>" />
			
		<table class="table">
			<tbody>
				<tr>
					<td>
						Costo: 
						<label class="mostrar">$<?php echo $r->cost ;?></label>
						<label class="mostrardoble" style="display: none">$<?php echo $cost2 ;?></label>
					</td>
					
				</tr>
				<tr>
					<?php echo $che ;?>
				</tr>
				<tr>
					<td>
						<br />
						Método de pago: <select name="payment" id="fpaag" class="form-control">
			  				<?php
			  				$opciones=array('Tarjeta de Credito/Debito','Efectivo MN','Firma por cobrar');
							foreach($opciones as $opc){
								//seleccionar uno de los 3 formas de pago definidos
			  					if($opc==$r->payment){
			  						echo "<option value='".$opc."' selected>".$opc."</option>";
			  					}else{
			  						echo "<option value='".$opc."'>".$opc."</option>";
			  					}				  				
							}
			  				?>
			  				
			  			</select>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-danger close-user" data-dismiss="modal">Cancelar</button>
		<button type="button" class="btn-cob btn btn-primary">Cobrar</button>
	</div>	
	</div>
</div>