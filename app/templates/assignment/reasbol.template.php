<?php
    $d = templateDriver::getData('id');
	if(!$d){
		
	}else{

		$hoy=date("Y-m-d");	
		$hoym=date("Y-m-d",strtotime($hoy));
		$otra = strtotime ( '+1 day' , strtotime ( $hoym ) ) ;
		$otra=date("Y-m-d G:i", $otra);	
		//se busca el ticket con un id especifico							
		$tic=Ticket::find_by_id($d['id']);
							
	}
//se asignan los boletos para los servicios especificando la unidad, operador, el tipo de servicio, etc.
?>
<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Asignación Boleto # <?php echo $tic->folio?></h4>
			</div>
			<div class="modal-body" class="res" style="height: 100px">
				
				<div class="row" style="height: 17%!important;padding-bottom: 33px!important;">
					<div class="col-md-4 col-md-offset-2">
						Asignar Unidad: 
					</div>
					<div class="col-md-4">
						<input type="hidden" class="reser-id"  value="<?php echo isset($tic->id)?$tic->id:null ?>"/>
						<select class="form-control unidad" style="width: 215px;">
							<option value="">Seleccionar Operador-Unidad</option>
							<?php
				     			$op=Unity::all(array("order"=>"economic asc", 'conditions'=> 'enable = 1 AND (status=2 OR status=3)'));
						 		foreach ($op as $p) {
						 			//muestra todos las unidades con operadores con su número económico y su nombre de usuario
						 			$ope=Operator::find_by_id($p->driver);
									 echo "<option value=".$p->id.">".$p->economic."(".$p->type.")-".$ope->username."</option>";
								 }?>
						</select>
					</div>
				</div>
				
				<div class="row" style="height: 17%!important;">
					<div class="col-md-4 col-md-offset-2">
						Tipo de servicio: 
					</div>
					<div class="col-md-4 asig-type">
						<select class="form-control tipo" style="width: 215px;">
							<option value="Directo">Directo</option>
							<option value="Regreso">Regreso</option>
						</select>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger close-user" style="float: right;margin-left: 10px;" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn-reasig-boleto btn btn-primary" style="float: right">Guardar cambios</button>
			</div>
		</div>
	</div>