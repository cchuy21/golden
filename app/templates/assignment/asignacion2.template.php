<?php
//pantalla de asignaciones
templateDriver::renderSection("asignaciones.funciones");

?>
<div class="container" id="content" style="width: 100% !important;">
	<?php
	if(authDriver::getSUser()->group=="OBSERVADOR")
		echo "<div id='inab' style='height: 100%; width: 100%; position: absolute; z-index: 10000; background: rgba(158, 158, 158, 0.29);'></div>";
	?>
	<div class="panel panel-default">
		<div class="panel-body user-panel">
			<table class="table table-bordered" style="height: 700px; margin-top: 0px;">
				<tbody>
					<tr>
						<td style="width: 70%">
							<div class="subasigb" style="margin-top:8px;" id="titulollegadas">
								<?php
								//muestra los datos del archivo titulollegadas, donde se muestran los servicios que van llegando
						  			templateDriver::renderSection("asignaciones.titulollegadas");
								?>
							</div>
							<div class="subasiga" style="height: 240px;">
								<div class="subasig">
									<div class="row">
									  <div class="col-md-3 subasige" id="ticketsasignados" style="background: rgb(52, 206, 255); border-right: 1px solid white;"> 
									  	<?php
									  	//muestra los tickets asignados de las asignaciones
									  		templateDriver::renderSection("asignaciones.ticketsasignados");
									  	?>
									  </div>
									  <!--muestra en una columna las llegadas de los servicios en color rgb azul-->
									  <div class="col-md-9 subasige" id="llegadasasignadas" style="background: rgb(52, 206, 255)">
									  	<?php
									  		templateDriver::renderSection("asignaciones.llegadasasignadas");
									  	?>
									  </div>
									</div>
								</div>
								<div class="subasig">
									<div class="row">
									  <div class="col-md-3 subasige" id="tickets" style="background-color: rgb(180,3,3); border-right: 1px solid white;">
										<?php
									  		templateDriver::renderSection("asignaciones.tickets");
									  	?>
									  </div>
									  <div class="col-md-9 subasige" id="llegadas" style="background: rgb(180, 3, 3);">
									  	<?php
									  		templateDriver::renderSection("asignaciones.llegadas");
									  	?>
									  </div>
									</div>
								</div>
							</div>
							<div class="subasigb" id="titulosalidas" style="margin-top: 0px;">
								<?php
								//las asignaciones que van de salida
							  		templateDriver::renderSection("asignaciones.titulosalidas");
								?>
							</div>
							<div class="subasiga" style="height: 400px;">
								<div class="subasigd" id="salidasasignadas" style="background: rgb(52, 206, 255)">
									<?php
										templateDriver::renderSection("asignaciones.salidasasignadas");
									?>
								</div>
								<div class="subasigd" id="salidas" style="background: rgb(180, 3, 3);">
									<?php
								  		templateDriver::renderSection("asignaciones.salidas");
									?>
										</div>
								<div class="subasigg" id="salidasmana" style="background: rgb(250, 140, 0)">
									<?php
									//se muestran las salidas que se realizaran el dia de mañana
							  			templateDriver::renderSection("asignaciones.salidasmana");
									?>
								</div>
							</div>
							
						</td>

						<td style="width: 12%;height: 700px;vertical-align: top;">
							
							<div class="subasigb" style="margin-top: 0px; font-weight: bold;">
								<div class="corte1">
									
								</div>
								<div class="corte2">
									
								</div>
								<!--apartado del lado derecho del apartado de asignaciones-->
								<a href="/assignment/corteturno" target="_blank">
									<button type="button" class="btn btn-default tool btn-xs" title="Corte de Turno" style="float: left;background: black; color: white;">
  										<span style="color: white" class="glyphicon">VIAJES</span>
									</button>
								</a>
								<button type="button" class="btn btn-default tool btn-xs reas" title="Reasignar" style="float: left;background: black; color: white;">
									<span style="color: white" class="glyphicon glyphicon-transfer"></span>
								</button>
							COBRANZA
							</div>
							<div class="subasiga" id="cobranza" style="background:#e7e7e7; height: 361.6px!important">

								<?php
						  			templateDriver::renderSection("asignaciones.cobranza");
									$un=Unity::find('all',array('conditions'=>'enable = 1 AND (status=2 OR status=3)',"order"=>"id asc"));

								?>
							
							</div>
							<div id="ttviajes" class="subasigb" style="height:20px; font-weight: bold; margin-top:0px;">
								<?php
							  		templateDriver::renderSection("asignaciones.ttviajes");
								?>
							</div>
							<div class="subasigb" id="titrecur" style="height:28px;font-weight: bold; margin-top:0px;">
								<?php
							  		templateDriver::renderSection("asignaciones.titulorecursos");
								?>
							</div>
							<div class="subasiga" style="overflow-y: auto" id="recursos">
								<?php
							  		templateDriver::renderSection("asignaciones.recursos");
								?>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
