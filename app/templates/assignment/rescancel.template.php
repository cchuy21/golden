<?php
//muestra las reservaciones que han sido canceladas determinando si era sencillo o redondo
	$hoy=date("Y-m-d");
$reser=Reservation::find("all",array("conditions" =>"status=0 AND reservation_date = {ts '".$hoy."'}"));

?>
<div class="modal-dialog" style="width:900px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Reservación(es) Cancelada(s)</h4>
			</div>
	<div class="modal-body assin" style="padding:40px;padding-bottom: 0px;">
	
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>#</th>
					<th>Num_Reservación</th>
					<th>Tipo</th>
					<th>Fecha de Cancelación</th>
					<th>Cancelo:</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$c=1;
				foreach ($reser as $res) {
					$ca=Canceled::find_by_reservation1($res->id);
					//sino existe una primera reservación, se busca la segunda
					if(!$ca)
						$ca=Canceled::find_by_reservation2($res->id);
					
					 if($ca->reservation2)
						$tipo="Redonda";
					else 
						$tipo="Sencilla";
					echo "<tr><td>".$c."</td><td>".$ca->id."</td><td>".$tipo."</td><td>".date("d-m-Y",strtotime($ca->created_at))."</td><td>".$ca->attended."</td></tr>";
					$c++;
				}
				
				?>
			</tbody>
		</table>
	</div>
	<div class="modal-footer">
			<button type="button" class="btn btn-danger close-user" data-dismiss="modal">Cerrar</button>
			
		</div>
</div>
</div>