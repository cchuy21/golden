<?php
    $d = templateDriver::getData('id');
	 $r = templateDriver::getData('res');
	if(!$d){
		
	}else{
		$z=Unity::find_by_id($d['id']);
	}

$res=Service::find_by_reservation1($r);
//sino tiene la primera reservación, se busca si tiene reservación 2
if(!$res)
$res=Service::find_by_reservation2($r);

?>
<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Asignación a Reservación # <?php echo $res->id	//número de reservación 2 del servicio;  ?></h4>
			</div>
			<div class="modal-body" class="res" style="height: 100px">
				
				<div class="row" style="height: 17%!important;padding-bottom: 33px!important;">
					<div class="col-md-4 col-md-offset-2">
						Asignar Unidad: 
					</div>
					<div class="col-md-4">
						<input type="hidden" class="reser-id"  value="<?php echo isset($res->id)?$res->id:null ?>"/>
						<select class="form-control unidad">
							<option value="">Seleccionar Operador-Unidad</option>
							<?php
							//muestra las opciones de las unidades disponibles para atender el servicio
				     			$op=Unity::all(array('conditions'=> 'enable = 1 AND (status=2 OR status=3)'));
						 		foreach ($op as $p) {
						 			$ope=Operator::find_by_id($p->driver);
									 echo "<option value=".$p->id.">".$ope->username."-".$p->id."(".$p->type.")</option>";
								 }?>
						</select>
					</div>
				</div>
				
				<div class="row" style="height: 17%!important;">
					<div class="col-md-4 col-md-offset-2">
						Tipo de servicio: 
					</div>
					<div class="col-md-4 asig-type">
						<select class="form-control tipo">
							<option value="">Seleccionar Tipo</option>
							<option value="1">Directo</option>
							<option value="1">Regreso</option>
						</select>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger close-user" style="float: right;margin-left: 10px;" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn-reasig-reser btn btn-primary" style="float: right">Guardar cambios</button>
			</div>
		</div>
	</div>