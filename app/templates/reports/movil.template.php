<div class="panel-default systemuser-block">
	<div class="panel-body" style="background: white; padding: 0px; height: 500px; overflow: auto;padding-left: 10px;">
 		<div class="col-md-12" id="excelink">
 			<!--input para seleccionar una fecha especifica, esto generara 2 botones uno que genera un archivo en excel del corte a las 3 a.m. y otro que lo genera de las 3 p.m.-->
 			<div class="col-md-12"><span>Seleccione un mes y un año:</span></div>
 			<div class="col-md-5">
 	 		<select class="form-control col-md-6 mes">
				<option>mes</option>
 	 			<option value="1">Enero</option>
 	 			<option value="2">Febrero</option>
 	 			<option value="3">Marzo</option>
 	 			<option value="4">Abril</option>
 	 			<option value="5">Mayo</option>
 	 			<option value="6">Junio</option>
 	 			<option value="7">Julio</option>
 	 			<option value="8">Agosto</option>
 	 			<option value="9">Septiembre</option>
 	 			<option value="10">Octubre</option>
 	 			<option value="11">Noviembre</option>
 	 			<option value="12">Diciembre</option>
 	 		</select>
 	 		</div>
 	 		<div class="col-md-5">
 	 		<?php
 	 		$años=array();
 	 		$añoa=date("Y");
 	 		for($i=2015;$i<=$añoa;$i++){
 	 			array_push($años, $i);
 	 		}
			?>
			<select class="form-control col-md-6 anio">
				<option>año</option>
			<?php
			foreach($años as $a){
				echo "<option value='".$a."'>".$a."</option>";
			}
 	 		?>
 	 		</select>
 	 		</div>
 	 		<div class="col-md-2">
 	 			<button id="btn-repmovil" class="form-control btn btn-success">BUSCAR</button>
 	 		</div>
	 		<div class="col-md-12" id="panel-cnt">
	 			
	 		</div>
 		</div>
 	</div>
</div>
