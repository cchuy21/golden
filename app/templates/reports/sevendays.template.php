<?php
$u = templateDriver::getData("modo");
$id = templateDriver::getData("id");
if(!$id){
	$id['id']=-1;
}
    if(!$u || $u['modo']=="dia" || $u['modo']=="can") {
    	if(!$u)
		$u['modo']="dia";
?>
    <script type="text/javascript">
// 	cargar una versión especifica
      google.load('visualization', '1');

    </script>

    <script type="text/javascript">
    
		drawVisualization();
	
  
      function drawVisualization() {
		var data;
// 		método por el que se envian los datos , los archivos que requiere, el titulo y tamaño de la gráfica
			jQuery.ajax({
		  		type: "POST",
		  		url: "/reportes/sevendays/<?php echo $u['modo'];?>/"+<?php echo "'".$id['id']."'"; ?>,
		  		dataType:"JSON",
		  		data: {}, 
		  		success:function(data){
		  			var dia0=data[0],
		  			dia1=data[1],
		  			dia2=data[2],
		  			dia3=data[3],
		  			dia4=data[4],
		  			dia5=data[5],
		  			dia6=data[6],
		  			dia7=data[7];		  			
		  			
		  			var wrapper = new google.visualization.ChartWrapper({
				
		          chartType: 'ColumnChart',
				
		          dataTable: [dia0,dia1,dia2,dia3,dia4,dia5,dia6,dia7],

		          options: {'title': 'Servicios',"width":600,"height":500, isStacked: true},
		
		          containerId: 'grrango'

		        });
					wrapper.draw();
		         google.setOnLoadCallback(drawVisualization);
		  		},
		  		error:function(){
		  			console.log("error de extraciion de datos");
		  		}
			});
      }
    </script>
    

<?php
}else{
		?>
    <script type="text/javascript">
       drawChart();
 
    // Load the Visualization API and the piechart package.
    google.load('visualization', '1', {'packages':['corechart']});
      
    // Set a callback to run when the Google Visualization API is loaded.
    google.setOnLoadCallback(drawChart);
      
     function drawChart() {
    	var fec=<?php echo "'".$u['modo']."'"; ?>;
    	var id=<?php echo "'".$id['id']."'"; ?>;
      var jsonData = jQuery.ajax({
          url: "/reportes/sevendays/"+fec+"/"+id,
          dataType:"json",
          async: false
          }).responseText;
          
          jsonData = JSON.parse(jsonData);
      // Create our data table out of JSON data loaded from server.
		var opciones = {
        title: 'Servicios',
        width:600,
        height:500

    };
      var data = google.visualization.arrayToDataTable(jsonData);

      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.ColumnChart(document.getElementById('grrango'));
      chart.draw(data, opciones);
    }
    </script>
    

<?php	
}

?>