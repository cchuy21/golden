<!--MUESTRA LA BITACORA DE LAS LLEGADAS CON LOS DATOS CORRESPONDIENTES-->
<div class="panel-default">
	<div class="panel-body" style="background-color: white; height: 501px; overflow: auto;padding-top: 0px;">
	<style>
			table tr td{
				text-align:center;
				border: solid rgb(196, 192, 192) 1px;
				height: 18px;
				font-size:12px;
			}
		
		</style>
		<?php
			$fecha=templateDriver::getData('fecha');
			if(!$fecha){
				$fecha=date("Y-m-d");
			}
			$man=templateDriver::getData('page');
			$tmp=templateDriver::getData('temp');
			echo "<input type='hidden' id='tempbit' value='".$tmp."'>
			<label style='float: left; margin-top: 5px; margin-right: 20px;'>Fecha de Bitacora:</label> <input type='text' class='form-control' id='febit' style='width: 100px;' value='".$fecha."' />";
			echo "<a target='_blank' href='http://resv.goldenmty.com/reportes/xlsbit/APTO-MTY/".$fecha."'><button class='btn btn-success' style='float: right;margin-top: -41px;'>Descargar Excel</button></a>";
		?>
		<table class="table table-bordered" id="bitl" style="font-size: 10px;">
			<thead style="text-align: center">
        <tr>
			<th>ID</th>
			<th>HORA</th>
			<th>RES.</th>
			<th>CLIENTE</th>
			<th>CALLE Y NUM.</th>
			<th>COLONIA</th>
			<th>EMP.</th>
			<th>T. DE V.</th>
			<th>COSTO</th>
			<th>U. DE SERVICIO</th>
			<th>PAGO</th>
			<th>TELÉFONO</th>
			<th>TIPO</th>
          <!--<th>PAGADO</th>-->
        </tr>
      </thead>
			<tbody>
				
			
			<?php	
			
			$hoy=date('Y-m-d');
			$diac=strtotime('-1day', strtotime($hoy));
			$diac=date('Y-m-d', $diac);
			$diag=strtotime('+1 day', strtotime($diac));
			$diag=date('Y-m-d', $diag);
			if(!empty($fecha)){
				$reser=Reservation::find('all', array('order' => 'reservation_time','conditions' =>' reservation_date = {ts "'.$fecha.'"}'));
			}else{
				$reser=Reservation::find('all', array('order' => 'reservation_time','conditions' =>' reservation_date BETWEEN {ts "'.$diac.'"} AND {ts "'.$diag.'"}'));
			}
			$cot=0;
			
			foreach($reser as $value){
				
				if($value->id){
				if($value->type=='APTO-MTY'){
					if($value->vehicle_id==1){
						$vel="AUT";
					}else{
						$vel="CAM";
					}
				$assig=Assignment::find_by_reservation_id($value->id);
					$unity=Unity::find_by_id($assig->unity_id);
					$oper=Operator::find_by_id($assig->operator_id);
					$query='select * from services where reservation1='.$value->id;
					$ser=Service::find_by_sql($query);
					if(!$ser){
						//selecciona todos los servicios de la reservacion2 de un usuario definiendo si el servicio es redondo o sencillo
						$query2='select * from services where reservation2='.$value->id;
						$ser=Service::find_by_sql($query2);
						$serredsen='DAD';
					}else{
						if($ser->reservation2!=""){
							$serredsen='ADA';	
						}else{
							$serredsen='AD';
						}
					}
					
				$direccion=$value->addresses;
				//separa las direcciones apartir de una coma
				$direc=explode(",", $direccion);
				$addressfinal ='';
					for($i=0;$i<count($direc);$i++){
						if(substr($direc[$i],0,1)=='a'){
								$address=Address::find_by_id(substr($direc[$i],1));
								$sub=Suburb::find_by_id($address->suburb_id);
								$a='d';
								$addressfinal=$address->street;
						break;
					}else{
							$a='c';
							$sub=Suburb::find_by_id(substr($direc[$i],1));
			
					}
		
	}
								
				foreach($ser as $s){
					$u=User::find_by_id($s->user_id);
				     if(!$u){
				     	continue;
				     }    
					if($s->id>0 && $s->id!=null){
						$cot++;
						$ln=30*($man-1);
						$lm=30*$man;  
						if ($cot<=$lm && $cot>$ln) {
					?>
					
					<tr>
						<td>
							<?php echo $cot ;?> 
						</td>
						<td>
							<?php echo substr($value->reservation_time,0,5);?> 
						</td>
						
						<td><?php echo $s->id;?></td>
				
						
						<td>
							<?php 
								
									$pay=$s->payment;
									if($pay=='Tarjeta de Credito/Debito')$pay='TC';
									else if($pay=='Cortesia')$pay='CO';
									else if($pay=='Firma por cobrar')$pay='FxC';
									else if($pay=='Efectivo MN')$pay='EF';
									
									$namefull=$u->name;
									
									if($u->business_id){
									//si el usuario tiene convenio se selecciona el servicio particular	
											$emp2=User::find("all",array("conditions"=>"id='".$u->business_id."'")); 
											if(!count($emp2)){
												$emp="PAR";
											}else{
												$emp=User::find($u->business_id); 
												$emp=$emp->name;
											}	
									}
									else
										$emp="PAR"; 
									$contname=strlen($namefull);
										if ($contname>=25) {
											echo substr($namefull, 0, 25).'.';
										}else{
											echo $namefull;
										}
							?>
						</td>
						<td>
							<?php
							    if($s->user_id=='53380'){
							        echo explode("Entre Calles:",explode("Calle:",$s->annotations)[1])[0];
							    }else{
    								$streetfull = '';
    							    $streetfull=$addressfinal;
								    $contstreet=strlen($streetfull);
								    //si la calle de la dirección tiene más de 25 caracteres, solo se muestran los 25 principales
								   if ($contstreet>=25) {
									   echo substr($streetfull, 0, 25).'.';
								   }else{
								   	   echo $streetfull;
								   }
							    }
							?>
						</td>
						<td>
							<?php $subfull=$sub->suburb;
								$contsub=strlen($subfull);
								   
								if ($contsub>=25) {
									echo substr($subfull, 0, 25).'.';
								}else{
									echo $subfull;
								}
								if(substr($s->cost, 0,1)=="m"){
								 $s->cost=substr($s->cost,1);
								}	
							?>
						</td>
						<td>
							<?php echo substr($emp,0,3);?>
						</td>
						<td>
							<?php echo $vel;?>
						</td>
						<td>
							$<?php echo $s->cost;?>
						</td>
						<td>
							<?php echo $unity->economic;?>-<?php echo $oper->name;?> <?php echo $oper->lastname;?>
						</td>
						<td>
							<?php echo $pay;?>
						</td>
						<td>
							<?php if($a=='d'){echo $address->telephones;}?>
						</td>
						<td>
							<?php echo $serredsen;?>
						</td>
						<!--<td>
							<?php if($ser->pagado){
								echo "SI";
							}else{
								echo "NO";
							}
							?>
						</td>-->
				
					</tr>
					<?php
					
					}
					}
				}
				}
            }
            }
            
			?>	
				
			</tbody>
		</table>
		
		
		
		<?php
		
		$vr=(int)($cot/30);
		$vr++;
		for($i=1;$i<=$vr;$i++){
			if($man==$i)
			echo "<button class='actpage btn btn-xm btn-default' data-page='{$i}' data-temp='{$tmp}' style='margin-left: 5px;'>{$i}</button>";
			else
			echo "<button class='bitpage btn btn-xm btn-default' data-page='{$i}' data-temp='{$tmp}' style='margin-left: 5px;'>{$i}</button>";
		}
		if(!$fecha){
			$fecha=date("Y-m-d");
		}
		
		echo '<a href="/reportes/pdf/APTO-MTY/'.$fecha.'" target="_blank"><button class="btn btn-xm btn-default"  style="margin-left: 5px;">IMPRIMIR</button></a>';
		?>
		</div>
	<div class="panel-body" style="background-color: white; height: 501px; overflow: auto;padding-top: 0px;">
	    <table class="table table-hover">
	        <thead>
	            <tr>
	                <td></td>
	                <td>Imprimir</td>
	            </tr>
	        </thead>
	    <?php
	    $nuser = [];
		foreach($reser as $value){
			if($value->type=='APTO-MTY'){
    		    $query2='select * from services where reservation2="'.$value->id.'" or reservation1="'.$value->id.'"';
    			$ser = Service::find_by_sql($query2);
    			$u = User::find($ser[0]->user_id);
    			if(!in_array($u->name,$nuser) && $u){
    			    array_push($nuser,$u->name);
    			}
			}
        }
        echo "<tbody>";
        foreach($nuser as $nu){
            echo "<tr>
                <td><input type='checkbox' checked class='form-control form-impl'></td>
                <td>".$nu."</td>
            </tr>";
        }
	    ?>
	    </table>
	    <button class="btn btn-primary btn-imptotal">Imprimir</button>
	</div>
</div>