<?php
$u = templateDriver::getData("modo");
$id = templateDriver::getData("id");
if(!$id){
	$id['id']=-1;
}	
?>	
	<script type="text/javascript">
     drawChart();
 
    // Load the Visualization API and the piechart package.
    google.load('visualization', '1', {'packages':['corechart']});
      
    // Set a callback to run when the Google Visualization API is loaded.
    google.setOnLoadCallback(drawChart);
      
    function drawChart() {
    	var fec=jQuery("#f2").val()+"/"+jQuery("#f1").val();
    	var val=<?php echo "'".$u['modo']."'" ?>;
    	var id=<?php echo "'".$id['id']."'"; ?>;
      var jsonData = jQuery.ajax({
          url: "/reportes/difechas/"+fec+"/"+val+"/"+id,
          dataType:"json",
          async: false
          }).responseText;

          jsonData = JSON.parse(jsonData);
      // Create our data table out of JSON data loaded from server.
      if(val=="dia"){
		var opciones = {
        title: 'Servicios',

        width:700,
        height:500
    };
    }else{
    	var opciones = {
        title: 'Servicios',

        width:600,
        height:500
    };
    }
      var data = google.visualization.arrayToDataTable(jsonData);

      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.ColumnChart(document.getElementById('grrango'));
      chart.draw(data, opciones);
   		
    }
    </script>
    

