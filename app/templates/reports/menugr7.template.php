<?php
$tip= authDriver::getSUser()->group;
//si el usuario es un administrador muestra el menu de las bitacoras de llegadas y salidas de los servicios

?>
<div class="panel-default systemuser-block">
	
		
    <div class="panel-body" style="background: white; padding: 0px; height: 501px">
    	<?php
    	
    	if($tip=="ADMINISTRADOR" || $tip=="OBSERVADOR"){
    		echo '<button type="button" data-temp="content" id="SerRea" class="repprin rep btn btn-menurep">Servicios realizados<span class="glyphicon glyphicon-chevron-right imgmenurep"></span></button>';
    		echo '<button type="button" data-temp="operadores" id="grope" class="repprin rep btn btn-menurep">Operadores<span class="glyphicon glyphicon-chevron-right imgmenurep"></span></button>';
    		echo '<button type="button" data-temp="call" id="grcall" class="repprin rep btn btn-menurep">call center<span class="glyphicon glyphicon-chevron-right imgmenurep"></span></button>';
    		echo '<button type="button" data-temp="canceled" id="grcan" class="repprin rep btn btn-menurep">cancelados<span class="glyphicon glyphicon-chevron-right imgmenurep"></span></button>';
		}
    	?>
		    <button type="button" data-temp="bitacoral" id="Bitacora" class="repprin rep btn btn-menurep">Bitacora llegadas<span class="glyphicon glyphicon-chevron-right imgmenurep"></span></button>
	        <button type="button" data-temp="bitacoras" id="Bitacora" class="repprin rep btn btn-menurep">Bitacora salidas<span class="glyphicon glyphicon-chevron-right imgmenurep"></span></button>
	        <button type="button" data-temp="bitempresa" id="Bitacora" class="repprin rep btn btn-menurep">Servicios por Empresa<span class="glyphicon glyphicon-chevron-right imgmenurep"></span></button>
	        <button type="button" data-temp="colonias" id="cloniaext" class="repprin rep btn btn-menurep">Colonias<span class="glyphicon glyphicon-chevron-right imgmenurep"></span></button>
			
</div></div>