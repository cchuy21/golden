<?php
/*se encuentra en el apartado de bitacoras-servicios realizados,
*si el usuario es administrador ingresa a las opciones para seleccionar los servicios que 
 * se han realizado, por tipo(dia, usuario, colonia, cancelados) y por una fechas especificas  
 **/
$tip= authDriver::getSUser()->group;
if($tip=="ADMINISTRADOR"){
?>
<div class="panel-default">
	
	<div class="panel-body" style="padding-left: 10px;background-color: white;padding-top: 0px;">
				
		  <ul class="nav nav-tabs" style="border: none"> 
					<li class="dropdown">
					    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
					      Tipo <span class="caret"></span>
					    </a>
					    <ul class="dropdown-menu" role="menu">
					    	<li><a data-rep="dia" class="rep stro vert" id="gradia">Dia</a></li>
					    	<li><a data-rep="user" class="rep vert" id="grauser">Usuario</a></li>
				            <li><a data-rep="col" class="rep vert" id="gracol">Colonia</a></li>
				            <li><a data-rep="can" class="rep vert" id="gracan">Cancelados</a></li>
				 		</ul>
					</li>
				
		  
		  	<li class="dropdown" style="margin-left: 10px;">
		    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
		      fechas <span class="caret"></span>
		    </a>
			    <ul class="dropdown-menu" role="menu">
			    	<li><a class="rep2 stro2" id="grhoy">Hoy</a></li>
			    	<li><a class="rep2" id="grayer">Ayer</a></li>
				    <li><a class="rep2" id="gr7">Ultimos 7 Dias</a></li>
		            <li><a class="rep2" id="gr15">Ultimos 15 Dias</a></li>
		            <li><a class="rep2" id="grmesa">Mes actual</a></li>
		            <li><a class="rep2" id="grmesp">Mes pasado</a></li>
		            <li><a class="rep2" id="grrng">Rango</a></li>
		            <li class="divider"></li>
		            <li>
		            </li>
			    </ul>
		  </li>
	</ul>
		
<div class="tab-content" style="background-color: white;">

 		<div class="row rn" style="opacity: 0">
		  <div class="col-xs-2 fromto" style="padding-left: 5px;padding-right: 5px;text-align: left;">DESDE<input class="form-control" type="text" id="f2" style="width: 69px;"/></div>
		  <div class="col-xs-2 fromto" style="padding-left: 5px;padding-right: 5px;text-align: left;">HASTA<input class="form-control" type="text" id="f1" style="width: 69px;"/></div>
		</div>
			<div class="tab-pane active" id="grng">
					<div class="container"  style="padding-left: 0">
					  		
					<div class="col-md-3" style="padding:0">
						<div style="position: relative;margin-left: 7px;margin-top: -39px;width: 76px;">
						</div>
					</div>
					<div class="col-md-2" style="display: none; padding:0;position: relative;margin-top: -31px;margin-left: -64px;">
						<input type="text" class="form-control" style="display: none;" id="grngusutxt" />
						<input type="text" class="form-control" style="display: none;" id="grngcoltxt" />
					</div>
					<div class="col-md-10">
						<div id="grrango">
							
						</div>
					</div>
				</div>				
			</div>
		  </div>
		</div>
	</div>
<?php
}
?>