<?php
$u = templateDriver::getData("modo");
$id = templateDriver::getData("id");
if(!$id){
	$id['id']=-1;
}
    if(!$u || $u['modo']=="dia" || $u['modo']=="can") {
?>
    <script type="text/javascript">
	//se carga la libreria que requiere asi como la versión que se necesita
      google.load('visualization', '1');
    </script>
    <script type="text/javascript">
		//crea nuestra tabla de datos para visualizarse en el explorador
		drawVisualization();
  //permite visualizar la bitacora de 15 dias
      function drawVisualization() {
		var data;
		//método por el cual van a ser enviados los datos, 
			jQuery.ajax({
		  		type: "POST", 
		  		url: "/reportes/fifteendays/<?php echo $u["modo"]; ?>/"+<?php echo "'".$id['id']."'"; ?>,
		  		dataType:"JSON",
		  		data: {}, 
		  		success:function(data){
		  			var dia0=data[0],
		  			dia1=data[1],
		  			dia2=data[2],
		  			dia3=data[3],
		  			dia4=data[4],
		  			dia5=data[5],
		  			dia6=data[6],
		  			dia7=data[7],
		  			dia8=data[8],
		  			dia9=data[9],
		  			dia10=data[10],
		  			dia11=data[11],
		  			dia12=data[12],
		  			dia13=data[13],
		  			dia14=data[14],
		  			dia15=data[15];
		  		
		  			//define las caracteristicas que va a tener graficamente en el navegador web
		  			var wrapper = new google.visualization.ChartWrapper({
				
		          chartType: 'ColumnChart',
				
		          dataTable: [dia0, dia1, dia2, dia3, dia4, dia5, dia6, dia7, dia8, dia9, dia10, dia11, dia12, dia13, dia14,dia15],
					//muestra el titulo de la gráfica de servicios con un tamaño de 700x500 con la forma de gráfica de barras
		          options: {'title': 'Servicios',"width":700,"height":500, isStacked: true},
		
		          containerId: 'grrango'

		        });
					wrapper.draw();
		         google.setOnLoadCallback(drawVisualization);
		  		},
		  		error:function(){
		  			console.log("error de extraciion de datos");
		  		}
			});
      }
    </script>
    
<?php
}else{
		?>
    <script type="text/javascript">
    drawChart();
    // Load the Visualization API and the piechart package.
    //cargar las paqueteria que se necesita, la versión, de jQuery
    google.load('visualization', '1', {'packages':['corechart']});
      
    // Set a callback to run when the Google Visualization API is loaded.
    google.setOnLoadCallback(drawChart);
      
    function drawChart() {
    	var fec=<?php echo "'".$u['modo']."'"; ?>;
    	var id=<?php echo "'".$id['id']."'"; ?>;
      var jsonData = jQuery.ajax({
          url: "/reportes/fifteendays/"+fec+"/"+id,
          dataType:"json",
          async: false
          }).responseText;
          
          jsonData = JSON.parse(jsonData);
      // Create our data table out of JSON data loaded from server.
		var opciones = {
        title: 'Servicios',
        width:600,
        height:500

    };
      var data = google.visualization.arrayToDataTable(jsonData);
      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.ColumnChart(document.getElementById('grrango'));
      chart.draw(data, opciones);
    }
    </script>
    <?php	
}

?>