<!--muestra todas los servicios de hoy a pasado mañana y los servicios pasados, pero redondos-->
<p class="lead" style="font-size: 17px!important;margin-bottom: 8px!important;"><b>LLEGADAS</b></p>
<?php

function numres2($ser)
	{
	    $i=strlen($ser);
	    switch ($i) {
	    	case 0:
		        $resasig='xxxxxx';
		        break;
		    case 1:
		        $resasig='00000'.$ser;
		        break;
		    case 2:
		        $resasig='0000'.$ser;
		        break;
			case 3:
		        $resasig='000'.$ser;
		        break;
			case 4:
		        $resasig='00'.$ser;
		        break;
			case 5:
		        $resasig='0'.$ser;
		        break;
		    case 6:
		        $resasig=$ser;
		        break;
		    default:
			    $resasig=$ser;
		}
		return $resasig;
	}
		function numnam2($nameus)
	{
		
	    $i=strlen($nameus);
				    if($i>=18) {
				    	$nameus=substr($nameus,0,18);
					}else if($i<=6){
						$nameus='xxxxxxxxxxxxxxx';
					}else{
						$nameus=$nameus;
					}
		return $nameus ;
	}
	
$hoy=date('Y-m-d');
$diag=strtotime('+1 day', strtotime($hoy));
$diag=date('Y-m-d', $diag);
$ahora=date("G:i");
$horat = strtotime ( '-3 hour' , strtotime ( $ahora ) ) ;
$horat=date("G:i", $horat);
$diaa = strtotime ( '+1 day' , strtotime ( $hoy ) ) ;
$diaa=date("Y-m-d", $diaa);
$diab = strtotime ( '+2 day' , strtotime ( $hoy ) ) ;
$diab=date("Y-m-d", $diab);
//mostrar el dia y hora de las reservaciones del dia presente
$resera=Reservation::find('all', array('select'=>"vehicle_id,reservation_time,id",'order' => 'reservation_time','conditions' =>'status <> 0 AND type = "APTO-MTY" AND reservation_date = {ts "'.$hoy.'"} AND reservation_time BETWEEN {ts "00:00"} AND {ts "'.$ahora.'"}'));
$c=0;

?>
<style>
th {
text-align: center;
}

.tablaocups > thead > tr > th, .tablaocups > tbody > tr > th, .tablaocups > tfoot > tr > th, .tablaocups > thead > tr > td, .tablaocups > tbody > tr > td, .tablaocups > tfoot > tr > td {
padding: 0px;
line-height: 1.428571429;
background-color: white;
vertical-align: top;
border-top: 1px solid #dddddd;
}
</style>
<div class="row" style="height: 300px!important;margin-bottom: 10px;">

  <div class="col-md-3 ocupacionalt">
  	<p class="ocupacionlitle"><label style="float: left"><?php
	echo "&nbsp;&nbsp;".count($resera);
?></label>PASADAS</p>
  	<div style="height: 100%;width: 100%;border: solid rgb(196, 192, 192) 1px;overflow-y: auto;background: black;">
  	<table style="width: 100%" class="tablaocup table-stripedocupocup">
  		<tbody>
  		<?php
  		//las reservaciones que ya paso su tiempo y dia del servicio 
  	  	foreach($resera as $r){
  		$hora=substr($r->reservation_time,0,5);
		if($r->vehicle_id==1){
			$auto='Aut';
		}else{
			$auto='Cam';
		}
		$ser=Service::find_by_reservation1($r->id,array("select"=>"id,user_id"));
		if(!$ser)
		    $ser=Service::find_by_reservation2($r->id,array("select"=>"id,user_id"));
		if($ser){
    		$res=numres2($ser->id);
    		$u=User::find_by_id($ser->user_id,array("select"=>"name"));
    		$nombre=$u->name;
    		$nombre=numnam2($nombre);
    		//visualiza todas las reservaciones con sus datos y un botón para visualizar su ticket
    		echo '<tr style="width: 100%;background-color: white;border-bottom: 1px solid #dddddd;"><td>'.$hora.'</td><td>R:'.$res.'</td><td>'.$auto.'</td><td style="text-align:left">'.$nombre.'</td><td style="background:while;"><button type="button" class="btn btn-default btn-xs stic" data-reser="'.$r->id.'" style="float:right"><span class="glyphicon glyphicon-eye-open"></span></button> </td></tr>';
		}
	}

  	$resera=Reservation::find('all', array('select'=>"vehicle_id,reservation_time,id",'order' => 'reservation_time','conditions' =>'status <> 0 AND type = "APTO-MTY" AND reservation_date = {ts "'.$hoy.'"} AND reservation_time BETWEEN {ts "'.$ahora.'"} AND {ts "23:59"}'));	$c=0;

  	?>
  		</tbody>
  	</table>
  	</div>
  </div>
  <div class="col-md-3 ocupacionalt">
  	  	
  	  	<p class="ocupacionlitle"><label style="float: left"><?php
	echo "&nbsp;&nbsp;".count($resera);
?></label>HOY</p>
  	<div style="height: 100%;width: 100%;border: solid rgb(196, 192, 192) 1px;overflow-y: auto;background: black;">
  	<table style="width: 100%" class="tablaocup table-stripedocupocup ">
  		<tbody>
  	<?php
  	//muestra todas las reservaciones del dia
	  foreach($resera as $r){
	    $hora=substr($r->reservation_time,0,5);
	    if($r->vehicle_id==1){
		  $auto='Aut';
	    }else{
		  $auto='Cam';
	    }
    	$ser=Service::find_by_reservation1($r->id,array("select"=>"id,user_id"));
    	if(!$ser)
    	$ser=Service::find_by_reservation2($r->id,array("select"=>"id,user_id"));
    	if($ser){
    	    $res=numres2($ser->id);
            $u=User::find_by_id($ser->user_id,array("select"=>"name"));
            $nombre=$u->name;
        	$nombre=numnam2($nombre);
        	//visualiza todas las reservaciones con sus datos y un botón para visualizar su ticket
        	echo '<tr style="width: 100%;background-color: white;border-bottom: 1px solid #dddddd;"><td>'.$hora.'</td><td>R:'.$res.'</td><td>'.$auto.'</td><td style="text-align:left">'.$nombre.'</td><td style="background:while;"><button type="button" class="btn btn-default btn-xs stic" data-reser="'.$r->id.'" style="float:right"><span class="glyphicon glyphicon-eye-open"></span></button> </td></tr>';
    	}
	 }
	 //todas las reservaciones de mañana
	 $resera=Reservation::find('all', array('select'=>"vehicle_id,reservation_time,id",'order' => 'reservation_time','conditions' =>'status <> 0 AND type = "APTO-MTY" AND reservation_date = {ts "'.$diaa.'"}'));
	
  	?>
  		</tbody>
  	</table>
  	</div>
  </div>
  <div class="col-md-3 ocupacionalt">
<p class="ocupacionlitle"><label style="float: left"><?php
	echo "&nbsp;&nbsp;".count($resera);
?></label>MAÑANA</p>
  	<div style="height: 100%;width: 100%;border: solid rgb(196, 192, 192) 1px;overflow-y: auto;background: black;">
  	<table style="width: 100%" class="tablaocup table-stripedocupocup ">
  		<tbody>
  	<?php
  	//muestra todos las reservaciones por realizar el dia de mañana
  	foreach($resera as $r){
	    $hora=substr($r->reservation_time,0,5);
	    if($r->vehicle_id==1){
		    $auto='Aut';
	    }else{
		    $auto='Cam';
	    }
	    $ser=Service::find_by_reservation1($r->id,array("select"=>"id,user_id"));
	    if(!$ser)
	        $ser=Service::find_by_reservation2($r->id,array("select"=>"id,user_id"));
	    if($ser){
    	    $res=numres2($ser->id);
    	    $u=User::find_by_id($ser->user_id,array("select"=>"name"));
    	    $nombre=$u->name;
    	    $nombre=numnam2($nombre);
    	    //visualiza todas las reservaciones con sus datos y un botón para visualizar su ticket
    	    echo '<tr style="width: 100%;background-color: white;border-bottom: 1px solid #dddddd;"><td>'.$hora.'</td><td>R:'.$res.'</td><td>'.$auto.'</td><td style="text-align:left">'.$nombre.'</td><td style="background:while;"><button type="button" class="btn btn-default btn-xs stic" data-reser="'.$r->id.'" style="float:right"><span class="glyphicon glyphicon-eye-open"></span></button> </td></tr>';
	    }
	}
//las reservaciones a realizarse para pasado mañana
  	$resera=Reservation::find('all', array('select'=>"vehicle_id,reservation_time,id",'order' => 'reservation_time','conditions' =>'status <> 0 AND type = "APTO-MTY" AND reservation_date = {ts "'.$diab.'"}'));
  	?>
  		</tbody>
  	</table>
  	</div>
  </div>
  <div class="col-md-3 ocupacionalt">
<p class="ocupacionlitle"><label style="float: left"><?php
	echo "&nbsp;&nbsp;".count($resera);
?></label>PASADO MAÑANA</p>
  	<div style="height: 100%;width: 100%;border: solid rgb(196, 192, 192) 1px;overflow-y: auto;background: black;">
  	<table style="width: 100%" class="tablaocup table-stripedocupocup ">
  		<tbody>
  	<?php
  
  foreach($resera as $r){
	$hora=substr($r->reservation_time,0,5);
  if($r->vehicle_id==1){
	  $auto='Aut';
  }else{
	  $auto='Cam';
  }
  $ser=Service::find_by_reservation1($r->id,array("select"=>"id,user_id"));
  if(!$ser)
  $ser=Service::find_by_reservation2($r->id,array("select"=>"id,user_id"));
  if($ser){
      $res=numres2($ser->id);
      $u=User::find_by_id($ser->user_id,array("select"=>"name"));
      $nombre=$u->name;
      $nombre=numnam2($nombre);
      //visualiza todas las reservaciones con sus datos y un botón para visualizar su ticket
      echo '<tr style="width: 100%;background-color: white;border-bottom: 1px solid #dddddd;"><td>'.$hora.'</td><td>R:'.$res.'</td><td>'.$auto.'</td><td style="text-align:left">'.$nombre.'</td><td style="background:while;"><button type="button" class="btn btn-default btn-xs stic" data-reser="'.$r->id.'" style="float:right"><span class="glyphicon glyphicon-eye-open"></span></button> </td></tr>';
  }
  }
$resera=Reservation::find('all', array('select'=>"vehicle_id,reservation_time,id",'order' => 'reservation_time','conditions' =>'status <> 0 AND type = "MTY-APTO" AND reservation_date = {ts "'.$hoy.'"} AND reservation_time BETWEEN {ts "00:00"} AND {ts "'.$ahora.'"}'));	

  	?>
  		</tbody>
  	</table>
  	</div>
  </div>
</div>
<!--MUESTRA LAS SALIDAS DE LOS SERVICIOS PRÓXIMOS-->
<p class="lead" style="font-size: 17px!important;margin-bottom: 8px!important;"><b>SALIDAS</b></p>
<div class="row" style="height: 300px!important;margin-bottom: 10px;">
  <div class="col-md-3 ocupacionalt">
  	<p class="ocupacionlitle"><label style="float: left"><?php
	echo "&nbsp;&nbsp;".count($resera);
?></label>PASADAS</p>
  	<div style="height: 100%;width: 100%;border: solid rgb(196, 192, 192) 1px;overflow-y: auto;background: black;">
  	<table style="width: 100%" class="tablaocup table-stripedocupocup ">
  		<tbody>
  	<?php
  
  foreach($resera as $r){
	$hora=substr($r->reservation_time,0,5);
  if($r->vehicle_id==1){
	  $auto='Aut';
  }else{
	  $auto='Cam';
  }
  $ser=Service::find_by_reservation1($r->id,array("select"=>"id,user_id"));
  if(!$ser)
  $ser=Service::find_by_reservation2($r->id,array("select"=>"id,user_id"));
  if($ser){
      $res=numres2($ser->id);
      $u=User::find_by_id($ser->user_id,array("select"=>"name"));
      $nombre=$u->name;
      $nombre=numnam2($nombre);
      //visualiza todas las reservaciones con sus datos y un botón para visualizar su ticket
      echo '<tr style="width: 100%;background-color: white;border-bottom: 1px solid #dddddd;"><td>'.$hora.'</td><td>R:'.$res.'</td><td>'.$auto.'</td><td style="text-align:left">'.$nombre.'</td><td style="background:while;"><button type="button" class="btn btn-default btn-xs stic" data-reser="'.$r->id.'" style="float:right"><span class="glyphicon glyphicon-eye-open"></span></button> </td></tr>';

  }
  }
  	$resera=Reservation::find('all', array('select'=>"vehicle_id,reservation_time,id",'order' => 'reservation_time','conditions' =>'status <> 0 AND type = "MTY-APTO" AND reservation_date = {ts "'.$hoy.'"} AND reservation_time BETWEEN {ts "'.$ahora.'"} AND {ts "23:59"}'));	$c=0;

  	?>
  		</tbody>
  	</table>
  	</div>
  </div>
  <div class="col-md-3 ocupacionalt">
<p class="ocupacionlitle"><label style="float: left"><?php
	echo "&nbsp;&nbsp;".count($resera);
?></label>HOY</p>
  	<div style="height: 100%;width: 100%;border: solid rgb(196, 192, 192) 1px;overflow-y: auto;background: black;">
  	<table style="width: 100%" class="tablaocup table-stripedocupocup ">
  		<tbody>
  	<?php
// los servicios que van de salida de todo el dia
foreach($resera as $r){
	$hora=substr($r->reservation_time,0,5);
  if($r->vehicle_id==1){
	  $auto='Aut';
  }else{
	  $auto='Cam';
  }
  $ser=Service::find_by_reservation1($r->id,array("select"=>"id,user_id"));
  if(!$ser)
  $ser=Service::find_by_reservation2($r->id,array("select"=>"id,user_id"));
  if($ser){
  $res=numres2($ser->id);
  $u=User::find_by_id($ser->user_id,array("select"=>"name"));
  $nombre=$u->name;
  $nombre=numnam2($nombre);
  //visualiza todas las reservaciones con sus datos y un botón para visualizar su ticket
  echo '<tr style="width: 100%;background-color: white;border-bottom: 1px solid #dddddd;"><td>'.$hora.'</td><td>R:'.$res.'</td><td>'.$auto.'</td><td style="text-align:left">'.$nombre.'</td><td style="background:while;"><button type="button" class="btn btn-default btn-xs stic" data-reser="'.$r->id.'" style="float:right"><span class="glyphicon glyphicon-eye-open"></span></button> </td></tr>';

  }
  }
  	$resera=Reservation::find('all', array('select'=>"vehicle_id,reservation_time,id",'order' => 'reservation_time','conditions' =>'status <> 0 AND type = "MTY-APTO" AND reservation_date = {ts "'.$diaa.'"}'));

  	?>
  		</tbody>
  	</table>
  	</div>
  </div>
  <div class="col-md-3 ocupacionalt">
<p class="ocupacionlitle"><label style="float: left"><?php
	echo "&nbsp;&nbsp;".count($resera);
?></label>MAÑANA</p>
  	<div style="height: 100%;width: 100%;border: solid rgb(196, 192, 192) 1px;overflow-y: auto;background: black;">
  	<table style="width: 100%" class="tablaocup table-stripedocupocup ">
  		<tbody>
  	<?php
//todas las reservaciones que estan registradas para mañana
foreach($resera as $r){
	$hora=substr($r->reservation_time,0,5);
  if($r->vehicle_id==1){
	  $auto='Aut';
  }else{
	  $auto='Cam';
  }
  $ser=Service::find_by_reservation1($r->id,array("select"=>"id,user_id"));
  if(!$ser)
  $ser=Service::find_by_reservation2($r->id,array("select"=>"id,user_id"));
  if($ser){
  $res=numres2($ser->id);
  $u=User::find_by_id($ser->user_id,array("select"=>"name"));
  $nombre=$u->name;
  $nombre=numnam2($nombre);
  //visualiza todas las reservaciones con sus datos y un botón para visualizar su ticket
  echo '<tr style="width: 100%;background-color: white;border-bottom: 1px solid #dddddd;"><td>'.$hora.'</td><td>R:'.$res.'</td><td>'.$auto.'</td><td style="text-align:left">'.$nombre.'</td><td style="background:while;"><button type="button" class="btn btn-default btn-xs stic" data-reser="'.$r->id.'" style="float:right"><span class="glyphicon glyphicon-eye-open"></span></button> </td></tr>';
    }
      
  }
	  	$resera=Reservation::find('all', array('select'=>"vehicle_id,reservation_time,id",'order' => 'reservation_time','conditions' =>'status <> 0 AND type = "MTY-APTO" AND reservation_date = {ts "'.$diab.'"}'));
	
  	?>
  		</tbody>
  	</table>
  	</div>
  </div>
  <div class="col-md-3 ocupacionalt">
<p class="ocupacionlitle"><label style="float: left"><?php
	echo "&nbsp;&nbsp;".count($resera);
?></label>PASADO MAÑANA</p>
  	<div style="height: 100%;width: 100%;border: solid rgb(196, 192, 192) 1px;overflow-y: auto;background: black;">
  	<table style="width: 100%" class="tablaocup table-stripedocupocup ">
  		<tbody>
  	<?php
  //los servicios que se realizaran  pasado mañana 
  foreach($resera as $r){
	$hora=substr($r->reservation_time,0,5);
  if($r->vehicle_id==1){
	  $auto='Aut';
  }else{
	  $auto='Cam';
  }
  $ser=Service::find_by_reservation1($r->id,array("select"=>"id,user_id"));
  if(!$ser)
  $ser=Service::find_by_reservation2($r->id,array("select"=>"id,user_id"));
  if($ser){
  $res=numres2($ser->id);
  $u=User::find_by_id($ser->user_id,array("select"=>"name"));
  $nombre=$u->name;
  $nombre=numnam2($nombre);
  //visualiza todas las reservaciones con sus datos y un botón para visualizar su ticket
  echo '<tr style="width: 100%;background-color: white;border-bottom: 1px solid #dddddd;"><td>'.$hora.'</td><td>R:'.$res.'</td><td>'.$auto.'</td><td style="text-align:left">'.$nombre.'</td><td style="background:while;"><button type="button" class="btn btn-default btn-xs stic" data-reser="'.$r->id.'" style="float:right"><span class="glyphicon glyphicon-eye-open"></span></button> </td></tr>';
    }
      
  }
  	?>
  		</tbody>
  	</table>
  	</div>
  </div>
</div>
