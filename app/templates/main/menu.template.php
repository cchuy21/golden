<!--barra de menú principal del proyecto-->
	<div id="menu_container">
		<ul class="nav nav-tabs" id="myTab">
		  <li class="active"><a href="#reservaciones" data-toggle="tab">Reservaciones</a></li>
		  <li><a href="#asignaciones" data-toggle="tab">Asignaciones</a></li>
		  <li><a href="#reportes" data-toggle="tab">Reportes</a></li>
		  <li><a href="#configuracion" data-toggle="tab">Configuración</a></li>
		</ul>
	</div>

<script>
$('#myTab a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})
</script>