<!--pantalla principal de login para ingresar al sistema 
	manda los datos por el método post para no dejar visualizar los datos enviados-->
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2 form-login">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<h5 align="center">Iniciar Sesión</h5>
				</div>
			</div>
			<br />
			<br />
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="row">
						<form id="login" action="/login" method="post">
							  <div class="form-group">
							  	<label for="inputuser" class="col-sm-offset-1 col-sm-4 control-label">User:</label>
							  	<div class="col-sm-6">
									<input type="text" class="form-control" id="inputuser" placeholder="Text">
								</div>
							  </div>
							  <br />
							  <br />
							  <div class="form-group">
							  	<label for="inputuser" class="col-sm-offset-1 col-sm-4 control-label">Password:</label>
							  	<div class="col-sm-6">
									<input type="password" class="form-control" id="inputpassword" placeholder="Password">
								</div>
							  </div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>