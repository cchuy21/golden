<!--muestra todos los operadores que pertenecen actualmente a la empresa-->
<div class="panel-default systemuser-block">
	
    <div class="panel-body" style="background: white; padding: 0px; height: 500px; overflow: auto;padding-left: 10px;">
    	<button class="btn btn-success tool system-add" data-typ='operator' data-toggle='tooltip' data-placement='top' title='Agregar' style="float:right;">
 		   <span class="btn-address glyphicon glyphicon-plus" style="cursor: pointer;"><strong>Agregar</strong></span>
		</button>
    	<table class="table table-hover">
    		<tr>
    			<th>
    				#
    			</th>
    			<th>
    				Nombre
    			</th>
    			<th>
    				Apodo
    			</th>
    			<th>
    				Celular
    			</th>
    			<th>
    				Turno
    			</th>
    			<th>
    				
    			</th>
    		</tr>
    	<?php
    	$systemuser=Operator::find('all',array("order"=>"username asc"));
    	$cont=1;
		$class="spn-dire sa";
    	 foreach($systemuser as $sys){    	 	
					if($sys->enable!=0){
				 	echo "<tr>
    	 			<td>{$cont}</td>
    	 			<td>".$sys->name." ".$sys->lastname."</td>
    	 			<td>".$sys->username."</td><td>";
						echo $sys->cellular;
    	 			echo "</td><td>".$sys->turn."</td>
    	 			<td>
    	 			<div class='sys' data-typ='operator' data-system='".$sys->id."'><button type='button' data-aid='{$sys->id}' class='system-delete btn exsm addr btn-default btns-golden btn-sm tool' data-toggle='tooltip' data-placement='top' title='Borrar'><span class='glyphicon glyphicon-remove-circle'></span></button><button type='button' data-aid='{$sys->id}' class='system-edit btn exsm addr btn-default btns-golden btn-sm tool' data-toggle='tooltip' data-placement='top' title='Editar'><span class='glyphicon glyphicon-pencil'></span></button></div></td>";
					$cont++;
					}
				}?>
    	</table>
    </div>
</div>
<div class="modal fade"id="systemuser-modal">
	
</div>
  