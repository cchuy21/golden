<?php
//muestra todos las unidades con sus respectivos datos
    $d = templateDriver::getData('id');
	if(!$d){

	}else{
		$u=Unity::find_by_id($d['id']);
	}



?>
<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Unidad</h4>
			</div>
		<div class="modal-body" style="padding: 10px 15px;height: 143px;">
		<table style="width: 50%!important; float: left">
			<tbody>
				<tr>
					<td>
						Tipo:
					</td>
					<td>
						<select name="type" class="form-control system_data">
			  				<?php
			  				$opciones=array('AUTOMOVIL','CAMIONETA');
							foreach($opciones as $opc){
								if($u){

				  					if($opc==$u->type){
				  						echo "<option value='".$opc."' selected>".$opc."</option>";
				  					}else{
				  						echo "<option value='".$opc."'>".$opc."</option>";
				  					}
				  				}else{
				  					echo "<option value='".$opc."'>".$opc."</option>";
				  				}
							}
			  				?>

			  			</select>
					</td>
				</tr>
				<tr>
					<td>
						Color:
					</td>
					<td>
						<input type="text" name="color" class="system_data form-control" placeholder="Color" value="<?php echo isset($u->color) ? $u->color : ''; ?>" />
					</td>
				</tr>
				<tr>
					<td>
						Modelo
					</td>
					<td>
						<input type="text" name="model" class="system_data form-control" placeholder="Modelo" value="<?php echo isset($u->model) ? $u->model : ''; ?>" />
					</td>
				</tr>
                <tr>
					<td>
						Placas
					</td>
					<td>
						<input type="text" name="license_plate" class="system_data form-control" placeholder="Placas" value="<?php echo isset($u->license_plate) ? $u->license_plate : ''; ?>" />
					</td>
				</tr>
                <tr>
					<td>
						Device-id
					</td>
					<td>
						<select name="phone_id" class="system_data form-control" >
						<?php
						echo "<option value='0'></option>";
						
						$phones=Phone::find("all");
						foreach($phones as $p){
							echo "<option value='".$p->id."'>".$p->device_id."</option>";
						}
						?>
						</select>
					</td>
				</tr>
				<tr>
					<td>
						Nº economico
					</td>
					<td>
						<input type="hidden" name="id" class="system_data form-control" value="<?php echo isset($u->id) ? $u->id : '-1'; ?>" />
						<input type="text" name="economic" class="system_data form-control" placeholder="Nº economico" value="<?php echo isset($u->economic) ? $u->economic : ''; ?>" />
					</td>
				</tr>
			</tbody>
		</table>
		<table style="width: 50%!important; float: left;margin-bottom: 30px;">
			<tbody>
				<tr>
					<td>
						Año
					</td>
					<td>
						<input type="text" name="year" class="system_data form-control" placeholder="Año" value="<?php echo isset($u->year) ? $u->year : ''; ?>" />
					</td>
				</tr>
				<tr>
					<td>
						Marca
					</td>
					<td>
						<input type="text" name="brand" class="system_data form-control" placeholder="Marca" value="<?php echo isset($u->brand) ? $u->brand : ''; ?>" />
					</td>
				</tr>
				<tr>
					<td>
						Pòliza:
					</td>
					<td>
						<input type="text" name="insurance_policy" class="system_data form-control" placeholder="Póliza de serguros" value="<?php echo isset($u->insurance_policy) ? $u->insurance_policy : ''; ?>" />
					</td>
				</tr>
				<tr>
					<td>
						Nº de serie
					</td>
					<td>
						<input type="text" name="serial_number" class="system_data form-control" placeholder="Nº póliza" value="<?php echo isset($u->serial_number) ? $u->serial_number : ''; ?>" />
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-danger close-user" data-dismiss="modal">Cancelar</button>
		<button type="button" class="btn-usersave btn btn-primary">Guardar cambios</button>
	</div>
	</div>
</div>
