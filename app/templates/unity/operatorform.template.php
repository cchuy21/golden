<?php
//muestra los datos de los operadores 
    $d = templateDriver::getData('id');
	if(!$d){
		
	}else{
		$u=Operator::find_by_id($d['id']);
	}



?>
<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Operador</h4>
			</div>
		<div class="modal-body" style="padding: 10px 15px;">
	
		
  	<table class="tablamarar"> 
  		
  		<tbody>
	  		<tr>
	  			<td class="tdstatico">
	  			<span class="tool glyphicon glyphicon-user iconoesl" data-toggle="tooltip" data-placement="top" title="Nombre"></span>
	  		</td>
	  		<td>
	  			 <div class="form-inline">
		  			
		  		    <input name="name" type="text" placeholder="Nombre" value="<?php echo isset($u->name) ? $u->name : ''; ?>" class="form-control system_data confnombre">
		            <input name="lastname" type="text" placeholder="Apellido" value="<?php echo isset($u->lastname) ? $u->lastname : ''; ?>" class="form-control system_data confnombre">   
	            </div>              
	  		</td>
	  		</tr>
	  		<tr>
	  			<td class="tdstatico">
	  				<span class="tool glyphicon glyphicon-envelope iconoesl" data-toggle="tooltip" data-placement="top" title="E-mail"></span>
	  			</td>
	  			<td>
	  			 <div class="form-inline">
		  		    <input name="username" value="<?php echo isset($u->username) ? $u->username : ''; ?>" type="text" class="form-control system_data confuser" placeholder="Apodo">
		            <input name="email" type="text" value="<?php echo isset($u->email) ? $u->email : ''; ?>" class="form-control system_data confemail" placeholder="Email">   
	            </div>              
	  			</td>
	  		</tr>
	  		<tr>
	  			<td class="tdstatico">
	  				<span class="tool glyphicon glyphicon-briefcase iconoesl" data-toggle="tooltip" data-placement="top" title="Datos laborales"></span>
	  			</td>
	  			<td>
	  				<div class="form-inline">
		           		<input name="id" style="width: 49% !important;" type="hidden" value="<?php echo isset($u->id) ? $u->id : "-1"; ?>" class="form-control system_data confemail" placeholder="Nº de empleado">   
			  			<select name="turn" class="form-control system_data confnombre" style="width: 49% !important;">
			  				<?php
			  				$opci=array('MIXTO','DOBLANDO','NOCHE');
							foreach($opci as $opc){
				  				if($u){
					  				
				  					if($opc==$u->turn){
				  						echo "<option value='".$opc."' selected>".$opc."</option>";
				  					}else{
				  						echo "<option value='".$opc."'>".$opc."</option>";
				  					}
				  				
								}else{
									echo "<option value='".$opc."'>".$opc."</option>";
								}
								
							}
			  				?>
			  				
			  			</select>
		  		    <!--<input name="group" type="text" value="<?php echo isset($u->group) ? $u->group : ' '; ?>" class="form-control system_data confnombre" placeholder="Group">
		            <input name="turno" type="text" value="<?php echo isset($u->turno) ? $u->turno : ' '; ?>" class="form-control system_data confnombre" placeholder="Turno">-->  
		            </div>              
	  			</td>
	  		</tr>
	  		<tr>
	  			<td class="tdstatico">
	  			<span class="tool glyphicon glyphicon-home iconoesl" data-toggle="tooltip" data-placement="top" title="Direccion"></span>
	  		</td>
	  		<td>
	  			 <div class="form-inline">
	  		    <input name="address" style="width: 32.5%;" type="text" value="<?php echo isset($u->address) ? $u->address : ''; ?>" class="form-control system_data confnombre" placeholder="Direccion">
	            <input name="suburb" style="width: 32.5%;" type="text" value="<?php echo isset($u->suburb) ? $u->suburb : ''; ?>" class="form-control sub system_data confnombre" placeholder="Colonia">   
	            <input name="between" style="width: 32.5%;" type="text" value="<?php echo isset($u->between) ? $u->between : ''; ?>" class="form-control system_data confnombre" placeholder="Entre Calles">   
	            </div>              
	  		</td>
	  		</tr>
	  		<tr>
	
				<td class="tdstatico">
					<span class="tool glyphicon glyphicon-earphone iconoesl" data-toggle="tooltip" data-placement="top" title="Telephone"></span>
				</td>
				<td>
					<div class="form-inline" style="padding-left: 7px; padding-right: 7px;">
					  <input name="cellular" type="text" value="<?php echo isset($u->cellular) ? $u->cellular : ''; ?>" class="system_data form-control" style="width: 50%" placeholder="Telefono Celular">
					  <input name="telephones" type="text" value="<?php echo isset($u->telephones) ? $u->telephones : ''; ?>" class="system_data form-control" style="width:49%" placeholder="Telefono Particular">
					  </div>              
				</td>
			</tr>
			<tr>
	
				<td class="tdstatico">
					<span class="tool glyphicon glyphicon-unchecked iconoesl" data-toggle="tooltip" data-placement="top" title="Telephone"></span>
				</td>
				<td>
					<div class="form-inline" style="padding-left: 7px; padding-right: 7px;">
					  <input name="picture" type="file" id="pic" class="form-control system_data" placeholder="Imagen">
					  </div>              
				</td>
			</tr>
		</tbody>
	</table> 
	</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-danger close-user" data-dismiss="modal">Cancelar</button>
			<button type="button" class="btn-usersave btn btn-primary">Guardar cambios</button>
		</div>
		</div>
	</div>