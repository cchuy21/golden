<?php
//ESTE ARCHIVO ACTUALMENTE NO ESTA EN USO
$reser=Reservation::find_by_id(1);
$users=User::find_by_id(17);
$address=Address::find_by_id(3);
$sub=Suburb::find_by_id($address->suburb_id);
$unities=Unity::find_by_id($reser->vehicle_id);
?>

<!DOCTYPE >
<html>
	<head>
		<meta charset="UTF-8">
		<style>
			body{
				width:7.5cm!important;
			}
			table{
				margin:10px 10px;
				border-collapse:collapse;
				width: 7.5cm;
			}
			table tr td{
				text-align:center;
				border: solid rgb(196, 192, 192) 1px;
			}
			.dist{
				margin-left:5px;
				margin-right:5px;
			}
			.container-pri{
				background: rgb(236,236,236); 
				width:8cm; 
				height: auto;
				padding: 20px 0px;
			}
			.mini-desc,.mini-descc{
				font-size: 12px;
				border:none;
			}
			.mini-desc{
				text-align: left!important;
			}
			.mini-descc{
				text-align: center!important;
			}
			.fn12{
				font-size:12px;
			}
		    .tabla-res{
			    margin:20px 10px 0px 10px!important;
		    }
		    .tabla-fecha{
			    margin:0px 10px 20px 10px!important;
			
		    }
		    .td-sb{
			    border-top:0px;
			    
		    }	
		    
		</style>
		<script type="text/javascript">
function PrintThisDiv(id) {
var HTMLContent = document.getElementById(id);
var Popup = window.open('about:blank', id, 'width=500,height=500');

Popup.document.writeln('<html><head>');
Popup.document.writeln('<style type="text/css">');
Popup.document.writeln('body{width:7.2cm!important}');
Popup.document.writeln('table{margin:20px 15px;	border-collapse:collapse;width: 7cm;}');
Popup.document.writeln('table tr td{text-align:center;	border: solid rgb(196, 192, 192) 1px;}');
Popup.document.writeln('.dist{margin-left:5px;	margin-right:5px;}');
Popup.document.writeln('.container-pri{	background: rgb(236,236,236); width:8cm; height: auto;	padding: 20px 0px;}');
Popup.document.writeln('.mini-desc,.mini-descc{	font-size: 12px;border:none;}');
Popup.document.writeln('.mini-desc{	text-align: left!important;	}');
Popup.document.writeln('.fn12{font-size:12px;	}');
Popup.document.writeln('.no-print{display: none;}');
Popup.document.writeln('.tabla-res{margin:20px 15px 0px 15px!important;}');
Popup.document.writeln('.tabla-fecha{margin:0px 15px 20px 15px!important;}');
Popup.document.writeln('.td-sb{border-top:0px;}');
Popup.document.writeln('</style>');
Popup.document.writeln('</head><body>');
Popup.document.writeln(HTMLContent.innerHTML);
Popup.document.writeln('</body></html>');
Popup.document.close();
Popup.focus();
}

</script>
	</head>
	<body>
		<div id="imp" class="container-pri" >
			<table class="tabla-res">
				<tbody>
					<tr>
						<td width="50%">
						Reservación	
						</td>
						<td rowspan="2";widtwidth="50%">
						<STRONG>SALIDA DOMICILIO</STRONG>
						</td>
					</tr>
					<tr>
						<td width="50%">
					    <strong><?php echo $reser->id;?></strong>
						</td>
					</tr>
					</tbody>
			</table>
			<table class="tabla-fecha">
				<tbody>
					<tr>
						<td width="60%" class="td-sb">
						
						<?php echo $reser->reservation_date;?>
						</td>
						<td width="40%" class="td-sb">
						
						<?php echo $reser->flight_hour;?>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="mini-desc">
							Pas: <input type="checkbox" class="dist"> Distinguido
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<?php echo $users->name;?> <?php echo $users->lastname;?> <?php echo $users->secondlastname;?></td>
					</tr>
					<tr>
						<td colspan="2" class="mini-desc">Notas y Observaciones de la Reservación</td>
					</tr>
					<tr>
						<td colspan="2"><?php echo $users->annotations;?></td>
					</tr>
					<tr>
						<td colspan="2" class="mini-desc">Calle Y Número:</td>
					</tr>
					<tr>
						<td colspan="2"><?php echo $address->street;?> #<?php echo $address->number;?>,<?php echo $sub->suburb;?> </td>
					</tr>
					<tr>
						<td colspan="2" class="mini-desc">Entre las Calles:</td>
					</tr>
					<tr>
						<td colspan="2"><?php echo $address->between;?></td>
					</tr>
					<tr>
						<td colspan="2" class="mini-desc">Colonia:</td>
					</tr>
					<tr>
						<td colspan="2"><?php echo $sub->suburb;?></td>
					</tr>
					<tr>
						<td colspan="2" class="mini-desc">Instrucciones de como llegar:</td>
					</tr>
					<tr>
						<td colspan="2"><?php echo $address->reference;?></td>
					</tr>
					<tr>
						<td colspan="2" class="mini-desc">Notas especiales del pasajero:</td>
					</tr>
					<tr>
						<td colspan="2"><?php echo $users->annotations;?></td>
					</tr>
				</tbody>
			</table>
		    <table class="tabla-res">
		    	<tbody>
					<tr>
						<td class="mini-desc">
							Teléfono:
						</td>
						<td class="mini-desc"></td>
						<td class="mini-desc">
							Cuadrante:
						</td> 
					</tr>
					<tr>
						<td>
							<strong><?php echo $address->telephones;?></strong>
						</td>
						<td class="mini-desc"></td>
						<td>
							<strong><?php echo $address->quadrant;?></strong>
						</td>
					</tr>
				</tbody>
		    </table>
			<table class="tabla-fecha">
				<tbody>
					<tr>
						<td colspan="2" class="mini-desc"> </td>
					</tr>
					<tr>
						<td>
							Viaje
						</td>
						<td>
							Redondo ADA(tipo)
						</td>
					</tr>
						
					<tr>
						<td colspan="2">
							<div>DATOS DE VUELO</div>
							<div class="img" class="mini-desc"><img width="40px" height="40px" align="left"/></div>
							<div>aereolina hora terminal nacional/Inter</div>
						</td>
					</tr>			
				</tbody>
			</table>
			<table>
				<tbody>
					<tr>
						<td class="mini-descc">
							PASAJEROS
						</td>
						<td class="mini-descc">
							DOMICILIOS AD
						</td>
						<td class="mini-descc">
							11º Grat.
						</td>
					</tr>
					<tr>
						<td>
							<strong><?php echo $reser->passengers;?></strong>
						</td>
						<td>
							<strong>#</strong>
						</td>
						<td class="mini-descc">
							<input type="checkbox" />
						</td>
					</tr>
				</tbody>
			</table>
			<table>
				<tbody>
					<tr>
						<td class="mini-desc">
							Vehiculo:
						</td>
						<td width="75%">
							<strong><?php echo $unities->type;?></strong>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="mini-descc">
							COSTOS Y FORMA DE PAGO
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<strong><?php echo $reser->cost;?> , <?php echo $reser->terminal;?></strong>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<strong>T. de Crédito/Débito(tipo)</strong>
						</td>
					</tr>
					<tr>
						<td class="mini-desc">
							Empresa:
						</td>
						<td>
							Particular(tipo)
						</td>
					</tr>
					<tr>
						<td class="mini-desc">
							Agencia:
						</td>
						<td>
							Particular(tipo)
						</td>
					</tr>
					<tr>
						<td class="mini-desc">
							0 de 0:
						</td>
						<td>
							
						</td>
					</tr>
				</tbody>
			</table>
			<table>
				<tbody>
					<tr>
						<td class="mini-desc">
							Recibita via: (telefonica)
						</td>
						<td class="mini-desc">
							Registró: System User
						</td>
					</tr>
					<tr>
						<td class="mini-desc" colspan="2"> </td>
					</tr>
					<tr>
						<td class="fn12">
							FECHA DE REGISTRO
						</td>
						<td class="fn12">
							IMPRESA
						</td>
					</tr>
					<tr>
						<td>
							aqui es la fchade cuando se hizo el registro de la reservación
						</td>
						<td>
							aqui es la fchade cuando se hizo la impresión de este ticked
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<a class="no-print" href="javascript:;" onclick="PrintThisDiv('imp')">Previsualizar impresión</a>
	</body>
</html>

