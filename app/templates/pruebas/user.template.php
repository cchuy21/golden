<?php
//obtiene los convenios y beneficios que tiene un usuario
    $u = User::find_by_id(53);
    if(!$u) {
        $u = new User();
        $u->id = -1;
        $u->benefit_id = -1;
        $valagr = new stdClass();
        $valagr->description = '';
        $valagr->agreement = '';
    } else {
        $valagr=Agreement::find_by_id($u->agreement_id);
        $bn= Revenue::find_by_user_id($u->id);
    }
    templateDriver::setData('user', $u);
    $convenio= Agreement::all();
    $beneficio= Benefit::all();
    
?>
<html>
<head>
	<link rel="stylesheet" href="/../static/css/bootstrap.css">
	<link rel="stylesheet" href="/../static/css/bootstrap.min.css">
	<link rel="stylesheet" href="/../static/css/style.css">
	<link rel="stylesheet" href="/../static/css/estilo.css">
	<script src="/../static/js/jquery.js" languaje="text/javascript"></script>
	<script src="/../static/js/bootstrap.js" languaje="text/javascript"></script>
	<script src="/../static/js/bootstrap.min.js" languaje="text/javascript"></script>
	<script src="/../static/js/app.js" languaje="text/javascript"></script>
	<script>
$('#tabsUsr a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})
</script>
	</head>
	<body>
<div class="panel panel-default">
    <div id="app-breadcrumbs" class="panel-heading top-bar container align-normal">
        <div id="cab">	
        	<div id="menu_user">
				<ul class="nav nav-tabs" id="tabsUsr">
		  			<li class="active"><a href="#datos" data-toggle="tab">Datos de Usuario</a></li>
		 			<li><a href="#direcciones" data-toggle="tab">Direcciones y Comentarios</a></li>
				</ul>
	</div>
	</div>
        <div id="service-id">
        </div>
    </div>
    <div class="panel-body">
    	
	<div class="container align-normal" id="user" style="transform: scale(0, 0);">
            <input type="hidden" class="user-data id-user" name="id" id="user-id" placeholder="Nombre" value="<?php echo $u->id ? $u->id : -1 ?>">
            <div class="tab-content">
	    <div class="row datos tab-pane active" id="datos">
		    <div class="col-md-4">
				<table id="name-tel" class="table table-bordered tables_c ">
					<tbody>
						<tr>
							<td colspan="2" class="active act pasajero">
								<span>Pasajero</span>
								<button type="button" class="user-button btn tool penb exsm btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Editar"  style="float: right">
									<span class="glyphicon glyphicon-pencil"></span>
								</button>
								<button type="button" class="user-button btn tool okb exsm btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Guardar"  style="float: right;display: none">
									<span class="glyphicon glyphicon-ok"></span>
								</button>
										
							</td>
						</tr>
						<tr>
							<td>
								<span class="tool glyphicon glyphicon-user" data-toggle="tooltip" data-placement="top" title="Nombre"></span>
								<?php
								if($u->distinguished==0)$col="gray";else$col="dis";
								
								
								?>
								<span class="glyphicon glyphicon-star tool <?php echo $col;?>" data-toggle="tooltip" data-placement="top" title="Distinguido" style="display: none"></span>
								
							</td>
							<td>
								<span class="name_complete spn">
								<?php echo  $u->name ? $u->name  . " " : null ;
								echo $u->lastname ? $u->lastname  . " " : null;
								echo$u->secondlastname ? $u->secondlastname  . " " :null;
								 ?>
								 </span>
								 <?php
								 if($u->distinguished!=0){
								
	                               echo '<span id="ds-star" class="glyphicon glyphicon-star toolbasde dis" data-toggle="tooltip" data-placement="top" title="" style="margin-left:10px; " data-original-title="Distinguido"></span>';
	                                }else{  	
	                                }
	                                ?>
								 <div class="form-inline">
                                        <input value="<?php echo $u->distinguished ? $u->distinguished: 0 ?>"  name="distinguished" class="user-data" id="user-distinguished" type="hidden">
                                        <input name="name" type="hidden" class="form-control user-data required" id="user-name" placeholder="Nombre" value="<?php echo $u->name ? $u->name : '' ?>">
                                        <input name="lastname" type="hidden" class="form-control user-data required" id="user-lastname" placeholder="Apellido paterno" value="<?php echo $u->lastname ? $u->lastname : '' ?>" >
                                        <input name="secondlastname" type="hidden" class="form-control user-data" id="user-secondlastname" placeholder="Apellido materno" value="<?php echo $u->secondlastname ? $u->secondlastname : '' ?>">
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<span class="glyphicon glyphicon-envelope tool" data-toggle="tooltip" data-placement="top" title="Email"></span>
							</td>
							<td>
								<span class="email_complete spn"><a href="mailto:<?php echo $u->email ? $u->email : "" ?>"><?php echo $u->email ? $u->email : "" ?></a></span>
								<input name="email" type="hidden" class="user-data form-control" id="user-email" placeholder="Email" value="<?php echo $u->email ? $u->email : '' ?>">
							</td>
						</tr>
						<tr>
							<td>
								<span class="glyphicon glyphicon-earphone tool" data-toggle="tooltip" data-placement="top" title="Teléfono"></span>
							</td>
							<td>
								<span class="tel_complete spn">
								<?php	echo $tel=$u->telephones ? $tel=$u->telephones :" "	?>
								</span>
								<input name="telephones" type="hidden" class="form-control user-data" id="user-telephones" placeholder="Telefonos" value="<?php echo $u->telephones ? $u->telephones : '' ?>">
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-3">
				<table class="table tb-service table-bordered tables_c">
					<tbody>
						<tr>
							<td colspan="3" class="active act pasajero">
								<span>Servicios</span>
							</td>
						</tr>
						<tr>
							<td>
								Total:
							</td>
							<td>
								2
							</td>
							<td>
								<button type="button" class="btn tool exsm btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Lista de Servicios">
									<span class="glyphicon glyphicon-list"></span>
								</button>
							</td>
						</tr>
						<tr>
							<td>
								Primero:
							</td>
							<td colspan="2">
								
								
								<?php 
								//obtiene el primer servicio que tiene un usuario
								$first= isset($u->getFirstService()->service_datetime) ? $u->getFirstService()->service_datetime : 'Ninguno';
								if($u->id!=-1 && $first !='Ninguno')
									$first=substr($first,0,-15);
								else {
									$first="Ninguno";
								}
								echo $first;
								?>
							</td>
						</tr><tr>
							<td>
								Último:
							</td>
							<td colspan="2">
								<?php 
								//muestra el último servicio del usuario
								$second= isset($u->getLastService()->service_datetime) ? $u->getLastService()->service_datetime : 'Ninguno';
								if($u->id!=-1 && $second !='Ninguno')
								$second=substr($second,0,-15);
								else {
									$second="Ninguno";
								}
								echo $second;
								?>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-3">
				<table class="table table-bordered tables_c">
					<tbody>
						<tr>
							<td colspan="3" class="active act pasajero">
								<span>Beneficio</span>
								<button type="button" data-toggle="tooltip" data-placement="top" title="Editar" class="beneficio-button penb tool btn exsm btn-default btn-sm" style="float: right">
										<span class="glyphicon glyphicon-pencil"></span>
									</button>
								<button type="button" class="okb beneficio-button btn tool exsm btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Guardar"  style="float: right;display:none;">
										<span class="glyphicon glyphicon-ok"></span>
								</button>
							</td>
						</tr>
						<tr>
							<td rowspan="3" class="tdselec">
								<span class="spn b">11º</span><br />
								<span class="spn gr">Gratis</span>
								<select name="benefit_id" id="user-revenue_id" style="display: none" class="form-control user-data">
	                                    <?php 
	                                    echo '<option value="" >Sin Beneficio</option>';
	                                        foreach($beneficio as $ben){
	                                            $selected = ($ben->id == $u->benefit_id)?'selected':'';
	                                            echo '<option '.$selected.' value="'.$ben->id.'" >'.$ben->description.'</option>';
	                                        }
	                                    ?>
	                            </select>
							</td>
							<td>
								Usados
							</td>
		                    <td class="center">
		                    	<?php echo isset($bn->used) ? $bn->used : '0' ?>
		                    </td>
						</tr>
						<tr>
							<td>
								Acumulados
							</td>
		                    <td class="center">
		                    	<?php echo isset($bn->joined) ? $bn->joined : '0' ?>
		                    </td>
						</tr>
						<tr>
							<td>
								Disponibles
							</td>
		                    <td id="ben_disp"  class="center">
		                    	<?php echo isset($bn->available) ? $bn->available : '0' ?>
		                    </td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-2">
				<table class="table table-bordered tables_c">
					<tbody>
						<tr>
							<td colspan="2" class="active act pasajero">
								<span>Convenios</span>
								<button type="button"  data-toggle="tooltip" data-placement="top" data-title="Editar" class="convenio-button penb tool btn exsm btn-default btn-sm" style="float: right">
									<span class="glyphicon glyphicon-pencil"></span>
								</button>
								<button type="button" class="okb convenio-button btn tool exsm btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Guardar"  style="float: right; display:none">
									<span class="glyphicon glyphicon-ok"></span>
								</button>
							</td>
						</tr>
						<tr>
							<td>
								Convenio:
							</td>
							<td>
		                         <span class="spn agreement_complete">
		                            <?php 
		                        	$co=Agreement::find_by_id($u->agreement_id);
		                               echo  isset($co->agreement) ? $co->agreement : '' 
										
		                            ?>      
		                        </span>           
		                            <select name="agreement_id" style="display: none" id="user-agreement_id" class="form-control user-data">
	                                    <?php 
	                                	
	                                        foreach($convenio as $conv){
	                                            $selected = ($conv->id == $u->agreement_id)?'selected':'';
	                                            echo '<option '.$selected.' id="'.$conv->id.'" data-convenio="'.$conv->agreement.'" data-description="'.$conv->description.'" value="'.$conv->id.'" >'.$conv->agreement.'</option>';
												}
	                                    ?>  
	                                    
	                                </select>
		                         </select>
							</td>
						</tr>
						<tr>
							<td><!-- class="text-conv"-->
								Empresa:
							</td>
							<td>
								<input name="empresa" type="hidden" class="form-control user-data" id="user-empresa" placeholder="Empresa" value="">
							</td>
						</tr>
						<tr>
							<td><!-- class="text-conv"-->
								RFC:
							</td>
							<td>
								<span class="spn rfc_complete"><?php echo $u->taxid ? $u->taxid : '' ?></span>
								<input name="taxid" type="hidden" class="form-control user-data" id="user-taxid" placeholder="RFC" value="<?php echo $u->taxid ? $u->taxid : '' ?>">
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="row tab-pane" id="direcciones">
			<div class="col-md-6 div-address">
				<div id="addresses" class="panel-address" style="display: none;">
	                
	                <?php
	                    //Load php block
	                    templateDriver::renderSection('address.base');
	                ?>
	                
	            </div>
			</div>
			<div class="col-md-6 div-coment">
				<div class="panel panel-default coment">
	                <div class="panel-heading">
	                	<strong>Comentarios adicionales:</strong>
	                </div>
	                <div class="panel-body">
	                    <textarea name="annotations" id="user-annotations" class="form-control user-data" rows="3"><?php echo $u->annotations ? $u->annotations : '' ?></textarea>
	                </div>
	            </div>
			</div>
		</div>
		</div>
	    
	        <div class="col-md-6">
                    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                    <?php
                                             //templateDriver::renderSection('address.fields');
                                    ?>
                                    </div>
                            </div>
                    </div>
                </div>
	           
            </div>
        </div>
        
    </div>
    
</div>
<div class="modal fade"id="addreess-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Modal title</h4>
			</div>
		<div class="modal-body" style="padding: 10px 50px;">
			<p>One fine body&hellip;</p>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-danger close-adress" data-dismiss="modal">Cancelar</button>
			<button type="button" class="btn-save btn btn-primary">Guardar cambios</button>
		</div>
		</div>
	</div>
</div>
</body>
</html>