<table class="table table-hover" style="margin-bottom:0px; background: #e7e7e7">
	<tr>
		<th>
			Unidad
		</th>
		<th>
			Operador
		</th>
		<th>
			
		</th>
	</tr>
	<?php
	//obtiene el número economico de las unidades
	$un=Unity::find('all',array("order"=>"economic asc"));
	//recorre todas las unidades
	foreach ($un as $u) {
		
		unset($op);
		unset($as);
		unset($reser);
		//si la unidad tiene driver
		if($u->driver){
		$var=0;
		// el id del operador dependiendo del driver
		$op=Operator::find_by_id($u->driver);
		$hoy=date("Y-m-d"); 
		
		$fecha1=date("Y-m-d");//2015-1-7 03:30:00
		if($op->id)
		$as=Assignment::find("all", array('conditions'=>'(operator_id='.$op->id.' AND unity_id='.$u->id.") AND (created_at BETWEEN {ts'".$fecha1." 00:00:00'} AND {ts'".$fecha1." 23:59:59'})"));
		//si tiene una asignación con una unidad con operador
		if($as){
		foreach($as as $a){
			//obtiene los id de las reservaciones
			$reser=Reservation::find_by_id($a->reservation_id);
			if(!$reser){
				//obtener el id del ticket de una asignación
				$reser=Ticket::find_by_id($a->ticket_id);
			}
			$var=0;
			//si las reservaciones no han sido completadas o tiene fechas futuras
				if($reser->status==2 || $reser->status=="2" || $reser->status==5 || $reser->status=="5"){
					$var=1;
					break;
				}
			if($var==1)
				break;
			else {
				$var=0;
			}
			}
		}
		if($var==1){
			
		}else{
			//muestra el numero economico y nombre del operador del vehiculo
			echo "<tr><td style='border-right: 1px solid #ddd;'>";
			echo $u->economic."</td><td  class='optasi' data-op='{$u->economic}' style='cursor: pointer;' >";
			echo $op->username ? $op->username : "";
			echo "</td><td>
			  <span title='des-asignar' data-unit='".$u->id."' style='cursor:pointer' class='tool glyphicon glyphicon-remove desasunit'></span>
			</td></tr>";
		}
		
	}
	}
	
	$un=Unity::find('all',array("order"=>"id asc"));
	foreach ($un as $u) {		
		unset($op);
		unset($as);
		unset($reser);
		if($u->driver){
		$var=0;
		$op=Operator::find_by_id($u->driver);
		
		if($op->id)
		$as=Assignment::find("all", array('conditions'=>'(operator_id='.$op->id.' AND unity_id='.$u->id.") AND (created_at BETWEEN {ts'".$fecha1." 00:00:00'} AND {ts'".$fecha1." 23:59:59'})"));
		
		if($as){
		
		foreach($as as $a){
			//el id de una reservación
			$reser=Reservation::find_by_id($a->reservation_id);
			if(!$reser){
				//muestra el id del ticket de la reservación
				$reser=Ticket::find_by_id($a->ticket_id);
			}
			$var=0;
			//si la reservación no se ha completado aún
				if($reser->status==2 || $reser->status=="2" || $reser->status==5 || $reser->status=="5"){
					$var=1;
					break;
				}
			if($var==1)
				break;
			else {
				$var=0;
			}
			}
		}
		if($var==1){
			//muestra el numero economico con el nombre del operador correspondiente
			echo "<tr><td style='color:white;background-color:rgb(52, 206, 255); border-right: 1px solid #ddd;'>";
		echo $u->economic."</td><td   data-op='{$u->id}' style='cursor: pointer; color:white; background-color:rgb(52, 206, 255);' >";
		
		
		echo $op->username ? $op->username : "";
		echo "</td><td>
			  <span title='forzar liberar' data-unit='".$u->id."' style='cursor:pointer' class='tool glyphicon glyphicon-remove libera'></span>
			</td></tr>";
		}else{
				
		}	
	}
	}

	//las unidades que estan habilitadas 
	$un=Unity::find('all',array('conditions'=>'status = 1 AND enable = 1',"order"=>"id asc"));
	foreach ($un as $u) {
		echo "<tr><td style='border-right: 1px solid #ddd;'>";
		echo $u->economic."</td><td  class='optasi' data-op='{$u->id}' style='cursor: pointer;' ></td><td></td></tr>";
	}
	$un=Unity::find('all',array('conditions'=>'enable = 1 AND status=0',"order"=>"id asc"));
	foreach ($un as $u) {
		echo "<tr><td style='border-right: 1px solid #ddd; color:white;background-color:rgb(181, 16, 16);'>";
			echo $u->economic."</td><td colspan='2' class='tool' title='{$u->cause}' data-op='{$u->id}' style='cursor: pointer; color:white;background-color:rgb(181, 16, 16);' ></td></tr>";
	}
	?>
		
	
</table>