<!--ASIGNACIONES-SALIDAS-->

<table class="table table-bordered">
	<tbody>
		<?php
		//las reservaciones que van de salida que no se han acompletado entre la ayer, hoy y mañana
		$hoy = date("Y-m-d");

		$hora = date("G:i");
		if (strtotime($hora) < strtotime(date("G:i", strtotime("04:00"))) && strtotime($hora) > strtotime(date("G:i", strtotime("00:00")))) {

			$ayer = strtotime('-1 day', strtotime($hoy));
			$ayer = date('Y-m-d', $ayer);
			$res = Reservation::find("all", array("order" => "reservation_date asc, reservation_time asc", "conditions" => "(type='MTY-APTO' AND status=2 AND reservation_date = {ts '" . $ayer . "'})"));
			impresa($res);
			$reser = Reservation::find("all", array("order" => "reservation_date asc, reservation_time asc", "conditions" => "(type='MTY-APTO' AND status=2 AND reservation_date = { ts '" . $hoy . "'})"));

			impresa($reser);
		} else {

			$reser = Reservation::find("all", array("order" => "reservation_date asc, reservation_time asc", "conditions" => "(type='MTY-APTO' AND status=2 AND reservation_date = { ts '" . $hoy . "'})"));

			impresa($reser);
		}
		$mana = strtotime('+1 day', strtotime($hoy));
		$mana = date('Y-m-d', $mana);
		$reser = Reservation::find("all", array("order" => "reservation_date asc, reservation_time asc", "conditions" => "(type='MTY-APTO' AND status=5) OR (type='MTY-APTO' AND status=2 AND reservation_date = { ts '" . $mana . "'})"));
		impresa($reser);
		function impresa($reser) {
			$c = 0;
			foreach ($reser as $r) {
				$as = Assignment::find_by_reservation_id($r -> id);
				$hora = $as -> updated_at;
				//obtiene la hora en la que fue creada la asignación
				$hora = substr($hora, 17, 8);

				$ser = Service::find_by_reservation1($r -> id);
				if ($ser -> payment == "Efectivo MN" || $ser -> payment == "Tarjeta de Credito/Debito" || $ser -> payment == "Firma por cobrar") {
					$clas = "asignad";
				} else {
					$clas = "asignados";
				}
				$tip = ' (SEN)';
				//si exise una reservacion2 se determina redondo
				if ($ser -> reservation2) {
					$tip = ' (RED)';
				}
			
				if (!$ser) {
					$ser = Service::find_by_reservation2($r -> id);
					$tip = ' (COM)';
					$clas = "asignados";
				}
				if ($ser -> payment == "Tarjeta de Credito/Debito") {
					$pay = "TC";
				} else if ($ser -> payment == "Efectivo MN") {
					$pay = "EF";
				} else if ($ser -> payment == "movil") {
					if ($ser -> pagado)
						$pay = "Movil-pagada";
					else
						$pay = "Movil";
				} else {
					$pay = "FxC";
				}
				$tip = $tip . " " . $pay;
				$resasig = numres($ser -> id);
				$operres = Operator::find_by_id($as -> operator_id);
				$uni = Unity::find($as -> unity_id);
				$asbol = $uni -> economic . ' ' . $operres -> username;
				$user = $ser -> user;
				$emp=User::find_by_id($user->business_id);
				if(!$emp){
					$emp=User::find_by_id(5529);
				}
				$emp=substr($emp->name, 0,3);
				$nameus = $user -> name;
				$nameus = numnam($nameus);
				$tip=$tip."    <label style='margin-left:10px;'>".$emp."</label>";
				if ($r -> vehicle_id == '1') {$vei = ' Auto: ';
				} else {$vei = ' Cam: ';
				}
				$pass = numpas($r -> passengers);
				$type = $vei . $pass . "<label style='width:115px;'>" . $tip . '</label> ';
				$vuelo = $r -> flight_number;
				$dir = direc($r -> addresses);
				$tix = 'salida';
				//obtiene los primeros 90 caracteres de la dirección del servicio
				$dir = numdir($dir, $tix);
				if ($user -> telephones) {
					$telefono = $user -> telephones;
				} else {
					$telefono = 'xxxxxxxxxx';
				}
				if ($r -> terminal && $r -> terminal != 'null' && $r -> terminal != "Terminal") {
					$terminal = $r -> terminal;
				} else {
					$terminal = "X";
				}
				if ($c == 0) {
					$ba = "rgb(0, 191, 252)";
					$c++;
				} else {
					$ba = "rgb(52, 206, 255)";
					$c = 0;
				}
				if($r->abierto){
					$fechaHoy2 = strtotime("$r->reservation_date");
					if($r->reservation_date>0)
					$r->reservation_time="abierta";
				}
				$tip = authDriver::getSUser() -> group;
				echo "<tr class='primary' style='text-align: left;'>";
				//si el usuario en sesión es administrador, se accede a la opción
				if ($tip == "ADMINISTRADOR")
					echo "<td style='color:white;background-color: " . $ba . ";width: 20px;'><button type='button' class='btn btn-default btn-xs remo' data-reser='" . $r -> id . "' style='float:right;'><span class='glyphicon glyphicon-remove'></span></button> </td>";
				echo "<td style='color:white;background-color: " . $ba . ";width: 20px;'><button type='button' class='btn btn-default btn-xs stic' data-reser='" . $r -> id . "' style='float:right;'><span class='glyphicon glyphicon-eye-open'></span></button> </td><td style='color:white;background-color: " . $ba . ";width: 20px;'><button type='button' data-reser='" . $r -> id . "' class='btn btn-default btn-xs reasig' ><span class='glyphicon glyphicon-transfer'></span></button> </td><td style='color:white; background: rgb(0, 194, 255); margin-top:2px;cursor: pointer;' data-reser='" . $r -> id . "' class='" . $clas . "'><label style='font-weight: initial;margin-bottom: 0px;width: 120px;'>" . $asbol . "</label> - " . templateDriver::timelohrs($r -> reservation_date, $r -> reservation_time) . " [<label data-time='" . $hora . "' class='stopwatch'  style='font-weight: initial;margin-bottom: 0px;'>00:00</label>] - <strong>R: " . $resasig . "</strong> - " . $type . " - " . $terminal . " - <label style='font-weight: initial;margin-bottom: 0px;width: 160px;'>" . $nameus . "</label> - Tel: <label style='font-weight: initial;margin-bottom: 0px;width: 80px;'>" . $telefono . "</label> - Dir: " . $dir . " </td></tr>";
			}
		}
		?>
	</tbody>
</table>

