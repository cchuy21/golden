<!---->
<div class="panel panel-default">
    <div class="panel-heading">Direcciones:</div>
    <div class="panel-body">
        <div class="tabbable">
        <?php
            $data = templateDriver::getData("base");
            $user = User::find_by_id($data);

            echo '<ul class="nav nav-tabs">';
            $i = 1;
			//muestra las direcciones que tienen los usuarios con su id y alias 
            foreach($user->addresses as $address){           
        ?>
                <li class="<?php echo ($i === 1) ? "active" : '' ?>"><a href="#address<?php echo $address->id ?>" data-toggle="tab"><?php echo $address->alias ?></a></li>
        <?php
                $i++;
            }
            echo '</ul>';
            echo '<div class="tab-content">';
            $i = 0;
            foreach($user->addresses as $address){
                $d = array(
                    'i' => $i,
                    'address' => $address
                );
                templateDriver::renderSection("reservation.fieldsaddress", $d);
                $i++;
            }
            echo '</div>';
        ?>
        </div>
    </div>
</div>
