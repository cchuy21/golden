<!--MUESTRA LAS OPCIONES QUE TIENE EL SERVICIO DE APTO A DOMICILIO--> 
<table class="table table-condensed serv back_white">
	<tbody>
		<tr>
			<td colspan="5">
				El servicio Apto a Domicilio se requiere el:
			</td>
		</tr>
		<tr>
			<td colspan="3" class="col-7">
				<div class="col-12">
					<input type="date" class="form-control" />
				</div>
			</td>
			<td colspan="2" class="col-5">
				<div class="col-12">
					<label class="control-label col-3 no-pad">a las:</label>
					<div class="col-7 no-pad" >
						<input type="time" class="form-control">
					</div>
					<label class="control-label no-pad col-sm-offset-1 col-1">hrs.</label>
				</div>
			</td>
		</tr>
		<tr>
			<td style="width: 22%; padding-right: 0px !important;">
				  <select class="form-control">
				  	<option value="1">aereolina 1</option>
				  	<option value="2">aereolina 2</option>
				  	<option value="3">aereolina 3</option>
				  	<option value="4">aereolina 4</option>
				  	<option value="5">aereolina 5</option>
				  </select>
			</td>
			<td>
				<input type="text" class="form-control" placeholder="Vuelo" />
			</td>
			<td  style="width: 22%" class="no-pad">
				  <select class="form-control">
				  	<option value="1">terminal 1</option>
				  	<option value="2">terminal 2</option>
				  	<option value="3">terminal 3</option>
				  	<option value="4">terminal 4</option>
				  	<option value="5">terminal 5</option>
				  </select>
			</td>
			<td class="col-3">
				  <select class="form-control">
				  	<option value="1">Nacional</option>
				  	<option value="2">Internacional</option>
				  </select>
			</td>
			<td class="col-3">
					<input type="text" class="form-control" placeholder="Hora" />
			</td>
		</tr>
	</tbody>
</table>