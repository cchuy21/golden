<?php

    $u = User::find_by_id(templateDriver::getData("base"));
?>

<div id="app-breadcrumbs" class="top-bar container align-normal">
    <a href="/" class="btn btn-primary">< Regresar</a>
    <div id="service-id">
        <h4># de servicio <span class="label label-primary">00000</span></h4>
    </div>
</div>

<div class="container align-normal" id="user" style="transform: scale(0, 0);">
    <input type="hidden" class="user-data" name="id" id="user-id" placeholder="Nombre" value="<?php echo $u->id ? $u->id : -1 ?>">
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div id="user-name-txt" class="col-md-12"><h3><?php echo $u->name . " " . $u->lastname . " " . $u->secondlastname ?></h3></div>
                <br>
                <div id="user-name-fields" class="col-md-12">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input name="name" type="text" class="form-control user-data" id="user-name" placeholder="Nombre" value="<?php echo $u->name ? $u->name : '' ?>">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input name="lastname" type="text" class="form-control user-data" id="user-lastname" placeholder="Apellido paterno" value="<?php echo $u->lastname ? $u->lastname : '' ?>">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input name="secondlastname" type="text" class="form-control user-data" id="user-secondlastname" placeholder="Apellido materno" value="<?php echo $u->secondlastname ? $u->secondlastname : '' ?>">
                            </div>
                        </div>
                    </div>
                </div>
                    
            </div>
            <div class="row">
                <div class="col-md-offset-8 col-md-4">
                    <div class="checkbox">
                        <label>
                            <input checked="<?php echo $u->distinguished ? 'true' : 'false' ?>" name="distinguished" class="user-data" type="checkbox"> Pasajero distinguido
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="exampleInputEmail1"># de servicios</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Primer servicio</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Último servicio</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered">
                        <tr>
                            <th colspan="2" class="row-gray">Beneficio adicional</th>
                        </tr>
                        <tr>
                            <td colspan="2">Tipo beneficio</td>
                        </tr>
                        <tr>
                            <td>Acumulados</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Usados</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Disponibles</td>
                            <td></td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered">
                        <tr class="row-gray">
                            <th colspan="2">Convenios</th>
                        </tr>
                        <tr>
                            <td colspan="2">Sin convenio</td>
                        </tr>
                        <tr>
                            <td colspan="2">Particular</td>
                        </tr>
                        <tr>
                            <td>RFC:</td>
                            <td>GAFB09090909</td>
                        </tr>
                    </table>
                </div>
            </div> 
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Comentarios adicionales:</div>
                        <div class="panel-body">
                            <textarea name="annotations" id="user-annotations" class="form-control user-data" rows="3"><?php echo $u->annotations ? $u->annotations : '' ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="user-username" class="col-md-2">Email</label>
                                <div class="col-md-10">
                                    <input name="username" type="text" class="select2 form-control user-data" id="user-username" placeholder="Email" value="<?php echo $u->username ? $u->username : '' ?>">
                                </div>
                            </div>
                            <hr />
                            <div class="form-group">
                                <label for="user-telephones" class="col-md-2">Teléfonos</label>
                                <div class="col-md-10">
                                    <input name="telephones"  type="text" class="select2 form-control user-data" id="user-telephones" placeholder="Telefonos" value="<?php echo $u->telephones ? $u->telephones : '' ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="addresses" class="row">
                <div class="col-md-12">
                <?php
                    //Load php block
                    templateDriver::renderSection('reservation.address');
                ?>
                </div>
            </div>
        </div>
    </div>
</div>

