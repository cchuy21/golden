<!--formato para agregar una reservación -->
<div style="width: 600px;">
	<div class="col-md-12">
	<div class="row"> 
		<div class="col-md-3 col-md-offset-3">
			<p class="">usuario(administrador)</p>
		</div>
		<div class="col-md-3">
			<a><p class="text-right">Logout</p></a>
		</div>
	</div>
	<br />
	<br />
	<div class="col-md-6 col-md-offset-3">
		<p class="text-center"><button type="button" class="btn btn-default">Nuevo Usuario</button></p>
	</div>
	<br />
	<br />
	<br />
	<div class="col-md-8 col-md-offset-2">
		<div class="row">
			<div class="col-md-offset-1 col-md-10">
				<h4 class="text-center">Nueva reservación</h4>
				<br />
				<form class="form-horizontal" action="#" method="post">
					<div class="form-group">
						<label for="inputusuario" class="col-sm-2 control-label">Nombre:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="bus_nombre">
						</div>
					</div>
					<div class="form-group">
						<label for="inputtel" class="col-sm-2 control-label">Tel:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="bus_tel">
						</div>
					</div>
					<div class="form-group">
						<label for="inputemail" class="col-sm-2 control-label">Email:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="bus_email">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<p class="text-center"><button type="submit" class="btn btn-default">Reservar</button></p>
						</div>
					</div>									
				</form>
			</div>
		</div>
	</div>
	<div class="col-md-8 col-md-offset-2">
		<div class="row">
			<div class="col-md-offset-1 col-md-10">
				<h4 class="text-center">Editar reservación</h4>
				<br />
				<form class="form-horizontal" action="#">
					<div class="form-group">
						<label for="num_res" class="col-sm-5  control-label"># de Reservación:</label>
						<div class="col-sm-7">
							<input type="text" class="form-control" id="edit_res">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<p class="text-center"><button type="submit" class="btn btn-default">Editar</button></p>
						</div>
					</div>		
				</form>
			</div>
		</div>
	</div>
	</div>
</div>