<?php
    $d = templateDriver::getData('fieldsaddress');
    $i = $d['i'];
    $address = $d['address'];
?>
<div class="tab-pane address-tab <?php echo ($i === 0) ? "active" : '' ?>" data-id="<?php echo $address->id ?>" id="address<?php echo $address->id ?>">
    <div class="row">
        <div class="col-md-8">
            <div class="form-group">
                <input type="text" class="form-control" id="street-<?php echo $address->id ?>" name='address[<?php echo $i ?>][street]' placeholder="Calle" value="<?php echo $address->street ? $address->street : '' ?>">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <input type="text" class="form-control" id="number-<?php echo $address->id ?>" name='address[<?php echo $i ?>][number]' placeholder="Número" value="<?php echo $address->number ? $address->number : '' ?>">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <input type="text" class="form-control" id="between-<?php echo $address->id ?>" name='address[<?php echo $i ?>][between]' placeholder="Entre calle1 y calle2" value="<?php echo $address->between ? $address->between : '' ?>">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <input type="text" class="form-control" id="suburb-<?php echo $address->id ?>" name='address[<?php echo $i ?>][suburb]' placeholder="Colonia" value="<?php echo $address->suburb ? $address->suburb : '' ?>">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="form-group">
                <input type="text" class="form-control" id="quadrant_id-<?php echo $address->id ?>" name='address[<?php echo $i ?>][quadrant_id]' placeholder="Cuadrante" value="<?php echo $address->quadrant_id ? $address->quadrant_id : '' ?>">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <input type="text" class="form-control" id="map-<?php echo $address->id ?>" name='address[<?php echo $i ?>][map]' placeholder="Mapa" value="<?php echo $address->map ? $address->map : '' ?>">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            Referencia:
            <textarea id="reference-<?php echo $address->id ?>" name='address[<?php echo $i ?>][reference]' class="form-control" rows="3"><?php echo $address->reference ? $address->reference : '' ?></textarea>
        </div>
    </div>
</div>
