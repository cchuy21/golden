
<div class="panel-default systemuser-block">
	<!--muestra todos los boletos deshabilitados con la opción de borrar o habilitar-->
		
		<?php echo "<div class='ticsta' data-typ='tickets' data-system='".$sys->id."' style='float: right; '><button type='button' class='all-delete btn exsm addr btn-default btns-golden btn-sm tool' data-toggle='tooltip' data-placement='top' title='Borrar'><span class='glyphicon glyphicon-remove-circle'></span></button><button type='button' class='all-hab btn exsm addr btn-default btns-golden btn-sm tool' data-toggle='tooltip' data-placement='top' title='Habilitar'><span class='glyphicon glyphicon-ok'></span></button></div></td></tr>";?>

    <div class="panel-body panel-frm" style="background: white; padding: 0px; height: 500px;">
    	<div class="col-md-6 desab" style=" overflow: auto; height: 100%"><div style="height: 20px;"></div>
    		<div class="col-md-12"><strong>Deshabilitados</strong></div>
	    	<table class="table table-hover">
	    		<thead>
		    		<tr><th>
		    			<input type="checkbox" id="allboldes" />
		    			</th>
		    			<th>
		    				#
		    			</th>
		    			<th>
		    				folio
		    			</th>
		    			<th>
		    				fecha
		    			</th>
		    			<th>
		    				
		    			</th>
		    		</tr>
		    	</thead>
		    	<tbody>
		    		
	    	<?php
	    	$hoy=date("Y-m-d");	
							$otra = strtotime ( '+1 day' , strtotime ( $hoym ) ) ;
							$tic=Ticket::find("all", array("order"=>"created_at desc,time desc", "conditions" =>"status=1 AND created_at < {ts '".$hoy."'}","limit"=>"250"));
	    	$cont=1;
			$class="spn-dire sa";
	    	 foreach($tic as $sys){
					 	echo "<tr>
					 	<td><input type='checkbox' class='tickcheck' value='$sys->id' ></td>
	    	 			<td>{$cont}</td>
	    	 			<td>".$sys->folio."</td>
	    	 			<td>".templateDriver::timelo2($sys->created_at,1)."</td>
	    	 			<td>
	    	 			<div class='ticsta' data-typ='tickets' data-system='".$sys->id."'><button type='button' data-aid='{$sys->id}' class='ticket-delete btn exsm addr btn-default btns-golden btn-sm tool' data-toggle='tooltip' data-placement='top' title='Borrar'><span class='glyphicon glyphicon-remove-circle'></span></button><button type='button' data-aid='{$sys->id}' class='ticket-hab btn exsm addr btn-default btns-golden btn-sm tool' data-toggle='tooltip' data-placement='top' title='Habilitar'><span class='glyphicon glyphicon-ok'></span></button></div></td></tr>";
						$cont++;
				} 
	    	?>
	    	</tbody>
	    	</table>
    	</div>
    	<div class="col-md-6 habi" style=" overflow: auto; height: 100%">
<div class="col-md-12"><strong>Habilitados</strong></div>
	    	<table class="table table-hover">
	    		<thead>
		    		<tr>
		    			<th>
		    				<input type="checkbox" id="allbolhabi" />
		    			</th>
		    			<th>
		    				#
		    			</th>
		    			<th>
		    				folio
		    			</th>
		    			<th>
		    				fecha
		    			</th>
		    			<th>
		    				
		    			</th>
		    		</tr>
		    	</thead>
		    	<tbody>
		    		
	    	<?php
	    	$hoy=date("Y-m-d");	
							$otra = strtotime ( '+1 day' , strtotime ( $hoym ) ) ;
							$tic=Ticket::find("all", array("order"=>"created_at desc,time desc", "conditions" =>"status=4 AND created_at < {ts '".$hoy."'}"));
	    	$cont=1;
			$class="spn-dire sa";
	    	 foreach($tic as $sys){
					 	echo "<tr>
					 	<td><input type='checkbox' class='tickcheck' value='$sys->id' ></td>
	    	 			<td>{$cont}</td>
	    	 			<td>".$sys->folio."</td>
	    	 			<td>".templateDriver::timelo2($sys->created_at,1)."</td>
	    	 			<td>
	    	 			<div class='ticsta' data-typ='tickets' data-system='".$sys->id."'><button type='button' data-aid='{$sys->id}' class='ticket-delete btn exsm addr btn-default btns-golden btn-sm tool' data-toggle='tooltip' data-placement='top' title='Borrar'><span class='glyphicon glyphicon-remove-circle'></span></button><button type='button' data-aid='{$sys->id}' class='ticket-hab btn exsm addr btn-default btns-golden btn-sm tool' data-toggle='tooltip' data-placement='top' title='Habilitar'><span class='glyphicon glyphicon-ok'></span></button></div></td></tr>";
						$cont++;
				} 
			 
	    	?>
	    	
	    	</tbody>
	    	</table>
    	</div>

    </div>
</div>

  