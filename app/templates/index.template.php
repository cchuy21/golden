<!DOCTYPE HTML>
<html> <!--archivo principal del servicio, se mandan a llamar los estilos css y js-->
    <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
        
        <title>Golden Transportaciones</title>
        
	
        <!-- Bootstrap -->
        <link href="/static/css/bootstrap.css" rel="stylesheet">
        <link href="/static/css/animate.css" rel="stylesheet">
        <link href="/static/css/selectize.css" rel="stylesheet">
        <link href="/static/css/selectize.bootstrap3.css" rel="stylesheet">
        <link href="/static/css/jquery.datetimepicker.css" rel="stylesheet">
        <link href="/static/css/base/jquery.ui.all.css" rel="stylesheet">
        <link href="/static/css/bootstrap-editable.css" rel="stylesheet">
        <link href="/static/css/shadowbox.css" rel="stylesheet">
        <link href="/static/css/style.css" rel="stylesheet">
        <link href="/static/css/estilo.css" rel="stylesheet">
        <link href="/static/css/jquery.fs.stepper.css" rel="stylesheet">
        <link href="/static/css/notifIt.css" rel="stylesheet">
        <link href="/static/tags/bootstrap-tagsinput.css" rel="stylesheet">
        <link href="/static/js/sorter/styles.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    </head>
    <body>
        <?php
        //se inicia sesión para mostrar el menu 
            if(authDriver::isLoggedin())  {
                templateDriver::renderSection('systemuser.menu');
            }
            templateDriver::content();
        ?>
  </body>
  <script>
  	function cerrara(){
		alert("cerra");
	}
  </script>
 <!-- <script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script>-->
	  <script src="/static/js/jquery.js" type="text/javascript"></script>
  <script src="/static/js/json2.js" type="text/javascript"></script>
  <script src="/static/js/underscore.js" type="text/javascript"></script>
  <script src="/static/js/backbone.js" type="text/javascript"></script>
  <script src="/static/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="/static/js/alajax.plugin.js" type="text/javascript"></script>
  <script src="/static/js/transit.js" type="text/javascript"></script>
  <script src="/static/js/selectize.min.js" type="text/javascript"></script>
  <script src="/static/js/jquery.datetimepicker.js" type="text/javascript"></script>
  <script src="/static/js/bootstrap.touchspin.js" type="text/javascript"></script>
  <script src="/static/js/bootstrap-editable.min.js" type="text/javascript"></script>
  <script src="/static/js/simplelog.js" type="text/javascript"></script>
  <script src="/static/js/models.js" type="text/javascript"></script>
  <script src="/static/js/ui/jquery.ui.core.js"></script>
  <script src="/static/js/ui/jquery.ui.widget.js"></script>
  <script src="/static/js/ui/jquery.ui.mouse.js"></script>
  <script src="/static/js/ui/jquery.ui.draggable.js"></script>
  <script src="/static/js/ui/jquery.ui.position.js"></script>
  <script src="/static/js/ui/jquery.ui.resizable.js"></script>
  <script src="/static/js/ui/jquery.ui.button.js"></script>
  <script src="/static/js/ui/jquery.ui.dialog.js"></script>
  <script src="/static/js/ui/jquery.ui.menu.js"></script>
  <script src="/static/js/ui/jquery.ui.autocomplete.js"></script>
  <script src="/static/js/shadowbox.js"></script>
  <!--<script src="/static/js/jquery.ui.map.full.min.js" type="text/javascript"></script>-->
  <script src="/static/js/jquery.fs.stepper.js" type="text/javascript"></script>
  <script src="/static/js/jquery.masked.js" type="text/javascript"></script>
  <script src="/static/tags/bootstrap-tagsinput.min.js" type="text/javascript"></script>
  <script src="/static/tags/bootstrap-tagsinput.js" type="text/javascript"></script> 
  <script src="/static/js/notifIt.js" type="text/javascript"></script>
  <script src="/static/js/jquery.timer.js"></script>
  <script src="/static/js/sorter/jquery.tablesorter.js"></script>
  
   <?php
      $url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];
      
      if($url!="http://resv.goldentransportaciones.net:80/systemuser/cuenta")
  if(authDriver::getSUser()->username=="SLADER2110")
  echo '<script src="/static/js/app.js" type="text/javascript"></script>';
else{
      echo '<script src="/static/js/app.js" type="text/javascript"></script>';

}
	  else
  echo '<script src="/static/js/system.js" type="text/javascript"></script>';
   if(!isset($_SESSION)) 
    { 
       
  if(authDriver::getSUser()->group=="ADMINISTRADOR"){
  ?>
  <script type="text/javascript" src="//www.google.com/jsapi"></script>
  <div id="scrgraficas">
	  <?php
	 	 templateDriver::renderSection("reports.sevendays");
	  ?>
  </div>
  <?php
  }
    }
  ?>
  
  <div class="modal fade" id="modal-assigments" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  
</div>
  </html>