<?php
//creación de una nueva reservación para un usuario especifico
    
	$u=User::find_by_id(templateDriver::getData("window"));
	
    if(!$u) {
        $u = new User();
        $u->id = -1;
        $u->benefit_id = -1;
        $valagr = new stdClass(); 
        $valagr->description = '';
        $valagr->agreement = '';
    } else {
        $valagr=Agreement::find_by_id($u->agreement);
        $bn= Revenue::find_by_user_id($u->id);
    }
    templateDriver::setData('user', $u);
    if($u->business_id){
		$emp=User::find_by_id($u->business_id);
		
	}else{
		$emp=User::find_by_id(5529);
	}
    $beneficio= Benefit::all();
	//SELECCIONA LOS DATOS DE UN USUARIO ESPECIFICO PARA REALIZAR SU RESERVACIÓN Y MOSTRAR DATOS DE SU HISTORIAL
?>
<div class="container" id="content" style="padding: 0;">
<div class="panel panel-default">
	<div id="separador"></div>
    <div id="app-breadcrumbs" class="panel-heading top-bar container align-normal">
        <div id="cab">
        	<div id="menu_user">
				Datos del Pasajero
			</div>
		</div>
		<span class="close close-service" aria-hidden="true">Cancelar &times;</span>
        <div id="service-id">
            <!--<h4 style="font-size: 14px;"># Servicio <span class="label label-primary">00000</span></h4>-->
        </div>
    </div>
    <div class="panel-body user-panel">
	<div class="container align-normal" id="user" style="transform: scale(0, 0); padding: 0px">

        <input type="hidden" class="user-data id-user" name="id" id="user-id" placeholder="Nombre" value="<?php echo $u->id ? $u->id : -1 ?>">
		    <div class="row datos" id="datos">
		    	<table class="table table-bordered tables_c">
		    		<tbody>
		    			<tr>
		    				<td colspan="2" class="active act bord pasajero" style="width: 40%;">
								<span>Pasajero</span>		
							</td>
		    				<td colspan="3" class="active act pasajero bord" width="20%">
								<span>Servicios</span>
							</td>
							<td colspan="3"width="12%" class="active act pasajero bord">
								<span>Beneficio</span>
							</td>
							<td colspan="2" class="active act pasajero bord">
								<span>Convenios</span>
								<button type="button"  data-toggle="tooltip" data-placement="top" data-title="Editar" class="convenio-button penb btns-golden tool btn exsm btn-default btn-sm" style="float: right">
									<span class="glyphicon glyphicon-pencil"></span>
								</button>
								<button type="button" class="okb convenio-button btn tool btns-golden user-save exsm btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Guardar"  style="float: right; display:none; margin: 0;">
									<span class="glyphicon glyphicon-ok"></span>
								</button>
							</td>
		    			</tr>
		    			<tr>
		    				<td class="tdicon">
								<span class="tool glyphicon glyphicon-user" data-toggle="tooltip" data-placement="top" title="Nombre"></span>
								<?php
								if($u->distinguished==0)$col="gray";else$col="dis";
								
								
								?>
								<span class="glyphicon glyphicon-star tool <?php echo $col;?>" id="ds-starh" data-toggle="tooltip" data-placement="top" title="Distinguido" style="display: none"></span>
								
							</td>
							<td class="bord">
								<span class="name_complete spn"><b>
								<?php echo  $u->name ? $u->name  . " " : null ;
								 ?>
								 </b>
								 </span>
								 <?php
								 if($u->distinguished!=0){
	                               echo '<span id="ds-star" class="glyphicon glyphicon-star toolbasde dis" data-toggle="tooltip" data-placement="top" title="" style="margin-left:10px; " data-original-title="Distinguido"></span>';
	                                }else{  	
	                                }
	                                ?>
								 <div class="form-inline">
                                        <input value="<?php echo $u->distinguished ? $u->distinguished: 0 ?>"  name="distinguished" class="user-data" id="user-distinguished" type="hidden">
                                        <input name="name" type="hidden" class="form-control user-data required" id="user-name" placeholder="Nombre" value="<?php echo $u->name ? $u->name : '' ?>">
								</div>
							</td>
							<td>
								Total:
							</td>
							<td class="cservices" style="width: 2%">
								<b>
								<?php
								if($u->id!=6436 && $u->id!=27954){
									$ser=Service::all(array('conditions' => 'user_id = '.$u->id));

									echo count($ser);
									}
								?>
								</b>
							</td>
							<td class="bord" style="width: 10%;">
								<button type="button" class="btn tool btns-golden bt-list btn-default btn-sm" data-toggle="modal" data-target="#listser" data-toggle="tooltip" data-placement="top" title="Lista de Servicios">
									<span class="glyphicon glyphicon-list"></span>
								</button>
							</td>
							<td rowspan="3" class="tdselec">
								<span class="spn b">11º</span><br />
								<span class="spn gr">Gratis</span>
								<select name="benefit_id" id="user-revenue_id" style="display: none" class="form-control user-data">
	                                    <?php 
	                                        foreach($beneficio as $ben){
	                                            $selected = ($ben->id == $u->benefit_id)?'selected':'';
	                                            echo '<option '.$selected.' value="'.$ben->id.'" >'.$ben->description.'</option>';
	                                        }
											echo '<option value="" >Sin Beneficio</option>';
	                                    ?>
	                            </select>
							</td>
							<td>
								Usados
							</td>
		                    <td class="center bord benusa">
		                    	<?php echo isset($bn->used) ? $bn->used : '0' ?>
		                    </td>
		                    <td>
								Convenio:
							</td>
							<td class="bord">
		                         <span class="spn agreement_complete">
		                            <?php 
		                        	$co=Agreement::find_by_id($u->agreement);
		                               echo $co->agreement;
									   
									   $cov=explode(",",$emp->agreements_bus);
		                            ?>      
		                        </span>           
		                            <select name="agreement" style="display: none" id="user-agreement_id" class="form-control user-data">
                                    <?php 
									if($cov)                             			
                                        foreach($cov as $cnv){
                                        	echo $cnv;
                        					$conv=Agreement::find_by_id($cnv);
                                        	$selected = ($conv->id == $u->agreement)?'selected':'';
                                        	echo '<option '.$selected.' id="'.$conv->id.'" data-convenio="'.$conv->agreement.'" data-description="'.$conv->description.'" value="'.$conv->id.'" >'.$conv->agreement.'</option>';
										
											
									} else {
                                        	echo '<option selected id="SC" data-convenio="Sin Convenio" value="SC" >Sin Convenio</option>';
										}
                                    ?>  
	                                </select>
							</td>
		    			</tr>
		    			<tr>
		    				<td class="tdicon">
								<span class="glyphicon glyphicon-envelope tool" data-toggle="tooltip" data-placement="top" title="Email"></span>
							</td>
							<td class="bord">
								<span class="email_complete spn"><a href="mailto:<?php echo $u->email ? $u->email : "" ?>"><?php echo $u->email ? $u->email : "" ?></a></span>
								<input name="email" type="hidden" class="user-data form-control" id="user-email" placeholder="Email" value="<?php echo $u->email ? $u->email : '' ?>">
							</td>
							<td>
								Primero:
							</td>
							<td colspan="2" class="bord">
								
								
								<?php $first= isset($u->getFirstService()->created_at) ? $u->getFirstService()->created_at : 'Ninguno';
								if($u->id!=-1 && $first !='Ninguno'){
								$first=substr($first,0,-15);
								$first=templateDriver::timelo($first,3," ");
								}else {
									$first="Ninguno";
								}
								
								echo $first;
								?>
							</td>
							<td>
								Acumulados
							</td>
		                    <td class="center bord benacu">
		                    	<?php echo isset($bn->joined) ? $bn->joined : '0' ?>
		                    </td>
		                    <td><!-- class="text-conv"-->
								Empresa:
							</td>
							<td class="bord">
								<span style="font-weight: bold;" class="spn business_complete">
		                            <?php 
		                        	
		                               echo  $emp->name;
		                            ?>      
		                        </span>
		                        <select name="business_id" class="form-control user-data" style="display: none" id="user-empresa">
										<?php 
		                                   $empre=User::find('all',array("conditions"=>"isBusiness=1","order"=>"name asc"));
		                                        foreach($empre as $e){
		                                            $selected = ($emp->id == $e->id)?'selected':'';
													
		                                            echo '<option '.$selected.' value="'.$e->id.'" >'.$e->name.'</option>';
		                                        }
		                                         
		                                    ?>
									</select>
							</td>
		    			</tr>
		    			<tr>
		    				<td class="tdicon">
								<span class="glyphicon glyphicon-earphone tool" data-toggle="tooltip" data-placement="top" title="Teléfono"></span>
							</td>
							<td  class="bord">
								<span class="tel_complete spn">
								<?php	echo $tel=$u->telephones ? $tel=$u->telephones :" "	?>
								</span>
								<input name="telephones" type="hidden" class="form-control user-data" id="user-telephones" placeholder="Telefonos" value="<?php echo $u->telephones ? $u->telephones : '' ?>">
							</td>
							<td>
								Último:
							</td>
							<td colspan="2"  class="bord">
								<?php $second= isset($u->getLastService()->created_at) ? $u->getLastService()->created_at : 'Ninguno';
								if($u->id!=-1 && $second !='Ninguno'){
								$second=substr($second,0,-15);
								$second=templateDriver::timelo($second,3," ");
								}else {
									$second="Ninguno";
								}
								echo $second;
								?>
							</td>
							<td>
								Disponibles
							</td>
							<?php
							if(intval($bn->available)>0){
								$cl="green";
							}else{
								$cl="red";
							}
							?>
		                    <td id="ben_disp" style="font-weight: bold;font-size: 20px; color:<?php echo $cl; ?>" class="center bord bendis">
		                    	<?php echo isset($bn->available) ? $bn->available : '0' ?>
		                    </td>
		                    <td><!-- class="text-conv"-->
								
							</td>
							<td class="bord">
								<span class="spn rfc_complete"></span>
							</td>
		    			</tr>
		    		</tbody>
		    	</table>
			</div>
			<div class="row" id="direcciones">
				<table class="table table-bordered tables_c">
					<tbody>
						<tr>
							<td class="active act bord d_title" style="width: 50%">
								<strong>Direcciones:</strong> 
						        <!--data-toggle="modal" data-target="#myModal"-->
						    	<button class="btn btn-default btns-golden addir btn-sm tool add-btn" data-toggle='tooltip' data-placement='top' title='Agregar'>
						            <span class="btn-address pull-right glyphicon glyphicon-plus" style="cursor: pointer;"></span>
						        </button>
							</td>
							<td class="active act pasajero bord d_title" style="width: 50%;">
								<strong>Notas especiales del pasajeros</strong>
								
							</td>
							
						</tr>
						<tr>
							<td class="bord" style="padding: 0px!important; padding-bottom: 4px !important;">
								<div id="addresses" class="panel-address" style="display: none;">
		                
					                <?php
					                    //Load php block
					                    
					                    templateDriver::renderSection('address.base');
					                ?>
					            </div>
							</td>
							<td  style="padding: 0px!important;" class="bord">
								<label id="most-annotations"><?php echo $u->annotations ? $u->annotations : '' ?></label>
								<textarea name="annotations" style="display: none" id="user-annotations" style="background: rgb(153,255,153)!important;" class="form-control user-data" rows="3"><?php echo $u->annotations ? $u->annotations : '' ?></textarea>
							</td>
						</tr>
					</tbody>
				</table>				
		</div>
		<?php
		if($u->id==-1){
        		?>
        		<div class="butt col-md-3">
        			<button id="newu" class="btn btn-success">Crear Reservacion</button>
        		</div>
        		
        		<?php
        	}
        	?>
	    
		<div class="col-md-6">
			<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<?php
							 //templateDriver::renderSection('address.fields'); 
						?>
					</div>
				</div>
			</div>
		</div>          
	</div>
        </div>
        
    </div>
    
</div>
<div class="modal fade"id="addreess-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Modal title</h4>
			</div>
		<div class="modal-body" style="padding: 10px 50px;">
			<p>One fine body&hellip;</p>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-danger close-adress" data-dismiss="modal">Cancelar</button>
			<button type="button" class="btn-save btn btn-primary">Guardar cambios</button>
		</div>
		</div>
	</div>
</div>



<div class="modal fade" id="listser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" id="list-services">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Lista de Servicios<br /><center><a target="_BLANK" href="https://resv.goldenmty.com/user/xlslist/<?php echo $u->id; ?>"><button class="btn btn-success">Descargar</button></a></center></h4>
      </div>
      <div class="modal-body">
      	<?php
        	templateDriver::renderSection('service.list');
        ?>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
