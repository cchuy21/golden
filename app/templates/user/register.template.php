<?php
//crear un nuevo usuario
    $u = User::find_by_id(templateDriver::getData("window"));
    if(!$u) {
        $u = new User();
        $u->id = -1;
        $u->benefit_id = -1;
        $valagr = new stdClass();
        $valagr->description = '';
        $valagr->agreement = '';
    } else {
        $valagr=Agreement::find_by_id($u->agreement_id);
        $bn= Revenue::find_by_user_id($u->id);
    }
    templateDriver::setData('user', $u);
    $convenio= Agreement::all();
    $beneficio= Benefit::all();
	$ser=Service::all(array('conditions' => 'user_id = '.$u->id,'order' =>'service_datetime asc'));

?>
<div class="container" id="content" style="padding: 0;">
<div class="panel panel-default newuser">
	<div id="separador"></div>
    <div id="app-breadcrumbs" class="panel-heading top-bar container align-normal">
        <div id="cab">
        	
		  			Datos del Nuevo Usuario
		</div>
		<span class="close close-service" aria-hidden="true">Cancelar &times;</span>
        <div id="service-id">
        </div>
        
    </div>
    <div class="panel-body user-panel">
	<div class="container align-normal" id="user" style="transform: scale(0, 0); padding: 0">
	 <input type="hidden" class="user-data id-user id-usern" name="temporal" id="user-id" placeholder="Nombre" value="0">
        <input type="hidden" class="user-data id-user" name="id" id="user-id" placeholder="Nombre" value="-1">
	    <div class="tab-content">
		    <div class="row datos tab-pane active" id="datos">
		    	<div class="col-md-6 twin">
			    	<table class="table table-bordered tables_c">
			    		<tbody>
			    			<tr>
			    				<td colspan="2" class="active act bord pasajero" style="height: 28.5px;">
									<span>Pasajero</span>	
								</td>
			    			</tr>
			    			<tr>
			    				<td class="tdicon" style="width: 20px;">
									<span class="tool glyphicon glyphicon-user" data-toggle="tooltip" data-placement="top" title="Nombre"></span>
				
									
									
								</td>
								<td class="bord">
	
									
									 
		                                
									 <div class="form-inline">
	                                        <input value="<?php echo $u->distinguished ? $u->distinguished: 0 ?>"  name="distinguished" class="user-data" id="user-distinguished" type="hidden">
	                                        <input name="name" type="text" class="form-control user-data required" id="user-name" placeholder="Nombre" value="<?php echo $u->name ? $u->name : '' ?>">
											<span class="glyphicon glyphicon-star tool gray" id="ds-starh" data-toggle="tooltip" data-placement="top" title="Distinguido"></span>
									</div>
								</td>
			    			</tr>
			    			<tr>
			    				<td class="tdicon">
									<span class="glyphicon glyphicon-envelope tool" data-toggle="tooltip" data-placement="top" title="Email"></span>
								</td>
								<td class="bord">  
									<span class="email_complete spn"><a href="mailto:<?php echo $u->email ? $u->email : "" ?>"><?php echo $u->email ? $u->email : "" ?></a></span>
									<input name="email" type="text" class="user-data form-control" id="user-email" placeholder="Email" value="<?php echo $u->email ? $u->email : '' ?>">
								</td>
			    			</tr>
			    			<tr>
			    				<td class="tdicon">
									<span class="glyphicon glyphicon-earphone tool" data-toggle="tooltip" data-placement="top" title="Teléfono"></span>
								</td>
								<td  class="bord" style="height: 35px;">
									<span class="tel_complete spn">
									<?php	echo $tel=$u->telephones ? $tel=$u->telephones :" "	?>
									</span>
									<input name="telephones" type="text" class="form-control user-data" id="user-telephones" placeholder="Telefonos" value="<?php echo $u->telephones ? $u->telephones : '' ?>">
								</td>
			    			</tr>
			    		</tbody>
		    		</table>
		    	</div>
		    	<div class="col-md-6 twin">  <!--tabla de beneficios y convenios que se visualizar en agregar un nuevo usuario--->
			    	<table class="table table-bordered tables_c ">
			    		<tbody>
			    			<tr>
			    				
								<td colspan="3"width="45%!important" class="active act pasajero bord" style="height: 29px">
									<span>Beneficio</span>
									
								</td>
								<td colspan="2" class="active act pasajero bord" style="height: 29px">
									<span>Convenios</span>
									
								</td>						
			    			</tr>
			    			<tr>
			    				
								<td rowspan="3" class="tdselec" style="height: 121px">
									<span class="spn b">11º</span><br />
									<span class="spn gr">Gratis</span>
									<select name="benefit_id" id="user-revenue_id" class="form-control user-data">
		                                    <?php 
		                                   
		                                        foreach($beneficio as $ben){
		                                            $selected = ($ben->id == $u->benefit_id)?'selected':'';
		                                            echo '<option '.$selected.' value="'.$ben->id.'" >'.$ben->description.'</option>';
		                                        }
		                                         echo '<option value="" >Sin Beneficio</option>';
		                                    ?>
		                            </select>
								</td>
								<td>
									Usados
								</td>
			                    <td class="center bord">
			                    	<?php echo isset($bn->used) ? $bn->used : '0' ?>
			                    </td>
			                    <td>
									Convenio:
								</td>
								<td class="bord">         
			                            <select name="agreement" class="user-data form-control" id="user-agreement_id">
		                                   <option value='SC'>Seleccionar Convenio</option>
		                                </select>
								</td>						
			    			</tr>
			    			<tr>
			    				
								<td>
									Acumulados
								</td>
			                    <td class="center bord">
			                    	<?php echo isset($bn->joined) ? $bn->joined : '0' ?>
			                    </td>
			                    <td><!-- class="text-conv"-->
									Empresa:
								</td>
								<td class="bord">
		                        <select name="business_id" class="form-control user-data" id="user-empresa">
										<?php 
		                                   $empre=User::find('all',array("conditions"=>"isBusiness=1","order"=>"name asc"));
		                                        foreach($empre as $e){
		                                            $selected = ($emp->id == $e->id)?'selected':'';
													if($e->id==5529){
		                                            echo '<option selected value="'.$e->id.'" >'.$e->name.'</option>';
													}
		                                            echo '<option '.$selected.' value="'.$e->id.'" >'.$e->name.'</option>';
		                                        }
		                                         
		                                    ?>
									</select>
								</td>
			    			</tr>
			    			<tr>
								<td>
									Disponibles
								</td>
			                    <td id="ben_disp"  class="center bord">
			                    	<?php echo isset($bn->available) ? $bn->available : '0' ?>
			                    </td>
			                    <td style="height: 29px;"><!-- class="text-conv"-->
									
								</td>
								<td class="bord">
									
								</td>
			    			</tr>
			    		</tbody>
			    	</table>
			    </div>					<!--ADD STYLE--->
				<table style="display: none" class="tbdirec table table-bordered tables_c">
					<tbody>
						<tr>
							<td class="active act bord d_title">
								<strong>Direcciones:</strong> 
						        <!--data-toggle="modal" data-target="#myModal"-->
						    	<button class="btn btn-default btns-golden addir btn-sm tool add-btn" disabled data-toggle='tooltip' data-placement='top' title='Agregar'>
						            <span class="btn-address pull-right glyphicon glyphicon-plus" style="cursor: pointer;"></span>
						        </button>
							</td>
							<td class="active act bord d_title">
								<strong>Comentarios:</strong> 
							</td>
						</tr>
						<tr>
							<td class="bord disabled" style="padding: 0px!important; padding-bottom: 4px !important;"> 
								<div id="addresses" class="panel-address" style="display: none;">
		                
					                <?php
					                    //Load php block
					                    
					                    templateDriver::renderSection('address.base');
					                ?>
					                
					            </div>
							</td>
							<td  style="padding: 0px!important;" class="bord">
								<textarea name="annotations" id="user-annotations" class="form-control user-data" rows="3"><?php echo $u->annotations ? $u->annotations : '' ?></textarea>
							</td>
						</tr>
					</tbody>
				</table>
				<div id="divc" >
					<button type="button" class="btn btn-success user-save">Crear Usuario</button>
					<button type="button" style="display: none;" class="btn btn-success create-reser">Crear Reservacion</button>
				</div>
			</div>			
		</div>      
    </div>
    
</div>
</div>
</div>
<div class="modal fade"id="addreess-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Modal title</h4>
			</div>
		<div class="modal-body" style="padding: 10px 50px;">
			<p>One fine body&hellip;</p>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-danger close-adress" data-dismiss="modal">Cancelar</button>
			<button type="button" class="btn-save btn btn-primary">Guardar cambios</button>
		</div>
		</div>
	</div>
</div>

