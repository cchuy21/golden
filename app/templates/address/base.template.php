<?php

$user = templateDriver::getData('user'); 
//si la variable esta vacia
if(empty($user->id)){
$address1=Address::all(array('conditions' => array('user_id = ?',$user['id'])));
}else{
//si la variable existe en la base de datos y tiene un valor distinto a null
$address1=Address::all(array('conditions' => array('user_id = ?',$user->id)));	
}
?>
<div class="panel address-block" style="overflow-y: auto;height: 85px;">
    <div class="panel-body">
    	<?php
    	$cc=0;
    	 foreach($address1 as $address){
    	 	//si la direccion esta habilitada
    	 	if($address->status==1){
    	 		//encuentra el suburb correspondiente a la dirección
	          $col=Suburb::find_by_id($address->suburb_id);
				 if($cc==0){
				 	$class="spn-dire sd";
					 $cc++;
				 }else{ 
					 $class="spn-dire2 sd";
					 $cc=0;
				 }
				 //recolecta el nombre de la calle, el suburb y el telefono
				 $todo=$address->street.", ".$col->suburb.",  tel: ".$address->telephones;
				 //obtiene la longitud de todos los datos anteriores
				 $tod=strlen($todo);
				 //si los datos tienen una longitud mayor a 55
				 if($tod > 55){
				 	//se selecciona solo los primeros 45 caracteres
				 	$todo=substr($todo, 0,45)."...";
				 }
				 //impresión de todos los datos 
	              echo "<div class='".$class." id-address".$address->id."' data-addreess='".$address->id."'><span class='al' style='float:left!important;'>".$address->alias."</span>:&nbsp;<span class='show-add'>".$todo."</span><button type='button' data-aid='{$address->id}' class='address-delete btn exsm addr btn-default btns-golden btn-sm tool' data-toggle='tooltip' data-placement='top' title='Borrar'><span class='glyphicon glyphicon-remove-circle'></span></button><button type='button' data-aid='{$address->id}' class='address-edit btn exsm addr btn-default btns-golden btn-sm tool' data-toggle='tooltip' data-placement='top' title='Editar'><span class='glyphicon glyphicon-pencil'></span></button></div>" ;
			} 
		 }
    	?>
    </div>
</div>

