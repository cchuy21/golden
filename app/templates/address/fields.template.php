<?php
    $d = templateDriver::getData('fields');
    if($d !== null) {
        $address = Address::find_by_id($d['id']);
        //Y la dirección no pertenece a ese usuario...
        if(!$address) die("No se encontro la dirección solicitada");
    } else {
        $address = null;
        $i = 1;
        $tmp = templateDriver::getData("temporal");
		$add=Address::find("all",array("conditions"=>"user_id=".$tmp['uid']));
		$sig=1;
		$t=0;
		//si la ultima letra del alias de una dirección es mayor a 0
		foreach($add as $a){
			$n=intval(substr($a->alias,-1));
			if($n>$t)
			//el valor se pasa a t
				$t=$n;
		}
		$t++;
    }      
?>

<!--aqui se muestran los datos de un servicio que se va a realizar-->
<input type="hidden" class="address_data" name='id' value="<?php echo isset($address->id) ? $address->id : -1 ?>">
<input type="hidden" class="address_data" name='user_id' value="<?php echo isset($address->user_id) ? $address->user_id : $tmp['uid'] ?>">
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <input type="text" placeholder="Alias" class="form-control address_data alias" name='alias' value="<?php echo isset($address->alias) ? $address->alias : 'Nueva'.$t ?>">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-10" style="width: 100%">
        <div class="form-group">
            <input type="text" class="form-control address_data" name='street' placeholder="Calle" value="<?php echo isset($address->street) ? $address->street : '' ?>">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <input type="text" class="form-control suburb_id-ac" name='suburb_id-ac' placeholder="Colonia" value="<?php 
                    $ce = '';
                    if(isset($address->suburb_id)) {
                        $ce = Suburb::find_by_id($address->suburb_id)->suburb;
                    }
                    echo $ce; 
                ?>">
            <input type="hidden" class="form-control address_data" name='suburb_id' value="<?php echo isset($address->suburb_id) ? $address->suburb_id : '' ?>">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <input type="text" class="form-control address_data" name='between' placeholder="Entre calle1 y calle2" value="<?php echo isset($address->between) ? $address->between : '' ?>">
        </div>
    </div>
</div>
<div class="row">
	<div class="col-md-4">
		<div class="form-group">
       		<input type="text" class="form-control address_data" name='telephones' placeholder="telefono" value="<?php echo isset($address->telephones)? $address->telephones : '' ?>" >
    	</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
       		<input type="text" class="form-control address_data" name='quadrant' placeholder="Cuadrante" value="<?php echo isset($address->quadrant)? $address->quadrant : '' ?>" >
    	</div>
	</div>
    <div class="col-md-4">
        <div class="form-group">
            <button style="width:100%;" class="btn map-btn">Ubicar en el mapa</button>
            <input type="hidden" class="form-control address_data" name='map' value="<?php echo isset($address->map) ? $address->map : '' ?>">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
    	 <div class="form-group">
        	<textarea name='reference' placeholder="Referencias o Notas de como llegar" class="form-control address_data" rows="3"><?php echo isset($address->reference) ? $address->reference : '' ?></textarea>
    	 </div>
    </div>
</div>

