<table class="table table-bordered">
	<tbody>
		<?php
		$hoy = date("Y-m-d");
		$diag = strtotime("+1 day", strtotime($hoy));
		$diag = date("Y-m-d", $diag);
		$ah = date('G:i', strtotime("00:00"));
		$ahg = date('G:i', strtotime("04:59"));
		$hora = date("G:i");
		//mostrar las reservaciones que no se han realizado en las primera 5horas del dia
		$reser = Reservation::find("all", array("order" => "reservation_date asc, reservation_time asc", "conditions" => "status=1 AND reservation_date ={ts '" . $diag . "'} AND type='MTY-APTO' AND reservation_time BETWEEN {ts '" . $ah . "'} AND {ts '" . $ahg . "'}"));

		foreach ($reser as $r) {
			//mostrar los servicios relacionados con la reservación
			$ser = Service::find_by_reservation1($r -> id);

			$tip = ' (SEN)';

			if ($ser -> reservation2) {
				$tip = ' (RED)';
			}
			if (!$ser) {
				$ser = Service::find_by_reservation2($r -> id);
				$tip = ' (COM)';
				$clas = "asignado";
			}
			if ($ser -> payment == "Tarjeta de Credito/Debito") {
				$pay = "TC";
			} else if ($ser -> payment == "Efectivo MN") {
				$pay = "EF";
			} else if ($ser -> payment == "movil") {
				if ($ser -> pagado)
					$pay = "Movil-pagada";
				else
					$pay = "Movil";
			} else if($ser->payment=="Cortesia") {
					$pay = "CO";
				} else{
					$pay = "FxC";
				}
			$tip = "<label style='width:40px;'>".$tip . "</label> <label style='width:25px;'>" . $pay."</label>";
			//se obtiene el número de servicio
			$resasig = numres($ser -> id);
			$operres = Operator::find_by_id($as -> operator_id);
			$asbol = $as -> economic . ' ' . $operres -> username . ' - ';
			$user = User::find_by_id($ser -> user_id);
			$nameus = $user -> name;
			$emp=User::find_by_id($user->business_id);
			if(!$emp){
				$emp=User::find_by_id(5529);
			}
			$emp=substr($emp->name, 0,3);
			$tip=$tip."    <label style='margin-left:5px;'>".$emp."</label>";
			$nameus = numnam($nameus);
			if ($r -> vehicle_id == '1') {
				$vei = ' Auto: ';
			} else {
				$vei = ' Cam: ';
			}
			$pass = numpas($r -> passengers);
			$type = $vei . $pass . "<label style='width:115px;'>" . $tip . '</label> ';
			$dir = direc($r -> addresses);
			$tix = 'salida';
			$dir = numdir($dir, $tix);
			//si el usuario no tiene número telefonico
			if ($user -> telephones) {
				$telefono = $user -> telephones;
			} else {
				$telefono = 'xxxxxxxxxx';
			}
			if ($c == 0) {
				$ba = "rgb(250, 140, 0)";
				$c++;
			} else {
				$ba = "rgb(250, 140, 0)";
				$c = 0;
			}
			if (strtotime($hora) > strtotime(date("G:i", strtotime("20:00")))) {
				$cl = "por-asignars por-asignaram";
			} else {
				$cl = "por-asignaram";
			}
			if ($r -> flight_number)
				$vuelo = " - V: " . $r -> flight_number;
			else
				$vuelo = " - V: X";
			
			echo "<tr  style='text-align: left;background-color:" . $ba . ";'>
			<td style='background:" . $ba . "'>
				<button type='button' class='btn btn-default btn-xs stic' data-reser='" . $r -> id . "' style='float:right'>
					<span class='glyphicon glyphicon-eye-open'>
					</span>
				</button>
			</td>
			<td>
				<input type='checkbox' class='impr' value='" . $r -> id . "' >
			</td>
			<td style='background-color:" . $ba . "; margin-top:2px;cursor: pointer; color:black!important;' data-reser='" . $r -> id . "' class='" . $cl . "' >
			<strong>R: " . $resasig . "</strong> - Mañana " . templateDriver::timelohrs($r -> reservation_date, $r -> reservation_time) . " [<label data-time='" . $r -> reservation_time . "' class='timeatr' style='font-weight: initial;margin-bottom: 0px;color:black!important;'>00:00</label>] - <label style='width:240px; font-weight:normal'>" . $type . $vuelo . "</label> - " . $terminal . " - <label style='font-weight: initial;margin-bottom: 0px;width: 160px;'>" . $nameus . "</label> " . $dir . "</td></tr>";		}
		//nuevo
		$hoy = date("Y-m-d");
		$diag = strtotime("+1 day", strtotime($hoy));
		$diag = date("Y-m-d", $diag);
		$ah = date('G:i', strtotime("05:00"));
		$ahg = date('G:i', strtotime("06:00"));
		$hora = date("G:i");
		//mostrar las reservaciones que no se han realizado en las primera 5horas del dia
		$reser = Reservation::find("all", array("order" => "reservation_date asc, reservation_time asc", "conditions" => "status=1 AND reservation_date ={ts '" . $diag . "'} AND type='MTY-APTO' AND reservation_time BETWEEN {ts '" . $ah . "'} AND {ts '" . $ahg . "'}"));

		foreach ($reser as $r) {
			//mostrar los servicios relacionados con la reservación
			$ser = Service::find_by_reservation1($r -> id);

			$tip = ' (SEN)';

			if ($ser -> reservation2) {
				$tip = ' (RED)';
			}
			if (!$ser) {
				$ser = Service::find_by_reservation2($r -> id);
				$tip = ' (COM)';
				$clas = "asignado";
			}
			if ($ser -> payment == "Tarjeta de Credito/Debito") {
				$pay = "TC";
			} else if ($ser -> payment == "Efectivo MN") {
				$pay = "EF";
			} else if ($ser -> payment == "movil") {
				if ($ser -> pagado)
					$pay = "Movil-pagada";
				else
					$pay = "Movil";
			} else if($ser->payment=="Cortesia") {
				$pay = "CO";
			} else{
				$pay = "FxC";
			}
			$tip = "<label style='width:40px;'>".$tip . "</label> <label style='width:25px;'>" . $pay."</label>";
			//se obtiene el número de servicio
			$resasig = numres($ser -> id);
			$operres = Operator::find_by_id($as -> operator_id);
			$asbol = $as -> economic . ' ' . $operres -> username . ' - ';
			$user = User::find_by_id($ser -> user_id);
			$nameus = $user -> name;
			$nameus = numnam($nameus);
			$emp=User::find_by_id($user->business_id);
			if(!$emp){
				$emp=User::find_by_id(5529);
			}
			$emp=substr($emp->name, 0,3);
			$tip=$tip."    <label style='margin-left:5px;'>".$emp."</label>";
			if ($r -> vehicle_id == '1') {$vei = ' Auto: ';
			} else {$vei = ' Cam: ';
			}
			$pass = numpas($r -> passengers);
			$type = $vei . $pass . "<label style='width:115px;'>" . $tip . '</label> ';
			$vuelo = $r -> flight_number;
			$dir = direc($r -> addresses);
			$tix = 'salida';
			$dir = numdir($dir, $tix);
			//si el usuario no tiene número telefonico
			if ($user -> telephones) {
				$telefono = $user -> telephones;
			} else {
				$telefono = 'xxxxxxxxxx';
			}
			if ($c == 0) {
				$ba = "white";
				$c++;
			} else {
				$ba = "white";
				$c = 0;
			}
			if (strtotime($hora) > strtotime(date("G:i", strtotime("20:00")))) {
				$cl = "por-asignars por-asignaram";
			} else {
				$cl = "por-asignaram";
			}
			if($r->abierto){
				$r->reservation_time="abierta";
			}
			if ($r -> flight_number)
					$vuelo = " - V: " . $r -> flight_number;
				else
					$vuelo = " - V: X";
			if ($r -> terminal && $r -> terminal != 'null' && $r->terminal != "Terminal" && $r->terminal != "" && $r->terminal != " ") {
					$terminal = $r -> terminal;
				} else {
					$terminal = "X";
				}
				$vuelo = $r -> flight_number;
				//obtiene el número de vuelo
				$vuelo = numvuelo($vuelo);
				//las direcciones del servicio
				$dir = direc($r -> addresses);
				$tix = 'llegada';
				//número de direcciones
				$dir = numdir($dir, $tix);
				$nameus = $user -> name;
				$nameus = numnam($nameus);
				$user = $ser -> user;
				
				if ($r -> vehicle_id == '1') {$vei = ' Auto: ';
				} else {$vei = ' Cam: ';
				}
				//obtiene el número de pasajeros y el vehiculos
				$pass = numpas($r -> passengers);
				$type = $vei . $pass . "<label style='width:115px;'>" . $tip . '</label> ';
				if ($r -> flight_number)
					$vuelo = " - V: " . $r -> flight_number;
				else
					$vuelo = " - V: X";
				
				if ($r -> terminal && $r -> terminal != 'null' && $r->terminal != "Terminal" && $r->terminal != "" && $r->terminal != " ") {
					$terminal = $r -> terminal;
				} else {
					$terminal = "X";
				}
			echo "<tr  style='text-align: left;background-color:" . $ba . ";'>
			<td style='background:" . $ba . "'>
				<button type='button' class='btn btn-default btn-xs stic' data-reser='" . $r -> id . "' style='float:right'>
					<span class='glyphicon glyphicon-eye-open'>
					</span>
				</button>
			</td>
			<td>
				<input type='checkbox' class='impr' value='" . $r -> id . "' >
			</td>
			<td style='background-color:" . $ba . "; margin-top:2px;cursor: pointer; color:black!important;' data-reser='" . $r -> id . "' class='" . $cl . "' >
			<strong>R: " . $resasig . "</strong> - Mañana " . templateDriver::timelohrs($r -> reservation_date, $r -> reservation_time) . " <label style='color:black!important;'>[<label data-time='" . $r -> reservation_time . "' class='timeatr' style='font-weight: initial;margin-bottom: 0px;'>00:00</label>]</label> - <label style='width:240px; font-weight:normal'>" . $type . $vuelo . "</label> - " . $terminal . " - <label style='font-weight: initial;margin-bottom: 0px;width: 160px;'>" . $nameus . "</label> " . $dir . "</td></tr>";
		}
		?>
	</tbody>
</table>