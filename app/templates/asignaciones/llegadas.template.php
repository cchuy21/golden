<table class="table table-bordered">
	<tbody>
		<?php
		//se obtiene todas las reservaciones de ayer, de hoy y el dìa siguiente
		$hoy = date("Y-m-d");
		$hora = date("G:i");
		if (strtotime($hora) < strtotime(date("G:i", strtotime("4:00"))) && strtotime($hora) > strtotime(date("G:i", strtotime("00:00")))) {

			$ayer = strtotime('-1 day', strtotime($hoy));
			$ayer = date('Y-m-d', $ayer);
			$re = Reservation::find("all", array("select"=>"id,vehicle_id,reservation_time,terminal,passengers,flight_number,reservation_date,addresses,abierto,created_at,updated_at","order" => "reservation_date asc, reservation_time asc", "conditions" => "type='APTO-MTY' AND status=1 AND reservation_date = {ts '" . $ayer . "'}"));
			imprel($re);
			$reser = Reservation::find("all", array("select"=>"id,vehicle_id,reservation_time,terminal,passengers,flight_number,reservation_date,addresses,abierto,created_at,updated_at","order" => "reservation_date asc, reservation_time asc", "conditions" => "type='APTO-MTY' AND status=1 AND reservation_date = {ts '" . $hoy . "'}"));
			imprel($reser);
		} else {
			$reser = Reservation::find("all", array("select"=>"id,vehicle_id,reservation_time,terminal,passengers,flight_number,reservation_date,addresses,abierto,created_at,updated_at","order" => "reservation_date asc, reservation_time asc", "conditions" => "type='APTO-MTY' AND status=1 AND reservation_date = {ts '" . $hoy . "'}"));
			imprel($reser);
		}
		function imprel($reser) {
			$c = 0;
			foreach ($reser as $r) {
				$ser = Service::find_by_reservation1($r -> id,array("select"=>"id,reservation1,reservation2,payment,pagado,user_id"));

				$tip = ' (SEN)';
				//si existen dos reservaciones, se determina el servicio como redondo
				if ($ser -> reservation2) {
					$tip = ' (RED)';
				}
				if (!$ser) {
					$ser = Service::find_by_reservation2($r -> id,array("select"=>"id,reservation1,reservation2,payment,pagado,user_id"));
					$tip = ' (COM)';
					$clas = "asignado";
				}
				if ($ser -> payment == "Tarjeta de Credito/Debito") {
					$pay = "TC";
				} else if ($ser -> payment == "Efectivo MN") {
					$pay = "EF";
				} else if ($ser -> payment == "movil") {
					if($ser->pagado)
						$pay = "Movil-pagada";
					else
						$pay = "Movil";
				} else if($ser->payment=="Cortesia") {
					$pay = "CO";
				} else{
					$pay = "FxC";
				}
				$tip = "<label style='width:40px;'>".$tip . "</label> <label style='width:25px;'>" . $pay."</label>";
				$ahora = date("Y-m-d G:i:s");
				$menos15 = strtotime('-15 minutes', strtotime($ahora));
				//ver archivo, funciones.template.php
				$resasig = numres($ser -> id);
				//operador del servicio
				$operres = Operator::find_by_id($as -> operator_id);
				//unidad asignada para el servicio
				$uni = Unity::find($as -> unity_id);
				$asbol = $uni -> economic . ' ' . $operres -> username . ' - ';
				//usuario del servicio
				$user = $ser -> user;
				$emp=User::find_by_id($user->business_id);
				if(!$emp){
					$emp=User::find_by_id(5529);
				}
				$emp=substr($emp->name, 0,3);
				$nameus = $user -> name;
				$nameus = numnam($nameus);
				$tip=$tip."    <label style='margin-left:5px;'>".$emp."</label>";
				//se asigna el tipo de auto que se va a ocupar
				if ($r -> vehicle_id == '1') {$vei = ' Auto: ';
				} else {$vei = ' Cam: ';
				}
				$pass = numpas($r -> passengers);
				$type = $vei . $pass . "<label style='width:115px;'>" . $tip . '</label> ';
				$vuelo = $r -> flight_number;
				//muestra el número de vuelo
				$vuelo = numvuelo($vuelo);
				//retorna las direcciones de las asignaciones
				$dir = direc($r -> addresses);
				$tix = 'llegada';
				//muestra los primeros 55 caracteres de las direcciones enviadas, si es cualquier otra cosa muestra 90 caracteres de la dirección
				$dir = numdir($dir, $tix);
				if ($r -> terminal && $r -> terminal != 'null' && $r->terminal != "Terminal" && $r->terminal != "" && $r->terminal != " ") {
					$terminal = $r -> terminal;
				} else {
					$terminal = "X";
				}
				if ($c == 0) {
					$ba = "red";
					$c++;
				} else {
					$ba = "rgb(253, 85, 85);";
					$c = 0;
				}
				if ($r -> flight_number)
					$vuelo = " - V: " . $r -> flight_number;
				else
					$vuelo = " - V: X";
				if($r->abierto){
					$fechaHoy2 = strtotime("$r->reservation_date");
					if($r->reservation_date>0)
					$r->reservation_time="abierta";
				}
				if ($ser)
					//si la reservación fue creada en menos de 15 min de l a hora actual
					if ($menos15 < strtotime(date("Y-m-d G:i:s", strtotime($r -> created_at)))) {
						echo "<tr class='danger' style='text-align: left;'><td style=' background-color: rgb(180, 3, 3);'><input type='checkbox' class='impr imprlleg' value='" . $r -> id . "' ></td><td style=''background:rgb(180, 3 ,3);'><button type='button' class='btn btn-default btn-xs stic' data-reser='" . $r -> id . "' style='float:right'><span class='glyphicon glyphicon-eye-open'></span></button> </td><td style='color:white; background-color: rgb(180, 3, 3); margin-top:2px;cursor: pointer;' data-reser='" . $r -> id . "' class='por-asignarl' ><img src='/static/img/nuevo.gif' /> <strong>  R:" . $resasig . "</strong> " . templateDriver::timelohrs($r -> reservation_date, $r -> reservation_time) . " [<label data-time='" . $r -> reservation_time . "' class='timeatr' style='font-weight: initial;margin-bottom: 0px;'>00:00</label>] - <label style='width:240px; font-weight:normal'>" . $type . $vuelo . "</label> - <strong>" . $terminal . " </strong> - <label style='font-weight: initial;margin-bottom: 0px;width: 160px;'>" . $nameus . "</label> " . $dir . "</td></tr>";
						//si ha sido modificado hace 15 min atras
					} else if ($menos15 < strtotime(date("Y-m-d G:i:s", strtotime($r -> updated_at)))) {
						echo "<tr class='danger' style='text-align: left;'><td style=' background-color: rgb(180, 3, 3);'><input type='checkbox' class='impr imprlleg' value='" . $r -> id . "' ></td><td style=''background:rgb(180, 3 ,3);'><button type='button' class='btn btn-default btn-xs stic' data-reser='" . $r -> id . "' style='float:right'><span class='glyphicon glyphicon-eye-open'></span></button> </td><td style='color:white; background-color: rgb(180, 3, 3); margin-top:2px;cursor: pointer;' data-reser='" . $r -> id . "' class='por-asignarl' ><img src='/static/img/cambio.gif' /> <strong>  R:" . $resasig . "</strong> " . templateDriver::timelohrs($r -> reservation_date, $r -> reservation_time) . " [<label data-time='" . $r -> reservation_time . "' class='timeatr' style='font-weight: initial;margin-bottom: 0px;'>00:00</label>] - <label style='width:240px; font-weight:normal'>" . $type . $vuelo . "</label> - <strong>" . $terminal . "</strong> - <label style='font-weight: initial;margin-bottom: 0px;width: 160px;'>" . $nameus . "</label> " . $dir . "</td></tr>";
					} else {
						echo "<tr class='danger' style='text-align: left;'><td style=' background-color: rgb(180, 3, 3);'><input type='checkbox' class='impr imprlleg' value='" . $r -> id . "' ></td><td style=''background:rgb(180, 3 ,3);'><button type='button' class='btn btn-default btn-xs stic' data-reser='" . $r -> id . "' style='float:right'><span class='glyphicon glyphicon-eye-open'></span></button> </td><td style='color:white; background-color: rgb(180, 3, 3); margin-top:2px;cursor: pointer;' data-reser='" . $r -> id . "' class='por-asignarl' ><strong>R:" . $resasig . "</strong> " . templateDriver::timelohrs($r -> reservation_date, $r -> reservation_time) . " [<label data-time='" . $r -> reservation_time . "' class='timeatr' style='font-weight: initial;margin-bottom: 0px;'>00:00</label>] - <label style='width:240px; font-weight:normal'>" . $type . $vuelo . "</label> - <strong>" . $terminal . "</strong> - <label style='font-weight: initial;margin-bottom: 0px;width: 160px;'>" . $nameus . "</label> " . $dir . "</td></tr>";
					}

			}
		}
		?>
	</tbody>
</table>