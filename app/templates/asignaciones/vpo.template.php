<div class="modal-dialog" style="width: 300px;">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">Vueltas por Operador</h4>
		</div>
		<div class="modal-body assin" style="padding:40px;padding-bottom: 40px;">
			
		<table class="table">
			<tbody>
				<tr>
					<td style="text-align: center">Fecha:<br>
						<input type="text" class="form-control" id="feca">
					</td>
				</tr>
				<tr>
					<td>
						<br />
						Operador: <select id="opvuel" class="form-control">
							<option value="">Seleccionar Operador</option>
							<?php
				     			$op=Operator::all(array("order"=>"username asc",'conditions'=> 'enable = 1'));
						 		foreach ($op as $ope) {
									 echo "<option value=".$ope->id.">".$ope->username."</option>";
								 }?>	
			  			</select>
					</td>
				</tr>
			</tbody>
		</table>
		<div class="col-xs-12 cnt">
			
		</div>
	</div>
	<div class="modal-footer">
		<br />
		<br />
		
		<button type="button" class="btn btn-danger close-user" data-dismiss="modal">Cerrar</button>
		<button type="button" id="opvuelb" class="btn btn-primary" style="display: none">Buscar</button>
	</div>	
	</div>
</div>