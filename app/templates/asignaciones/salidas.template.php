<table class="table table-bordered">
	<tbody>
		<?php
		$hoy = date("Y-m-d");

		$hora = date("G:i");
		if (strtotime($hora) < strtotime(date("G:i", strtotime("4:00"))) && strtotime($hora) > strtotime(date("G:i", strtotime("00:00")))) {
			//seleccionar todas las reservaciones de ayer y hoy
			$ayer = strtotime('-1 day', strtotime($hoy));
			$ayer = date('Y-m-d', $ayer);
			$re = Reservation::find("all", array("select"=>"id,vehicle_id,reservation_time,terminal,passengers,flight_number,reservation_date,addresses,abierto,created_at,updated_at","order" => "reservation_date asc, reservation_time asc", "conditions" => "type='MTY-APTO' AND status=1 AND reservation_date = {ts '" . $ayer . "'}"));
			impres($re);
			$reser = Reservation::find("all", array("select"=>"id,vehicle_id,reservation_time,terminal,passengers,flight_number,reservation_date,addresses,abierto,created_at,updated_at","order" => "reservation_date asc, reservation_time asc", "conditions" => "type='MTY-APTO' AND status=1 AND reservation_date = {ts '" . $hoy . "'}"));
			impres($reser);
		} else {
			$reser = Reservation::find("all", array("select"=>"id,vehicle_id,reservation_time,terminal,passengers,flight_number,reservation_date,addresses,abierto,created_at,updated_at","order" => "reservation_date asc, reservation_time asc", "conditions" => "type='MTY-APTO' AND status=1 AND reservation_date = {ts '" . $hoy . "'}"));
			impres($reser);
		}
		function impres($reser) {
			$c = 0;
			foreach ($reser as $r) {
				$ser = Service::find_by_reservation1($r -> id,array("select"=>"id,reservation1,reservation2,payment,pagado,user_id"));
				$tip = ' (SEN)';
				if (!$ser) {
					$ser = Service::find_by_reservation2($r -> id,array("select"=>"id,reservation1,reservation2,payment,pagado,user_id"));
					$tip = ' (COM)';
					$clas = "asignado";
				}
				if ($ser -> reservation2) {
					$tip = ' (RED)';
				}
				
				if ($ser -> payment == "Tarjeta de Credito/Debito") {
					$pay = "TC";
				} else if ($ser -> payment == "Efectivo MN") {
					$pay = "EF";
				} else if ($ser -> payment == "movil") {
					if($ser->pagado)
						$pay = "Movil-pagada";
					else
						$pay = "Movil";
				} else if($ser->payment=="Cortesia") {
					$pay = "CO";
				} else{
					$pay = "FxC";
				}
				
				$tip = "<label style='width:40px;'>".$tip . "</label> <label style='width:25px;'>" . $pay."</label>";
				$ahora = date("Y-m-d G:i:s");
				$menos15 = strtotime('-15 minutes', strtotime($ahora));
				$resasig = numres($ser -> id);
				$operres = Operator::find_by_id($as -> operator_id);
				$uni = Unity::find($as -> unity_id);
				$asbol = $uni -> economic . ' ' . $operres -> username . ' - ';
				$user = $ser -> user;
				$emp=User::find_by_id($user->business_id);
				if(!$emp){
					$emp=User::find_by_id(5529);
				}
				$emp=substr($emp->name, 0,3);
				$nameus = $user -> name;
				$nameus = numnam($nameus);
				$tip=$tip."    <label style='margin-left:5px;'>".$emp."</label>";
				if ($r -> vehicle_id == '1') {$vei = ' Auto: ';
				} else {$vei = ' Cam: ';
				}
				//obtiene el número de pasajeros y el vehiculos
				$pass = numpas($r -> passengers);
				$type = $vei . $pass . "<label style='width:115px;'>" . $tip . '</label> ';
				$vuelo = $r -> flight_number;
				//obtiene el número de vuelo
				$vuelo = numvuelo($vuelo);
				//las direcciones del servicio
				$dir = direc($r -> addresses);
				$tix = 'llegada';
				//número de direcciones
				$dir = numdir($dir, $tix);
				if ($r -> terminal && $r -> terminal != 'null' && $r->terminal != "Terminal" && $r->terminal != "" && $r->terminal != " ") {
					$terminal = $r -> terminal;
				} else {
					$terminal = "X";
				}
				if ($c == 0) {
					$ba = "red";
					$c++;
				} else {
					$ba = "rgb(253, 85, 85);";
					$c = 0;
				}
				if ($r -> flight_number)
					$vuelo = " - V: " . $r -> flight_number;
				else
					$vuelo = " - V: X";
				if($r->abierto){
					$fechaHoy2 = strtotime("$r->reservation_date");
					if($r->reservation_date>0)
					$r->reservation_time="abierta";
				}
					if ($ser)
					//se imprimen los servicios que se ha han creado o actualizado hace 15 min atras
					if ($menos15 < strtotime(date("Y-m-d G:i:s", strtotime($r -> created_at)))) {
						echo "<tr class='danger' style='text-align: left;'><td style=' background-color: rgb(180, 3, 3);'><input type='checkbox' class='impr imprsal' value='" . $r -> id . "' ></td><td style=''background:rgb(180, 3 ,3);'><button type='button' class='btn btn-default btn-xs stic' data-reser='" . $r -> id . "' style='float:right'><span class='glyphicon glyphicon-eye-open'></span></button> </td><td style='color:white; background-color: rgb(180, 3, 3); margin-top:2px;cursor: pointer;' data-reser='" . $r -> id . "' class='por-asignars' ><img src='/static/img/nuevo.gif' />  <strong> R:" . $resasig . "</strong> " . templateDriver::timelohrs($r -> reservation_date, $r -> reservation_time) . " [<label data-time='" . $r -> reservation_time . "' class='timeatr' style='font-weight: initial;margin-bottom: 0px;'>00:00</label>] - <label style='width:240px; font-weight:normal'>" . $type . $vuelo . "</label> - " . $terminal . " - <label style='font-weight: initial;margin-bottom: 0px;width: 160px;'>" . $nameus . "</label> " . $dir . "</td></tr>";
					} else if ($menos15 < strtotime(date("Y-m-d G:i:s", strtotime($r -> updated_at)))) {
						echo "<tr class='danger' style='text-align: left;'><td style=' background-color: rgb(180, 3, 3);'><input type='checkbox' class='impr imprsal' value='" . $r -> id . "' ></td><td style=''background:rgb(180, 3 ,3);'><button type='button' class='btn btn-default btn-xs stic' data-reser='" . $r -> id . "' style='float:right'><span class='glyphicon glyphicon-eye-open'></span></button> </td><td style='color:white; background-color: rgb(180, 3, 3); margin-top:2px;cursor: pointer;' data-reser='" . $r -> id . "' class='por-asignars' ><img src='/static/img/cambio.gif' />  <strong> R:" . $resasig . "</strong> " . templateDriver::timelohrs($r -> reservation_date, $r -> reservation_time) . " [<label data-time='" . $r -> reservation_time . "' class='timeatr' style='font-weight: initial;margin-bottom: 0px;'>00:00</label>] - <label style='width:240px; font-weight:normal'>" . $type . $vuelo . "</label> - " . $terminal . " - <label style='font-weight: initial;margin-bottom: 0px;width: 160px;'>" . $nameus . "</label> " . $dir . "</td></tr>";
					} else {
						echo "<tr class='danger' style='text-align: left;'><td style=' background-color: rgb(180, 3, 3);'><input type='checkbox' class='impr imprsal' value='" . $r -> id . "' ></td><td style=''background:rgb(180, 3 ,3);'><button type='button' class='btn btn-default btn-xs stic' data-reser='" . $r -> id . "' style='float:right'><span class='glyphicon glyphicon-eye-open'></span></button> </td><td style='color:white; background-color: rgb(180, 3, 3); margin-top:2px;cursor: pointer;' data-reser='" . $r -> id . "' class='por-asignars' ><strong>R:" . $resasig . "</strong> " . templateDriver::timelohrs($r -> reservation_date, $r -> reservation_time) . " [<label data-time='" . $r -> reservation_time . "' class='timeatr' style='font-weight: initial;margin-bottom: 0px;'>00:00</label>] - <label style='width:240px; font-weight:normal'>" . $type . $vuelo . "</label> - " . $terminal . " - <label style='font-weight: initial;margin-bottom: 0px;width: 160px;'>" . $nameus . "</label> " . $dir . "</td></tr>";
					}

			}
		}
		?>
	</tbody>
</table>