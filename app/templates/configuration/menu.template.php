<!--menu principal del apartado de configuración-->

<div class="panel-default systemuser-block">
	
    <div class="panel-body" style="background: white; padding: 0px; height: 500px">
    	<button class="repprin rep btn btn-menurep mn-config" data-form="form" data-menu="systemuser.controluser">
    		Usuarios del Sistema
    		<span class="glyphicon glyphicon-chevron-right imgmenurep"></span>
    	</button>
    	<button class="repprin rep btn btn-menurep mn-config" data-menu="zone.controlzone">
    		Costos por Zona
    		<span class="glyphicon glyphicon-chevron-right imgmenurep"></span>
    	</button>
    	<button class="repprin rep btn btn-menurep mn-config" data-menu="suburb.controlsub">
    		Colonias
    		<span class="glyphicon glyphicon-chevron-right imgmenurep"></span>
    	</button>
    	<button class="repprin rep btn btn-menurep mn-config" data-menu="unity.controlunity">
    		Unidades
    		<span class="glyphicon glyphicon-chevron-right imgmenurep"></span>
    	</button>
    	<button class="repprin rep btn btn-menurep mn-config" data-menu="unity.controloperator">
    		Operadores
    		<span class="glyphicon glyphicon-chevron-right imgmenurep"></span>
    	</button>
    	<button class="repprin rep btn btn-menurep mn-config" data-menu="tickets.pendientes">
    		Boletos
    		<span class="glyphicon glyphicon-chevron-right imgmenurep"></span>
    	</button>
    	<button class="repprin rep btn btn-menurep mn-config" data-menu="business.control">
    		Empresas
    		<span class="glyphicon glyphicon-chevron-right imgmenurep"></span>
    	</button>
    	<button class="repprin rep btn btn-menurep mn-config" data-menu="agreement.controlagreement">
    		Convenios
    		<span class="glyphicon glyphicon-chevron-right imgmenurep"></span>
    	</button>
    	<button class="repprin rep btn btn-menurep mn-config" data-menu="service.orden">
    		Orden Servicios
    		<span class="glyphicon glyphicon-chevron-right imgmenurep"></span>
    	</button>
    	<button class="repprin rep btn btn-menurep mn-config" data-menu="corte.excel">
    		Corte en Excel tarjeta
    		<span class="glyphicon glyphicon-chevron-right imgmenurep"></span>
    	</button>
    	<button class="repprin rep btn btn-menurep mn-config" data-menu="corte.excelfirma">
    		Corte en Excel Firma
    		<span class="glyphicon glyphicon-chevron-right imgmenurep"></span>
    	</button>
    	<!--<button class="repprin rep btn btn-menurep mn-config" data-menu="configuration.restricciones">
			Restricciones
    		<span class="glyphicon glyphicon-chevron-right imgmenurep"></span>
    	</button>-->
    	<button style="display: none" class="repprin rep btn btn-menurep mn-config" data-menu="corte.movil">
    		Reservaciones App
    		<span class="glyphicon glyphicon-chevron-right imgmenurep"></span>
    	</button>
    	<button class="repprin rep btn btn-menurep mn-config" data-menu="reports.movil">
    		Historial App
    		<span class="glyphicon glyphicon-chevron-right imgmenurep"></span>
    	</button>
    	<button class="repprin rep btn btn-menurep mn-config" data-menu="configuration.cambios">
    		Cambios a confirmar
    		<span class="glyphicon glyphicon-chevron-right imgmenurep"></span>
    	</button>
    	
    </div>
</div>