<?php
//aqui se modifica los datos de  una zona
    $d = templateDriver::getData('id');
	if(!$d){
		
	}else{
		$zone=Restriction::find_by_id($d['id']);
	}

?>
<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Restricción</h4>
			</div>
	<div class="modal-body" style="padding: 10px 15px;">
	
		
  	<table class="tablamarar"> 
  		<tbody>
	  		<tr>
	  			<td class="tdstatico">
					Horario Corto:
	  			</td>
	  			<td>
	  			 <div class="form-inline">
		  			<input name="id" type="hidden" placeholder="id" value="<?php echo isset($zone->id) ? $zone->id : '-1'; ?>" class="form-control system_data confnombre">
		  		    <input name="name" type="text" placeholder="Nombre Horario" value="<?php echo isset($zone->name) ? $zone->name : ''; ?>" class="form-control system_data">
		         </div>              
	  		    </td>
	  		</tr>
	  		<tr>
	  			<td class="tdstatico">
	  				Horario: 
	  			</td>
	  			<td>
	  			   <div class="form-inline">
		  		    <input name="horary1" type="time" placeholder="desde" value="<?php echo isset($zone->horary1) ? $zone->horary1 : ''; ?>" class="form-control system_data confnombre">
		            <input name="horary2" type="time" placeholder="hasta" value="<?php echo isset($zone->horary2) ? $zone->horary2 : ''; ?>" class="form-control system_data confnombre">
		           </div>              
	  			</td>
	  		</tr>
	  		<tr>
	  			<td class="tdstatico">
				Lunes
	  			</td>
	  			<td>
	  			   <div class="form-inline">
		  		    <input name="ln" type="text" placeholder="Normal" value="<?php echo isset($zone->ln) ? $zone->ln : ''; ?>" class="form-control system_data confnombre">
		            <input name="ll" type="text" placeholder="Lluvia" value="<?php echo isset($zone->ll) ? $zone->ll : ''; ?>" class="form-control system_data confnombre">
		           </div>              
	  			</td>
	  		</tr>
	  		<tr>
	  			<td class="tdstatico">
				Martes-Viernes
	  			</td>
	  			<td>
	  			   <div class="form-inline">
		  		    <input name="vn" type="text" placeholder="Normal" value="<?php echo isset($zone->vn) ? $zone->vn : ''; ?>" class="form-control system_data confnombre">
		            <input name="vl" type="text" placeholder="Lluvia" value="<?php echo isset($zone->vl) ? $zone->vl : ''; ?>" class="form-control system_data confnombre">
		           </div>              
	  			</td>
	  		</tr>
		</tbody>
	</table> 
	</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-danger close-user" data-dismiss="modal">Cancelar</button>
			<button type="button" class="btn-usersave btn btn-primary">Guardar cambios</button>
		</div>
		</div>
	</div>