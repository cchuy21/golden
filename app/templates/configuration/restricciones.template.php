<div class="panel-default systemuser-block">
	<div class="panel-body" style="background: white; padding: 0px; height: 500px; overflow: auto;padding-left: 10px;">
		<div class="col-md-12">
			<div class="form-inline">
				<button data-typ="restricciones" class="btn btn-success system-add" style="float: right;">Agregar</button>
				<table class="table table-bordered">
					<thead>
						<tr>
							<th rowspan="3" style="text-align: center;width: 250px;">
								HORARIO
							</th>
							<th colspan="4" style="text-align: center">
								RESERVACIONES
							</th>
							<th style="visibility: hidden"></th>
						</tr>
						<tr>
							<th colspan="2" style="text-align: center">
								LUNES
							</th>
							<th colspan="2" style="text-align: center">
								MARTES - VIERNES
							</th>
							<th style="visibility: hidden"></th>
						</tr>
						<tr>
							<th style="text-align: center">NORMAL</th>
							<th style="text-align: center">LLUVIA</th>
							<th style="text-align: center">NORMAL</th>
							<th style="text-align: center">LLUVIA</th>
							<th style="visibility: hidden"></th>
						</tr>
					</thead>
					<tbody>
						<?php
							$rest=Restriction::find("all");
							$c1=0;
							$c2=0;
							$c3=0;
							$c4=0;
							foreach ($rest as $r) {
								echo "<tr>";
								echo "<td>".$r->name." <span style='font-size:11px;'>(".date("G:i",strtotime($r->horary1))."-".date("G:i",strtotime($r->horary2)).")</span></td>";
								echo "<td>".$r->ln."</td>";
								echo "<td>".$r->ll."</td>";
								echo "<td>".$r->vn."</td>";
								echo "<td>".$r->vl."</td>";
								echo "<td><div class='sys' data-typ='restricciones' data-system='".$r->id."'><button type='button' data-aid='{$r->id}' class='system-delete btn exsm addr btn-default btns-golden btn-sm tool' data-toggle='tooltip' data-placement='top' title='Borrar'><span class='glyphicon glyphicon-remove-circle'></span></button><button type='button' data-aid='{$r->id}' class='system-edit btn exsm addr btn-default btns-golden btn-sm tool' data-toggle='tooltip' data-placement='top' title='Editar'><span class='glyphicon glyphicon-pencil'></span></button></div></td>";
								echo "</tr>";
								$c1=$c1+$r->ln;
								$c2=$c2+$r->ll;
								$c3=$c3+$r->vn;
								$c4=$c4+$r->vl;
							}
							
							
						?>
						<tr>
							<td style="visibility: hidden">
							</td>
							<td>
								<?php echo $c1;?> 
							</td>
							<td>
								<?php echo $c2;?> 
							</td>
							<td>
								<?php echo $c3;?> 
							</td>
							<td>
								<?php echo $c4;?> 
							</td>
						</tr>
					</tbody>
				</table>
				<?php
				$b=Benefit::find(2);
				if($b->benefit=="0"){
					echo '<button class="btn-lluv btn btn-primary">Normal</button>';
				}else{
					echo '<button class="btn-lluv btn btn-primary">Lluvia</button>';
				}
				?>
			</div>
		</div> 
 	</div>
	
</div>
