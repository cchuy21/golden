<!--obtiene los usuarios habilitados que tiene registrado el sistema-->
<div class="panel-default systemuser-block">
	<div class="panel-body panel-frm" style="background: white; padding: 0px; height: 500px; overflow: auto;padding-left: 10px;">
		<table class="table">
			<thead>
				<tr>
					<td colspan="6"> Cambios a Confirmar </td>
				</tr>
			</thead>
			<tr>
				<td> # </td>
				<td> Reservación </td>
				<td> Campo </td>
				<td> Valor </td>
				<td> Aceptar </td>
				<td> Recahzar </td>
			</tr>

			<?php
			$c=0;
			$cambios=Change::find("all");
			foreach($cambios as $ca) {
			$c++;
			echo "<tr>";
			echo "<td>".$c."</td>";
			echo "<td>".$ca->id_service."</td>";
			echo "<td>".$ca->campo."</td>";
			echo "<td>".$ca->valor."</td>";
			echo '<td><button type="button" data-aid="15" class="cambios-ok btn exsm addr btn-success btns-golden btn-sm tool" data-toggle="tooltip" data-placement="top" title="Aceptar"><span class="glyphicon glyphicon-ok"></span></button></td>';
			echo '<td><button type="button" data-aid="15" class="cambios-delete btn exsm addr btn-danger btns-golden btn-sm tool" data-toggle="tooltip" data-placement="top" title="Borrar"><span class="glyphicon glyphicon-remove-circle"></span></button></td>';
			echo "</tr>";
			}
			?>
		</table>
		<table class="table table-hover">
			<tbody>
				<tr>
					<td> reservación </td>
					<td> campo </td>
					<td> valor </td>
				</tr>
				<tr>
					<td>
					<input type="text" class="form-control" id="reservacion_cambio">
					</td>
					<td>
					<select class="form-control" id="campo_cambio">
						<option value="">Selecciona una opción</option>
					</select></td>
					<td class="vlc"></td>
				</tr>
				<tr>
					<td colspan="3"></td>
				</tr>
				<tr>
					<td colspan="3" class="cambut"></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>