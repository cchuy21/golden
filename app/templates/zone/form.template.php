<?php
//aqui se modifica los datos de  una zona
    $d = templateDriver::getData('id');
	if(!$d){
		
	}else{
		$zone=Zone::find_by_id($d['id']);
	}

?>
<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Zona</h4>
			</div>
	<div class="modal-body" style="padding: 10px 15px;">
	
		
  	<table class="tablamarar"> 
  		<tbody>
	  		<tr>
	  			<td class="tdstatico">
	  				<span class="tool glyphicon glyphicon-globe iconoesl" data-toggle="tooltip" data-placement="top" title="Suburb"></span>
	  			</td>
	  			<td>
	  			 <div class="form-inline">
		  			<input name="id" type="text" placeholder="id" value="<?php echo isset($zone->id) ? $zone->id : ''; ?>" class="form-control system_data confnombre">
		  		    <input name="name" type="text" placeholder="Name" value="<?php echo isset($zone->name) ? $zone->name : ''; ?>" class="form-control system_data confnombre">
		         </div>              
	  		    </td>
	  		</tr>
  		</tbody>
  	</table>
  	<table class="tablamarar"> 
  		<tbody>
	  		<tr>
	  		    <td colspan=2>
	  		        <h4>Sistema de Reservaciones</h4>
	  		    </td>
	  		</tr>
	  		<tr>
	  			<td>
	  				<span class="tool glyphicon glyphicon-usd iconoesl" data-toggle="tooltip" data-placement="top" title="E-mail"></span>
	  				<label>Costo CD-APTO Automóvil</label>
	  			</td>
	  			<td>
	  			   <div class="form-inline">
		  		    <input name="cdapto_auto" type="text" placeholder="cdapto_auto" value="<?php echo isset($zone->cdapto_auto) ? $zone->cdapto_auto : ''; ?>" class="form-control system_data confnombre">
		           </div>              
	  			</td>
	  		</tr>
	  		<tr>
	  			<td>
	  				<span class="tool glyphicon glyphicon-usd iconoesl" data-toggle="tooltip" data-placement="top" title="E-mail"></span>
	  				<label>Costo APTO-CD Automóvil</label>
	  			</td>
	  			<td>
	  			   <div class="form-inline">
		            <input name="aptocd_auto" type="text" placeholder="aptocd_auto" value="<?php echo isset($zone->aptocd_auto) ? $zone->aptocd_auto : ''; ?>" class="form-control system_data confnombre">
		           </div>              
	  			</td>
	  		</tr>
	  		<tr>
	  			<td>
	  				<span class="tool glyphicon glyphicon-usd iconoesl" data-toggle="tooltip" data-placement="top" title="E-mail"></span>
	  				<label>Promo CD-APTO Automóvil</label>
	  			</td>
	  			<td>
	  			   <div class="form-inline">
		  		    <input name="promo" type="text" placeholder="promo" value="<?php echo isset($zone->promo) ? $zone->promo : ''; ?>" class="form-control system_data confnombre">
		           </div>              
	  			</td>
	  		</tr>
	  		<tr>
	  			<td>
	  				<span class="tool glyphicon glyphicon-usd iconoesl" data-toggle="tooltip" data-placement="top" title="E-mail"></span>
	  				<label>Costo CD-APTO Camioneta</label>
	  			</td>
	  			<td>
	  			   <div class="form-inline">
		  		    <input name="cdapto_cam" type="text" placeholder="cdapto_cam" value="<?php echo isset($zone->cdapto_cam) ? $zone->cdapto_cam : ''; ?>" class="form-control system_data confnombre">
		           </div>              
	  			</td>
	  		</tr>
	  		<tr>
	  			<td>
	  				<span class="tool glyphicon glyphicon-usd iconoesl" data-toggle="tooltip" data-placement="top" title="E-mail"></span>
	  				<label>Costo APTO-CD Camioneta</label>
	  			</td>
	  			<td>
	  			   <div class="form-inline">
		            <input name="aptocd_cam" type="text" placeholder="aptocd_cam" value="<?php echo isset($zone->aptocd_cam) ? $zone->aptocd_cam : ''; ?>" class="form-control system_data confnombre">
		           </div>              
	  			</td>
	  		</tr>
	  	</tbody>
  	</table>
    <table class="tablamarar"> 
  		<tbody>
	  		<tr>
	  		    <td colspan=2>
	  		        <h4>Sistema de Empresas</h4>
	  		    </td>
	  		</tr>
	  		<tr>
	  			<td>
	  				<span class="tool glyphicon glyphicon-usd iconoesl" data-toggle="tooltip" data-placement="top" title="E-mail"></span>
	  				<label>Costo Automóvil</label>
	  			</td>
	  			<td>
	  			   <div class="form-inline">
		  		    <input name="empresa_auto" type="text" placeholder="Costo Auto" value="<?php echo isset($zone->empresa_auto) ? $zone->empresa_auto : ''; ?>" class="form-control system_data confnombre">
		           </div>              
	  			</td>
	  		</tr>
	  		<tr>
	  			<td>
	  				<span class="tool glyphicon glyphicon-usd iconoesl" data-toggle="tooltip" data-placement="top" title="E-mail"></span>
	  				<label>Costo Camioneta</label>
	  			</td>
	  			<td>
	  			   <div class="form-inline">
		            <input name="empresa_cam" type="text" placeholder="Costo Camioneta" value="<?php echo isset($zone->empresa_cam) ? $zone->empresa_cam : ''; ?>" class="form-control system_data confnombre">
		           </div>              
	  			</td>
	  		</tr>
	  	</tbody>
  	</table>
    <table class="tablamarar"> 
  	    <tbody>
	  		<tr>
	  		    <td colspan=2>
	  		        <h4>codigo zip y area</h4>
	  		    </td>
	  		</tr>
	  		<tr>
	  			<td class="tdstatico">
	  			 	<span class="tool glyphicon glyphicon-barcode iconoesl" data-toggle="tooltip" data-placement="top" title="Suburb"></span>
	  			</td>
	  			<td>
	  			 <div class="form-inline">
		            <input name="zipcode" type="text" placeholder="Zipcode" value="<?php echo isset($zone->zipcode) ? $zone->zipcode : ''; ?>" class="form-control system_data confnombre">
		       		
		       		<select name="area" style="width:49%;" class="form-control system_data confnombre">
		       			<?php
			  				$opciones=array(4,0,1,2,3,5,6);
							foreach($opciones as $opc){
								switch ($opc) {
									case 4:
										$tex="Selecciona un Área";
										break;
									case 0:
										$tex="Metropolitana";
										break;
									case 1:
										$tex="Periferica";
										break;
									case 2:
										$tex="Foranea";
										break;
									case 3:
										$tex="Intermedia";
										break;
									case 5:
										$tex="Aeropuerto";
										break;
									case 6:
										$tex="Restringida";
										break;
								}
				  					if($opc==$zone->area){
				  						echo "<option value='".$opc."' selected>".$tex."</option>";
				  					}else{
				  						echo "<option value='".$opc."'>".$tex."</option>";
				  					}
				  				
							}
			  				?>
		       		</select>
		         </div>              
	  		    </td>
	  		</tr>
		</tbody>
	</table> 
	</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-danger close-user" data-dismiss="modal">Cancelar</button>
			<button type="button" class="btn-usersave btn btn-primary">Guardar cambios</button>
		</div>
		</div>
	</div>