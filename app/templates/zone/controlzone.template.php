
<div class="panel-default systemuser-block">
	
	<!--muestra los costos de un servicio, dependiendo de la zona en la que se encuentra-->
	
    <div class="panel-body" style="background: white; padding: 0px; height: 500px; overflow: auto;padding-left: 10px;">
    	<button class="btn btn-success tool system-add" data-typ="zone" data-toggle='tooltip' data-placement='top' title='Agregar' style="float:right;">
 		   <span class="btn-address glyphicon glyphicon-plus" style="cursor: pointer;"><strong>Agregar</strong></span>
		</button>
    	<table class="table table-hover">
    		<tr>
    			<th>
    				#
    			</th>
    			<th>
    				Id
    			</th>
    			<th>
    				Nombre
    			</th>
    			<th>
    				Costo CD-APTO Automóvil
    			</th>
    			<th>
    				Costo APTO-CD Automóvil
    			</th>
    			<th>
    				Costo Promo Automóvil
    			</th>
    			<th>
    				Costo CD-APTO Camioneta
    			</th>
    			<th>
    				Costo APTO-CD Camioneta
    			</th>
    			<th>
    				Costo Empresa Automóvil
    			</th>
    			<th>
    				Costo Empresa Camioneta
    			</th>
    			<th>
    				
    			</th>
    			
    		</tr>
    	<?php
    	$systemuser=Zone::find('all',array("order"=>"name asc"));
    	$cont=1;
		$class="spn-dire sa";
		//muestra todas los datos de las zonas habilitadas
    	 foreach($systemuser as $sys){
    	 	
				 if($sys->enable!=0){
				 	echo "<tr>
    	 			<td>{$cont}</td>
    	 			<td>".$sys->id."</td>
    	 			<td>".$sys->name."</td>
    	 			<td>".$sys->cdapto_auto."</td>
					<td>".$sys->aptocd_auto."</td>
					<td>".$sys->promo."</td>
					<td>".$sys->cdapto_cam."</td>
					<td>".$sys->aptocd_cam."</td>
					<td>".$sys->empresa_auto."</td>
					<td>".$sys->empresa_cam."</td>
    	 			<td>
    	 			<div class='sys' data-typ='zone' data-system='".$sys->id."'><button type='button' data-aid='{$sys->id}' class='system-delete btn exsm addr btn-default btns-golden btn-sm tool' data-toggle='tooltip' data-placement='top' title='Borrar'><span class='glyphicon glyphicon-remove-circle'></span></button><button type='button' data-aid='{$sys->id}' class='system-edit btn exsm addr btn-default btns-golden btn-sm tool' data-toggle='tooltip' data-placement='top' title='Editar'><span class='glyphicon glyphicon-pencil'></span></button></div></td>";
					$cont++;
				
				}
			} 
		 
    	?>
    	</table>
    </div>
</div>
<div class="modal fade"id="systemuser-modal">
	
</div>
  