<?php
//opción de modificar los datos de las zonas que esta disponibles en el apartado de configuración->costos por zona
    $d = templateDriver::getData('id');
	if(!$d){
		
	}else{
		$suburb=Suburb::find_by_id($d['id']);
	}



?>
<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Colonia</h4>
			</div>
			
		<div class="modal-body" style="padding: 10px 15px;">
	
  	<table class="tablamarar"> 
  		
  		<tbody>
	  		<tr>
	  			<td class="tdstatico">
	  				<span class="tool glyphicon glyphicon-home iconoesl" data-toggle="tooltip" data-placement="top" title="Suburb"></span>
	  			</td>
	  			<td>
	  		    	<div class="form-inline">
		  				<input name="id" type="hidden" placeholder="Suburb" value="<?php echo isset($suburb->id) ? $suburb->id : -1; ?>" class="form-control system_data">
		  		   	 	<input name="suburb" type="text" placeholder="Suburb" value="<?php echo isset($suburb->suburb) ? $suburb->suburb : ''; ?>" class="form-control system_data" style="width: 98.6%;">
		         	</div>              
	  			</td>
	  		</tr>
	  		<tr>
	  			<td class="tdstatico">
	  				<span class="tool glyphicon glyphicon-globe iconoesl" data-toggle="tooltip" data-placement="top" title="E-mail"></span>
	  			</td>
	  			<td>
	  				<div class="form-inline">
		            	<select name="id_zone" class="form-control system_data">
			  				<?php
			  				$zo=Zone::find('all');
							foreach($zo as $z){
								if($z->enable)
								if($z){
				  					
				  					if($z->id==$suburb->id_zone){
				  						echo "<option value='".$z->id."' selected>".$z->id."</option>";
				  					}else{
				  						echo "<option value='".$z->id."'>".$z->id."</option>";
				  					}
				  				}else{
				  					echo "<option value='".$z->id."'>".$z->id."</option>";
				  				}
							}
			  				?>
			  				
			  			</select>
	            	</div>              
	  			</td>
	  		</tr>
		</tbody>
	</table> 
	</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-danger close-user" data-dismiss="modal">Cancelar</button>
			<button type="button" class="btn-usersave btn btn-primary">Guardar cambios</button>
		</div>
     </div>
</div>