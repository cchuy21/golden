<?php
    $d = templateDriver::getData('id');
	if(!$d){
		
	}else{
		$zone=User::find_by_id($d['id']);
	}

//muestra las datos de las empresas

?>
<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Empresa</h4>
			</div>
	<div class="modal-body" style="padding: 10px 15px;">
	
		
  	<table class="tablamarar"> 
  		<tbody>
	  		<tr>
	  			<td class="tdstatico">
	  				<span class="tool glyphicon glyphicon-globe iconoesl" data-toggle="tooltip" data-placement="top" title="Suburb"></span>
	  			</td>
	  			<td>
	  			 <div class="form-inline">
		  			<input name="id" type="hidden" placeholder="id" value="<?php echo isset($zone->id) ? $zone->id : ''; ?>" class="form-control system_data confnombre">
		  		    <input name="name" type="text" placeholder="Nombre" value="<?php echo isset($zone->name) ? $zone->name : ''; ?>" class="form-control system_data confnombre">
		  		    <input name="email" type="text" placeholder="Correo" value="<?php echo isset($zone->email) ? $zone->email : ''; ?>" class="form-control system_data confnombre">
		         </div>              
	  		    </td>
	  		</tr>
	  		<tr>
	  			<td class="tdstatico">
	  				<span class="tool glyphicon glyphicon-user iconoesl" data-toggle="tooltip" data-placement="top" title="Suburb"></span>
	  			</td>
	  			<td>
	  			 <div class="form-inline">
		  		    <input name="username" type="text" placeholder="Usuario" value="<?php echo isset($zone->username) ? $zone->username : ''; ?>" class="form-control system_data confnombre">
		  		    <input name="password" type="password" placeholder="Contraseña" class="form-control system_data confnombre">
		         </div>              
	  		    </td>
	  		</tr>
	  		<tr>
	  			<td>
	  				<span class="tool glyphicon glyphicon-globe iconoesl" data-toggle="tooltip" data-placement="top" title="Suburb"></span>
	  			</td>
	  			<td style="padding-left:8px; padding-right: 8px;">
		  		    <input name="telephones" type="text" placeholder="Telefono" value="<?php echo isset($zone->telephones) ? $zone->telephones : ''; ?>" class="form-control system_data">
	  			</td>
	  		</tr>
	  		<tr>
	  			<td>
	  				
	  			</td>
	  			<td style="padding-left:8px; padding-right: 8px;">
	  				<input type="hidden"id="busi-agre" class="system_data" name="agreements_bus" />
	  				
	  				<?php
	  				
	  				$agr=Agreement::find('all');
					echo '<div id="agr" >';
					$aer=explode(",", $zone->agreements_bus);
	  					foreach ($agr as $ag) {
	  						$c=0;
	  						if(count($aer)>0)
	  							foreach($aer as $ae){
	  								
	  								if($ae==$ag->id){
	  									$c++;
	  								}
	  							}
								if($c){
									
									echo '<label class="checkbox-inline">
								  <input type="checkbox" checked value="'.$ag->id.'"> '.$ag->agreement.'
								</label>
								';
								}else{
								  echo '<label class="checkbox-inline">
								  <input type="checkbox" value="'.$ag->id.'"> '.$ag->agreement.'
								</label>
								';
							}
						  }
	  				?>
	  				</div>
	  			</td>
	  		</tr>
	  		<tr>
	  			<td>
	  				
	  			</td>
	  			<td style="padding-left:8px; padding-right: 8px;">
	  				<textarea name="annotations" class="form-control system_data"><?php echo isset($zone->annotations) ? $zone->annotations : ''; ?></textarea>
	  			</td>
	  		</tr>
		</tbody>
	</table> 
	</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-danger close-user" data-dismiss="modal">Cancelar</button>
			<button type="button" class="btn-usersave btn btn-primary">Guardar cambios</button>
		</div>
		</div>
	</div>