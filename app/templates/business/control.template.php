<!--muestra los datos básicos de las empresas que actualmente tienen convenio-->
<div class="panel-default systemuser-block">
    <div class="panel-body panel-frm" style="background: white; padding: 0px; height: 500px; overflow-y: auto">
    	<button class="btn btn-success tool system-add" data-typ="business" data-toggle='tooltip' data-placement='top' title='Agregar' style="float:right;">
 		   <span class="btn-address glyphicon glyphicon-plus" style="cursor: pointer;"><strong>Agregar</strong></span>
		</button>
	    	<table class="table table-hover">
	    		<thead>
		    		<tr>
		    			<th>
		    				#
		    			</th>
		    			<th>
		    				nombre
		    			</th>
		    			<th>
		    				teléfono
		    			</th>
		    			<th>
		    				correo
		    			</th>
		    			<th>
		    				
		    			</th>
		    		</tr>
		    	</thead>
		    	<tbody>
		    		
	    	<?php
	    	$hoy=date("Y-m-d");	
										
			$tic=User::find("all",array("conditions"=>"isBusiness=1","order"=>"name asc"));
	    	$cont=1;
			$class="spn-dire sa";
			//mostrar los datos básicos de un usuario
	    	 foreach($tic as $sys){
					 	echo "<tr>
	    	 			<td>{$cont}</td>
	    	 			<td>".$sys->name."</td>
	    	 			<td>".$sys->telephones."</td>
	    	 			<td>".explode(";",$sys->email)[0]."</td>
	    	 			<td>
	    	 			<div class='sys' data-typ='business' data-system='".$sys->id."'><a target='_blank' href='/business/unir/".$sys->id."'><button type='button' data-aid='{$sys->id}' class='bus-hab btn exsm addr btn-default btns-golden btn-sm tool' data-toggle='tooltip' data-placement='top'><span class='glyphicon glyphicon-plus'></span></button></a><button type='button' data-aid='{$sys->id}' class='system-delete btn exsm addr btn-default btns-golden btn-sm tool' data-toggle='tooltip' data-placement='top' title='Borrar'><span class='glyphicon glyphicon-remove-circle'></span></button><button type='button' data-aid='{$sys->id}' class='system-edit btn exsm addr btn-default btns-golden btn-sm tool' data-toggle='tooltip' data-placement='top' title='Editar'><span class='glyphicon glyphicon-pencil'></span></button></div></td>";
					$cont++;
				} 
	    	?>
	    	
	    	</tbody>
	    	</table>
    	</div>
    </div>

  