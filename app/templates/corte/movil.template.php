<div class="panel-default systemuser-block">
	<div class="panel-body" style="background: white; padding: 0px; height: 500px; overflow: auto;padding-left: 10px;">
		<div class="col-md-12">
			<div class="form-inline">
				<?php
				$z=Zone::find_by_id("app");
				
				?>
				<strong>Costo en la Aplicación</strong>
				<br />
			  <div class="form-group">
			    <label for="appauto">Automóvil:</label>
			    <input type="text" class="form-control" value="<?php echo $z->cost_car ?>" id="appauto" placeholder="">
			  </div>
			  <div class="form-group">
			    <label for="appcam">Camioneta:</label>
			    <input type="text" class="form-control" value="<?php echo $z->cost_van ?>" id="appcam" placeholder="">
			  </div>
			  <button type="submit" class="btn btn-default btn-costoapp" id="btcosto">Guardar</button>
			</div>
		</div> 
 	</div>
</div>
