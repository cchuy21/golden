<?php
//MODIFICAR LOS DATOS DEL USUAIRO DEL SISTEMA
    $d = templateDriver::getData('id');
	if(!$d){
		
	}else{
		$system=Systemuser::find_by_id($d['id']);
	}

?>
<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Usuario del Sistema</h4>
			</div>
		<div class="modal-body" style="padding: 10px 15px;">
	
		
  	<table class="tablamarar"> 
  		
  		<tbody>
	  		<tr>
	  			<td class="tdstatico">
	  			<span class="tool glyphicon glyphicon-user iconoesl" data-toggle="tooltip" data-placement="top" title="Nombre"></span>
	  		</td>
	  		<td>
	  			 <div class="form-inline">
		  			<input name="id" type="hidden" placeholder="Nombre" value="<?php echo isset($system->id) ? $system->id : -1; ?>" class="form-control system_data confnombre">
		  		    <input name="name" type="text" placeholder="Nombre" value="<?php echo isset($system->name) ? $system->name : ''; ?>" class="form-control system_data confnombre">
		            <input name="lastname" type="text" placeholder="Apellido" value="<?php echo isset($system->lastname) ? $system->lastname : ''; ?>" class="form-control system_data confnombre">   
	            </div>              
	  		</td>
	  		</tr>
	  		<tr>
	  			<td class="tdstatico">
	  				<span class="tool glyphicon glyphicon-envelope iconoesl" data-toggle="tooltip" data-placement="top" title="E-mail"></span>
	  			</td>
	  			<td>
	  			 <div class="form-inline">
		  		    <input name="username" value="<?php echo isset($system->username) ? $system->username : ''; ?>" type="text" class="form-control system_data confuser" placeholder="User">
		            <input name="email" type="text" value="<?php echo isset($system->email) ? $system->email : 'sin correo'; ?>" class="form-control system_data confemail" placeholder="Email">   
	            </div>              
	  			</td>
	  		</tr>
	  		<tr>
	  			<td class="tdstatico">
	  				<span class="tool glyphicon glyphicon-lock iconoesl" data-toggle="tooltip" data-placement="top" title="Contraseña"></span>
	  			</td>
	  			<td>
		  			 <div class="form-inline">
			  		    <input name="password" type="password" class="form-control system_data confnombre" placeholder="Contraseña">
			            <input name="password2" type="password" class="form-control system_data confnombre" placeholder="Repetir Contraseña">   
		            </div>              
	  			</td>
	  		</tr>
	  		<tr>
	  			<td class="tdstatico">
	  				<span class="tool glyphicon glyphicon-briefcase iconoesl" data-toggle="tooltip" data-placement="top" title="Datos laborales"></span>
	  			</td>
	  			<td>
	  				<div class="form-inline">
			  			<select name="group" class="form-control system_data confnombre" style="width: 49% !important;">
			  				<?php
			  				$opciones=array('ADMINISTRADOR','SUPER RESERVACIONES','RESERVACIONES','ASIGNACIONES','OBSERVADOR');
							foreach($opciones as $opc){
								if($system){
				  				
				  					if($opc==$system->group){
				  						echo "<option value='".$opc."' selected>".$opc."</option>";
				  					}else{
				  						echo "<option value='".$opc."'>".$opc."</option>";
				  					}
				  				}else{
				  					echo "<option value='".$opc."'>".$opc."</option>";
				  				}
							}
			  				?>
			  				
			  			</select>
			  			<select name="turn" class="form-control system_data confnombre" style="width: 49% !important;">
			  				
			  				<?php
			  				$opci=array('MIXTO','DOBLANDO','NOCHE');
							foreach($opci as $opc){
				  				if($system){
					  				echo $system->turn;
				  					if($opc==$system->turn){
				  						echo "<option value='".$opc."' selected>".$opc."</option>";
				  					}else{
				  						echo "<option value='".$opc."'>".$opc."</option>";
				  					}
				  				
								}else{
									echo "<option value='".$opc."'>".$opc."</option>";
								}
								
							}
			  				?>
			  				
			  			</select>
		  		    <!--<input name="group" type="text" value="<?php echo isset($system->group) ? $system->group : ' '; ?>" class="form-control system_data confnombre" placeholder="Group">
		            <input name="turno" type="text" value="<?php echo isset($system->turno) ? $system->turno : ' '; ?>" class="form-control system_data confnombre" placeholder="Turno">-->  
		            </div>              
	  			</td>
	  		</tr>
	  		<tr>
	  			<td class="tdstatico">
	  			<span class="tool glyphicon glyphicon-home iconoesl" data-toggle="tooltip" data-placement="top" title="Direccion"></span>
	  		</td>
	  		<td>
	  			 <div class="form-inline">
	  		    <input name="address" type="text" value="<?php echo isset($system->address) ? $system->address : ''; ?>" class="form-control system_data confnombre" placeholder="Direccion">
	            <input name="suburb" type="text" value="<?php echo isset($system->suburb) ? $system->suburb : ''; ?>" class="form-control sub system_data confnombre" placeholder="Colonia">   
	            </div>              
	  		</td>
	  		</tr>
	  		<tr>
	
				<td class="tdstatico">
					<span class="tool glyphicon glyphicon-earphone iconoesl" data-toggle="tooltip" data-placement="top" title="Telephone"></span>
				</td>
				<td>
					<div class="form-inline" style="padding-left: 7px; padding-right: 7px;">
					  <input name="telephones" type="text" value="<?php echo isset($system->telephones) ? $system->telephones : ''; ?>" class="system_data form-control" placeholder="Telefono">
					  </div>              
				</td>
			</tr>
		</tbody>
	</table> 
	</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-danger close-user" data-dismiss="modal">Cancelar</button>
			<button type="button" class="btn-usersave btn btn-primary">Guardar cambios</button>
		</div>
		</div>
	</div>