<!--obtiene los usuarios habilitados que tiene registrado el sistema-->
<div class="panel-default systemuser-block">
	<div class="panel-body panel-frm" style="background: white; padding: 0px; height: 500px; overflow: auto;padding-left: 10px;">
    	<button class="btn btn-success tool system-add" data-typ='systemuser' data-toggle='tooltip' data-placement='top' title='Agregar' style="float:right;">
 		   <span class="btn-address glyphicon glyphicon-plus" style="cursor: pointer;"><strong>Agregar</strong></span>
		</button>
    	<table class="table table-hover">
    		<thead>
	    		<tr>
	    			<th>
	    				#
	    			</th>
	    			<th>
	    				Nombre
	    			</th>
	    			<th>
	    				Grupo
	    			</th>
	    			<th>
	    				
	    			</th>
	    		</tr>
	    	</thead>
	    	<tbody>
    	<?php
    	$systemuser=Systemuser::all();
    	$cont=1;
		$class="spn-dire sa";
    	 foreach($systemuser as $sys){
    	 	//si el usuario esta habilitado y no es el usuario de la sesión actual
				 if($sys->enable!=0&& $sys->id!=authDriver::getSUser()->id){
				 	echo "<tr>
    	 			<td>{$cont}</td>
    	 			<td>".$sys->name." ".$sys->lastname."</td>
    	 			<td>".$sys->group."</td>
    	 			<td>
    	 			<div class='sys' data-typ='systemuser' data-system='".$sys->id."'><button type='button' data-aid='{$sys->id}' class='system-delete btn exsm addr btn-default btns-golden btn-sm tool' data-toggle='tooltip' data-placement='top' title='Borrar'><span class='glyphicon glyphicon-remove-circle'></span></button><button type='button' data-aid='{$sys->id}' class='system-edit btn exsm addr btn-default btns-golden btn-sm tool' data-toggle='tooltip' data-placement='top' title='Editar'><span class='glyphicon glyphicon-pencil'></span></button></div></td></tr>";
					$cont++;
				}
			
			} 
		 
    	?>
    	
    	</tbody>
    	</table>
    </div>
</div>

  