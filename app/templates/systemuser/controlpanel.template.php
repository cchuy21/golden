<!-- Nav tabs -->
<!--muestra los opciones para el menu de asignaciones dependiendo del usuario que inicio sesión-->
<!-- Tab panes -->
<?php
$tip= authDriver::getSUser()->group;
echo '<div class="tab-content tab-pages" style="background-color: transparent; padding-top: 12px">';
$e="active";

?>
    <div class="tab-pane <?php echo $e; ?>" id="reservations" >
		<div class="container" id="content">
	        <div id="selectionblock">
	            <div class="panel panel-default menu-options">
	                <div class="panel-heading"><strong class="text-serv">Elija una opción:</strong></div>
	                <div class="panel-body">
	                	<?php 
	                		if($tip!="OBSERVADOR"){
						?>
	                    <a data-info="1" data-st="1" searchbox="true" class="btn-menu" href="#"><img src="/static/images/iconos/pasajero.jpg" /><!--<span>Pasajero<br /> Sin Cuenta</span>--></a>
	                    <a data-info="0" data-st="0" searchbox="false" class="btn-menu" href="#"><img src="/static/images/iconos/user.png" /><!--<span>Nuevo Usuario<br />&nbsp;</span>--></a>
	                    <a data-info="2" data-st="3" searchbox="true" class="btn-menu" href="#"><img src="/static/images/iconos/editar.jpg" /><!--<span>Nuevo Usuario<br />&nbsp;</span>--></a>
	                   <?php
							}
						?>
	                    <a data-info="1" data-st="6" searchbox="true" class="btn-menu consult" href="#"><img src="/static/images/iconos/consultarUsuario.png" /><!--<span>Nuevo Usuario<br />&nbsp;</span>--></a>
						<a data-info="5" data-st="8" searchbox="true" class="btn-menu" href="#"><img src="/static/images/iconos/consultarTarifas.png" /><!--<span>Nuevo Usuario<br />&nbsp;</span>--></a>
	                    <a data-info="3" data-st="5" searchbox="true" class="btn-menu consult" href="#"><img src="/static/images/iconos/consultar.png" /><!--<span>Nuevo Usuario<br />&nbsp;</span>--></a>
						<a data-info="4" data-st="7" searchbox="true" class="btn-menu" href="#"><img src="/static/images/iconos/consultarc.png" /><!--<span>Nuevo Usuario<br />&nbsp;</span>--></a>
        	           <?php 
        	           		if($tip!="OBSERVADOR"){
						?>											
        	            <a data-info="2" data-st="4" searchbox="true" class="btn-menu" href="#"><img src="/static/images/iconos/cancelar.png" /><!--<span>Nuevo Usuario<br />&nbsp;</span>--></a>
	                   
	                    <?php
	                 	 	}
	                 	 	if(authDriver::getSUser()->username=="SLADER2110"){
	                 	 	    ?>
	                 	 	                            <br />
	                 	 	                            <br />

        	            <a data-reser="482920" class="neditar" href="#"><img src="/static/images/iconos/editar.jpg" /></a>
	                 	 	    <?php
	                 	 	}
	                 	 ?>
	                    <input type="hidden" id="tipo" name="type" />
	                    <div class="panel panel-default search-box"  style="display: none" >
			                <div class="panel-heading">Busqueda de cliente:</div>
			                <div class="panel-body chn">
			                    <input type="text" name="user[busqueda]" placeholder="Nombre Completo, Telefono o Correo" class="form-control box-input" />
			                    <input type="hidden" name="idOp" value="" />              	
			                </div>
			            </div>
			            <div id="log" style="display: none"><img src="/static/images/logo.jpg" width="100%" height="100%"></div>
			            <div class="con_div" style="display: none; width: 1000px; height: 336px; overflow: auto; background: white;">
			                		
	                	</div>
	                	<?php
	                		templateDriver::renderSection("systemuser.movil");
	                	?>
						<!--template movil-->
	                </div>
	            </div>
	        </div>
        </div>
    </div>
    
<div class="modal fade" id="modalmovil" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Reservación</h4>
      </div>
      <div class="modal-body" id="bdy">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
    <?php
	$e="";

if($tip!="OBSERVADOR" && $tip!="ADMINISTRADOR"){
	
   ?>
    <div class="tab-pane row" id="reports">
        <div class="container contnew" id="content">
	    	<?php
	    	templateDriver::renderSection("reports.reports");
	    	?>
	    </div>
	</div>
	<?php
	echo '<div class="tab-pane row" id="ocupacion">
        <div class="container contnew" id="content" style="width:100%">';
		
	    	//templateDriver::renderSection("reports.ocupacion");
	    	echo "</div>
	</div>";
	 echo '<div class="tab-pane row" id="monitoreo">
        <div class="container contnew" id="content" style="width:100%">';
		
	    	//templateDriver::renderSection("reports.monitoreo");
	    	echo "</div>
	</div>";
	}else{
		echo '<div class="tab-pane row '.$e.'" id="assignments">';
		echo '<div class="container" id="content" style="width: 100% !important;">';
		if(authDriver::getSUser()->username!="SLADER2110")
    	templateDriver::renderSection("assignment.asignacion");
    	
   echo '</div></div>';
   }
	if($tip=="ADMINISTRADOR" || $tip=="OBSERVADOR"){
    ?>
    <div class="tab-pane row" id="reports">
        <div class="container contnew" id="content">
	    	<?php
	    	templateDriver::renderSection("reports.reports");
	    	?>
	    </div>
	</div>
    <div class="tab-pane row" id="configuration">
        <div class="container contnew" id="content">
			<?php
	    	templateDriver::renderSection("configuration.configuration");
	    	?>
	    </div>
	</div>
	<?php
	
		   echo '<div class="tab-pane row" id="ocupacion">
        <div class="container contnew" id="content" style="width:100%">';
		
	    //	templateDriver::renderSection("reports.ocupacion");
	    	echo "</div>
	</div>";
	 echo '<div class="tab-pane row" id="monitoreo">
        <div class="container contnew" id="content" style="width:100%">';
		
	    //	templateDriver::renderSection("reports.monitoreo");
	    	echo "</div>
	</div>";
	echo '<div class="tab-pane row" id="especial">
        <div class="container contnew" id="content" style="width:100%">';
		
	    	templateDriver::renderSection("configuration.especial");
	    	echo "</div>
	</div>";
	
	}
	?>
</div>
