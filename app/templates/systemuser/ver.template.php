<?php
$d = templateDriver::getData('id');
$s = Service::find($d);
$r = Reservation::find($s -> reservation1);
$tipo = "Sencilla";
if ($s -> reservation2)
	$tipo = "Redondo";
?>
<table class="table">
	<tr>
		<td colspan="6">Reservaci&oacute;n #<?php echo $d; ?></td>
	</tr>
	<tr style='font-weight:bold'>
		<td> Cliente </td>
			<td> # Reservacion </td>
			<td> Tipo </td>
			<td> Vehiculo </td>
			<td> # Pasajeros </td>
			<td> Fecha Reservación </td>
	</tr>
	<?php
	if ($s) {
		$u = User::find($s -> user_id);
		if ($r -> type == "MTY-APTO")
			$tipo = "Salida " . $tipo;
		else
			$tipo = "Llegada " . $tipo;
		if ($s -> reservation2) {
			$red = "SI";
		}

		$value = str_replace("a", "", $r -> addresses);
		$dir = Address::find_by_id($value);
		$sub = Suburb::find_by_id($dir -> suburb_id);

		if ($s -> pagado)
			$pg = "SI";
		else
			$pg = "NO";

		if ($r -> vehicle_id == 1 || $r -> vehicle_id == "1") {
			$ve = "Automóvil";
		} else {
			$ve = "Camioneta";
		}
		echo "<tr>";
		echo "<td>" . $u -> name . "</td>";
		echo "<td>" . $s -> id . "</td>";
		echo "<td>" . $tipo . "</td>";
		echo "<td>" . $ve . "</td>";
		echo "<td>" . $r -> passengers . "</td>";
		echo "<td>" . templateDriver::timelo($r -> reservation_date, 1) . "</td>";

		echo "</tr>";
		echo "<tr style='font-weight:bold'>
					<td> Hora Reservación </td>
					<td> Tipo </td>
					<td> Destino </td>
					<td> Costo </td>
					<td> Pagado </td>
					<td> Hora de Vuelo </td>
										</tr><tr>";
		echo "<td>" . $r -> reservation_time . "</td>";
		echo "<td>" . $tipo . "</td>";
		echo "<td>" . $sub -> suburb . "</td>";
		echo "<td>" . $s -> cost . "</td>";
		echo "<td>" . $pg . "</td>";
		echo "<td>" . $r -> flight_hour . "</td></tr>";

		echo "<tr style='font-weight:bold'>
					<td> Terminal </td>
					<td> Tipo de vuelo </td>
					<td cols-span='4'> Notas </td>
					
										</tr><tr>";
		echo "<td>";
		if ($r -> terminal == "NULL") {
			echo "";
		} else {
			echo $r -> terminal;
		} echo "</td>";
		echo "<td>" . $r -> flight_type . "</td>";
		echo "<td cols-span='4'>" . $r -> annotations . "</td></tr>";
		$c++;
	}
	?>	
</table>