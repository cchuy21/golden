<div class="col-md-12" id="cont_app" style="padding:0px!important;background-color: white;width: 1000px;height: 400px;overflow: auto;margin-top: 50px;">

	<div style="width: 100%;height: 24px;background-color: #AAB8E2;">
		<?php
		if (authDriver::getSUser() -> group == "ADMINISTRADOR") {
			echo "<label class='' title='aceptados' style='    float: right;
    background-color: rgba(57, 126, 123, 1);
    width: 25px;
    height: 100%;
    color: white;'>" . count(Service::find("all", array("conditions" => "status=1"))) . "</label>";
			echo "<label class='' title='cancelados' style='    float: right;
    background-color: rgba(180, 3, 3, 1);
    width: 25px;
    height: 100%;
    color: white;'>" . count(Service::find("all", array("conditions" => "status=0 and payment='movil'"))) . "</label>";
		}
	?>
	<strong>Reservaciones App móvil Pendientes de Aprobación</strong>	</div>
	<table class="table table-hover">
		<tr>
			<td> # </td>
			<td> Cliente </td>
			<td> # Reservacion </td>
			<td> Tipo </td>
			<td> Vehiculo </td>
			<td> # Pasajeros </td>
			<td> Fecha Reservación </td>
			<td> Hora Reservación </td>
			<td> Redonda </td>
			<td> Destino </td>
			<td> Costo </td>
			<td> Pagado </td>
			<td> Aceptar </td>
			<td> Borrar </td>
			<td>
			Ver
			</td>
		</tr>
		<?php
		$reser = Reservation::find("all", array("conditions" => "status=-1"));
		$c = 1;

		foreach ($reser as $r) {
				$red = "NO";
				$s = Service::find_by_reservation1($r -> id);
				//if($s -> pagado){
					if ($s) {
						$u = User::find($s -> user_id);
						if ($r -> type == "MTY-APTO")
							$tipo = "Salida";
						else
							$tipo = "Llegada";
						if ($s -> reservation2) {
							$red = "SI";
						}
	
						$value = str_replace("a", "", $r -> addresses);
						$dir = Address::find_by_id($value);
						$sub = Suburb::find_by_id($dir -> suburb_id);
	
						if ($s -> pagado)
							$pg = "SI";
						else{
							$pg = "NO";
							$r->status=0;
							$r->save();
							continue;
						}
						
						if ($r -> vehicle_id == 1 || $r -> vehicle_id == "1") {
							$ve = "Automóvil";
						} else {
							$ve = "Camioneta";
						}
						echo "<tr>";
						echo "<td>" . $c . "</td>";
						echo "<td>" . $u -> name . "</td>";
						echo "<td>" . $s -> id . "</td>";
						echo "<td>" . $tipo . "</td>";
						echo "<td>" . $ve . "</td>";
						echo "<td>" . $r -> passengers . "</td>";
						echo "<td>" . templateDriver::timelo($r -> reservation_date, 1) . "</td>";
						echo "<td>" . $r -> reservation_time . "</td>";
						echo "<td>" . $red . "</td>";
						echo "<td>" . $sub -> suburb . "</td>";
						echo "<td>" . $s -> cost . "</td>";
						echo "<td>" . $pg . "</td>";
						echo '<td><button type="button" data-appid="' . $s -> id . '" class="rapp-hab btn exsm btn-success btns-golden btn-sm tool" data-toggle="tooltip" data-placement="top" title="Habilitar"><span class="glyphicon glyphicon-ok"></span></button></td>';
						echo '<td><button type="button" data-appid="' . $s -> id . '" class="rapp-del btn exsm btn-danger btns-golden btn-sm tool" data-toggle="tooltip" data-placement="top" title="Borrar"><span class="glyphicon glyphicon-remove-circle"></span></button></td>';
						echo '<td><button type="button" data-appid="'.$s->id.'" class="rapp-ver btn exsm btn-primary btns-golden btn-sm tool" data-toggle="tooltip" data-placement="top" title="Ver"><span class="glyphicon glyphicon-eye-open"></span></button></td>';
	
						echo "</tr>";
						$c++;
					}
				//}
			}
		
		?>
	</table>
</div>