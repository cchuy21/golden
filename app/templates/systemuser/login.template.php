<!--MENU DE INICIO DE SESIÓN PARA LOS USUARIOS-->
<div class="container">

    <form class="form-signin" role="form" action="/systemuser/login" method="post" style="display: none;">
        <h2 class="form-signin-heading">Iniciar sesión</h2>
        
        <img class="logo" src="https://www.goldenmty.com/images/logogold2.png" width="100%">
        <input class="form-control" placeholder="Nombre de Usuario" required="" autofocus="" type="text" name="user[username]">
        <input class="form-control" placeholder="Contraseña" required="" type="password" name="user[password]">
        <div style="display: none;" class="alert alert-danger">
            
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Iniciar</button>
        <a>Olvidé mi contraseña</a>
    </form>

</div> <!-- /container -->