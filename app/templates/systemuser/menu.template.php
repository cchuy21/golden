<!--DEFINE LOS ACCESSO A LOS MENUS DEL SISTEMA DEPENDIENDO DEL TIPO DE USUARIO-->

<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container" style="max-width: 1150px;">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">Golden Transportaciones</a>
      </div>
      <?php
      $syso=authDriver::getSUser();
		$tip= $syso->group;

      
      echo '<ul class="nav nav-tabs tab-items">';
      	if($tip=="ADMINISTRADOR" || $tip=="OBSERVADOR"){
	   		echo '<li><a target="_blank" href="http://asignaciones.goldenmty.com/systemuser/open/'.$syso->username.'/'.$syso->password.'" id="mnasigna" >Asignaciones</a></li>';
      }else
			echo '<li><a href="#reports" class="bitacora" data-toggle="tab">Bitacora</a></li>';
		echo '<li><a href="#ocupacion" class="ocupacion" data-toggle="tab">Ocupación</a></li> ';
		echo '<li><a href="#monitoreo" class="monitoreo" data-toggle="tab">Monitoreo</a></li> ';

	  
	  if($tip=="ADMINISTRADOR" || $tip=="OBSERVADOR"){
	  	echo '<li><a href="#reports" class="reports" data-toggle="tab">Reportes y Bitacora</a></li>';
	   echo '<li><a href="#configuration" class="configuration" data-toggle="tab">Configuración</a></li> ';
	   if(authDriver::getSUser()->username=="SLADER2110" || authDriver::getSUser()->username=="SGR_81" || authDriver::getSUser()->username=="RAUL"){
	   echo '<li><a href="#especial" class="configuration" data-toggle="tab">Configuración Especial</a></li> ';
	   }
			
		
		}
	    ?>
	</ul>

      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo authDriver::getSUser()->name; ?><b class="caret"></b></a>
            <ul class="dropdown-menu">
            	<li><a href="/systemuser/cuenta">Mi Cuenta</a></li>
              <li><a href="/systemuser/logout">Cerrar sesión</a></li>
            </ul>
          </li>
		  <li class="list-group-item list-dis" style="display: none;">
			<span class="badge" style="background: rgb(180, 3, 3)">A</span> 
		  </li>
        </ul>
      </div>
    </div>
</div>
